import sbt.Resolver

ThisBuild / scalaVersion := "2.11.7"
ThisBuild / organization := "com.uxforms"
ThisBuild / organizationName := "UX Forms"

val jarFilter: (Seq[String], File) => Boolean = (s, f) => s.exists(jarName => f.getName.startsWith(jarName))

val embeddedJarNames = Seq(
  "compiler-0.9.6",
  "caffeine",
  "scaffeine",
  "error_prone_annotations",
  "checker-qual"
)

val explodedJarNames = Seq.empty

val basePackage = "com.uxforms.govukdesignsystem.dsl"

val themeVersion = settingKey[String]("Version of govuk-design-system-theme to test against")
val themeTargetDirLocation = settingKey[Option[String]]("When working on themes locally, read templates from this location on your local computer instead of downloading a released version")
val testAgainstLocalTheme = settingKey[Boolean]("When true will attempt to locate the checked out theme from your ~/.uxforms/sbt-uxforms.conf file, otherwise from the setting 'themeTargetDirLocation'")

lazy val root = (project in file("."))
  .enablePlugins(SbtOsgi, BuildInfoPlugin)
  .settings(
    osgiSettings,
    name := "govuk-design-system-dsl",
    resolvers += "Big Bee Consultants" at "https://dl.bintray.com/rick-beton/maven",
    resolvers += "uxforms-public" at "https://artifacts-public.uxforms.net",
    resolvers += Resolver.bintrayRepo("hmrc", "releases"),
    libraryDependencies += "com.github.spullara.mustache.java" % "compiler" % "0.9.10",
    libraryDependencies += "com.uxforms" %% "uxforms-dsl" % "15.27.0" % "provided",
    libraryDependencies += "uk.co.bigbeeconsultants" %% "bee-client" % "0.29.1",
    libraryDependencies += "com.github.blemale" %% "scaffeine" % "4.1.0",

    themeVersion := "0.49.0",
    themeTargetDirLocation := None,
    testAgainstLocalTheme := false,
    libraryDependencies += "com.lihaoyi" %% "utest" % "0.6.7" % "test",
    libraryDependencies += "com.github.pathikrit" %% "better-files" % "3.9.1" % "test",
    libraryDependencies += "org.jsoup" % "jsoup" % "1.15.3" % "test",

    buildInfoPackage := "com.uxforms.govukdesignsystem.dsl.build",
    buildInfoKeys ++= Seq[BuildInfoKey](themeVersion, themeTargetDirLocation, testAgainstLocalTheme),

    mimaPreviousArtifacts := Set(organization.value %% name.value % "1.0.0"),

    OsgiKeys.bundleSymbolicName := basePackage,
    OsgiKeys.embeddedJars := (Compile / Keys.externalDependencyClasspath).value map (_.data) filter (f => jarFilter(embeddedJarNames, f)),
    OsgiKeys.explodedJars := (Compile / Keys.externalDependencyClasspath).value map (_.data) filter (f => jarFilter(explodedJarNames, f)),
    OsgiKeys.exportPackage := Seq(
      s"$basePackage.*"
    ),

    testFrameworks += new TestFramework("utest.runner.Framework"),

    publishTo := Some("publish-uxforms-public" at "s3://artifacts-public.uxforms.net")
  )