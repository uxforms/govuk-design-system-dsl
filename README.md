# govuk-design-system-dsl

An implementation of GOV.UK's [design system](https://design-system.service.gov.uk/) in UX Forms.

All of the widgets provided by this library have been written to support UX Forms' 
[govuk-design-system-theme](https://bitbucket.org/uxforms/govuk-design-system-theme/).

You can see this dsl in action in our [GOV.UK Design System Demo form](https://forms.uxforms.com/govuk-design-system-form).

## Usage instructions

### Getting started

In your form's `build.sbt` add a dependency on this library:

```"com.uxforms" %% "govuk-design-system-dsl" % "x.y.z"```

Then make sure your form is using a theme that is compatible. E.g. 

```themeName := "govuk-design-system-theme"```

### Building forms

#### FormDefinition

There is a `FormDefinitionBuilder` that makes it easier to instantiate your form definition with the sections and widgets
provided by this library.

#### Completed and CompletedPage

The new `FormDefinitionBuilder` also supports a new `Completed` trait for defining the view shown after the form is 
submitted. There is a ready-made implementation in `com.uxforms.govukdesignsystem.dsl.ConfirmationPage`.

#### Widgets

All implemented widgets can be found in `com.uxforms.govukdesignsystem.dsl.widgets`

#### Sections and Pages

Due to the unique layout of GOV.UK's design system, we also provide dedicated implementations of `Page`s and `Section`s.
One of the key differences is that they accept **two** lists of widgets - the first to be rendered before the main content
of the container, i.e. before the error summary box, and the second to be rendered after the error summary box. This is 
primarily to allow a "back" link to be rendered in the appropriate position, but you are free to put any number of any 
type of widget in here that you wish.

#### Messages

Where possible, every widget and container now accepts a **`Future`** of `Messages`. This is so that messages can be 
sourced from your theme or even a content management system, should you wish, without introducing a blocking process to 
fetch their contents.    

#### Template parameters

Template parameters are namespaced to allow widget-specific entries for the same template to live in the same file.

E.g. if `hint.text` is available in the context, then a widget will render its hint with its value. But if a 
`widgetName.hint.text` is available in the context, then that value will override that specified by `hint.text`. 

This means properties for multiple instances of the same widget can all live in the same file.

This also applies to constraint messages, too, which makes it much easier to provide widget-specific validation errors.

And, if you really want to, you can have all of your properties (apart from multiple choice values) in a single file. 

#### Helpers, utils and other goodies

`com.uxforms.govukdesignsystem.dsl.constraint.ConstraintHelper` has a couple of goodies, namely a `noConstraints` member 
that makes it easier to declare when widgets do not have any validation constraints. Also it provides an implicit 
`constraintAppender` to allow individual constraint instances to be appended together, just as you would in `uxforms-dsl`.

`com.uxforms.govukdesignsystem.dsl.message.MessageHelper` has two functions that make it easier to apply formatting 
arguments to entries in `Messages`. E.g. to be able to write `key.text=Today is {currentDayOfWeek}` in your `Messages` 
and apply a value when the form is rendered. 

#### Mustache rendering

This project uses a different Mustache rendering engine than `uxforms-dsl`. [Mustache.java](https://github.com/spullara/mustache.java)
is more actively maintained than the library used by `uxforms-dsl` and also does not suffer from the rendering bugs that the 
widgets in `uxforms-dsl` had to work around.

## Testing

### Template fixtures

We can now test widgets against the fixture html scenarios published by our govuk-design-system-theme, which is a packaged set
of fixture html from the version of govuk-frontend implemented by the theme.

There are a couple of new settings in build.sbt to support this:

* `themeVersion`: Set this to be the version of govuk-design-system-theme to test against
* `testAgainstLocalTheme`: Set this to `true` to test against theme files checked out on your local machine. Will attempt to infer their location based on the contents of your `~/.uxforms/sbt-uxforms.conf`.
* `themeTargetDirLocation`: Use this if the local theme files are somewhere else other than specified in your `~/.uxforms/sbt-uxforms.conf`.

Templates and fixtures are copied into the project's `target/theme` dir, and so do not need to be fetched if already present after previous runs.

### uTest

Experimenting with [µTest](https://github.com/lihaoyi/utest)

#### Good: 

* None of the cruft of ScalaTest
* IDE support (unlike microTest)

#### Bad:

* Stuck on version 0.6.7 as later versions don't support scala 2.11 (version-specific docs here: https://github.com/lihaoyi/utest/tree/v0.6.7)

## CHANGELOG

### Version 1.6.5

* Fixed a bug in MultipleChoice widgets (Radios, Checkboxes, Select) where the html id was not moved to the first item after items were re-ordered.

### Version 1.6.1

* Fixed a bug in CharacterCount's fallback hint text where .html hints were being written out as well as .text hints, and the count number wasn't displayed correctly.

### Version 1.6.0

* Added new feature to randomise item order for Checkboxes, Radios and Select.
* Added new representation for Dividers in Messages for Checkboxes, Radios, and Select. `ANYPREFIX.divider=text`.
* Deprecated `DIVIDER.label=text` special prefix to denote item dividers for Checkboxes, Radios and Select
* Fixed an issue where classes passed as ExtraTemplateRenderArgs to `Paragraph` builders (i.e. `Paragraph.body`, `.lead` and `.small`) were ignored
* Added new `merge` operation to make it easier to merge ExtraTemplateRenderArgs with our new TemplateArgManipulator 

### Version 1.5.0

* Upgraded to support govuk-frontend 1.4.0
* Implemented fixture tests to prove dsl and theme together produce the correct html
* New Div widget
* New TemplateArgManipulator to better handle Message and ExtraTemplateRenderArgs merging, collapsing and precedence
* First-class support for margins and padding

### Version 1.4.0

* New Table widget
* Support for testing against template fixtures published by govuk-frontend
* New Typography and Spacing helpers
* New CommonHtmlAttributes and CommonLegendHtmlAttributes traits to provide common functions across all widgets to easily add most-often used css classes and template flags

### Version 1.3.5

Radios and Checkboxes support multiple dividers

### Version 1.3.4

Bumped to the latest version of sbt and all its plugins

### Version 1.3.2

* `main.defaultButton` added to `Section.questionPage` and `Page.questionPage` builders to include a hidden button as the first button in the form, so hitting `<enter>` will submit the form and continue the journey, even if you have a `back` button as the first widget

### Version 1.3.1

* Locale added to `Page`, `Section` and `ConfirmationPage` template contexts

### Version 1.3.0

* Upgrade to support govuk-frontend version 3.13.0
  * New `ExclusiveChoice` constraint to enforce 'none of the above' option for Checkboxes on the server-side

### Version 1.2.3

* Bug fix in FileUpload widget - when adding or deleting files, unsaved values for other widgets on the same Page or Section were not being persisted

### Version 1.2.1

* Section now also renders its ProgressBar (when present)

### Version 1.2.0

* New `Pre` widget for rendering `<pre>` elements. Not part of GOV.UK's Design System.
* New `MessageContent` widget for rendering strings from Messages.
* New `Tag` widget implementing [GOV.UK's Tag Component](https://design-system.service.gov.uk/components/tag/)
* New `TaskList` widget and supporting classes and helpers implementing [GOV.UK's Task List pattern](https://design-system.service.gov.uk/patterns/task-list-pages/)

### Version 1.1.6

* Bug fix: Section's isMandatory now behaves as expected

### Version 1.1.4

* New `Tabs` widget implementing [GOV.UK's Tabs Component](https://design-system.service.gov.uk/components/tabs/)

### Version 1.1.3

* Fixed accessibility bug with validated widgets - ErrorSummary now links to the appropriate input element rather than its enclosing div
* Fixed html bug with CharacterCount and Textarea - no longer writes duplicate id attribute values

### Version 1.1.2

* Bee-client no longer marked as `provided`, to allow it to be included properly when running in UX Forms 2.0

### Version 1.1.1

* Bug fix for id attribute on `Select` widget

### Version 1.1.0

* New `Select` widget implementing [GOV.UK's Select Component](https://design-system.service.gov.uk/components/select/)

### Version 1.0.5

* Changed `uxforms-dsl` and `bee-client` dependencies to `"provided"` scope as they are already provided by UX Forms' runtime
and don't need to be explicitly included in the .jar produced by this library.

### Version 1.0.3

* Upgraded to sbt 1.4.1. Primarily to pick up [BSP support](https://www.scala-lang.org/blog/2020/10/27/bsp-in-sbt.html)
which makes it more efficient when switching between IntelliJ and the sbt terminal
* Added this README

### Version 1.0.2

* Removed properties files that were not needed

### Version 1.0.1

* Add a plugin to the build that will enforce binary compatibility for all future versions with `1.0.0`.

### Version 1.0.0

* First released version.