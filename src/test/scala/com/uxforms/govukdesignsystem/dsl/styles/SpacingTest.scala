package com.uxforms.govukdesignsystem.dsl.styles

import utest._
import Spacing._
import Spacing.direction._

object SpacingTest extends TestSuite {

  override def tests: Tests = Tests {

    "responsiveMargin" - {
      "should set direction" - {
        val generatedClass = responsiveMargin(4, right)
        assert(generatedClass == "govuk-!-margin-right-4")
      }
    }

    "responsivePadding" - {
      "should set direction" - {
        val generatedClass = responsivePadding(9, top)
        assert(generatedClass == "govuk-!-padding-top-9")
      }
    }
  }
}
