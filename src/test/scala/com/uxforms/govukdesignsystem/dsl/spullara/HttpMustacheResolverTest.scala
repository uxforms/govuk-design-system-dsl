package com.uxforms.govukdesignsystem.dsl.spullara

import java.io.{BufferedReader, IOException}
import java.net.URL

import com.github.mustachejava.MustacheException
import uk.co.bigbeeconsultants.http.header.MediaType
import uk.co.bigbeeconsultants.http.request.Request
import uk.co.bigbeeconsultants.http.response.{Response, Status}
import utest._

import scala.util.control.Exception.ignoring

object HttpMustacheResolverTest extends TestSuite {

  override def tests: Tests = Tests {

    val body = "{{template body}}"

    "getReader" - {

      def resolver(status: Status) = new HttpMustacheResolver("https://example.com") {
        override protected[spullara] def get(resourceName: String): Response = {
          Response(Request.get(new URL("https://example.com")), status, MediaType.APPLICATION_OCTET_STREAM, body)
        }
      }

      "should throw exception when template not found" - {
        val exception = intercept[MustacheException] {
          resolver(Status.S404_NotFound).getReader("me.mustache")
        }
        assert(exception.getMessage.startsWith("Template me.mustache not found"))
      }

      "should throw exception for any other non-OK response code" - {
        val exception = intercept[MustacheException] {
          resolver(Status.S408_ClientTimeout).getReader("you.mustache")
        }
        assert(exception.getMessage.startsWith(s"Unexpected response code ${Status.S408_ClientTimeout.code}"))
      }

      "should return a Reader when OK" - {
        val r = resolver(Status.S200_OK).getReader("they.mustache")
        val br = new BufferedReader(r)
        val result = Stream.continually(br.readLine()).takeWhile(_ != null).mkString("")

        try {
          assert(result == body)
        } finally {
          ignoring(classOf[IOException])(r.close())
          ignoring(classOf[IOException])(br.close())
        }
      }
    }
  }
}
