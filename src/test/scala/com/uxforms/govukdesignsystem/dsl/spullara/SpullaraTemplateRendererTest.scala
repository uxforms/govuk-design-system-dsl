package com.uxforms.govukdesignsystem.dsl.spullara

import com.uxforms.govukdesignsystem.dsl.test.TemplateRendererBuilder

import java.util
import utest._

object SpullaraTemplateRendererTest extends TestSuite {

  override val tests: Tests = Tests {

    "toJava" - {
      val renderer = new SpullaraTemplateRenderer("myTheme", None, TemplateRendererBuilder.config)

      "should convert a map" - {
        val given = Map("key" -> "value")
        val result = renderer.toJava(given)
        assert(result.isInstanceOf[java.util.Map[_, _]])
      }

      "should convert a seq" - {
        val given = Seq("first", "second")
        val result = renderer.toJava(given)
        assert(result.isInstanceOf[java.util.AbstractCollection[_]])
      }

      "should convert a map in a seq" - {
        val given = Seq(Map("first" -> "one"), Map("second" -> "two"))
        val result = renderer.toJava(given)
        assert(result.isInstanceOf[java.util.AbstractCollection[_]])

        val iter = result.asInstanceOf[util.AbstractCollection[Object]].iterator()
        val first = iter.next()
        assert(first.isInstanceOf[java.util.Map[_, _]])

        val second = iter.next()
        assert(second.isInstanceOf[java.util.Map[_, _]])
      }
    }

    "staticAssetsThemeUrl" - {
      "should be built from the staticAssetsBaseURL and theme name" - {
        val actual = new SpullaraTemplateRenderer("themeName", None, TemplateRendererBuilder.config).staticAssetsThemeUrl
        val expected = s"${TemplateRendererBuilder.config.getString("staticAssetsBaseURL")}/themeName"
        assert(actual == expected)
      }
    }

    "buildPath" - {
      "should handle trailing slashes on all members" - {
        val resultWithTrailingSlashes = new SpullaraTemplateRenderer("themeName/", Some("template/path/"), TemplateRendererBuilder.config).buildPath
        val expectedWithTrailingSlashes = s"${TemplateRendererBuilder.config.getString("staticAssetsBaseServerURL")}/themeName/template/path/"
        assert(resultWithTrailingSlashes == expectedWithTrailingSlashes)

        val resultWithoutTrailingSlashes = new SpullaraTemplateRenderer("themeName", Some("path"), TemplateRendererBuilder.config).buildPath
        val expectedWithoutTrailingSlashes = s"${TemplateRendererBuilder.config.getString("staticAssetsBaseServerURL")}/themeName/path/"
        assert(resultWithoutTrailingSlashes == expectedWithoutTrailingSlashes)
      }
    }

  }
}
