package com.uxforms.govukdesignsystem.dsl

import com.uxforms.domain.constraint.Required
import com.uxforms.domain.widget.Constrained
import com.uxforms.domain.{EmptyMessages, Messages}
import com.uxforms.dsl.containers
import com.uxforms.dsl.helpers.ContainerBuilders.SectionDetails
import com.uxforms.govukdesignsystem.dsl.test.FormDefinitionBuilder.formDef
import com.uxforms.govukdesignsystem.dsl.test.TemplateRendererBuilder
import com.uxforms.govukdesignsystem.dsl.widget.Input
import utest._
import com.uxforms.govukdesignsystem.dsl.constraint.ConstraintHelper.noConstraints
import com.uxforms.dsl.helpers.ContainerHelper.isMandatory

import java.util.Locale
import scala.concurrent.Future

object SectionTest extends TestSuite {

  private implicit val locale: Locale = Locale.UK

  override def tests: Tests = Tests {

    val sectionMessages = Future.successful(Messages.empty())
    implicit val formMessages: Future[EmptyMessages] = Future.successful(Messages.empty())
    implicit val templateRenderer: TemplateRenderer = TemplateRendererBuilder.concrete("myTheme")
    val section = Section.section("myPage", sectionMessages, SectionDetails())

    "sectionArgs" - {
      "should set the assertUrl from templateRenderer's staticAssetsThemeUrl" - {
        val args = section.sectionArgs(formDef(Seq.empty[containers.Section]: _*), Map.empty)
        val actual = args("assetUrl")
        val expected = templateRenderer.staticAssetsThemeUrl
        assert(actual == expected)
      }
    }

    "constructor" - {
      "should add a required field to the section's widgets if the section is mandatory" - {
        val sectionWithWidget = Section.section("first", sectionMessages, SectionDetails(), Input.inputText("it", sectionMessages, noConstraints))
        assert(sectionWithWidget.widgets.length == 1)

        val requiredSectionWithWidget = Section.section("second", sectionMessages, isMandatory("uniqueFieldName"), Input.inputText("it", sectionMessages, noConstraints))
        assert(requiredSectionWithWidget.widgets.length == 2)
        assert(requiredSectionWithWidget.widgets.collectFirst{ case w: Constrained if w.constraints.exists(_.isInstanceOf[Required[_]]) => w}.isDefined)
      }
    }


  }
}