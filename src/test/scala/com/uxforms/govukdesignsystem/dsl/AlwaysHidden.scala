package com.uxforms.govukdesignsystem.dsl

import com.uxforms.domain.FormData
import com.uxforms.dsl.widgets.WidgetVisibility

object AlwaysHidden extends WidgetVisibility {
  override def shown(data: FormData): Boolean = false
}
