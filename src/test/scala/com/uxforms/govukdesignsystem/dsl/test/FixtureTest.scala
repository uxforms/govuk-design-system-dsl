package com.uxforms.govukdesignsystem.dsl.test

import com.uxforms.domain.widget.Widget
import com.uxforms.domain.{ExtraTemplateRenderArgs, FormData, NoExtraRenderArgs, ValidationErrors}
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.template.Template
import com.uxforms.govukdesignsystem.dsl.test.FormBuilder.form
import com.uxforms.govukdesignsystem.dsl.test.FormDefinitionBuilder.formDef
import com.uxforms.govukdesignsystem.dsl.test.HtmlUtils.renderAndParseHtml
import com.uxforms.govukdesignsystem.dsl.test.UTestUtils._
import com.uxforms.govukdesignsystem.dsl.widget.templateargs.ExtraTemplateRenderArgsMergeOps.ExtraTemplateRenderArgsOperations
import play.api.libs.json.{JsNumber, JsString, JsValue, Json}

import java.util.Locale
import scala.concurrent.{ExecutionContext, Future}

trait FixtureTest extends TemplateFixturesSetup {
  val componentName: String

  val excludedFixtureNames: Set[String] = Set.empty
  val individuallyTestedFixtureNames: Set[String] = Set.empty

  lazy val parsedFixtures: Seq[Fixture] = {
    val fixturesFile = fixturesFileForComponentName(componentName)
    fixturesFile
      .inputStream
      .map(i => Json.parse(i).as[Fixtures])
      .get()
      .fixtures
  }

  def individuallyTestedFixtures: Seq[Fixture] = {
    parsedFixtures.filter(f => individuallyTestedFixtureNames.contains(f.name))
  }

  def fixtures: Seq[Fixture] = {
    parsedFixtures
      .filterNot(f => excludedFixtureNames.contains(f.name) || individuallyTestedFixtureNames.contains(f.name))
  }

  def convertAttributes(attributeValue: JsValue): Seq[Map[String, Any]] = {
    attributeValue.as[Map[String, JsValue]].map {
      case (k, JsString(v)) => Map("attribute" -> k, "value" -> v)
      case (k, JsNumber(v)) => Map("attribute" -> k, "value" -> v)
    }.toSeq
  }

  def optionAttributes(fixture: Fixture, builder: Seq[Map[String, Any]] => ExtraTemplateRenderArgs): Option[ExtraTemplateRenderArgs] = {
    fixture.options.get("attributes").map(attributes => {
      val attrs = convertAttributes(attributes)
      builder(attrs)
    })
  }

  def flattenExtraTemplateRenderArgs(args: Option[ExtraTemplateRenderArgs]*): ExtraTemplateRenderArgs = {
    args.flatten.foldLeft(NoExtraRenderArgs.asInstanceOf[ExtraTemplateRenderArgs])((result, item) => result merge item)
  }

  def testFixtures[T](test: Fixture => Future[T])(implicit ec: ExecutionContext): Future[String] = {
    Future.sequence(fixtures.map { fixture =>
      test(fixture).map(_ => fixture.name)
    }).map(fixtureNames => s"Scenarios tested: ${fixtureNames.mkString(", ")}\nScenarios excluded: ${excludedFixtureNames.mkString(", ")}")
  }

}

trait WidgetFixtureTest[W <: Widget] extends FixtureTest {

  def widgetFromFixture(fixture: Fixture): W

  def formDataFromFixture(fixture: Fixture): FormData = FormData()

  def validationErrorsFromFixture(fixture: Fixture): ValidationErrors = Seq.empty


  def runScenario(fixture: Fixture)(test: WidgetFixtureScenario[W] => Unit)(implicit ec: ExecutionContext): Future[Unit] = {
    val widget = widgetFromFixture(fixture)
    val validationErrors = validationErrorsFromFixture(fixture)
    val formData = formDataFromFixture(fixture)
    val expectedHtml = HtmlUtils.parse(fixture.html)

    renderAndParseHtml(form(formData, formDef(Seq[Widget](): _*)), validationErrors, widget).map { actualHtml =>
      val scenario = new WidgetFixtureScenario(widget, expectedHtml, actualHtml)
      withClues("fixture" -> fixture.name) {
        test(scenario)
      }
    }
  }

}

trait TemplateFixtureTest[T <: Template] extends FixtureTest {

  def templateFromFixture(fixture: Fixture): T

  def argsFromFixture(fixture: Fixture): Map[String, Any]

  def runScenario(fixture: Fixture)(test: TemplateFixtureScenario[T] => Unit)(implicit ec: ExecutionContext, renderer: TemplateRenderer, locale: Locale): Future[Unit] = {
    val template = templateFromFixture(fixture)
    val expectedHtml = HtmlUtils.parse(fixture.html)
    val args = argsFromFixture(fixture)

    renderAndParseHtml(template, args).map { oElement =>

      val scenario = new TemplateFixtureScenario(template, expectedHtml, oElement.getOrElse(HtmlUtils.parse("")))
      withClues("fixture" -> fixture.name) {
        test(scenario)
      }
    }

  }
}