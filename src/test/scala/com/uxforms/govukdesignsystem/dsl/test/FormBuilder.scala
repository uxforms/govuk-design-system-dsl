package com.uxforms.govukdesignsystem.dsl.test

import com.uxforms.domain.{FormData, FormDefinition}
import com.uxforms.dsl.Form
import org.joda.time.Instant

import java.util.UUID

trait FormBuilder {

  def form(formDefinition: FormDefinition): Form = {
    form(FormData(), formDefinition)
  }

  def form(data: FormData, formDefinition: FormDefinition): Form = {
    Form(data, formDefinition, UUID.randomUUID(), Instant.now)
  }

}

object FormBuilder extends FormBuilder
