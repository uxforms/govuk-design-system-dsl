package com.uxforms.govukdesignsystem.dsl.test

import com.github.mustachejava.DefaultMustacheFactory
import com.github.mustachejava.resolver.FileSystemResolver
import com.typesafe.config.{Config, ConfigFactory, ConfigValueFactory}
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.spullara.SpullaraTemplateRenderer

import java.io.StringWriter

trait TemplateRendererBuilder {

  val config: Config = ConfigFactory.empty()
    .withValue("staticAssetsBaseURL", ConfigValueFactory.fromAnyRef("https://example.com/base"))
    .withValue("staticAssetsBaseServerURL", ConfigValueFactory.fromAnyRef("https://example.com/baseServer"))

  def concrete(themeName: String): TemplateRenderer = {
    new SpullaraTemplateRenderer(themeName, None, config)
  }

  lazy val anon: TemplateRenderer = new TemplateRenderer {
    override def staticAssetsThemeUrl: String = "https://example.com"

    override def render(templateName: String, args: Map[String, Any]): String = ""
  }

  lazy val local: TemplateRenderer = new TemplateRenderer {
    override def staticAssetsThemeUrl: String = ""

    val mf = new DefaultMustacheFactory(new FileSystemResolver((TemplateFixturesSetup.themeContentsDir / "assets" / "templates").toJava))

    override def render(templateName: String, args: Map[String, Any]): String = {
      val template = mf.compile(templateName)
      val writer = new StringWriter()
      template.execute(writer, toJava(args))
      writer.toString
    }

    //@formatter:off
    protected def toJava(x: Any): Any = {
      import scala.collection.JavaConverters._
      x match {
        case ml: scala.collection.MapLike[_, _, _] =>
          ml.map { case (k, v) => toJava(k) -> toJava(v)}.asJava
        case sl: scala.collection.SetLike[_, _] =>
          sl.map { toJava }.asJava
        case it: Iterable[_] =>
          it.map { toJava }.asJava
        case it: Iterator[_] =>
          toJava(it.toIterable)
        case _ => x
      }
    }
    //@formatter:on
  }

}

object TemplateRendererBuilder extends TemplateRendererBuilder