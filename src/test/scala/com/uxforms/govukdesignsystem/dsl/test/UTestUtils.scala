package com.uxforms.govukdesignsystem.dsl.test

import utest.{AssertionError, TestValue}

import scala.reflect.runtime.universe.TypeTag

trait UTestUtils {

  /**
   * An adaption of ScalaTest's withClue for uTest.
   *
   * utest's fancy assert macro is good for printing values used in the assertion when it fails.
   * But quite often we also want to know other values in the test case as well, so this is a way of
   * passing in more info to an assertion failure's error report.
   *
   * Particularly useful when iterating over a set of scenarios, to tell you which one failed:
   *
   * {{{
   *   for (scenario <- testScenarios) {
   *     withClue("scenario name" -> scenario.name) {
   *       assert(scenario.value == codeUnderTest.value)
   *     }
   *   }
   * }}}
   *
   * @param name A name of the additional info. Typically variable name.
   * @param value The value to be printed.
   * @param body Code block containing your assertions
   */
  def withClues[T](testValue: TestValue, testValues: TestValue*)(body: => T): T = {
    try {
      body
    } catch {
      case ae: AssertionError => throw ae.copy(captured = testValue +: (testValues ++ ae.captured))
    }
  }

  implicit def tupleToTestValue[V](tuple: (String, V))(implicit tag: TypeTag[V]): TestValue =
    TestValue(tuple._1, tag.tpe.typeSymbol.name.toString, tuple._2)
}

object UTestUtils extends UTestUtils
