package com.uxforms.govukdesignsystem.dsl.test

import com.uxforms.govukdesignsystem.dsl.test.HtmlUtils._
import utest._

object HtmlUtilsTest extends TestSuite {

  override def tests: Tests = Tests {

    "diff" - {

      "attributes with different values should have a diff" - {
        val htmlA = parse("""<div attr1="One"></div>""")
        val htmlB = parse("""<div attr1="1"></div>""")
        assert(diff(htmlA, htmlB).nonEmpty)
      }

      "different number of attributes should have a diff" - {
        val htmlA = parse("""<div attr1="1" attr2="2"></div>""")
        val htmlB = parse("""<div attr1="1"></div>""")
        assert(diff(htmlA, htmlB).nonEmpty)

        assert(diff(htmlB, htmlA).nonEmpty)
      }

      "attributes should be the same regardless of order" - {
        val htmlA = parse("""<div attr1="1" attr2="2"></div>""")
        val htmlB = parse("""<div attr2="2" attr1="1"></div>""")
        assert(diff(htmlA, htmlB).isEmpty)
      }

      "different tags should have a diff" - {
        val htmlA = parse("<span>text</span>")
        val htmlB = parse("<div>text</div>")
        assert(diff(htmlA, htmlB).nonEmpty)
      }

      "different children should have a diff" - {
        val html = "<div>Text<%1$s>more</%1$s></div>"
        val htmlA = parse(html.format("span"))
        val htmlB = parse(html.format("p"))
        assert(diff(htmlA, htmlB).nonEmpty)
      }

      "different text content should have a diff" - {
        val htmlA = parse("""<p>Content</p>""")
        val htmlB = parse("""<p>More Content</p>""")
        assert(diff(htmlA, htmlB).nonEmpty)
      }

      "ignore selected attributes" - {
        val html = """<div id="%1$s" name="foo" tabindex="1"><span id="%2$s">text</span></div>"""
        val htmlA = parse(html.format("1", "2"))
        val htmlB = parse(html.format("10", "20"))
        assert(diff(htmlA, htmlB).nonEmpty)
        assert(diff(htmlA, htmlB, "id").isEmpty)
      }

      "either is empty should have a diff" - {
        val firstEmpty = diff(parse(""), parse("<p>Text</p>"))
        assert(firstEmpty.nonEmpty)
        assert(diff(parse("<p>Text</p>"), parse("")).nonEmpty)
        assert(diff(parse(""), parse("")).isEmpty)
      }
    }
  }
}
