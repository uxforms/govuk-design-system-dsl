package com.uxforms.govukdesignsystem.dsl.test

import com.uxforms.domain._
import com.uxforms.domain.widget.Widget
import com.uxforms.dsl.containers._
import com.uxforms.dsl.{Form, containers}

import java.util.Locale
import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

trait FormDefinitionBuilder {

  def formDef(formDefWidgets: Widget*)(implicit dummy: DummyImplicit): FormDefinition = {
    formDef(new containers.Section {
      override val name: String = "mySection"
      override val canContinue: SectionCanContinue = SectionCanAlwaysContinue
      override val widgets: Seq[Widget] = formDefWidgets
      override val title: String = "myTitle"
      override val receivers: Seq[DataTransformer] = Seq.empty
      override val redirectBeforeRender: RedirectBeforeRender = NoRedirectBeforeRender

      override def render(form: Form, errors: ValidationErrors, requestInfo: RequestInfo, runtimeWidgets: Widget*)(implicit ec: ExecutionContext): Future[String] = {
        Future.sequence(widgets.map(_.render(form, errors, requestInfo))).map(_.mkString(" "))
      }

      override val submissions: Seq[DataTransformer] = Seq.empty
      override val redirectAfterSubmissions: RedirectAfterSubmissions = AlwaysRedirectToForm
    })
  }

  def formDef(formDefSections: containers.Section*)(implicit dummy1: DummyImplicit, dummy2: DummyImplicit): FormDefinition = {
    new FormDefinition {
      override val name: FormDefinitionName = FormDefinitionName("my-form")
      override val messages: Messages[_] = Messages.empty()
      override val locale: Locale = Locale.UK
      override val pages: Seq[containers.Page] = Seq.empty
      override val resumePages: Seq[containers.Page] = Seq.empty
      override val submissions: Seq[DataTransformer] = Seq.empty
      override val inflightDataRetention: Duration = 5.minutes
      override val sections: Seq[containers.Section] = formDefSections

      override def renderComplete(form: Form, requestInfo: RequestInfo)(implicit ec: ExecutionContext): Future[String] = {
        Future.successful("complete")
      }
    }
  }

  def formDef(formDefPages: Page*)(implicit dummy1: DummyImplicit, dummy2: DummyImplicit, dummy3: DummyImplicit): FormDefinition = {
    new FormDefinition {
      override val name: FormDefinitionName = FormDefinitionName("my-form")
      override val messages: Messages[_] = Messages.empty()
      override val locale: Locale = Locale.UK
      override val pages: Seq[Page] = formDefPages
      override val resumePages: Seq[Page] = Seq.empty
      override val submissions: Seq[DataTransformer] = Seq.empty
      override val inflightDataRetention: Duration = 5.minutes
      override val sections: Seq[Section] = Seq.empty

      override def renderComplete(form: Form, requestInfo: RequestInfo)(implicit ec: ExecutionContext): Future[String] = {
        Future.successful("complete")
      }
    }
  }

}

object FormDefinitionBuilder extends FormDefinitionBuilder