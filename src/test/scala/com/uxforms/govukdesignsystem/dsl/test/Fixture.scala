package com.uxforms.govukdesignsystem.dsl.test

import com.uxforms.domain.constraint.{Constraint, ValidationError, WidgetValidationError}
import com.uxforms.domain.widget.{Named, Persisted, Widget}
import com.uxforms.domain.{ExtraTemplateRenderArgs, SimpleExtraTemplateRenderArgs}
import play.api.libs.json.{Format, JsValue, Json, Reads}

/**
 * Mapping to represent GOV.UK Frontend test fixture JSON
 *
 * https://frontend.design-system.service.gov.uk/testing-your-html/#using-the-html-test-files
 */
case class Fixtures(component: String, fixtures: Seq[Fixture])

object Fixtures {
  implicit val fixturesFormat: Format[Fixtures] = Json.format[Fixtures]
}

case class Fixture(name: String, options: Map[String, JsValue], html: String, hidden: Boolean) {

  def option[T](name: String)(implicit reads: Reads[T]): Option[T] = options.get(name).map(_.as[T])

  def optionArg[T](name: String, argPrefix: String)(implicit reads: Reads[T]): Option[ExtraTemplateRenderArgs] = option[T](name).map(t => SimpleExtraTemplateRenderArgs(argPrefix -> t))

  def errorForField(widget: Widget with Persisted, fieldName: String, msg: JsValue): Seq[WidgetValidationError] = {
    Seq(
      WidgetValidationError(widget, fieldName,
        new Constraint {
          override def displayMessage(error: ValidationError): String = (msg \ "text").as[String]
        }
      )
    )
  }

  def error(widget: Widget with Persisted with Named): Seq[ValidationError] = option[JsValue]("errorMessage").fold(Seq.empty[ValidationError])(msg =>
    errorForField(widget, widget.name, msg)
  )

  def error(widget: Widget with Persisted, widgetName: String): Seq[ValidationError] = option[JsValue]("errorMessage").fold(Seq.empty[ValidationError])(msg =>
    errorForField(widget, widgetName, msg)
  )
}

object Fixture {
  implicit val fixtureFormat: Format[Fixture] = Json.format[Fixture]
}
