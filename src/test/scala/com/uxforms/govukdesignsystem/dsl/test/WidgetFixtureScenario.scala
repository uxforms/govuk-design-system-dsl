package com.uxforms.govukdesignsystem.dsl.test

import com.uxforms.domain.widget.Widget
import com.uxforms.govukdesignsystem.dsl.template.Template
import org.jsoup.nodes.Element
import utest.TestValue

import scala.language.implicitConversions

case class WidgetFixtureScenario[W <: Widget](widget: W, expected: Element, actual: Element)

object WidgetFixtureScenario {
  implicit def scenarioToTestValues[W <: Widget](scenario: WidgetFixtureScenario[W]): Seq[TestValue] = Seq(
    TestValue("expected", classOf[Element].getName, scenario.expected),
    TestValue("actual", classOf[Element].getName, scenario.actual)
  )
}

case class TemplateFixtureScenario[T <: Template](template: T, expected: Element, actual: Element)

object TemplateFixtureScenario {
  implicit def scenarioToTestValues[T <: Template](scenario: TemplateFixtureScenario[T]): Seq[TestValue] = Seq(
    TestValue("expected", classOf[Element].getName, scenario.expected),
    TestValue("actual", classOf[Element].getName, scenario.actual)
  )
}