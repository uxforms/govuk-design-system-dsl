package com.uxforms.govukdesignsystem.dsl.test

import com.uxforms.domain.RequestInfo

import java.util.Locale

trait RequestInfoBuilder {

  val local: RequestInfo = RequestInfo("localhost", "/my-form", remoteAddress = "127.0.0.1", locale = Locale.UK)
}

object RequestInfoBuilder extends RequestInfoBuilder
