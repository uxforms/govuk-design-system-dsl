package com.uxforms.govukdesignsystem.dsl.test

import com.uxforms.domain.ValidationErrors
import com.uxforms.domain.widget.Widget
import com.uxforms.dsl.Form
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.template.Template
import com.uxforms.govukdesignsystem.dsl.test.HtmlUtils.safeHtml
import org.jsoup.Jsoup
import org.jsoup.helper.Consumer
import org.jsoup.nodes._

import java.util.Locale
import scala.collection.JavaConverters.collectionAsScalaIterableConverter
import scala.compat.java8.FunctionConverters.asJavaConsumer
import scala.concurrent.{ExecutionContext, Future}

trait HtmlUtils {

  val standardAttributesToIgnore: Seq[String] = Seq(
    "id",
    "for",
    "aria-describedby",
    "data-aria-controls"
  )

  def parse(s: String): Element = {
    val parsed = Jsoup.parseBodyFragment(s)
    parsed.outputSettings().prettyPrint(true)
    parsed.selectFirst("body").firstElementChild()
  }

  def renderAndParseHtml(form: Form, validationErrors: ValidationErrors, w: Widget)(implicit ec: ExecutionContext): Future[Element] = {
    w.render(form, validationErrors, RequestInfoBuilder.local).map { html =>
      parse(html)
    }
  }

  def renderAndParseHtml(t: Template, args: Map[String, Any])(implicit ec: ExecutionContext, templateRenderer: TemplateRenderer, l: Locale): Future[Option[Element]] = {
    t.render(args).map { html =>
      html.map(parse)
    }
  }

  def cleanAttributes(element: Element): Element = {
    if (element != null) {
      element.getAllElements.forEach(asJavaConsumer[Element](e =>
        e.attributes().forEach(asJavaConsumer[Attribute](a =>
          a.setValue(a.getValue.trim.replaceAll("\n", " ").replaceAll(" {2,}", " "))))))
    }
    element
  }

  def stripAttributes(element: Element, attributeNames: String*): Element = {
    if (element != null) {
      element.getAllElements.forEach(asJavaConsumer[Element](e =>
        e.attributes().forEach(asJavaConsumer[Attribute](a =>
          if (attributeNames.contains(a.getKey)) {
            e.removeAttr(a.getKey)
          }
        ))
      ))
    }
    element
  }

  def stripEmptyTextNodes(element: Element): Element = {
    if (element != null) {
      element.forEachNode(new Consumer[Node] {
        override def accept(t: Node): Unit = t match {
          case node: TextNode if node.isBlank => t.remove()
          case _ => ()
        }
      })
    }
    element
  }

  def diff[W <: Widget](scenario: WidgetFixtureScenario[W], attributeNamesToStrip: Seq[String]): DiffResult = {
    diff(scenario.actual, scenario.expected, attributeNamesToStrip: _*)
  }

  def diff[T <: Template](scenario: TemplateFixtureScenario[T], attributeNamesToStrip: Seq[String]): DiffResult = {
    diff(scenario.actual, scenario.expected, attributeNamesToStrip: _*)
  }

  def diff(actual: Element, expected: Element, attributeNamesToStrip: String*): DiffResult = {

    val actualCleaned = cleanAttributes(stripAttributes(stripEmptyTextNodes(actual), attributeNamesToStrip: _*))
    val expectedCleaned = cleanAttributes(stripAttributes(stripEmptyTextNodes(expected), attributeNamesToStrip: _*))

    def compareAttributes(aAttributes: Attributes, bAttributes: Attributes): Seq[Diff] = {

      val aAttrs = aAttributes.asList().asScala.map(a => a.getKey -> a.getValue).toSet
      val bAttrs = bAttributes.asList().asScala.map(a => a.getKey -> a.getValue).toSet

      def attrToString(a: (String, String)) = {
        s"""${a._1}="${a._2}""""
      }

      (aAttrs.diff(bAttrs) ++ bAttrs.diff(aAttrs))
        .map(a => Diff(s"""attribute ${attrToString(a)} only present in one of ${aAttrs.map(attrToString).mkString(",")} or ${bAttrs.map(attrToString).mkString(",")}""")).toSeq
    }

    val actualCleanedAllElements = Option(actualCleaned).map(_.getAllElements.asScala).getOrElse(Seq.empty)
    val expectedCleanedAllElements = Option(expectedCleaned).map(_.getAllElements.asScala).getOrElse(Seq.empty)

    val sizeCheck = (actualCleanedAllElements.size, expectedCleanedAllElements.size) match {
      case (a, b) if a == b => Seq.empty
      case (a, b) if a != b => Seq(Diff(s"actual has $a elements but expected has ${b}"))
    }

    val diffs = actualCleanedAllElements.zip(expectedCleanedAllElements).collect {
      case (a, b) if (a.normalName() != b.normalName()) => Seq(Diff(s"element name mismatch: ${a} vs ${b}"))
      case (a, b) if compareAttributes(a.attributes(), b.attributes()).nonEmpty => compareAttributes(a.attributes(), b.attributes())
      case (a, b) if (a.ownText() != b.ownText()) => Seq(Diff(s"text mismatch between ${a} and ${b}: ${a.ownText()} vs ${b.ownText()}"))
    }.flatten.toSeq

    DiffResult(actualCleaned, expectedCleaned, sizeCheck ++ diffs)
  }

  def safeHtml(e: Element): String = {
    if (e != null) e.html
    else ""
  }
}

case class DiffResult(a: Element, b: Element, diff: Seq[Diff]) {
  def isEmpty: Boolean = diff.isEmpty

  def nonEmpty: Boolean = diff.nonEmpty

  override def toString: String = s"""\n${diff.mkString("\n")} \nActual html:\n${safeHtml(a)}\nExpected html:\n${safeHtml(b)}"""

}

object DiffResult {
  implicit def diffResultToBoolean(dr: DiffResult): Boolean = dr.isEmpty
}

case class Diff(description: String) {
  override def toString: String = "[x] " + description
}

object HtmlUtils extends HtmlUtils
