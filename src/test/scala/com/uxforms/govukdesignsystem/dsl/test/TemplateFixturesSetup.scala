package com.uxforms.govukdesignsystem.dsl.test

import better.files.Dsl._
import better.files.File.home
import better.files._
import com.typesafe.config.ConfigFactory
import com.uxforms.govukdesignsystem.dsl.build.BuildInfo
import uk.co.bigbeeconsultants.http.HttpClient

import java.net.URL
import java.nio.file.{FileAlreadyExistsException, Files, StandardCopyOption}
import scala.collection.JavaConverters.collectionAsScalaIterableConverter

trait TemplateFixturesSetup {

  private lazy val httpClient = new HttpClient

  private lazy val localThemeTargetDir: File = {
    File(BuildInfo.themeTargetDirLocation.getOrElse {
      val themeConfig = ConfigFactory.parseFile((home / ".uxforms" / "sbt-uxforms.conf").toJava)
      themeConfig.getConfigList("themes").asScala.find(c => c.getString("name") == "govuk-design-system-theme").map(_.getString("location")).get
    })
  }

  private def downloadZipToFile(url: URL, file: File) = {
    System.out.println(s"Downloading $url to $file")
    val response = httpClient.get(url)
    scala.util.control.Exception.ignoring(classOf[FileAlreadyExistsException]) {
      Files.copy(response.body.inputStream, file.path, StandardCopyOption.REPLACE_EXISTING)
    }
  }

  val themeContentsDir: File = ("target" / "theme" / "theme-contents").createDirectoryIfNotExists(createParents = true)
  if (themeContentsDir.isEmpty) {

    if (BuildInfo.testAgainstLocalTheme) {
      System.out.println(s"Copying theme from $localThemeTargetDir")
      scala.util.control.Exception.ignoring(classOf[FileAlreadyExistsException]) {
        localThemeTargetDir.copyTo(themeContentsDir)
      }
    } else {
      val themeFile = File(s"target/theme/theme-${BuildInfo.themeVersion}.zip")
      val url = new URL(s"https://artifacts-public.uxforms.net/com/uxforms/govuk-design-system-theme/${BuildInfo.themeVersion}/govuk-design-system-theme-${BuildInfo.themeVersion}.zip")
      downloadZipToFile(url, themeFile)
      themeFile.unzipTo(themeContentsDir)
    }
  }

  val fixturesDir: File = ("target" / "theme" / "fixtures").createDirectoryIfNotExists()
  if (fixturesDir.isEmpty) {
    if (BuildInfo.testAgainstLocalTheme) {
      val localComponentsDir = localThemeTargetDir / `..` / `..` / "node_modules" / "govuk-frontend" / "govuk" / "components"
      System.out.println(s"Copying fixtures from $localComponentsDir")
      localComponentsDir.glob("**/fixtures.json", includePath = false).foreach(f => {
        scala.util.control.Exception.ignoring(classOf[FileAlreadyExistsException]) {
          f.copyToDirectory((fixturesDir / f.parent.name).createDirectoryIfNotExists())
        }
      })
    } else {
      val fixturesFile = File(s"target/theme/theme-fixtures-${BuildInfo.themeVersion}.zip")
      val url = new URL(s"https://artifacts-public.uxforms.net/com/uxforms/govuk-design-system-theme/${BuildInfo.themeVersion}/govuk-design-system-theme-fixtures-${BuildInfo.themeVersion}.zip")
      downloadZipToFile(url, fixturesFile)
      fixturesFile.unzipTo(fixturesDir)
    }
  }

  def fixturesFileForComponentName(componentName: String): File = {
    fixturesDir / componentName / "fixtures.json"
  }
}

object TemplateFixturesSetup extends TemplateFixturesSetup
