package com.uxforms.govukdesignsystem.dsl.constraint

import com.uxforms.domain.constraint.WidgetValidationError
import com.uxforms.domain.widget.DateWidget
import com.uxforms.domain.{EmptyMessages, FormData, Messages}
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.test.TemplateRendererBuilder
import com.uxforms.govukdesignsystem.dsl.widget.DateInput
import dispatch.Future
import play.api.libs.json.Json
import utest._

import scala.concurrent.ExecutionContext.Implicits.global


object FullDateTest extends TestSuite {

  private implicit val formMessages: Future[Messages[EmptyMessages]] = Future.successful(Messages.empty())
  private implicit val templateRenderer: TemplateRenderer = TemplateRendererBuilder.anon

  override def tests: Tests = Tests {

    val fullDate = new FullDate(Messages.empty)
    val dateWidget = DateInput.date("myDate", Future.successful(Messages.empty()), Seq.empty)

    "validate" - {
      "should return OK when" - {
        "the widget has no value in FormData" - {
          fullDate.validate(dateWidget, dateWidget.name, FormData()).map { result =>
            assert(result.errors.isEmpty)
          }
        }
        "the widget has no field values in FormData" - {
          fullDate.validate(dateWidget, dateWidget.name, buildData(dateWidget.name, None, None, None)).map { result =>
            assert(result.errors.isEmpty)
          }
        }
        "all fields are present and have numeric values" - {
          fullDate.validate(dateWidget, dateWidget.name, buildData(dateWidget.name, Some(1), Some(2), Some(2020))).map { result =>
            assert(result.errors.isEmpty)
          }
        }
      }

      "should return an error when any one field is missing" - {
        val table = Seq(
          buildData(dateWidget.name, None, None, Some(2020)) -> DateWidget.dayField(dateWidget.name),
          buildData(dateWidget.name, None, Some(2), None) -> s"${dateWidget.name}.year",
          buildData(dateWidget.name, None, Some(2), Some(2020)) -> DateWidget.dayField(dateWidget.name),
          buildData(dateWidget.name, Some(1), None, None) -> s"${dateWidget.name}.year",
          buildData(dateWidget.name, Some(1), Some(2), None) -> s"${dateWidget.name}.year",
          buildData(dateWidget.name, Some(1), None, Some(2020)) -> DateWidget.monthField(dateWidget.name)
        )
        Future.sequence(
          table.map {
            case (data, expectedField) => fullDate.validate(dateWidget, dateWidget.name, data).map { result =>
              assert(result.errors.nonEmpty)
              val firstError = result.errors.head
              assert(firstError.isInstanceOf[WidgetValidationError])
              val firstWidgetValidationError = firstError.asInstanceOf[WidgetValidationError]
              assert(firstWidgetValidationError.fieldName == expectedField)
            }
          }
        )
      }
    }
  }

  private def buildData(widgetName: String, day: Option[Short], month: Option[Short], year: Option[Short]) = {
    FormData(Json.obj(widgetName -> Json.obj(
      "day" -> day,
      "month" -> month,
      "year" -> year
    )))
  }

}
