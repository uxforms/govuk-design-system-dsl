package com.uxforms.govukdesignsystem.dsl.constraint

import com.uxforms.domain.{EmptyMessages, FormData, ListMessages, Messages, ValidationResult}
import com.uxforms.dsl.helpers.MessageHelper._
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.constraint.ConstraintHelper._
import com.uxforms.govukdesignsystem.dsl.test.TemplateRendererBuilder
import com.uxforms.govukdesignsystem.dsl.widget.Checkboxes
import play.api.libs.json.Json
import utest._

import java.util.Locale
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{ExecutionContext, Future}

object ExclusiveChoiceTest extends TestSuite {

  private implicit val messages: Future[EmptyMessages] = Future.successful(Messages.empty())
  private implicit val renderer: TemplateRenderer = TemplateRendererBuilder.anon
  private implicit val locale: Locale = Locale.UK
  private val widget = Checkboxes.checkboxes("check", messages, noConstraints, Future.successful(Messages.empty()))

  private val choicesWithoutUnique: Messages[ListMessages] = Seq(
    "FIRST.label.text" -> "First",
    "SECOND.label.text" -> "Second"
  )
  private val choicesWithUnique: Messages[ListMessages] = Seq(
    "FIRST.label.text" -> "First",
    "FIRST.hint.text" -> "first",
    "SECOND.label.text" -> "Second",
    "SECOND.hint.text" -> "second",
    "SECOND.behaviour" -> "exclusive",
    "THIRD.label.text" -> "Third",
    "THIRD.hint.text" -> "third"
  )

  override def tests: Tests = Tests {

    "validate" - {

      val withoutUnique = ExclusiveChoice.exclusiveChoice(Future.successful(choicesWithoutUnique))
      val withUnique = ExclusiveChoice.exclusiveChoice(Future.successful(choicesWithUnique))

      "should return OK when there are no values" - {
        for {
          valWithout <- validate(withoutUnique, FormData())
          valWith <- validate(withUnique, FormData())
        } yield {
          assert(valWithout.errors.isEmpty)
          assert(valWith.errors.isEmpty)
        }
      }

      "should return OK when the values are empty" - {
        for {
          valWithout <- validate(withoutUnique, FormData(Json.obj("check" -> Seq.empty[String])))
          valWith <- validate(withUnique, FormData(Json.obj("check" -> Seq.empty[String])))
        } yield {
          assert(valWithout.errors.isEmpty)
          assert(valWith.errors.isEmpty)
        }
      }

      "should return OK when values are selected but there is no exclusive set" - {
        for {
          single <- validate(withoutUnique, FormData(Json.obj("check" -> Seq("FIRST"))))
          multiple <- validate(withoutUnique, FormData(Json.obj("check" -> Seq("FIRST", "SECOND"))))
        } yield {
          assert(single.errors.isEmpty)
          assert(multiple.errors.isEmpty)
        }
      }

      "should return OK even when incorrect values are selected but there is no exclusive set" - {
        validate(withoutUnique, FormData(Json.obj("check" -> Seq("WHOOPS")))).map { result =>
          assert(result.errors.isEmpty)
        }
      }

      "should return OK when multiple non-exclusive values are selected" - {
        validate(withUnique, FormData(Json.obj("check" -> Seq("FIRST, THIRD")))).map { result =>
          assert(result.errors.isEmpty)
        }
      }

      "should return OK when just the unique value is selected" - {
        validate(withUnique, FormData(Json.obj("check" -> Seq("SECOND")))).map { result =>
          assert(result.errors.isEmpty)
        }
      }

      "should fail if a unique and non-unique value is selected" - {
        validate(withUnique, FormData(Json.obj("check" -> Seq("FIRST", "SECOND")))).map { result =>
          assert(result.errors.nonEmpty)
        }
      }

      "should fail if multiple non-unique and the unique value is selected" - {
        validate(withUnique, FormData(Json.obj("check" -> Seq("FIRST", "SECOND", "THIRD")))).map { result =>
          assert(result.errors.nonEmpty)
        }
      }
    }
  }

  private def validate[C <: Messages[C], E <: Messages[E]](exclusive: ExclusiveChoice[C, E], data: FormData)(implicit ec: ExecutionContext): Future[ValidationResult] = {
    exclusive.validate(widget, "check", data)
  }
}
