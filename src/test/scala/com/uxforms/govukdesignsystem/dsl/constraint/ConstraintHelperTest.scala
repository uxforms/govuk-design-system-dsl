package com.uxforms.govukdesignsystem.dsl.constraint

import com.uxforms.domain.constraint.Constraint
import com.uxforms.domain.{EmptyMessages, Messages}
import utest._
import com.uxforms.domain.constraint.Required.required

object ConstraintHelperTest extends TestSuite {

  override def tests: Tests = Tests {

    implicit val msgs: Messages[EmptyMessages] = Messages.empty()

    "should make it easy to build a Seq of Constraint by appending them together" - {
      import ConstraintHelper.constraintAppender
      val con1 = required()
      val con2 = required()

      // If this compiles we're good to go
      val constraints: Seq[Constraint] = con1 ++ con2
    }

    "should fail to compile appending constraints without the helper in scope" - {
      val con1 = required()
      val con2 = required()

      compileError("""val constraints: Seq[Constraint] = con1 ++ con2""")
    }
  }
}
