package com.uxforms.govukdesignsystem.dsl.template

import com.uxforms.govukdesignsystem.dsl.test.{Fixture, HtmlUtils, TemplateFixtureTest, TemplateRendererBuilder}
import play.api.libs.json.JsValue
import utest._

import java.util.Locale
import scala.concurrent.ExecutionContext.Implicits.global

object PhaseBannerTest extends TestSuite with TemplateFixtureTest[DummyTemplate] {

  override def tests: Tests = Tests {

    "should pass all fixture scenarios" - {

      implicit val templateRenderer = TemplateRendererBuilder.local
      implicit val locale = Locale.UK

      testFixtures { fixture =>
        runScenario(fixture) { scenario =>
          val result = HtmlUtils.diff(scenario.actual, scenario.expected, HtmlUtils.standardAttributesToIgnore: _*)
          assert(result)
        }
      }
    }

  }

  override def templateFromFixture(fixture: Fixture): DummyTemplate = {
    new DummyTemplate("phase-banner/template.mustache")
  }

  override def argsFromFixture(fixture: Fixture): Map[String, Any] = {
    Seq(
      fixture.option[JsValue]("tag").flatMap(j => (j \ "text").asOpt[String].map(t => "govukTag.text" -> t)),
      fixture.option[JsValue]("tag").flatMap(j => (j \ "html").asOpt[String].map(t => "govukTag.html" -> t)),
      fixture.option[JsValue]("tag").flatMap(j => (j \ "classes").asOpt[String].map(t => "govukTag.classes" -> t)),
      fixture.option[String]("html").map(h => "phaseBanner.html" -> h),
      fixture.option[String]("text").map(h => "phaseBanner.text" -> h),
      fixture.option[String]("classes").map(c => "phaseBanner.classes" -> c),
      fixture.option[JsValue]("attributes").map(a => "phaseBanner.attributes" -> convertAttributes(a))
    ).flatten.toMap

  }

  override val componentName: String = "phase-banner"
}
