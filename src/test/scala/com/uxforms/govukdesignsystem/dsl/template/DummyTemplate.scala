package com.uxforms.govukdesignsystem.dsl.template

import com.uxforms.govukdesignsystem.dsl.TemplateRenderer

import java.util.Locale
import scala.concurrent.{ExecutionContext, Future}

/**
 * We don't need a concrete class for some templates that are included by the others directly.
 * But it makes our test easier to create one.
 */
class DummyTemplate(templateName: String) extends Template {

  override def render(args: Map[String, Any])(implicit renderer: TemplateRenderer, locale: Locale, executionContext: ExecutionContext): Future[Option[String]] =
    Future.successful {
      Some(renderer.render(templateName, args))
    }

}
