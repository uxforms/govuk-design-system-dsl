package com.uxforms.govukdesignsystem.dsl.template

import com.uxforms.govukdesignsystem.dsl.test.{Fixture, HtmlUtils, TemplateFixtureTest, TemplateRendererBuilder}
import play.api.libs.json.JsValue
import utest._

import java.util.Locale
import scala.concurrent.ExecutionContext.Implicits.global

object HeaderTest extends TestSuite with TemplateFixtureTest[DummyTemplate] {

  override def tests: Tests = Tests {

    "should pass all fixture scenarios" - {

      implicit val templateRenderer = TemplateRendererBuilder.local
      implicit val locale = Locale.UK

      testFixtures { fixture =>
        runScenario(fixture) { scenario =>
          val result = HtmlUtils.diff(scenario.actual, scenario.expected, HtmlUtils.standardAttributesToIgnore: _*)
          assert(result)
        }
      }
    }
  }

  override def templateFromFixture(fixture: Fixture): DummyTemplate = {
    new DummyTemplate("header/template.mustache")
  }

  override def argsFromFixture(fixture: Fixture): Map[String, Any] = {


    val args = Seq(
      fixture.option[String]("serviceName").map(n => Map("header.serviceName" -> n)),
      fixture.option[String]("serviceUrl").map(n => Map("header.serviceUrl" -> n)),
      fixture.options.get("serviceName").orElse(fixture.options.get("navigation")).map(_ => Map("header.serviceNameOrNavigation" -> true)),
      fixture.option[Seq[JsValue]]("navigation").map(navigation => Map(
        "header.navigation" -> navigation.map { item =>
          Map.empty ++
            (item \ "href").asOpt[String].map(s => "item.href" -> s) ++
            (item \ "text").asOpt[String].map(s => "item.text" -> s) ++
            (item \ "html").asOpt[String].map(s => "item.html" -> s) ++
            (item \ "html").asOpt[String].orElse((item \ "text").asOpt[String]).map(_ => "item.htmlOrText" -> true) ++
            (item \ "active").asOpt[Boolean].map(s => "item.active" -> s) ++
            (item \ "attributes").asOpt[JsValue].map(j => "item.attributes" -> convertAttributes(j))
        },
        "header.hasNavigation" -> true
      )),
      fixture.option[String]("navigationLabel").map(s => Map("header.navigationLabel" -> s)),
      fixture.option[String]("menuButtonText").map(s => Map("header.menuButtonText" -> s)),
      fixture.option[String]("menuButtonLabel").map(s => Map("header.menuButtonLabel" -> s)),
      fixture.option[String]("navigationClasses").map(s => Map("header.navigationClasses" -> s)),
      fixture.option[String]("productName").map(s => Map("header.productName" -> s)),
      fixture.option[String]("containerClasses").map(s => Map("header.containerClasses" -> s)),
      fixture.option[JsValue]("attributes").map(j => Map("header.attributes" -> convertAttributes(j))),
      fixture.option[String]("classes").map(s => Map("header.classes" -> s))
    )

    val flattened = args.flatten
    if (flattened.isEmpty) Map.empty else flattened.reduce(_ ++ _)
  }

  override val componentName: String = "header"
}
