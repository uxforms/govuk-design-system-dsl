package com.uxforms.govukdesignsystem.dsl.template

import com.uxforms.govukdesignsystem.dsl.test.{Fixture, HtmlUtils, TemplateFixtureTest, TemplateRendererBuilder}
import play.api.libs.json.JsValue
import utest._

import java.util.Locale
import scala.concurrent.ExecutionContext.Implicits.global

object SkipLinkTest extends TestSuite with TemplateFixtureTest[DummyTemplate] {

  override def tests: Tests = Tests {

    "should pass all fixture scenarios" - {

      implicit val templateRenderer = TemplateRendererBuilder.local
      implicit val locale = Locale.UK

      testFixtures { fixture =>
        runScenario(fixture) { scenario =>
          val result = HtmlUtils.diff(scenario.actual, scenario.expected, HtmlUtils.standardAttributesToIgnore: _*)
          assert(result)
        }
      }
    }

  }

  override def templateFromFixture(fixture: Fixture): DummyTemplate = {
    new DummyTemplate("skip-link/template.mustache")
  }

  override def argsFromFixture(fixture: Fixture): Map[String, Any] = {
    Seq(
      fixture.option[String]("html").map(h => "govukSkipLink.html" -> h),
      fixture.option[String]("text").map(h => "govukSkipLink.text" -> h),
      fixture.option[String]("href").map(h => "govukSkipLink.href" -> h),
      fixture.option[String]("classes").map(c => "govukSkipLink.classes" -> c),
      fixture.option[JsValue]("attributes").map(a => "govukSkipLink.attributes" -> convertAttributes(a))
    ).flatten.toMap

  }

  override val componentName: String = "skip-link"
}
