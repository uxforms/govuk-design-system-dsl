package com.uxforms.govukdesignsystem.dsl.template

import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.test.{Fixture, HtmlUtils, TemplateFixtureTest, TemplateRendererBuilder}
import play.api.libs.json.JsValue
import utest._

import java.util.Locale
import scala.concurrent.ExecutionContext.Implicits.global

object HintTest extends TestSuite with TemplateFixtureTest[Hint] {

  private val hint = new Hint("widgetId", "widgetName", "templateName")

  private val minimumEntries = Map("hint.id" -> hint.id)

  override def templateFromFixture(fixture: Fixture): Hint = {
    val widgetId = fixture.option[String]("id").getOrElse("widgetId")
    new Hint(widgetId, "widgetName")
  }

  override def argsFromFixture(fixture: Fixture): Map[String, Any] = {
    Seq(
      fixture.option[String]("text").map(h => Map("hint.text" -> h)),
      fixture.option[String]("html").map(h => Map("hint.html" -> h)),
      fixture.option[String]("classes").map(c => Map("hint.classes" -> c)),
      fixture.option[JsValue]("attributes").map(a => Map("hint.attributes" -> convertAttributes(a)))
    ).flatten.flatten.toMap

  }

  override val componentName: String = "hint"

  val tests = Tests {

    "should pass all fixture scenarios" - {
      implicit val templateRenderer: TemplateRenderer = TemplateRendererBuilder.local
      implicit val locale = Locale.UK
      testFixtures { fixture =>
        runScenario(fixture) { scenario =>
          val result = HtmlUtils.diff(scenario, HtmlUtils.standardAttributesToIgnore)
          assert(result)
        }
      }
    }


    "buildArgs" - {
      "should leave an empty map alone" - {
        val result = hint.buildArgs(Map.empty)
        assert(result == minimumEntries)
      }

      "should do nothing when there are no widget-specific entries" - {
        val given = Map("hint.text" -> "my hint")
        val expected = given ++ minimumEntries
        val result = hint.buildArgs(given)
        assert(result == expected)
      }

      "should add a hint.text entry when there is a widget.hint.text entry present" - {
        val given = Map("widgetName.hint.text" -> "my hint")
        val expected: Map[String, Any] = minimumEntries ++ given + ("hint.text" -> "my hint")
        val result = hint.buildArgs(given)
        assert(result == expected)
      }

      "should override an existing hint.text entry when a widget.hint.text entry is given" - {
        val given = Map("widgetName.hint.text" -> "overridden hint", "hint.text" -> "default hint")
        val expected = minimumEntries ++ given + ("hint.text" -> "overridden hint")
        val result = hint.buildArgs(given)
        assert(result == expected)
      }

      "should lave a global label alone" - {
        val given = Map("hint.text" -> "another hint")
        val expected = minimumEntries ++ given
        assert(hint.buildArgs(given) == expected)
      }
    }
  }

}
