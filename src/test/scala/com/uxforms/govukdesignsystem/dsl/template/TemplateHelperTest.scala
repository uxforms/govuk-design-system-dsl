package com.uxforms.govukdesignsystem.dsl.template

import com.uxforms.domain.constraint.MaxLength.maxLength
import com.uxforms.domain.constraint.WidgetValidationError
import com.uxforms.domain.{EmptyMessages, Messages}
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.constraint.ConstraintHelper.constraintAppender
import com.uxforms.govukdesignsystem.dsl.template.TemplateHelper._
import com.uxforms.govukdesignsystem.dsl.test.TemplateRendererBuilder
import com.uxforms.govukdesignsystem.dsl.widget.Input.inputText
import utest._

import java.util.Locale
import scala.concurrent.Future

object TemplateHelperTest extends TestSuite {

  override def tests: Tests = Tests {

    implicit val locale: Locale = Locale.UK

    "overrideWithWidgetSpecificEntries" - {
      "should leave the map alone if there are no relevant overrides" - {
        val given = Map("anotherWidget.hint.text" -> "another widget's hint value", "hint.text" -> "hint value")
        val result = overrideWithWidgetSpecificEntries("myWidget", "hint", given)
        assert(result == given)
      }
      "should override the key value when given a widget-specific key" - {
        val given = Map("hint.text" -> "default hint value", "myWidget.hint.text" -> "overridden hint value")
        val expected = Map("hint.text" -> "overridden hint value", "myWidget.hint.text" -> "overridden hint value")
        val result = overrideWithWidgetSpecificEntries("myWidget", "hint", given)
        assert(result == expected)
      }
    }

    "widgetValidationMessage" - {
      implicit val formLevelMessages: Future[Messages[EmptyMessages]] = Future.successful(Messages.empty())
      implicit val templateRenderer: TemplateRenderer = TemplateRendererBuilder.anon

      val constraint = maxLength(20)(Messages.empty())
      val widget = inputText("myInput", Future.successful(Messages.empty()), constraint)
      val error = WidgetValidationError(widget, widget.name, constraint)

      "should apply message formatting to a MessageConstraint's message, with the error prepended as the first argument" - {
        val result = widgetValidationMessage(Map("myInput.constraint.maxlength.errorMessage" -> "Your name must be {1,number,#} characters or fewer."), error)
        val expected = "Your name must be 20 characters or fewer."
        assert(result == expected)
      }

      "should prefer the widget-prefixed key when available" - {
        val result = widgetValidationMessage(Map("constraint.maxlength.errorMessage" -> "default", "myInput.constraint.maxlength.errorMessage" -> "for widget"), error)
        val expected = "for widget"
        assert(result == expected)
      }

      "should use the non-prefixed key when no widget-prefixed key is present" - {
        val result = widgetValidationMessage(Map("constraint.maxlength.errorMessage" -> "default"), error)
        val expected = "default"
        assert(result == expected)
      }

      "should fall back to the constraint's own displayMessage function if no relevant keys are present" - {
        val result = widgetValidationMessage(Map.empty, error)
        val expected = constraint.displayMessage(error)
        assert(result == expected)
      }
    }

    "format" - {
      "should apply icu4j formatting rules to a message" - {
        val result = format("{0,number,#} is a number", Seq(20))
        val expected = "20 is a number"
        assert(result == expected)
      }
    }

    "appendAttributes" - {
      "should add an attributes entry if there is not one already" - {
        val attrsToAdd = Seq(Map("attribute" -> "attrName", "value" -> "attrValue"))
        val preexistingArgs = Map("other" -> "ignore me")
        val expected = Map("prefix.attributes" -> attrsToAdd) ++ preexistingArgs
        val result = appendAttributes("prefix", preexistingArgs, attrsToAdd)
        assert(result == expected)
      }

      "should append attributes to an existing value" - {
        val attrsToAdd = Seq(Map("attribute" -> "attrName", "value" -> "attrValue"))
        val preexisting = Map("other" -> "ignore me", "prefix.attributes" -> Seq(Map("attribute" -> "existingAttrName", "value" -> "existingValue")))
        val expected = preexisting ++ Map("prefix.attributes" -> Seq(Map("attribute" -> "existingAttrName", "value" -> "existingValue"), Map("attribute" -> "attrName", "value" -> "attrValue")))
        val result = appendAttributes("prefix", preexisting, attrsToAdd)
        assert(result == expected)
      }
    }

    "appendClasses" - {
      "should add a classes entry if there is not one already" - {
        val preexisting = Map("other" -> "ignore me")
        val expected = Map("prefix.classes" -> "new-class") ++ preexisting
        val result = appendClasses("prefix", preexisting, "new-class")
        assert(result == expected)
      }

      "should append classes to an existing value" - {
        val expected = Map("prefix.classes" -> "old-class new-class")
        val result = appendClasses("prefix", Map("prefix.classes" -> "old-class"), "new-class")
        assert(result == expected)
      }
    }
  }
}
