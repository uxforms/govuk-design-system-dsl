package com.uxforms.govukdesignsystem.dsl.template

import com.uxforms.domain.constraint.MaxLength.maxLength
import com.uxforms.domain.constraint.Required.required
import com.uxforms.domain.constraint.{Constraint, ValidationError, WidgetValidationError}
import com.uxforms.domain.{EmptyMessages, Messages, NoExtraRenderArgs}
import com.uxforms.dsl.widgets.alwaysShown
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.test.{Fixture, HtmlUtils, TemplateFixtureTest, TemplateRendererBuilder}
import com.uxforms.govukdesignsystem.dsl.widget.TextArea
import play.api.libs.json.JsValue
import utest._

import java.util.Locale
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object ErrorMessageTest extends TestSuite with TemplateFixtureTest[ErrorMessage] {

  private implicit val locale: Locale = Locale.UK
  private implicit val renderer: TemplateRenderer = TemplateRendererBuilder.anon
  private implicit val formMessages: Future[EmptyMessages] = Future.successful(Messages.empty())
  private val widgetMessages = Messages.empty()
  private val requiredConstraint = required()(Messages.empty)
  private val maxLengthConstraint = maxLength(5)(Messages.empty)
  private val widget =
    new TextArea("myTextArea", "text area", Future.successful(widgetMessages), requiredConstraint,
      "textarea/template.mustache", NoExtraRenderArgs,
      alwaysShown)
  private val requiredValidationError: WidgetValidationError = WidgetValidationError(widget, widget.name, requiredConstraint)
  private val maxLengthValidationError: WidgetValidationError = WidgetValidationError(widget, widget.name, maxLengthConstraint)

  override def templateFromFixture(fixture: Fixture): ErrorMessage = {
    new ErrorMessage("13", widget.name, Seq(WidgetValidationError(widget, widget.name, new Constraint {
      override def displayMessage(error: ValidationError): String = ""
    })))
  }

  override def argsFromFixture(fixture: Fixture): Map[String, Any] = {
    Map() ++
      fixture.option[String]("visuallyHiddenText").map(t => "errorMessage.visuallyHiddenText" -> t) ++
      fixture.option[String]("text").map(t => "errorMessage.text" -> t) ++
      fixture.option[String]("html").map(h => "errorMessage.html" -> h) ++
      fixture.option[String]("classes").map(c => "errorMessage.classes" -> c) ++
      fixture.option[JsValue]("attributes").map(a => "errorMessage.attributes" -> convertAttributes(a))
  }

  override val componentName: String = "error-message"


  val tests: Tests = Tests {

    val errorMessage = new ErrorMessage("widgetId", widget.name, Seq(requiredValidationError, maxLengthValidationError))

    "fixture scenarios" - {
      implicit val renderer: TemplateRenderer = TemplateRendererBuilder.local
      testFixtures { fixture =>
        runScenario(fixture) { scenario =>
          val result = HtmlUtils.diff(scenario, HtmlUtils.standardAttributesToIgnore :+ "href")
          assert(result)
        }
      }
    }

    "buildArgs" - {
      "should add the element id" - {
        val given = Map.empty[String, Any]
        val expected = Some(errorMessage.id)
        assert(errorMessage.buildArgs(given).get("errorMessage.id") == expected)
      }

      "should put multiple errors in the same html key, with a <br /> to put them each on their own line" - {
        val given = Map.empty[String, Any]
        val expected = Some(requiredConstraint.displayMessage(requiredValidationError) + "<br />" + maxLengthConstraint.displayMessage(maxLengthValidationError))
        assert(errorMessage.buildArgs(given).get("errorMessage.html") == expected)
      }

      "should override general properties with widget-specific ones" - {
        val given = Map(
          "errorMessage.classes" -> "global-default",
          s"${widget.name}.errorMessage.classes" -> "widget-override"
        )
        val expected = Some("widget-override")
        val result = errorMessage.buildArgs(given).get("errorMessage.classes")
        assert(result == expected)
      }
    }

    "render" - {
      "should render nothing if there are no errors" - {
        val errorMessage = new ErrorMessage("widgetId", widget.name, Seq.empty)

        val expected = None
        errorMessage.render(Map.empty).map(result => assert(result == expected))
      }
    }
  }

}
