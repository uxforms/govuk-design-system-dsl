package com.uxforms.govukdesignsystem.dsl.template

import com.uxforms.govukdesignsystem.dsl.test.{Fixture, HtmlUtils, TemplateFixtureTest, TemplateRendererBuilder}
import play.api.libs.json.JsValue
import utest._

import java.util.Locale
import scala.concurrent.ExecutionContext.Implicits.global

object FooterTest extends TestSuite with TemplateFixtureTest[DummyTemplate] {

  override def tests: Tests = Tests {

    "should pass all fixture scenarios" - {

      implicit val templateRenderer = TemplateRendererBuilder.local
      implicit val locale = Locale.UK

      testFixtures { fixture =>
        runScenario(fixture) { scenario =>
          val result = HtmlUtils.diff(scenario.actual, scenario.expected, HtmlUtils.standardAttributesToIgnore: _*)
          assert(result)
        }
      }
    }
  }

  override def templateFromFixture(fixture: Fixture): DummyTemplate = {
    new DummyTemplate("footer/template.mustache")
  }

  override def argsFromFixture(fixture: Fixture): Map[String, Any] = {

    val args = Seq(
      fixture.option[JsValue]("contentLicence").flatMap(j => {
        (j \ "html").asOpt[String].map(s => Map("footer.contentLicence.html" -> s))
          .orElse((j \ "text").asOpt[String].map(t => Map("footer.contentLicence.text" -> t)))
      }),
      fixture.option[JsValue]("copyright").flatMap(j => {
        (j \ "html").asOpt[String].map(s => Map("footer.copyright.html" -> s))
          .orElse((j \ "text").asOpt[String].map(s => Map("footer.copyright.text" -> s)))
      }),
      fixture.option[String]("classes").map(c => Map("footer.classes" -> c)),
      fixture.option[JsValue]("attributes").map(a => Map("footer.attributes" -> convertAttributes(a))),
      fixture.option[JsValue]("meta").flatMap(j => (j \ "visuallyHiddenTitle").asOpt[String].map(s => Map("footer.meta.visuallyHiddenTitle" -> s))),
      fixture.option[JsValue]("meta").flatMap(j => (j \ "items").asOpt[Seq[JsValue]].map(s => Map("footer.meta.items.nonEmpty" -> s.nonEmpty))),
      fixture.option[JsValue]("meta").map(_ => Map("footer.meta" -> true)),
      fixture.option[JsValue]("meta").flatMap(j => (j \ "text").asOpt[String].map(t => Map("footer.meta.text" -> t))),
      fixture.option[JsValue]("meta").flatMap(j => (j \ "html").asOpt[String].map(t => Map("footer.meta.html" -> t))),
      fixture.option[JsValue]("meta").flatMap(j => (j \ "html").asOpt[String].orElse((j \ "text").asOpt[String]).map(_ => Map("footer.meta.htmlOrText" -> true))),
      fixture.option[JsValue]("meta").flatMap(j => (j \ "items").asOpt[Seq[JsValue]].map(items => Map("footer.meta.items" -> items.map { item =>
        Map.empty[String, Any] ++
          (item \ "href").asOpt[String].map(s => "href" -> s) ++
          (item \ "text").asOpt[String].map(s => "text" -> s) ++
          (item \ "attributes").asOpt[JsValue].map(j => "attributes" -> convertAttributes(j))
      }))),

      fixture.option[Seq[JsValue]]("navigation").map(j => {
        Map(
          "footer.navigation" -> j.map { nav =>
            Map(
              "nav.items.nonEmpty" -> (nav \ "items").asOpt[Seq[JsValue]].exists(_.nonEmpty),
              "nav.items" -> (nav \ "items").asOpt[Seq[JsValue]].map(_.map { item =>
                Map.empty ++
                  (item \ "href").asOpt[String].map(s => "href" -> s) ++
                  (item \ "text").asOpt[String].map(s => "text" -> s) ++
                  (item \ "attributes").asOpt[JsValue].map(j => "attributes" -> convertAttributes(j))
              }).getOrElse(Seq.empty)
            ) ++
              (nav \ "columns").asOpt[Int].map(i => "nav.columns" -> i) ++
              (nav \ "title").asOpt[String].map(s => "nav.title" -> s) ++
              (nav \ "width").asOpt[String].map(s => "nav.width" -> s)
          },
          "footer.navigation.nonEmpty" -> j.nonEmpty)
      }),
      fixture.option[String]("containerClasses").map(s => Map("footer.containerClasses" -> s))
    )

    val flattened = args.flatten
    if (flattened.isEmpty) Map.empty else flattened.reduce(_ ++ _)

  }

  override val componentName: String = "footer"
}
