package com.uxforms.govukdesignsystem.dsl.template

import com.uxforms.govukdesignsystem.dsl.test.{Fixture, HtmlUtils, TemplateFixtureTest, TemplateRendererBuilder}
import play.api.libs.json.JsValue
import utest._

import java.util.Locale
import scala.concurrent.ExecutionContext.Implicits.global

object LabelTest extends TestSuite with TemplateFixtureTest[Label] {

  override def tests: Tests = Tests {

    "should pass all fixture scenarios" - {

      implicit val templateRenderer = TemplateRendererBuilder.local
      implicit val locale = Locale.UK

      testFixtures { fixture =>
        runScenario(fixture) { scenario =>
          val result = HtmlUtils.diff(scenario.actual, scenario.expected, HtmlUtils.standardAttributesToIgnore: _*)
          assert(result)
        }
      }
    }

  }

  override def templateFromFixture(fixture: Fixture): Label = {
    new Label("id", "widgetName")
  }

  override def argsFromFixture(fixture: Fixture): Map[String, Any] = {
    Seq(
      fixture.option[String]("text").map(s => Map("label.text" -> s)),
      fixture.option[String]("html").map(s => Map("label.html" -> s)),
      fixture.option[String]("classes").map(c => Map("label.classes" -> c)),
      fixture.option[Boolean]("isPageHeading").map(h => Map("label.isPageHeading" -> h)),
      fixture.option[String]("for").map(f => Map("label.for" -> f)),
      fixture.option[JsValue]("attributes").map(a => Map("label.attributes" -> convertAttributes(a)))
    ).flatten.flatten.toMap

  }

  override val componentName: String = "label"
}
