package com.uxforms.govukdesignsystem.dsl.template

import com.uxforms.domain.constraint.Required.required
import com.uxforms.domain.constraint.{Constraint, GlobalValidationError, ValidationError, WidgetValidationError}
import com.uxforms.domain.{EmptyMessages, HtmlIdGenerator, Messages}
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.constraint.ConstraintHelper._
import com.uxforms.govukdesignsystem.dsl.test.FormDefinitionBuilder.formDef
import com.uxforms.govukdesignsystem.dsl.test.{Fixture, HtmlUtils, TemplateFixtureTest, TemplateRendererBuilder}
import com.uxforms.govukdesignsystem.dsl.widget.Input
import play.api.libs.json.JsValue
import utest._

import java.util.Locale
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object ErrorSummaryTest extends TestSuite with TemplateFixtureTest[ErrorSummary] {

  private implicit val uk: Locale = Locale.UK
  private implicit val formMessages: EmptyMessages = Messages.empty()
  private implicit val futureFormMessages: Future[EmptyMessages] = Future.successful(Messages.empty())
  private implicit val templateRenderer = TemplateRendererBuilder.anon

  private val constraint = required()
  private val widget = Input.inputText("myInput", Future.successful(Messages.empty), constraint)
  private val formDefinition = formDef(widget)


  override val excludedFixtureNames: Set[String] = Set(
    "error list with attributes", // This can be done by overriding the way the Template builds up its args but it's extremely niche.
    "error list with html", // GlobalValidationErrors by default are written out as text as we can't assume the markup is safe.
    "error list with html link" // WidgetValidationErrors by default are written out as text as we can't assume the markup is safe
  )


  override val individuallyTestedFixtureNames: Set[String] = Set(
    "autofocus disabled",
    "autofocus explicitly enabled"
  )

  override def templateFromFixture(fixture: Fixture): ErrorSummary = {

    val errors: Seq[ValidationError] = fixture.option[Seq[JsValue]]("errorList").map(_.map {
      case e if (e \ "href").asOpt[String].isDefined => WidgetValidationError(widget, (e \ "href").as[String].substring(1), new Constraint {
        override def displayMessage(error: ValidationError): String = ((e \ "text").asOpt[String].orElse((e \ "html").asOpt[String]).getOrElse(""))
      })
      case e => GlobalValidationError(((e \ "text").asOpt[String].orElse((e \ "html").asOpt[String]).getOrElse("")))
    }).getOrElse(Seq(GlobalValidationError("We need at least one error for hte ErrorSummary to be rendered at all. So this is a hack for scenarios where no errors are provided")))

    new ErrorSummary(formDefinition, errors)
  }

  override def argsFromFixture(fixture: Fixture): Map[String, Any] = {
    Map() ++
      fixture.option[String]("titleText").map(t => "errorSummary.titleText" -> t) ++
      fixture.option[String]("titleHtml").map(h => "errorSummary.titleHtml" -> h) ++
      fixture.option[String]("descriptionText").map(t => "errorSummary.descriptionText" -> t) ++
      fixture.option[String]("descriptionHtml").map(h => "errorSummary.descriptionHtml" -> h) ++
      fixture.option[String]("classes").map(c => "errorSummary.classes" -> c) ++
      fixture.option[JsValue]("attributes").map(a => "errorSummary.attributes" -> convertAttributes(a)) ++
      fixture.option[Boolean]("disableAutoFocus").map(f => "errorSummary.disableAutoFocus" -> f)
  }

  override val componentName: String = "error-summary"


  override def tests: Tests = Tests {

    "fixture scenarios" - {
      implicit val templateRenderer: TemplateRenderer = TemplateRendererBuilder.local

      "autofocus disabled" - {
        val fixture = individuallyTestedFixtures.find(_.name == "autofocus disabled").get
        runScenario(fixture) { scenario =>
          assert(scenario.actual.attr("data-disable-auto-focus") == "true")
        }
      }

      "autofocus explicitly enabled" - {
        val fixture = individuallyTestedFixtures.find(_.name == "autofocus explicitly enabled").get
        runScenario(fixture) { scenario =>
          assert(scenario.actual.attr("data-disable-auto-focus") == "false")
        }
      }

      "all" - {
        testFixtures { fixture =>
          runScenario(fixture) { scenario =>
            val result = HtmlUtils.diff(scenario, HtmlUtils.standardAttributesToIgnore :+ "href")
            assert(result)
          }
        }
      }
    }


    "buildArgs" - {
      "should write item.text for a GlobalValidationError" - {

        val error = GlobalValidationError("my global validation error message")
        val summary = new ErrorSummary(formDefinition, Seq(error))

        val args = summary.buildArgs(Map.empty)
        val actual = args("errorSummary.errorList").asInstanceOf[Seq[Map[String, Any]]]

        assert(actual.head("item.text") == error.validationMessage)
      }

      "should link to the generated elementId for a WidgetValidationError" - {

        val error = WidgetValidationError(widget, widget.name, constraint)
        val summary = new ErrorSummary(formDefinition, Seq(error))

        val args = summary.buildArgs(Map.empty)
        val actual = args("errorSummary.errorList").asInstanceOf[Seq[Map[String, Any]]]

        val expected = "#" + HtmlIdGenerator.elementId(formDefinition, widget)

        assert(actual.head("item.href") == expected)
      }
    }
  }

}