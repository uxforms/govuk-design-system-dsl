package com.uxforms.govukdesignsystem.dsl.widget

import utest._
import MultipleChoiceItemsHelper._

object MultipleChoiceItemsHelperTest extends TestSuite {

  override def tests: Tests = Tests {

    "valueFromKey" - {
      "should return the key if it does not contain any periods" - {
        val actual = valueFromKey("ABAREKEY")
        val expected = "ABAREKEY"
        assert(actual == expected)
      }

      "should return the value before the first period" - {
        val actual = valueFromKey("FIRST.SECOND.THIRD")
        val expected = "FIRST"
        assert(actual == expected)
      }
    }

    "groupByOrdered" - {
      "should handle countries" - {
        val source = Seq(
          "ENGLAND.label.text",
          "SCOTLAND.label.text",
          "WALES.label.text",
          "NIRELAND.label.text",
          "DIVIDER.text",
          "ABROAD.label.text"
        )
        val expectedKeys = Seq(
          "ENGLAND", "SCOTLAND", "WALES", "NIRELAND", "DIVIDER", "ABROAD"
        )
        val result = source.groupByOrdered(valueFromKey)
        val actualKeys = result.map(_._1)
        assert(actualKeys == expectedKeys)
      }
    }
  }
}
