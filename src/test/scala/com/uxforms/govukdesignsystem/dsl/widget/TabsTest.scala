package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain._
import com.uxforms.domain.widget.Widget
import com.uxforms.dsl.Form
import com.uxforms.dsl.widgets.Content
import com.uxforms.govukdesignsystem.dsl.template.TemplateHelper
import com.uxforms.govukdesignsystem.dsl.template.TemplateHelper.buildAttribute
import com.uxforms.govukdesignsystem.dsl.test.FormBuilder.form
import com.uxforms.govukdesignsystem.dsl.test.FormDefinitionBuilder.formDef
import com.uxforms.govukdesignsystem.dsl.test.{Fixture, HtmlUtils, RequestInfoBuilder, TemplateRendererBuilder, WidgetFixtureTest}
import com.uxforms.govukdesignsystem.dsl.widget.HtmlAssertions.{alwaysHidden, assertAttributes, assertId, assertVisibilityClass}
import com.uxforms.govukdesignsystem.dsl.{AlwaysHidden, TemplateRenderer}
import org.jsoup.nodes.Node
import org.jsoup.select.NodeFilter
import org.jsoup.select.NodeFilter.FilterResult
import play.api.libs.json.{JsValue, Json}
import utest._

import java.util.Locale
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{ExecutionContext, Future}


object TabsTest extends TestSuite with WidgetFixtureTest[Tabs[_, _]] {

  private implicit val renderer: TemplateRenderer = TemplateRendererBuilder.local
  private implicit val formMessage: Future[EmptyMessages] = Future.successful(Messages.empty())

  private val tabTitles: Messages[ResourceBundleMessages] = ResourceBundleMessages.utf8("tabTitles", getClass.getClassLoader)
  private val requestInfo = RequestInfoBuilder.local

  private def buildTabs[M <: Messages[M]](name: String = "myTab", tabTitles: Messages[ResourceBundleMessages] = tabTitles, persisted: Boolean = true) = {
    if (persisted) {
      Tabs.persistedTabs(name, Future.successful(tabTitles), Seq(buildWidget("somethingToTest")))
    } else {
      Tabs.tabs(name, Future.successful(tabTitles), Seq(buildWidget("somethingToTest")))
    }
  }

  override def tests: Tests = Tests {

    "should pass all fixture scenarios" - {
      testFixtures { fixture =>
        // We have to add our own hidden input field to persist
        val removeHiddenInputs = new NodeFilter {
          override def head(node: Node, depth: Int): FilterResult = {
            if (node.nodeName() == "input" && Option(node.attr("type")).contains("hidden")) FilterResult.REMOVE
            else FilterResult.CONTINUE
          }
        }
        runScenario(fixture) { scenario =>
          val result = HtmlUtils.diff(scenario.actual.filter(removeHiddenInputs), scenario.expected, (HtmlUtils.standardAttributesToIgnore :+ "href"): _*)
          assert(result)
        }
      }
    }

    "should set its id attribute" - assertId(
      Tabs.tabs("", Future.successful(Messages.empty()), Seq.empty)
    )
    "should set the visibility class when hidden" -
      assertVisibilityClass(
        Tabs.tabs("btn", Future.successful(Messages.empty()), Seq.empty, Tabs.classes("my-custom-class"), alwaysHidden),
        Set("govuk-tabs", "my-custom-class")
      )

    "should set attributes" - {
      assertAttributes(
        Tabs.tabs("btn", Future.successful(Messages.empty()), Seq.empty, Tabs.attributes(buildAttribute("data-custom", "true"))),
        Map("data-custom" -> "true")
      )
    }


    "tabContentToTemplateArguments" - {

      val widget = buildTabs("rw")
      val formDefinition = formDef(widget)
      val countryWidgets = Seq(buildWidget("OutputEngland"), buildWidget("OutputScotland"),
        buildWidget("OutputWales"), buildWidget("OutputNorthernIreland"))
      val items = widget.tabContentToTemplateArguments(tabTitles, countryWidgets, form(formDefinition), Seq.empty, requestInfo)

      "should set a valid values for label and html" - {

        items.map { values =>
          val expectedItems = Seq("England", "Scotland", "Wales", "Northern Ireland")
          val actualItems = values._2.map(args => args("label").toString)
          val actualItemsBody = values._2.map(args => args("html").toString)
          assert(expectedItems == actualItems)
          val expectedBody = Seq("OutputEngland", "OutputScotland", "OutputWales", "OutputNorthernIreland")
          assert(expectedBody == actualItemsBody)
        }
      }

      "should set a first value should be selected" - {
        widget.tabContentToTemplateArguments(tabTitles, countryWidgets,
          form(formDefinition), Seq.empty, requestInfo)
        items.map { values =>
          val firstItem = values._2.head("selected").asInstanceOf[Boolean]
          assert(firstItem)
          val otherItems = values._2.tail.map(items => items("selected").asInstanceOf[Boolean])
          assert(otherItems.forall(_ == false))
        }
      }
    }

    "templateArgs" - {

      "should set a valid id for the widget" - {
        evalTemplateArgs(buildTabs()).map { args =>
          WidgetScenarios.assertAutoIdFormat(args("tabs.id").toString)
        }
      }

      "should set the value from formData if the widget is persisted" - {
        val tabs = buildTabs()
        val formWithData = form(FormData(Json.obj(tabs.name -> 2)), formDef(tabs))
        tabs.templateArgs(formWithData, Seq.empty, requestInfo).map { args =>
          val actual = args("tabs.value")
          assert(actual == 2)
        }
      }

      "should set a value of 0 if the widget is not persisted" - {
        val tabs = buildTabs(persisted = false)
        val formWithData = form(FormData(Json.obj(tabs.name -> 3)), formDef(tabs))
        tabs.templateArgs(formWithData, Seq.empty, requestInfo).map { args =>
          val actual = args("tabs.value")
          assert(actual == 0)
        }
      }

      "should add the hidden class to other classes passed in" - {
        val hiddenInput = Tabs.tabs("hiddenTab", Future.successful(tabTitles), Seq(buildWidget("somethingToTest")),
          SimpleExtraTemplateRenderArgs("tabs.classes" -> "custom-class another-custom-class"), AlwaysHidden)

        evalTemplateArgs(hiddenInput).map { args =>
          val actual = WidgetScenarios.parseClassProperty(args("tabs.classes"))
          val expected = Set("custom-class", "another-custom-class", TemplateHelper.hiddenCssClassName)
          assert(actual == expected)
        }
      }

      "should add the hidden class to other classes passed in with a namespace" - {
        val widgetName = "hiddenTab"
        val hiddenInput = Tabs.tabs(widgetName, Future.successful(tabTitles), Seq(buildWidget("somethingToTest")),
          SimpleExtraTemplateRenderArgs(s"$widgetName.tabs.classes" -> "custom-class another-custom-class"), AlwaysHidden)

        evalTemplateArgs(hiddenInput).map { args =>
          val actual = WidgetScenarios.parseClassProperty(args("tabs.classes"))
          val expected = Set("custom-class", "another-custom-class", TemplateHelper.hiddenCssClassName)
          assert(actual == expected)
        }
      }

      "should add a hidden class if this widget should be hidden" - {
        val hiddenTabs = Tabs.tabs("hiddenTab", Future.successful(tabTitles), Seq(buildWidget("somethingToTest")),
          visibility = AlwaysHidden)
        hiddenTabs.templateArgs(form(formDef(hiddenTabs)), Seq.empty, RequestInfoBuilder.local).map(args => {
          WidgetScenarios.assertHiddenClass(args("tabs.classes"))
        })
      }
    }

    "visibility" - {
      WidgetScenarios.assertVisibility(v =>
        Tabs.tabs("n", Future.successful(tabTitles), Seq(buildWidget("SomeWidget")),
          visibility = v)
      )
    }

    "toJson" - {
      "should fail gracefully if the submitted value is not an index value" - {
        val tabs = buildTabs()
        val actual = tabs.toJson(Map(tabs.name -> Seq("whoops")))
        assert(actual == Json.obj())
      }

      "should return an empty object if the widget is not configured to persist its state" - {
        val unpersistedTabs = buildTabs(persisted = false)
        val actual = unpersistedTabs.toJson(Map(unpersistedTabs.name -> Seq("3")))
        assert(actual == Json.obj())
      }

      "should save the index as an int" - {
        val tabs = buildTabs()
        val actual = tabs.toJson(Map(tabs.name -> Seq("2")))
        val expected = Json.obj(tabs.name -> 2)
        assert(actual == expected)
      }
    }
  }

  def evalTemplateArgs[M <: Messages[M], G <: Messages[G]](i: Tabs[M, G], errors: ValidationErrors = Seq.empty, requestInfo: RequestInfo = RequestInfoBuilder.local): Future[Map[String, Any]] = {
    i.templateArgs(form(formDef(i)), errors, requestInfo)
  }

  private def buildWidget(output: String): Widget = {
    new Widget {
      override def render(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit executionContext: ExecutionContext): Future[String] = {
        Future.successful(output)
      }
    }
  }


  override val excludedFixtureNames: Set[String] = Set(
    "item with attributes", // TODO: Support easily setting attributes on items
    "panel with attributes", // TODO: Support easily setting attributes on panels
    "html as text" // Doesn't make sense to escape widget contents in tabs
  )

  override def widgetFromFixture(fixture: Fixture): Tabs[_, _] = {
    import com.uxforms.dsl.helpers.MessageHelper._
    implicit val locale = Locale.UK
    implicit val renderer = TemplateRendererBuilder.local

    val args = flattenExtraTemplateRenderArgs(
      fixture.option[String]("classes").map(Tabs.classes),
      fixture.option[String]("title").map(t => SimpleExtraTemplateRenderArgs("tabs.title" -> t)),
      optionAttributes(fixture, Tabs.attributes)
    )

    val messages: ListMessages = fixture.option[Seq[JsValue]]("items").map(items => {
      new ListMessages(
        items.flatMap { item =>
          (item \ "label").asOpt[String].map(s => ListMessageEntry("fixtureTab.label", s))
        },
        locale
      )
    }).getOrElse(Seq.empty[(String, String)])

    val tabContents = fixture.option[Seq[JsValue]]("items").map(_.flatMap { item =>
      (item \ "panel" \ "text").asOpt[String].map(s => Content(s"""<p class="govuk-body">$s</p>"""))
        .orElse((item \ "panel" \ "html").asOpt[String].map(Content.apply))
    }).getOrElse(Seq.empty)

    Tabs.tabs("fixtureTab", Future.successful(messages), tabContents, args)
  }

  override val componentName: String = "tabs"
}
