package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain.{Messages, SimpleExtraTemplateRenderArgs}
import com.uxforms.govukdesignsystem.dsl.test.{Fixture, HtmlUtils, TemplateRendererBuilder, WidgetFixtureTest}
import utest._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object TagTest extends TestSuite with WidgetFixtureTest[Content[_, _]] {

  override def tests: Tests = Tests {

    "should pass all fixture scenarios" - {
      testFixtures { fixture =>
        runScenario(fixture) { scenario =>
          val result = HtmlUtils.diff(scenario.actual, scenario.expected, HtmlUtils.standardAttributesToIgnore: _*)
          assert(result)
        }
      }
    }

  }

  override def widgetFromFixture(fixture: Fixture): Content[_, _] = {
    implicit val templateRenderer = TemplateRendererBuilder.local
    implicit val formLevelMessages = Future.successful(Messages.empty())

    val args = flattenExtraTemplateRenderArgs(
      fixture.option[String]("text").map(t => SimpleExtraTemplateRenderArgs("tag.text" -> t)),
      fixture.option[String]("html").map(t => SimpleExtraTemplateRenderArgs("tag.html" -> t)),
      fixture.option[String]("classes").map(Tag.classes),
      optionAttributes(fixture, Tag.attributes)
    )

    Tag.tag("name", Future.successful(Messages.empty()), args)
  }

  override val componentName: String = "tag"
}
