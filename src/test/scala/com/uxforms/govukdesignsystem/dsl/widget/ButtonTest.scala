package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain.{EmptyMessages, Messages, NoExtraRenderArgs, SimpleExtraTemplateRenderArgs}
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.template.TemplateHelper.buildAttribute
import com.uxforms.govukdesignsystem.dsl.test.FormBuilder._
import com.uxforms.govukdesignsystem.dsl.test.FormDefinitionBuilder._
import com.uxforms.govukdesignsystem.dsl.test.HtmlUtils.renderAndParseHtml
import com.uxforms.govukdesignsystem.dsl.test.{Fixture, HtmlUtils, TemplateRendererBuilder, WidgetFixtureTest}
import com.uxforms.govukdesignsystem.dsl.widget.HtmlAssertions.{alwaysHidden, assertAttributes, assertId, assertVisibilityClass}
import utest._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object ButtonTest extends TestSuite with WidgetFixtureTest[Content[_, _]] {

  override val componentName: String = "button"

  private implicit val formLevelMessages: Future[EmptyMessages] = Future.successful(Messages.empty())
  private implicit val renderer: TemplateRenderer = TemplateRendererBuilder.local

  override def tests: Tests = Tests {

    "should set its id attribute" - assertId(
      Button.button("btn", Future.successful(Messages.empty()))
    )
    "should set the visibility class when hidden" -
      assertVisibilityClass(
        Button.button("btn", Future.successful(Messages.empty()), Button.classes("my-custom-class"), alwaysHidden),
        Set("govuk-button", "my-custom-class")
      )

    "should set attributes" - {
      assertAttributes(
        Button.button("btn", Future.successful(Messages.empty()), Button.attributes(buildAttribute("data-custom", "true"))),
        Map("data-custom" -> "true")
      )
    }

    "input" - {
      val button = Button.input("buttonInput", Future.successful(Messages.empty))
      "should set the button's name" - {
        renderAndParseHtml(form(formDef(button)), Seq.empty, button).map { e =>
          assert(e.attr("name") == "buttonInput")
        }
      }
    }

    "button" - {
      val button = Button.button("buttonName", Future.successful(Messages.empty))
      "should set the button's name" - {
        renderAndParseHtml(form(formDef(button)), Seq.empty, button).map { e =>
          assert(e.attr("name") == "buttonName")
        }
      }
    }

    "should pass all fixture scenarios" - {
      testFixtures { fixture =>
        runScenario(fixture) { scenario =>
          val result = HtmlUtils.diff(scenario, Seq("id", "name"))
          assert(result)
        }
      }
    }
  }

  def widgetFromFixture(fixture: Fixture): Content[EmptyMessages, EmptyMessages] = {
    val isStartButton = fixture.options.get("isStartButton").exists(_.as[Boolean])
    val isInput = fixture.options.get("element").exists(_.as[String] == "input")
    val isLink = fixture.options.contains("href") || fixture.options.get("element").exists(_.as[String] == "a")
    val args = flattenExtraTemplateRenderArgs(
      fixture.options.get("text").map(text => SimpleExtraTemplateRenderArgs("button.text" -> text.as[String])),
      fixture.options.get("html").map(html => SimpleExtraTemplateRenderArgs("button.html" -> html.as[String])),
      fixture.options.get("disabled").map(disabled => if (disabled.as[Boolean]) Button.disabled else NoExtraRenderArgs),
      fixture.options.get("href").map(href => SimpleExtraTemplateRenderArgs(Button.href(href.as[String]))),
      fixture.options.get("name").map(name => SimpleExtraTemplateRenderArgs("button.name" -> name.as[String])),
      fixture.options.get("preventDoubleClick").map(p => Button.preventDoubleClick(p.as[Boolean])),
      fixture.options.get("classes").map(classes => Button.classes(classes.as[String])),
      optionAttributes(fixture, Button.attributes),
      fixture.options.get("type").map(t => SimpleExtraTemplateRenderArgs("button.type" -> t.as[String])),
      fixture.options.get("value").map(v => Button.withValue(v.as[String]))
    )
    if (isStartButton) {
      if (isLink) {
        Button.link("btn", Future.successful(Messages.empty()), Button.asStart ++ args)
      } else {
        Button.button("btn", Future.successful(Messages.empty()), Button.asStart ++ args)
      }
    } else if (isInput) {
      Button.input("btn", Future.successful(Messages.empty()), args)
    } else if (isLink) {
      Button.link("btn", Future.successful(Messages.empty()), args)
    } else {
      Button.button("btn", Future.successful(Messages.empty()), args)
    }
  }
}
