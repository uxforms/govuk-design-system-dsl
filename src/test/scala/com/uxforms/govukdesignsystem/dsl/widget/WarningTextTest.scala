package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain.Messages
import com.uxforms.govukdesignsystem.dsl.test.{Fixture, HtmlUtils, TemplateRendererBuilder, WidgetFixtureTest}
import utest._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object WarningTextTest extends TestSuite with WidgetFixtureTest[Content[_, _]] {

  override def tests: Tests = Tests {

    "should pass all fixture scenarios" - {
      testFixtures { fixture =>
        runScenario(fixture) { scenario =>
          val result = HtmlUtils.diff(scenario.actual, scenario.expected, HtmlUtils.standardAttributesToIgnore: _*)
          assert(result)
        }
      }
    }
  }


  override val excludedFixtureNames: Set[String] = Set(
    "html" // We don't need to exclusively write html _or_ text
  )

  override def widgetFromFixture(fixture: Fixture): Content[_, _] = {
    implicit val templateRenderer = TemplateRendererBuilder.local
    implicit val formLevelMessages = Future.successful(Messages.empty())

    val args = flattenExtraTemplateRenderArgs(
      fixture.optionArg[String]("text", "warningText.text"),
      fixture.optionArg[String]("html", "warningText.html"),
      fixture.optionArg[String]("iconFallbackText", "warningText.iconFallbackText"),
      optionAttributes(fixture, WarningText.attributes),
      fixture.option[String]("classes").map(WarningText.classes)
    )

    WarningText.warningText("wt", Future.successful(Messages.empty()), args)
  }

  override val componentName: String = "warning-text"
}
