package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain._
import com.uxforms.dsl.helpers.MessageHelper._
import com.uxforms.govukdesignsystem.dsl.{AlwaysHidden, TemplateRenderer}
import com.uxforms.govukdesignsystem.dsl.constraint.ConstraintHelper.noConstraints
import com.uxforms.govukdesignsystem.dsl.template.TemplateHelper.buildAttribute
import com.uxforms.govukdesignsystem.dsl.test.FormBuilder._
import com.uxforms.govukdesignsystem.dsl.test.FormDefinitionBuilder._
import com.uxforms.govukdesignsystem.dsl.test.{Fixture, HtmlUtils, RequestInfoBuilder, TemplateRendererBuilder, WidgetFixtureTest}
import com.uxforms.govukdesignsystem.dsl.widget.HtmlAssertions.{alwaysHidden, assertAttributes, assertId, assertVisibilityClass}
import org.jsoup.nodes.Element
import play.api.libs.json._
import utest._

import java.time.LocalDate
import java.util.Locale
import scala.compat.java8.FunctionConverters.asJavaConsumer
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object DateInputTest extends TestSuite with WidgetFixtureTest[DateInput[_, _]] {

  private implicit val renderer: TemplateRenderer = TemplateRendererBuilder.local
  private implicit val formLevelMessages: Future[EmptyMessages] = Future.successful(Messages.empty())
  private implicit val locale: Locale = Locale.UK

  private val dateWidget = DateInput.date("d", Future.successful(Messages.empty()), noConstraints)

  override val componentName: String = "date-input"


  override val excludedFixtureNames: Set[String] = Set(
    "with errors only", // We only support 1 validation error per field, where this has a single error for three fields.
    "with errors and hint", // We only support 1 validation error per field, where this has a single error for three fields.
    "with id on items", // We don't really support this. UX Forms needs to manage element ids to manage widget visibility
    "custom pattern", // We don't support only rendering some of the 3 input fields
    "custom inputmode", // We don't support only rendering some of the 3 input fields
    "with nested name", // Automatically setting label values inside the template makes no sense for us
    "suffixed id", // UX Forms needs to manage ids
    "with error and describedBy", // UX Forms needs to be in control of element ids
    "items without classes" // We always add classes
  )


  override val individuallyTestedFixtureNames: Set[String] = Set(
    "with values",
    "items with classes"
  )

  def widgetName(fixture: Fixture) = {
    fixture.option[String]("namePrefix").map(p => s"${p}-").getOrElse("di")
  }

  override def validationErrorsFromFixture(fixture: Fixture): ValidationErrors = {
    val widget = widgetFromFixture(fixture)
    fixture.option[JsValue]("errorMessage").flatMap(msg =>
      fixture.option[Seq[JsValue]]("items").map {
        case items if items.exists(i => (i \ "name").asOpt[String].contains("day") && (i \ "classes").asOpt[String].exists(_.contains("govuk-input--error"))) => fixture.errorForField(widget, widget.dayField, msg)
        case items if items.exists(i => (i \ "name").asOpt[String].contains("month") && (i \ "classes").asOpt[String].exists(_.contains("govuk-input--error"))) => fixture.errorForField(widget, widget.monthField, msg)
        case items if items.exists(i => (i \ "name").asOpt[String].contains("year") && (i \ "classes").asOpt[String].exists(_.contains("govuk-input--error"))) => fixture.errorForField(widget, widget.yearField, msg)
        case _ => Seq.empty
      }

    ).getOrElse(Seq.empty)
  }


  override def widgetFromFixture(fixture: Fixture): DateInput[_, _] = {
    val name = widgetName(fixture)
    implicit val formLevelMessages: Future[Messages[ListMessages]] = Future.successful(Seq(
      "day.label.text" -> "Day",
      "month.label.text" -> "Month",
      "year.label.text" -> "Year"
    ))

    def fieldOpts(field: String) = {
      fixture
        .option[Seq[JsValue]]("items")
        .flatMap(
          _.find(i => (i \ "name").asOpt[String].contains(field))
            .map(fieldOptions =>
              flattenExtraTemplateRenderArgs(
                (fieldOptions \ "autocomplete").asOpt[String].map(a => SimpleExtraTemplateRenderArgs(s"$name.$field.input.attributes" -> Seq(buildAttribute("autocomplete", a)))),
                (fieldOptions \ "pattern").asOpt[String].map(a => SimpleExtraTemplateRenderArgs(s"$name.$field.input.attributes" -> Seq(buildAttribute("pattern", a)))),
                (fieldOptions \ "attributes").asOpt[Map[String, String]].map(o => SimpleExtraTemplateRenderArgs(s"$name.$field.input.attributes" -> o.map { case (n, v) => buildAttribute(n, v) }.toSeq)),
                (fieldOptions \ "value").asOpt[JsValue].map {
                  case JsString(s) => SimpleExtraTemplateRenderArgs(s"$name.$field.input.value" -> s)
                  case JsNumber(n) => SimpleExtraTemplateRenderArgs(s"$name.$field.input.value" -> n)
                },
                (fieldOptions \ "classes").asOpt[String].map(c => SimpleExtraTemplateRenderArgs(s"$name.$field.input.classes" -> c))
              )
            )
        )
    }

    val args = flattenExtraTemplateRenderArgs(
      fixture.option[JsValue]("hint").flatMap(j => (j \ "text").asOpt[String].map(text => SimpleExtraTemplateRenderArgs(s"$name.hint.text" -> text))),
      fixture.option[JsValue]("hint").flatMap(j => (j \ "html").asOpt[String].map(html => SimpleExtraTemplateRenderArgs(s"$name.hint.html" -> html))),
      fixture.option[JsValue]("fieldset").flatMap(j => (j \ "legend" \ "text").asOpt[String]).map(text => SimpleExtraTemplateRenderArgs(s"$name.dateInput.legend.text" -> text)),
      fixture.option[JsValue]("fieldset").flatMap(j => (j \ "legend" \ "html").asOpt[String]).map(html => SimpleExtraTemplateRenderArgs(s"$name.dateInput.legend.html" -> html)),
      fixture.option[JsValue]("formGroup").flatMap(j => (j \ "classes").asOpt[String].map(c => DateInput.formGroupClasses(c))),
      fieldOpts("day"),
      fieldOpts("month"),
      fieldOpts("year"),
      fixture.option[String]("classes").map(DateInput.classes),
      optionAttributes(fixture, DateInput.attributes)
    )
    DateInput.date(name, Future.successful(Messages.empty()), noConstraints, args)
  }


  override def tests: Tests = Tests {

    "fixture scenarios" - {

      "with values" - {
        val fixture = individuallyTestedFixtures.find(_.name == "with values").get
        runScenario(fixture) { scenario =>
          def yearValue(e: Element, name: String) = e.selectFirst(s"input[name='$name']").`val`

          val actualYear = yearValue(scenario.actual, s"${scenario.widget.name}.year")
          val expectedYear = yearValue(scenario.expected, "year")

          assert(actualYear == expectedYear)

        }
      }

      "items with classes" - {
        val fixture = individuallyTestedFixtures.find(_.name == "items with classes").get
        runScenario(fixture) { scenario =>
          def inputClasses(e: Element, name: String) = e.selectFirst(s"input[name='$name']").className()

          def expectedClass(name: String) = fixture.option[Seq[JsValue]]("items").flatMap(_.collectFirst { case (i) if (i \ "name").asOpt[String].contains(name) => (i \ "classes").asOpt[String] }).flatten.get

          assert(inputClasses(scenario.actual, s"${widgetName(fixture)}.day").contains(expectedClass("day")))
        }
      }

      "all" - {
        testFixtures { fixture =>

          /**
           * Fixtures assume input fields have bare 'name's, e.g. "day" and "month". But that falls apart when
           * there are multiple date widgets on the same page. So we automatically prefix the fields with the widget's name.
           */
          def bodgeElementNames(element: Element): Element = {
            val wn = widgetName(fixture)
            val prefix = fixture.option[String]("namePrefix").map(p => s"${p}-").getOrElse("")
            element.select("input").forEach(asJavaConsumer[Element](e => {
              if (e.attr("name").startsWith(wn)) {
                e.attr("name", prefix + e.attr("name").substring(wn.length + 1))
              }
            }))
            element
          }

          runScenario(fixture) { scenario =>

            val diffs = HtmlUtils.diff(scenario.expected, bodgeElementNames(scenario.actual), HtmlUtils.standardAttributesToIgnore: _*)
            assert(diffs)

          }
        }
      }
    }


    "toJson" - {
      "should write as many values as present" - {
        val validPairs = Seq(
          dateWidget.toJson(Map(dateWidget.dayField -> Seq("6"))) -> Json.obj("day" -> 6, "month" -> JsNull, "year" -> JsNull),
          dateWidget.toJson(Map(dateWidget.monthField -> Seq("2"))) -> Json.obj("day" -> JsNull, "month" -> 2, "year" -> JsNull),
          dateWidget.toJson(Map(dateWidget.yearField -> Seq("2020"))) -> Json.obj("day" -> JsNull, "month" -> JsNull, "year" -> 2020),
          dateWidget.toJson(Map(dateWidget.dayField -> Seq("6"), dateWidget.monthField -> Seq("2"), dateWidget.yearField -> Seq("2020"))) ->
            Json.obj("day" -> 6, "month" -> 2, "year" -> 2020)
        )
        validPairs.foreach { case (actual, expected) =>
          assert((actual \ dateWidget.name).get == expected)
        }
      }

      "should return an empty object if none of its fields were posted" - {
        val actual = dateWidget.toJson(Map("somethingElse" -> Seq("a value")))
        val expected = Json.obj()
        assert(actual == expected)
      }

      "should return an empty object if none of its fields' posted values contained anything" - {
        val actual = dateWidget.toJson(Map(dateWidget.dayField -> Seq.empty, dateWidget.monthField -> Seq.empty, dateWidget.yearField -> Seq.empty))
        val expected = Json.obj()
        assert(actual == expected)
      }

      "should return an empty object if its fields' posted values contain empty strings" - {
        val actual = dateWidget.toJson(Map(dateWidget.dayField -> Seq(""), dateWidget.monthField -> Seq(""), dateWidget.yearField -> Seq("")))
        val expected = Json.obj()
        assert(actual == expected)
      }
    }

    "templateArgs" - {
      "should set a valid id for the surrounding formGroup" - {
        evalTemplateArgs(dateWidget).map { args =>
          WidgetScenarios.assertAutoId(args("dateInput.formGroup.attributes"))
        }
      }

      "should set properties if a legend is present" - {
        evalTemplateArgs(DateInput.date("d", Future.successful(messages(Seq("d.dateInput.legend.text" -> "my legend"))), noConstraints.toSeq)).map { args =>
          val fieldset = args("dateInput.fieldset")
          val hasLegend = args("dateInput.fieldset.hasLegend")
          assert(fieldset == hasLegend)
        }
      }
    }

    "buildDateFields" - {

      "should set values from FormData for each of the individual date fields" - {
        val dateFields = dateWidget.buildDateFields(
          FormData(Json.obj(dateWidget.name -> Map("day" -> 1, "month" -> 2, "year" -> 2020))),
          Map.empty,
          Seq.empty
        )

        for {
          dayArgs <- InputTest.evalTemplateArgs(dateFields.head)
          monthArgs <- InputTest.evalTemplateArgs(dateFields(1))
          yearArgs <- InputTest.evalTemplateArgs(dateFields(2))
        } yield {
          val dayValue = dayArgs("input.value")
          assert(dayValue == "1")

          val monthValue = monthArgs("input.value")
          assert(monthValue == "2")

          val yearValue = yearArgs("input.value")
          assert(yearValue == "2020")
        }
      }

      "should set date and width classes for each of the individual date fields" - {
        val dateFields = dateWidget.buildDateFields(FormData(), Map.empty, Seq.empty)
        for {
          dayArgs <- InputTest.evalTemplateArgs(dateFields.head)
          monthArgs <- InputTest.evalTemplateArgs(dateFields(1))
          yearArgs <- InputTest.evalTemplateArgs(dateFields(2))
        } yield {
          val dayClasses = dayArgs("input.classes")
          assert(dayClasses == "govuk-date-input__input govuk-input--width-2")

          val monthClasses = monthArgs("input.classes")
          assert(monthClasses == "govuk-date-input__input govuk-input--width-2")

          val yearClasses = yearArgs("input.classes")
          assert(yearClasses == "govuk-date-input__input govuk-input--width-4")
        }
      }

      "should set widget names that end with their date field names so they can pick up the correct messages entries, and post their values with the expected names" - {
        val dateFields = dateWidget.buildDateFields(FormData(), Map.empty, Seq.empty)
        assert(dateFields.head.name == s"${dateWidget.name}.day")
        assert(dateFields(1).name == s"${dateWidget.name}.month")
        assert(dateFields(2).name == s"${dateWidget.name}.year")
      }

      "should set the id on the first input field to be the elementId of the enclosing date widget, so ErrorSummary can link directly to it" - {
        val dateFields = dateWidget.buildDateFields(FormData(), Map.empty, Seq.empty)
        val fd = formDef(dateWidget)
        for {
          dayArgs <- dateFields.head.templateArgs(form(fd), Seq.empty, RequestInfoBuilder.local)
          monthArgs <- InputTest.evalTemplateArgs(dateFields(1))
          yearArgs <- InputTest.evalTemplateArgs(dateFields(2))
        } yield {
          val dateWidgetId = HtmlIdGenerator.elementId(fd, dateWidget)
          WidgetScenarios.assertElementId(dayArgs("input.id"))
          assert(dayArgs("input.id") == dateWidgetId)
          WidgetScenarios.assertRandomIdValue(monthArgs("input.id").toString)
          assert(monthArgs("input.id") != dateWidgetId)
          WidgetScenarios.assertRandomIdValue(yearArgs("input.id").toString)
          assert(yearArgs("input.id") != dateWidgetId)
        }
      }

    }

    "visibility" - {
      WidgetScenarios.assertVisibility(v => DateInput.date("n", Future.successful(Messages.empty()), noConstraints.toSeq, NoExtraRenderArgs, v))
    }

    "dateOfBirth" - {
      val dob = DateInput.dateOfBirth("dob", Future.successful(Messages.empty()), Seq.empty)

      "should set autocomplete attribute values on the three input fields" - {
        val dateFields = dob.buildDateFields(FormData(), Map.empty, Seq.empty)
        for {
          dayArgs <- InputTest.evalTemplateArgs(dateFields.head)
          monthArgs <- InputTest.evalTemplateArgs(dateFields(1))
          yearArgs <- InputTest.evalTemplateArgs(dateFields(2))
        } yield {
          val dayAuto = dayArgs("input.autocomplete")
          assert(dayAuto == "bday-day")

          val monthAuto = monthArgs("input.autocomplete")
          assert(monthAuto == "bday-month")

          val yearAuto = yearArgs("input.autocomplete")
          assert(yearAuto == "bday-year")
        }
      }
    }

    "extract" - {
      "should return None when there is no value in FormData" - {
        val result = dateWidget.extract(FormData())
        assert(result.isEmpty)
      }

      "should return None when not all fields are set" - {
        val result = dateWidget.extract(FormData(Json.obj(dateWidget.name -> Json.obj("day" -> 2))))
        assert(result.isEmpty)
      }

      "should return a LocalDate if all fields have viable values" - {
        val result = dateWidget.extract(FormData(Json.obj(dateWidget.name -> Json.obj("day" -> 1, "month" -> 2, "year" -> 2020))))
        val expected = Some(LocalDate.of(2020, 2, 1))
        assert(result == expected)
      }

      "should return None if fields have viable values but the widget is hidden" - {
        val hiddenWidget = DateInput.date("d", Future.successful(Messages.empty()), noConstraints, NoExtraRenderArgs, AlwaysHidden)
        val result = hiddenWidget.extract(FormData(Json.obj(dateWidget.name -> Json.obj("day" -> 1, "month" -> 2, "year" -> 2020))))
        assert(result.isEmpty)
      }
    }

    "should set its id attribute" - assertId(
      DateInput.date("d", Future.successful(Messages.empty()), noConstraints)
    )
    "should set the visibility class when hidden" -
      assertVisibilityClass(
        DateInput.date("d", Future.successful(Messages.empty()), noConstraints, DateInput.formGroupClasses("my-custom-class"), alwaysHidden),
        Set("govuk-form-group", "my-custom-class")
      )
    "should set attributes" - {
      assertAttributes(
        DateInput.date("d", Future.successful(Messages.empty()), noConstraints, SimpleExtraTemplateRenderArgs("dateInput.formGroup.attributes" -> buildAttribute("data-custom", "true"))),
        Map("data-custom" -> "true")
      )
    }

  }

  private def evalTemplateArgs[M <: Messages[M], F <: Messages[F]](d: DateInput[M, F], errors: ValidationErrors = Seq.empty, requestInfo: RequestInfo = RequestInfoBuilder.local) = {
    d.templateArgs(form(formDef(d)), errors, requestInfo)
  }

}
