package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain.constraint.ValidationError
import com.uxforms.domain.{ListMessages, SimpleExtraTemplateRenderArgs}
import com.uxforms.dsl.containers.Section
import com.uxforms.dsl.helpers.MessageHelper._
import com.uxforms.govukdesignsystem.dsl.test.{FormBuilder, FormDefinitionBuilder, RequestInfoBuilder}
import com.uxforms.govukdesignsystem.dsl.widget.MessageContent._
import utest._

import java.util.Locale
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object MessageContentTest extends TestSuite {

  private implicit val locale: Locale = Locale.UK
  private val formDef = FormDefinitionBuilder.formDef(Seq.empty[Section]: _*)
  private val form = FormBuilder.form(formDef)
  private val noErrors = Seq.empty[ValidationError]

  override def tests: Tests = Tests {

    val messages: Future[ListMessages] = Future.successful("firstKey" -> "firstValue {firstParameter}")

    "render" - {

      "should apply formatting arguments" - {
        messageContent(messages, "firstKey", Map("firstParameter" -> "hello")).render(form, noErrors, RequestInfoBuilder.local).map { result =>
          val expected = "firstValue hello"
          assert(result == expected)
        }
      }

      "extraTemplateRenderArgs should take precedence over templateArgs" - {
        val messagesArgs = Map("firstParameter" -> "fromMessagesArgs")
        val extraArgs = SimpleExtraTemplateRenderArgs("firstParameter" -> "fromExtraArgs")

        messageContent(messages, "firstKey", messagesArgs, extraArgs).render(form, noErrors, RequestInfoBuilder.local).map { result =>
          val expected = "firstValue fromExtraArgs"
          assert(result == expected)
        }
      }

      "should render an empty string if the key is not found" - {
        messageContent(messages, "whoops").render(form, noErrors, RequestInfoBuilder.local).map { result =>
          val expected = ""
          assert(result == expected)
        }
      }
    }

  }
}
