package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain.{Messages, SimpleExtraTemplateRenderArgs}
import com.uxforms.govukdesignsystem.dsl.test.{Fixture, HtmlUtils, TemplateRendererBuilder, WidgetFixtureTest}
import utest._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object PanelTest extends TestSuite with WidgetFixtureTest[Content[_, _]] {

  override def tests: Tests = Tests {

    "should pass all fixture scenarios" - {
      testFixtures { fixture =>
        runScenario(fixture) { scenario =>
          val result = HtmlUtils.diff(scenario.actual, scenario.expected, HtmlUtils.standardAttributesToIgnore: _*)
          assert(result)
        }
      }
    }
  }

  override def widgetFromFixture(fixture: Fixture): Content[_, _] = {
    implicit val templateRenderer = TemplateRendererBuilder.local
    implicit val formLevelMessages = Future.successful(Messages.empty())

    val args = flattenExtraTemplateRenderArgs(
      fixture.option[String]("titleHtml").map(t => SimpleExtraTemplateRenderArgs("panel.titleHtml" -> t)),
      fixture.option[String]("titleText").map(t => SimpleExtraTemplateRenderArgs("panel.titleText" -> t)),
      fixture.option[String]("text").map(t => SimpleExtraTemplateRenderArgs("panel.text" -> t)),
      fixture.option[String]("html").map(t => SimpleExtraTemplateRenderArgs("panel.html" -> t)),
      fixture.option[Int]("headingLevel").map(l => SimpleExtraTemplateRenderArgs("panel.headingLevel" -> l)),
      fixture.option[String]("classes").map(Panel.classes),
      optionAttributes(fixture, Panel.attributes)
    )
    Panel.panel("name", Future.successful(Messages.empty()), args)
  }

  override val componentName: String = "panel"
}
