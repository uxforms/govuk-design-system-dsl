package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain.constraint.FileUploadWithAllFormDataConstraint.noFileUploadWithAllFormDataConstraints
import com.uxforms.domain.constraint.{Constraint, ValidationError, WidgetValidationError}
import com.uxforms.domain._
import com.uxforms.dsl.widgets.FileUploadMetadata
import com.uxforms.govukdesignsystem.dsl.constraint.ConstraintHelper.noConstraints
import com.uxforms.govukdesignsystem.dsl.template.Label
import com.uxforms.govukdesignsystem.dsl.template.TemplateHelper.buildAttribute
import com.uxforms.govukdesignsystem.dsl.test.FormBuilder.form
import com.uxforms.govukdesignsystem.dsl.test.FormDefinitionBuilder.formDef
import com.uxforms.govukdesignsystem.dsl.test.{Fixture, HtmlUtils, RequestInfoBuilder, TemplateRendererBuilder, WidgetFixtureTest}
import com.uxforms.govukdesignsystem.dsl.widget.HtmlAssertions.{alwaysHidden, assertAttributes, assertId, assertVisibilityClass}
import com.uxforms.govukdesignsystem.dsl.{AlwaysHidden, TemplateRenderer}
import play.api.libs.json.{JsValue, Json}
import utest._

import java.nio.file.Files
import java.util.{Locale, UUID}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object FileUploadTest extends TestSuite with WidgetFixtureTest[FileUpload[_, _]] {

  private implicit val renderer: TemplateRenderer = TemplateRendererBuilder.local
  private implicit val formMessages: Future[Messages[EmptyMessages]] = Future.successful(Messages.empty())

  override val componentName: String = "file-upload"


  override def fixtures: Seq[Fixture] = super.fixtures

  override def validationErrorsFromFixture(fixture: Fixture): ValidationErrors = {
    val widget = widgetFromFixture(fixture)
    fixture.option[JsValue]("errorMessage").fold(Seq.empty[ValidationError])(msg =>
      Seq(WidgetValidationError(widget, widget.name,
        new Constraint {
          override def displayMessage(error: ValidationError): String = (msg \ "text").as[String]
        })
      )
    )

  }

  override def widgetFromFixture(fixture: Fixture): FileUpload[_, _] = {
    val widgetName = fixture.option[String]("name").getOrElse("fu")
    val args = flattenExtraTemplateRenderArgs(
      fixture.option[JsValue]("label").flatMap(j => (j \ "text").asOpt[String].map(l => SimpleExtraTemplateRenderArgs("label.text" -> l))),
      fixture.option[JsValue]("label").flatMap(j => (j \ "isPageHeading").asOpt[Boolean].map(l => if (l) Label.asPageHeading else NoExtraRenderArgs)),
      fixture.option[JsValue]("hint").flatMap(j => (j \ "text").asOpt[String].map(l => SimpleExtraTemplateRenderArgs("hint.text" -> l))),
      fixture.option[JsValue]("hint").flatMap(j => (j \ "html").asOpt[String].map(l => SimpleExtraTemplateRenderArgs("hint.html" -> l))),
      fixture.option[JsValue]("formGroup").flatMap(j => (j \ "classes").asOpt[String].map(FileUpload.formGroupClasses)),
      fixture.option[String]("classes").map(FileUpload.classes),
      optionAttributes(fixture, FileUpload.attributes),
      fixture.optionArg[String]("value", "fileUpload.value")
    )
    FileUpload.fileUpload(widgetName, Future.successful(Messages.empty()), noConstraints, noFileUploadWithAllFormDataConstraints, None, args)
  }

  override def tests: Tests = Tests {

    "should pass all fixture scenarios" - {
      testFixtures { fixture =>
        runScenario(fixture) { scenario =>
          val result = HtmlUtils.diff(scenario, HtmlUtils.standardAttributesToIgnore)
          assert(result)
        }
      }
    }

    "should set its id attribute" - assertId(
      FileUpload.fileUpload("f", Future.successful(Messages.empty()), noConstraints, noFileUploadWithAllFormDataConstraints)
    )
    "should set the visibility class when hidden" -
      assertVisibilityClass(
        FileUpload.fileUpload("f", Future.successful(Messages.empty()), noConstraints, noFileUploadWithAllFormDataConstraints, None, FileUpload.formGroupClasses("my-custom-class"), alwaysHidden),
        Set("govuk-form-group", "my-custom-class")
      )

    "should set attributes" - {
      assertAttributes(
        FileUpload.fileUpload("f", Future.successful(Messages.empty()), noConstraints, noFileUploadWithAllFormDataConstraints, None, SimpleExtraTemplateRenderArgs("fileUpload.formGroup.attributes" -> Seq(buildAttribute("data-custom", "true")))),
        Map("data-custom" -> "true")
      )
    }

    "templateArgs" - {

      implicit val uk: Locale = Locale.UK
      val upload = FileUpload.fileUpload("myFileUpload", Future.successful(Messages.empty()), noConstraints, noFileUploadWithAllFormDataConstraints)
      val fd = formDef(upload)
      val f = form(fd)

      "should set a htmlId on the outermost formGroup" - {
        upload.templateArgs(f, Seq.empty, RequestInfoBuilder.local).map { args =>
          WidgetScenarios.assertAutoId(args("fileUpload.formGroup.attributes"))
        }
      }

      "should set an elementId on the file element" - {
        upload.templateArgs(f, Seq.empty, RequestInfoBuilder.local).map { args =>
          WidgetScenarios.assertElementId(args("fileUpload.id").toString)
        }
      }
    }

    "extract" - {
      "should return None when there is no value in FormData" - {
        val result = FileUpload.extract("widgetName", FormData())
        assert(result.isEmpty)
      }

      "should return None when there is a value but the widget is hidden" - {
        val hiddenWidget = FileUpload.fileUpload("fu", Future.successful(Messages.empty()), noConstraints, Set.empty, visibility = AlwaysHidden)
        val result = hiddenWidget.extract(FormData(Json.obj(hiddenWidget.name -> Seq(FileUploadMetadata("name.txt", 23L, "text", UUID.randomUUID())))))
        assert(result.isEmpty)
      }

      "should return the value when present" - {
        val metadata = Seq(FileUploadMetadata("name.txt", 23L, "text", UUID.randomUUID()))
        val upload = FileUpload.fileUpload("fu", Future.successful(Messages.empty()), noConstraints, Set.empty)
        val result = upload.extract(FormData(Json.obj(upload.name -> metadata)))
        assert(result.contains(metadata))
      }
    }

    "validateSubmission" - {
      "should add the newly uploaded file to FormData" - {
        val existingFormData = FormData(Json.obj("otherField" -> "other value"))
        val upload = FileUpload.fileUpload("fu", Future.successful(Messages.empty()), noConstraints, noFileUploadWithAllFormDataConstraints)
        val uploadedFile = Files.createTempFile("FileUploadTest", "tmp").toFile

        upload.validateSubmission(Map(
          "fu-submit" -> Seq.empty,
          "fu" -> Seq("filename", "1024", "text/plain", UUID.randomUUID().toString)
        ), existingFormData, Some(uploadedFile)).map { validationResult =>
          assert((validationResult.formData.textData \ "otherField").as[String] == "other value")
          assert((validationResult.formData.textData \ "fu").as[Seq[FileUploadMetadata]].head.sizeBytes == 1024)
        }
      }
    }

  }
}
