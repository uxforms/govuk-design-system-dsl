package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain._
import com.uxforms.dsl.helpers.MessageHelper.seqToMessages
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.template.TemplateHelper.buildAttribute
import com.uxforms.govukdesignsystem.dsl.test.{Fixture, HtmlUtils, TemplateRendererBuilder, WidgetFixtureTest}
import com.uxforms.govukdesignsystem.dsl.widget.HtmlAssertions.{alwaysHidden, assertAttributes, assertId, assertVisibilityClass}
import utest._

import java.util.Locale
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object BackLinkTest extends TestSuite with WidgetFixtureTest[Content[_, _]] {

  override val componentName: String = "back-link"

  private implicit val locale = Locale.UK
  private implicit val formLevelMessages: Future[ListMessages] = Future.successful(Seq("backLink.text" -> "Back"))
  private implicit val renderer: TemplateRenderer = TemplateRendererBuilder.local

  override def tests: Tests = Tests {

    "backLink" - {

      "should set its id attribute" - assertId(
        BackLink.linkToLatestSection("back", Future.successful(Messages.empty()))
      )
      "should set the visibility class when hidden" -
        assertVisibilityClass(
          BackLink.buttonToPreviousSection("back", Future.successful(Messages.empty()), BackLink.classes("my-custom-class"), alwaysHidden),
          Set("govuk-back-link", "my-custom-class")
        )

      "should set attributes" - {
        assertAttributes(
          BackLink.buttonToPreviousSection("back", Future.successful(Messages.empty()), BackLink.attributes(buildAttribute("data-custom", "true"))),
          Map("data-custom" -> "true")
        )
      }

      "should pass all fixture scenarios" - {
        testFixtures { fixture =>
          runScenario(fixture) { scenario =>
            val result = HtmlUtils.diff(scenario.expected, scenario.actual, "id")
            assert(result)
          }
        }
      }
    }
  }

  def widgetFromFixture(fixture: Fixture): Content[EmptyMessages, ListMessages] = {
    val args = flattenExtraTemplateRenderArgs(
      fixture.options.get("href").map(href => SimpleExtraTemplateRenderArgs(BackLink.href(href.as[String]))),
      fixture.options.get("text").map(text => SimpleExtraTemplateRenderArgs("backLink.text" -> text.as[String])),
      fixture.options.get("html").map(html => SimpleExtraTemplateRenderArgs("backLink.html" -> html.as[String])),
      fixture.options.get("classes").map(classes => BackLink.classes(classes.as[String])),
      optionAttributes(fixture, BackLink.attributes)
    )
    BackLink.linkToLatestSection("bl", Future.successful(Messages.empty()), args)
  }
}
