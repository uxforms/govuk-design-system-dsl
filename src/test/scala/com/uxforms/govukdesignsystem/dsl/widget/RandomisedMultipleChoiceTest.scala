package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain.{FormData, ListMessages, Messages, NoExtraRenderArgs}
import com.uxforms.dsl.widgets.alwaysShown
import com.uxforms.govukdesignsystem.dsl.test.{FormBuilder, FormDefinitionBuilder, RequestInfoBuilder, TemplateRendererBuilder}
import play.api.libs.json.Json
import utest._

import java.util.Locale
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object RandomisedMultipleChoiceTest extends TestSuite {

  override def tests: Tests = Tests {

    def randomiseAndCollateIndices(items: Seq[Map[String, Any]]) = {
      val indexCounts = items.map(_("value") -> Seq.empty[Int]).toMap

      val randomised = (1 to 1000).foldLeft(indexCounts) { (result, _) =>
        val randomised = RandomisedMultipleChoice.randomiseItems(items)
        result.map {
          case (key, x) => key -> (x :+ randomised.indexWhere(_("value") == key))
        }
      }
      randomised.map { case (key, indexes) => key -> indexes.distinct.size }
    }

    "randomiseItems" - {
      "should take a seq of already-built items and randomise their order" - {
        val countOfIndices = randomiseAndCollateIndices(Seq(
          Map("value" -> "FIRST"),
          Map("value" -> "SECOND"),
          Map("value" -> "THIRD")
        ))

        for ((_, count) <- countOfIndices) {
          assert(count > 1)
        }
      }


      "should preserve the order of already-built items marked with preserveOrder=true" - {
        val countOfIndices = randomiseAndCollateIndices(Seq(
          Map("value" -> "FIRST"),
          Map("value" -> "SECOND"),
          Map("value" -> "THIRD", "preserveOrder" -> "true")
        ))

        assert(countOfIndices("FIRST") > 1)
        assert(countOfIndices("SECOND") > 1)
        assert(countOfIndices("THIRD") == 1)

        val countOfIndicesForBoolean = randomiseAndCollateIndices(Seq(
          Map("value" -> "FIRST"),
          Map("value" -> "SECOND"),
          Map("value" -> "THIRD", "preserveOrder" -> true)
        ))

        assert(countOfIndicesForBoolean("FIRST") > 1)
        assert(countOfIndicesForBoolean("SECOND") > 1)
        assert(countOfIndicesForBoolean("THIRD") == 1)
      }

      "should preserve order of dividers" - {
        val countOfIndices = randomiseAndCollateIndices(Seq(
          Map("value" -> "FIRST"),
          Map("value" -> "OR", "divider" -> "Or", "item.divider" -> "Or", "preserveOrder" -> true),
          Map("value" -> "SECOND", "preserveOrder" -> true),
          Map("value" -> "THIRD")
        ))

        assert(countOfIndices("FIRST") > 1)
        assert(countOfIndices("OR") == 1)
        assert(countOfIndices("SECOND") == 1)
      }
    }

    "buildItemsWithPersistedOrder" - {
      "should ensure the first item has the 'real' element id" - {

        implicit val locale = Locale.UK
        implicit val formMessages = Future.successful(Messages.empty())
        implicit val renderer = TemplateRendererBuilder.anon
        import com.uxforms.dsl.helpers.MessageHelper._
        val choices: Future[Messages[ListMessages]] = Future.successful(Seq(
          "FIRST.label.text" -> "First",
          "SECOND.label.text" -> "Second"
        ))
        val checkboxes = new CheckboxesWidget("ch", "checkboxes", Future.successful(Messages.empty()), Set.empty, choices, Some("rando"), Checkboxes.noChoiceWidgets, "checkboxes/template.mustache", NoExtraRenderArgs, alwaysShown)
        val formDef = FormDefinitionBuilder.formDef(checkboxes)

        RandomisedMultipleChoice.buildItemsWithPersistedOrder(
          "rando",
          FormData(Json.obj("rando" -> Seq("SECOND", "FIRST"))),
          CheckboxesWidget.buildItems(checkboxes.name, choices, Checkboxes.noChoiceWidgets)(FormBuilder.form(formDef), Seq.empty, RequestInfoBuilder.local)
        ).map { ordered =>
          assert(ordered.head("id").asInstanceOf[String].startsWith("elementId"))
          assert(ordered.tail.head("id").asInstanceOf[String].startsWith("id_"))
        }
      }
    }
  }
}
