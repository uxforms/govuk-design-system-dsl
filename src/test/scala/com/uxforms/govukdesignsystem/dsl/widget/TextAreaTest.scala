package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain._
import com.uxforms.domain.constraint.Required.required
import com.uxforms.domain.constraint.{Constraint, ValidationError, WidgetValidationError}
import com.uxforms.dsl.helpers.MessageHelper._
import com.uxforms.govukdesignsystem.dsl.constraint.ConstraintHelper._
import com.uxforms.govukdesignsystem.dsl.template.{Label, TemplateHelper}
import com.uxforms.govukdesignsystem.dsl.test.FormBuilder.form
import com.uxforms.govukdesignsystem.dsl.test.FormDefinitionBuilder.formDef
import com.uxforms.govukdesignsystem.dsl.test.{Fixture, HtmlUtils, RequestInfoBuilder, TemplateRendererBuilder, WidgetFixtureTest}
import com.uxforms.govukdesignsystem.dsl.{AlwaysHidden, TemplateRenderer}
import play.api.libs.json.{JsValue, Json}
import utest._

import java.util.Locale
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object TextAreaTest extends TestSuite with WidgetFixtureTest[TextArea[_, _]] {

  private implicit val formLevelMessages: Future[Messages[EmptyMessages]] = Future.successful(Messages.empty())
  private implicit val renderer: TemplateRenderer = TemplateRendererBuilder.anon
  private implicit val locale = Locale.UK

  private val textArea = TextArea.textArea("ta", Future.successful(Messages.empty()), noConstraints)
  private val formDefinition = formDef(textArea)


  override def fixtures: Seq[Fixture] = {
    super.fixtures //.filter(_.name == "with label as page heading")
  }

  override def widgetFromFixture(fixture: Fixture): TextArea[_, _] = {
    implicit val renderer: TemplateRenderer = TemplateRendererBuilder.local

    val widgetName = fixture.option[String]("name").getOrElse("ta")

    val args = flattenExtraTemplateRenderArgs(
      fixture.option[JsValue]("label").flatMap(v => (v \ "text").asOpt[String].map(t => SimpleExtraTemplateRenderArgs("label.text" -> t))),
      fixture.option[JsValue]("label").flatMap(v => (v \ "html").asOpt[String].map(t => SimpleExtraTemplateRenderArgs("label.html" -> t))),
      fixture.option[JsValue]("label").flatMap(v => (v \ "isPageHeading").asOpt[Boolean].map(h => if (h) Label.asPageHeading else NoExtraRenderArgs)),
      fixture.option[JsValue]("hint").flatMap(v => (v \ "text").asOpt[String].map(t => SimpleExtraTemplateRenderArgs("hint.text" -> t))),
      fixture.option[JsValue]("hint").flatMap(v => (v \ "html").asOpt[String].map(t => SimpleExtraTemplateRenderArgs("hint.html" -> t))),
      fixture.options.get("formGroup").flatMap(g => (g \ "classes").asOpt[String].map(TextArea.formGroupClasses)),
      fixture.optionArg[String]("value", "textArea.value"),
      fixture.optionArg[Int]("rows", "textArea.rows"),
      fixture.optionArg[String]("autocomplete", "textArea.autocomplete"),
      fixture.option[Boolean]("spellcheck").map(TextArea.spellcheck),
      fixture.option[String]("classes").map(TextArea.classes),
      optionAttributes(fixture, TextArea.attributes)
    )

    TextArea.textArea(widgetName, Future.successful(Messages.empty()), Seq.empty, args)
  }


  override def validationErrorsFromFixture(fixture: Fixture): ValidationErrors = {
    val widget = widgetFromFixture(fixture)
    fixture.option[JsValue]("errorMessage").map(j => Seq(WidgetValidationError(widget, widget.name, new Constraint {
      override def displayMessage(error: ValidationError): String = (j \ "text").asOpt[String].orElse((j \ "html").asOpt[String]).getOrElse("")
    }))).getOrElse(Seq.empty)
  }

  override val componentName: String = "textarea"

  override def tests: Tests = Tests {

    "fixture scenarios" - {
      testFixtures { fixture =>

        runScenario(fixture) { scenario =>
          val result = HtmlUtils.diff(scenario, HtmlUtils.standardAttributesToIgnore)
          assert(result)
        }
      }
    }

    "templateArgs" - {

      "should set a valid id for the surrounding formGroup" - {
        textArea.templateArgs(form(formDef(textArea)), Seq.empty, RequestInfoBuilder.local).map { args =>
          WidgetScenarios.assertAutoId(args("textArea.formGroup.attributes"))
        }
      }

      "should set a valid id for the textarea element" - {
        textArea.templateArgs(form(formDef(textArea)), Seq.empty, RequestInfoBuilder.local).map { args =>
          WidgetScenarios.assertElementId(args("textArea.id"))
        }
      }

      "should set the name of the widget" - {
        textArea.templateArgs(form(formDefinition), Seq.empty, RequestInfoBuilder.local).map { args =>
          val actual = args("textArea.name")
          val expected = textArea.name
          assert(actual == expected)
        }
      }

      "should set its value if present in FormData" - {
        val expected = "myValue"
        textArea.templateArgs(form(FormData(Json.obj(textArea.name -> expected)), formDefinition), Seq.empty, RequestInfoBuilder.local).map { args =>
          val actual = args("textArea.value")
          assert(actual == expected)
        }
      }

      "should add a hidden class if the widget is hidden" - {
        val shownTextArea = TextArea.textArea("myTextArea", Future.successful(Messages.empty()), noConstraints)
        shownTextArea.templateArgs(form(formDef(shownTextArea)), Seq.empty, RequestInfoBuilder.local).map { args =>
          val actual = WidgetScenarios.parseClassProperty(args("textArea.formGroup.classes"))
          val expected = Set.empty
          assert(actual == expected)
        }

        val hiddenTextArea = TextArea.textArea("myTextArea", Future.successful(Messages.empty()), noConstraints, visibility = AlwaysHidden)
        hiddenTextArea.templateArgs(form(formDef(shownTextArea)), Seq.empty, RequestInfoBuilder.local).map { args =>
          val actual = WidgetScenarios.parseClassProperty(args("textArea.formGroup.classes"))
          val expected = Set(TemplateHelper.hiddenCssClassName)
          assert(actual == expected)
        }
      }

      "should append hidden class and extra formGroup classes" - {
        val hiddenTextArea = TextArea.textArea("myTextArea", Future.successful(Messages.empty()), noConstraints,
          SimpleExtraTemplateRenderArgs("textArea.formGroup.classes" -> "another-class"),
          AlwaysHidden
        )
        hiddenTextArea.templateArgs(form(formDef(hiddenTextArea)), Seq.empty, RequestInfoBuilder.local).map { args =>
          val actual = WidgetScenarios.parseClassProperty(args("textArea.formGroup.classes"))
          val expected = Set(TemplateHelper.hiddenCssClassName, "another-class")
          assert(actual == expected)
        }

      }

      "describedBy" - {
        "should be omitted if there is nothing to associate with" - {
          val textAreaWithNoMessages = TextArea.textArea("mt", Future.successful(Messages.empty()), noConstraints)
          textAreaWithNoMessages.templateArgs(form(formDef(textAreaWithNoMessages)), Seq.empty, RequestInfoBuilder.local).map { args =>
            val actual = args.get("textArea.describedBy")
            assert(actual.isEmpty)
          }
        }

        "should include the id of the validation error and hint elements, without any leading or trailing spaces" - {
          val req = required()(Messages.empty())
          val textAreaWithHint = TextArea.textArea("wh", Future.successful(tupleToMessages("wh.hint.text" -> "my hint")), req)
          textAreaWithHint.templateArgs(form(formDef(textAreaWithHint)), Seq(WidgetValidationError(textAreaWithHint, textAreaWithHint.name, req)), RequestInfoBuilder.local).map { args =>
            val actual = args("textArea.describedBy")
            assert(actual == actual.asInstanceOf[String].trim)
            val parsed = WidgetScenarios.parseAriaDescribedbyProperty(actual)
            assert(parsed.size == 2)
          }
        }

        "should append the id of its hint element to any passed-in values" - {
          val textAreaWithExistingDescribedBy = TextArea.textArea("tawe", Future.successful(messages(Seq("tawe.hint.text" -> "hint", "textArea.describedBy" -> "another_id"))), noConstraints)
          textAreaWithExistingDescribedBy.templateArgs(form(formDef(textAreaWithExistingDescribedBy)), Seq.empty, RequestInfoBuilder.local).map { args =>
            val parsed = WidgetScenarios.parseAriaDescribedbyProperty(args("textArea.describedBy"))
            assert(parsed.size == 2)
            assert(parsed.contains("another_id"))
          }
        }
      }
    }

    "extract" - {
      "should return None when there is no value in FormData" - {
        val result = textArea.extract(FormData())
        assert(result.isEmpty)
      }

      "should return None when there is a value but the widget is hidden" - {
        val hiddenWidget = TextArea.textArea("ta", Future.successful(Messages.empty()), noConstraints, NoExtraRenderArgs, AlwaysHidden)
        val result = hiddenWidget.extract(FormData(Json.obj(hiddenWidget.name -> "my value")))
        assert(result.isEmpty)
      }

      "should return the value when present" - {
        val result = textArea.extract(FormData(Json.obj(textArea.name -> "a value")))
        assert(result.contains("a value"))
      }
    }

  }
}
