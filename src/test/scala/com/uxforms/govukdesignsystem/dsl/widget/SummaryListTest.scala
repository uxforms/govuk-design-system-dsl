package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain.{EmptyMessages, Messages, SimpleExtraTemplateRenderArgs}
import com.uxforms.govukdesignsystem.dsl.test.FormBuilder._
import com.uxforms.govukdesignsystem.dsl.test.FormDefinitionBuilder._
import com.uxforms.govukdesignsystem.dsl.test.{Fixture, HtmlUtils, RequestInfoBuilder, TemplateRendererBuilder, WidgetFixtureTest}
import com.uxforms.govukdesignsystem.dsl.{AlwaysHidden, TemplateRenderer}
import play.api.libs.json.{JsNumber, JsString, JsValue}
import utest._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object SummaryListTest extends TestSuite with WidgetFixtureTest[SummaryList[_, _]] {

  private implicit val renderer: TemplateRenderer = TemplateRendererBuilder.anon
  private implicit val formMessages: Future[EmptyMessages] = Future.successful(Messages.empty())

  override def tests: Tests = Tests {

    "should pass all fixture scenarios" - {
      testFixtures { fixture =>
        runScenario(fixture) { scenario =>
          val result = HtmlUtils.diff(scenario, HtmlUtils.standardAttributesToIgnore)
          assert(result)
        }
      }
    }

    "templateArgs" - {

      val summary = SummaryList.summaryList("sl", Future.successful(Messages.empty()), Seq.empty)

      "should set a valid id for the definition list" - {
        summary.templateArgs(form(formDef(summary)), Seq.empty, RequestInfoBuilder.local).map(args =>
          WidgetScenarios.assertAutoId(args("summaryList.attributes"))
        )
      }

      "should add a hidden class if this widget should be hidden" - {
        val hiddenSummary = SummaryList.summaryList("sl", Future.successful(Messages.empty()), Seq.empty, visibility = AlwaysHidden)
        hiddenSummary.templateArgs(form(formDef(hiddenSummary)), Seq.empty, RequestInfoBuilder.local).map(args =>
          WidgetScenarios.assertHiddenClass(args("summaryList.classes"))
        )
      }
    }
  }


  override val excludedFixtureNames: Set[String] = Set(
    "with falsey values",
    "actions with attributes", // No value in testing attributes passed this way into action as we take Widgets instead
    "classes on items" // No value in testing classes passed this way into actions as we take Widgets instead
  )

  override def widgetFromFixture(fixture: Fixture): SummaryList[_, _] = {
    implicit val renderer: TemplateRenderer = TemplateRendererBuilder.local
    val rows = fixture.option[Seq[JsValue]]("rows").map(rows => rows.map(row =>
      SummaryListRow(
        Map.empty ++
          (row \ "key" \ "text").asOpt[String].map(t => "row.key.text" -> t) ++
          (row \ "key" \ "html").asOpt[String].map(t => "row.key.html" -> t) ++
          (row \ "key" \ "classes").asOpt[String].map(t => "row.key.classes" -> t) ++
          (row \ "value" \ "text").asOpt[JsValue].map {
            case JsString(s) => "row.value.text" -> s
            case JsNumber(n) => "row.value.text" -> n.toString
          } ++
          (row \ "value" \ "html").asOpt[String].map(t => "row.value.html" -> t) ++
          (row \ "value" \ "classes").asOpt[String].map(t => "row.value.classes" -> t) ++
          (row \ "classes").asOpt[String].map(c => "row.classes" -> c) ++
          (row \ "actions" \ "classes").asOpt[String].map(c => "row.actions.classes" -> c),

        (row \ "actions" \ "items").asOpt[Seq[JsValue]].map(_.map { item =>
          // We need to get creative with the item action here as UX Forms allows any widget you like, not just links.
          val content = (item \ "html").asOpt[String].orElse((item \ "text").asOpt[String]).getOrElse("")
          val linkArgs = flattenExtraTemplateRenderArgs(
            (item \ "visuallyHiddenText").asOpt[String].filter(_.nonEmpty) match {
              case None => (item \ "html").asOpt[String].map(s => SimpleExtraTemplateRenderArgs("link.html" -> s))
                .orElse((item \ "text").asOpt[String].map(s => SimpleExtraTemplateRenderArgs("link.text" -> s)))
              case Some(h) => Some(SimpleExtraTemplateRenderArgs("link.html" -> s"""${content}<span class="govuk-visually-hidden">${h}</span>"""))
            }
          )
          Link.toExternalUrl("", Future.successful(Messages.empty()), (item \ "href").asOpt[String].getOrElse("#"), linkArgs)
        }).getOrElse(Seq.empty)
      )
    )).getOrElse(Seq.empty)

    val args = flattenExtraTemplateRenderArgs(
      fixture.option[String]("classes").map(SummaryList.classes),
      fixture.option[JsValue]("card").flatMap(j => (j \ "title" \ "text").asOpt[String].map(s => SimpleExtraTemplateRenderArgs("summaryList.card.title.text" -> s))),
      fixture.option[JsValue]("card").flatMap(j => (j \ "title" \ "html").asOpt[String].map(s => SimpleExtraTemplateRenderArgs("summaryList.card.title.html" -> s))),
      fixture.option[JsValue]("card").flatMap(j => (j \ "title" \ "headingLevel").asOpt[Int].map(s => SimpleExtraTemplateRenderArgs("summaryList.card.title.headingLevel" -> s))),
      fixture.option[JsValue]("card").flatMap(j => (j \ "classes").asOpt[String].map(s => SimpleExtraTemplateRenderArgs("summaryList.card.classes" -> s))),
      fixture.option[JsValue]("card").flatMap(j => (j \ "attributes").asOpt[JsValue].map(j => SimpleExtraTemplateRenderArgs("summaryList.card.attributes" -> convertAttributes(j)))),
      optionAttributes(fixture, SummaryList.attributes)
    )

    fixture.option[JsValue]("card") match {
      case None => SummaryList.summaryList("s", Future.successful(Messages.empty()), rows, args)
      case Some(_) => {
        val cardActions = fixture.option[JsValue]("card").flatMap(j =>
          (j \ "actions" \ "items").asOpt[Seq[JsValue]]
            .map(_.map { item =>
              val linkArgs = flattenExtraTemplateRenderArgs(
                (item \ "text").asOpt[String].map(t => SimpleExtraTemplateRenderArgs("link.text" -> t))
              )
              Link.toExternalUrl("", Future.successful(Messages.empty()), (item \ "href").asOpt[String].getOrElse("#"), linkArgs)
            })
        ).getOrElse(Seq.empty)
        SummaryList.summaryCard("s", Future.successful(Messages.empty()), rows, cardActions, args)
      }
    }
  }

  override val componentName: String = "summary-list"
}
