package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain.{EmptyMessages, Messages}
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.test.FormBuilder.form
import com.uxforms.govukdesignsystem.dsl.test.FormDefinitionBuilder.formDef
import com.uxforms.govukdesignsystem.dsl.test.HtmlUtils._
import com.uxforms.govukdesignsystem.dsl.test.TemplateRendererBuilder
import utest._

import scala.collection.JavaConverters.asScalaSetConverter
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object ParagraphTest extends TestSuite {

  override def tests: Tests = Tests {

    implicit val renderer: TemplateRenderer = TemplateRendererBuilder.local
    implicit val formLevelMessages: Future[EmptyMessages] = Future.successful(Messages.empty())

    "body" - {
      "should preserve classes passed in as ExtraTemplateRenderArgs" - {

        val p = Paragraph.body("p", Future.successful(Messages.empty()))
        renderAndParseHtml(form(formDef(p)), Seq.empty, p).map(element =>
          assert(element.classNames().asScala == Set("govuk-body"))
        )

        val pWithClasses = Paragraph.body("p", Future.successful(Messages.empty()), Paragraph.classes("my-special-class"))
        renderAndParseHtml(form(formDef(pWithClasses)), Seq.empty, pWithClasses).map(element =>
          assert(element.classNames().asScala == Set("govuk-body", "my-special-class"))
        )
      }
    }

  }
}
