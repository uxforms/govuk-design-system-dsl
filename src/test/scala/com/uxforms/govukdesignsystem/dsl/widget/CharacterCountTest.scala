package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain._
import com.uxforms.domain.constraint.{Constraint, ValidationError, WidgetValidationError}
import com.uxforms.dsl.helpers.MessageHelper._
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.constraint.ConstraintHelper._
import com.uxforms.govukdesignsystem.dsl.template.Label
import com.uxforms.govukdesignsystem.dsl.template.TemplateHelper.buildAttribute
import com.uxforms.govukdesignsystem.dsl.test.{Fixture, HtmlUtils, TemplateRendererBuilder, WidgetFixtureTest}
import com.uxforms.govukdesignsystem.dsl.widget.HtmlAssertions.{alwaysHidden, assertAttributes, assertId, assertVisibilityClass}
import org.jsoup.nodes.Element
import utest._

import java.util.Locale
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object CharacterCountTest extends TestSuite with WidgetFixtureTest[TextArea[_, _]] {

  override val componentName: String = "character-count"

  private implicit val locale = Locale.UK
  private implicit val renderer: TemplateRenderer = TemplateRendererBuilder.local
  private implicit val formLevelMessages: Future[Messages[EmptyMessages]] = Future.successful(Messages.empty())

  override def tests: Tests = Tests {

    val formGroupSelector: Element => Element =
      _.selectFirst("div.govuk-form-group")

    "should set its id attribute" - assertId(
      CharacterCount.characterCount("cnt", Future.successful(Messages.empty()), noConstraints, 50),
      formGroupSelector
    )


    "should set the visibility class when hidden" -
      assertVisibilityClass(
        CharacterCount.characterCount("cnt", Future.successful(Messages.empty()), noConstraints, 50, TextArea.formGroupClasses("my-custom-class"), alwaysHidden),
        Set("govuk-form-group", "my-custom-class"),
        formGroupSelector
      )

    "should set attributes" - {
      assertAttributes(
        CharacterCount.characterCount("cnt", Future.successful(Messages.empty()), noConstraints, 50, CharacterCount.attributes(buildAttribute("data-custom", "true"))),
        Map("data-custom" -> "true")
      )
    }


    "should pass all fixture scenarios" - {
      testFixtures { fixture =>
        runScenario(fixture) { scenario =>
          val result = HtmlUtils.diff(scenario, HtmlUtils.standardAttributesToIgnore)
          assert(result)
        }
      }
    }

  }

  override def validationErrorsFromFixture(fixture: Fixture): ValidationErrors = {
    fixture.options.get("errorMessage").fold(Seq.empty[ValidationError]) { msg =>
      val widget = widgetFromFixture(fixture)
      Seq(WidgetValidationError(widget, widget.name, new Constraint {
        override def displayMessage(error: ValidationError): String = (msg \ "text").as[String]
      }))
    }
  }

  override def widgetFromFixture(fixture: Fixture): TextArea[EmptyMessages, ListMessages] = {

    def i8nOptions(key: String) = {
      fixture.options.get(key)
        .map(lt => SimpleExtraTemplateRenderArgs(lt.as[Map[String, String]]
          .map { case (k, v) => s"characterCount.$key.$k" -> v }))
    }

    implicit val formLevelMessages: Future[Messages[ListMessages]] =
      Future.successful("characterCount.info.hint.default" -> "You can enter up to {count,number,#} {maxProperty, select, maxwords {words} other {characters}}")
    val widgetName = fixture.options.get("name").map(_.as[String]).getOrElse("cc")
    val maxLength = fixture.options.get("maxlength").map(_.as[Int])
    val maxWords = fixture.options.get("maxwords").map(_.as[Int])
    val args = flattenExtraTemplateRenderArgs(
      fixture.options.get("label").map(label => flattenExtraTemplateRenderArgs(
        (label \ "text").asOpt[String].map(text => SimpleExtraTemplateRenderArgs(s"$widgetName.label.text" -> text)),
        (label \ "isPageHeading").asOpt[Boolean].map(ph => if (ph) Label.asPageHeading else NoExtraRenderArgs)
      )),
      fixture.options.get("textareaDescriptionText").map(t => SimpleExtraTemplateRenderArgs(s"$widgetName.info.hint.text" -> maxLength.map(l => t.as[String].replace("%{count}", s"${l}")).getOrElse(t.as[String]))),
      fixture.options.get("hint").flatMap(h => (h \ "text").asOpt[String].map(hint => SimpleExtraTemplateRenderArgs(s"$widgetName.hint.text" -> hint))),
      fixture.options.get("value").map(v => SimpleExtraTemplateRenderArgs("textArea.value" -> v.as[String])),
      fixture.options.get("rows").map(r => TextArea.rows(r.as[Int])),
      fixture.options.get("threshold").map(t => CharacterCount.threshold(t.as[Int])),
      fixture.options.get("classes").map(c => TextArea.classes(c.as[String])),
      fixture.options.get("formGroup").flatMap(g => (g \ "classes").asOpt[String].map(CharacterCount.formGroupClasses)),
      i8nOptions("charactersUnderLimitText"),
      fixture.option[String]("charactersAtLimitText").map(t => SimpleExtraTemplateRenderArgs("characterCount.charactersAtLimitText" -> t)),
      i8nOptions("charactersOverLimitText"),
      i8nOptions("wordsUnderLimitText"),
      fixture.option[String]("wordsAtLimitText").map(t => SimpleExtraTemplateRenderArgs("characterCount.wordsAtLimitText" -> t)),
      i8nOptions("wordsOverLimitText"),
      fixture.options.get("countMessage").flatMap(cm => (cm \ "classes").asOpt[String].map(m => SimpleExtraTemplateRenderArgs(s"$widgetName.info.hint.classes" -> m))),
      fixture.options.get("spellcheck").map(sc => CharacterCount.spellcheck(sc.as[Boolean])),
      fixture.option[String]("textareaDescriptionText").map(t => SimpleExtraTemplateRenderArgs("characterCount.info.hint.text" -> t)),
      optionAttributes(fixture, TextArea.attributes)
    )

    maxLength match {
      case Some(l) => CharacterCount.characterCount(widgetName, Future.successful(Messages.empty()), noConstraints, l, args)
      case None => maxWords match {
        case Some(w) => CharacterCount.wordCount(widgetName, Future.successful(Messages.empty()), noConstraints, w, args)
        case None => CharacterCount.characterCountWithoutLimit(widgetName, Future.successful(Messages.empty()), noConstraints, args)
      }
    }

  }
}
