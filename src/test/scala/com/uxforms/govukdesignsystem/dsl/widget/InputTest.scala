package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain._
import com.uxforms.domain.constraint.Required.required
import com.uxforms.domain.constraint.{Constraint, ValidationError, WidgetValidationError}
import com.uxforms.dsl.helpers.MessageHelper.{messages, tupleToMessages}
import com.uxforms.govukdesignsystem.dsl.constraint.ConstraintHelper._
import com.uxforms.govukdesignsystem.dsl.template.TemplateHelper.buildAttribute
import com.uxforms.govukdesignsystem.dsl.template.{Label, TemplateHelper}
import com.uxforms.govukdesignsystem.dsl.test.FormBuilder._
import com.uxforms.govukdesignsystem.dsl.test.FormDefinitionBuilder._
import com.uxforms.govukdesignsystem.dsl.test.{Fixture, HtmlUtils, RequestInfoBuilder, TemplateRendererBuilder, WidgetFixtureTest}
import com.uxforms.govukdesignsystem.dsl.widget.HtmlAssertions.{alwaysHidden, assertAttributes, assertId, assertVisibilityClass}
import com.uxforms.govukdesignsystem.dsl.{AlwaysHidden, TemplateRenderer}
import play.api.libs.json.{JsValue, Json}
import utest._

import java.util.Locale
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object InputTest extends TestSuite with WidgetFixtureTest[Input[_, _]] {

  private implicit val formLevelMessages: Future[Messages[EmptyMessages]] = Future.successful(Messages.empty())
  private implicit val renderer: TemplateRenderer = TemplateRendererBuilder.local
  private implicit val locale = Locale.UK

  override val componentName: String = "input"

  private val input = Input.inputText("myInput", Future.successful(Messages.empty()), noConstraints)
  private val formDefinition = formDef(input)

  override def tests: Tests = Tests {

    "should set its id attribute" - assertId(
      Input.inputText("i", Future.successful(Messages.empty()), noConstraints)
    )
    "should set the visibility class when hidden" -
      assertVisibilityClass(
        Input.inputText("i", Future.successful(Messages.empty()), noConstraints, Input.formGroupClasses("my-custom-class"), alwaysHidden),
        Set("govuk-form-group", "my-custom-class")
      )

    "should set attributes" - {
      assertAttributes(
        Input.inputText("i", Future.successful(Messages.empty()), noConstraints, SimpleExtraTemplateRenderArgs("input.formGroup.attributes" -> Seq(buildAttribute("data-custom", "true")))),
        Map("data-custom" -> "true")
      )
    }

    "templateArgs" - {
      "should set a valid id for the surrounding formGroup" - {
        evalTemplateArgs(input).map { args =>
          WidgetScenarios.assertAutoId(args("input.formGroup.attributes"))
        }
      }

      "should set a valid id for the input field itself" - {
        evalTemplateArgs(input).map { args =>
          WidgetScenarios.assertElementId(args("input.id"))
        }
      }

      "should set the name of the widget" - {
        evalTemplateArgs(input).map { args =>
          val actual = args("input.name")
          val expected = input.name
          assert(actual == expected)
        }
      }

      "should allow the name to be overridden" - {
        val inputWithDefaultName = Input.inputText("originalName", Future.successful(Messages.empty()), noConstraints, SimpleExtraTemplateRenderArgs("input.name" -> "overriddenName"))
        evalTemplateArgs(inputWithDefaultName).map { args =>
          val actual = args("input.name")
          assert(actual == "overriddenName")
        }
      }

      "should set its value if present in FormData" - {
        val expected = "myValue"
        input.templateArgs(form(FormData(Json.obj(input.name -> expected)), formDefinition), Seq.empty, RequestInfoBuilder.local).map { args =>
          val actual = args("input.value")
          assert(actual == expected)
        }
      }

      "should allow its value to be overridden" - {
        val inputWithOverriddenData = Input.inputText("n", Future.successful(Messages.empty()), noConstraints, SimpleExtraTemplateRenderArgs("input.value" -> "overridden data"))
        inputWithOverriddenData.templateArgs(form(FormData(Json.obj(input.name -> "from data")), formDef(inputWithOverriddenData)), Seq.empty, RequestInfoBuilder.local).map { args =>
          val actual = args("input.value")
          assert(actual == "overridden data")
        }
      }

      "should add a hidden class if this widget should be hidden" - {
        val hiddenInput = Input.inputText("hiddenInput", Future.successful(Messages.empty()), noConstraints, NoExtraRenderArgs, AlwaysHidden)
        evalTemplateArgs(hiddenInput).map { args =>
          val actual = args("input.formGroup.classes")
          assert(actual == TemplateHelper.hiddenCssClassName)
        }
      }

      "should add the hidden class to other classes passed in" - {
        val hiddenInput = Input.inputText("hiddenInput", Future.successful(Messages.empty()), noConstraints,
          SimpleExtraTemplateRenderArgs("input.formGroup.classes" -> "custom-class another-custom-class"), AlwaysHidden)

        evalTemplateArgs(hiddenInput).map { args =>
          val actual = WidgetScenarios.parseClassProperty(args("input.formGroup.classes"))
          val expected = Set("custom-class", "another-custom-class", TemplateHelper.hiddenCssClassName)
          assert(actual == expected)
        }
      }

      "should add the hidden class to other classes passed in with a namespace" - {
        val widgetName = "hiddenInput"
        val hiddenInput = Input.inputText(widgetName, Future.successful(Messages.empty()), noConstraints,
          SimpleExtraTemplateRenderArgs(s"$widgetName.input.formGroup.classes" -> "custom-class another-custom-class"), AlwaysHidden)

        evalTemplateArgs(hiddenInput).map { args =>
          val actual = WidgetScenarios.parseClassProperty(args("input.formGroup.classes"))
          val expected = Set("custom-class", "another-custom-class", TemplateHelper.hiddenCssClassName)
          assert(actual == expected)
        }
      }

      "set properties if a prefix is present" - {
        val expectedPrefix = "a prefix"
        val input = Input.inputText("it", Future.successful(tupleToMessages("it.input.prefix.text" -> expectedPrefix)), noConstraints)
        evalTemplateArgs(input).map { args =>
          val hasPrefix = args("input.hasPrefix")
          assert(hasPrefix == true)

          val hasPrefixOrSuffix = args("input.hasPrefixOrSuffix")
          assert(hasPrefixOrSuffix == true)

          val prefix = args("input.prefix.text")
          assert(prefix == expectedPrefix)
        }
      }

      "set properties if a suffix is present" - {
        val expectedSuffix = "a suffix"
        val input = Input.inputText("it", Future.successful(tupleToMessages("it.input.suffix.text" -> expectedSuffix)), noConstraints)
        evalTemplateArgs(input).map { args =>
          val hasSuffix = args("input.hasSuffix")
          assert(hasSuffix == true)

          val hasPrefixOrSuffix = args("input.hasPrefixOrSuffix")
          assert(hasPrefixOrSuffix == true)

          val suffix = args("input.suffix.text")
          assert(suffix == expectedSuffix)
        }
      }

      "describedBy" - {
        val describedByKey = "input.describedBy"
        "should be omitted if there is nothing to associate with" - {
          val inputWithNoMessages = Input.inputText("mt", Future.successful(Messages.empty()), noConstraints)
          evalTemplateArgs(inputWithNoMessages).map { args =>
            val actual = args.get(describedByKey)
            assert(actual.isEmpty)
          }
        }

        "should include the id of the validation error and hint elements, without any leading or trailing spaces" - {
          val req = required()(Messages.empty())
          val inputWithHint = Input.inputText("wh", Future.successful(tupleToMessages("wh.hint.text" -> "my hint")), req)
          evalTemplateArgs(inputWithHint, Seq(WidgetValidationError(inputWithHint, inputWithHint.name, req))).map { args =>
            val actual = args(describedByKey)
            assert(actual == actual.asInstanceOf[String].trim)
            val parsed = WidgetScenarios.parseAriaDescribedbyProperty(actual)
            assert(parsed.size == 2)
          }
        }

        "should append the id of its hint element to any passed-in values" - {
          val inputWithExistingDescribedBy = Input.inputText("iwe", Future.successful(messages(Seq("iwe.hint.text" -> "hint", describedByKey -> "another_id"))), noConstraints)
          evalTemplateArgs(inputWithExistingDescribedBy).map { args =>
            val parsed = WidgetScenarios.parseAriaDescribedbyProperty(args(describedByKey))
            assert(parsed.size == 2)
            assert(parsed.contains("another_id"))
          }
        }
      }
    }

    "visibility" - {
      WidgetScenarios.assertVisibility(v => Input.inputText("n", Future.successful(Messages.empty()), noConstraints, NoExtraRenderArgs, v))
    }

    "wholeNumber" - {
      val wholeNumber = Input.inputWholeNumber("wn", Future.successful(Messages.empty()), noConstraints)

      "should set inputmode to numeric" - {
        evalTemplateArgs(wholeNumber).map { args =>
          val actual = args("input.inputmode")
          assert(actual == "numeric")
        }
      }

      "should provide an input pattern" - {
        evalTemplateArgs(wholeNumber).map { args =>
          val actual = args("input.pattern")
          assert(actual == "[0-9]*")
        }
      }

      "should set the spellcheck attribute to [false]" - {
        evalTemplateArgs(wholeNumber).map { args =>
          val actual = WidgetScenarios.parseAttribute(args("input.attributes"), "spellcheck").get.asInstanceOf[Boolean]
          assert(!actual)
        }
      }
    }

    "decimalnumber" - {
      val decimalNumber = Input.inputDecimalNumber("dn", Future.successful(Messages.empty()), noConstraints)

      "should set spellcheck attribute to [false]" - {
        evalTemplateArgs(decimalNumber).map { args =>
          val actual = WidgetScenarios.parseAttribute(args("input.attributes"), "spellcheck").get.asInstanceOf[Boolean]
          assert(!actual)
        }
      }
    }

    "extract" - {
      "should return None when there is no value in FormData" - {
        val result = input.extract(FormData())
        assert(result.isEmpty)
      }

      "should return None when there is a value but the widget is hidden" - {
        val hiddenWidget = Input.inputText("it", Future.successful(Messages.empty()), noConstraints, NoExtraRenderArgs, AlwaysHidden)
        val result = hiddenWidget.extract(FormData(Json.obj(hiddenWidget.name -> "my value")))
        assert(result.isEmpty)
      }

      "should return the value when present" - {
        val result = input.extract(FormData(Json.obj(input.name -> "a value")))
        assert(result.contains("a value"))
      }
    }

    "should pass all fixture scenarios" - {
      testFixtures { fixture =>
        runScenario(fixture) { scenario =>
          val result = HtmlUtils.diff(scenario.actual, scenario.expected, HtmlUtils.standardAttributesToIgnore: _*)
          assert(result)
        }
      }
    }

  }

  def evalTemplateArgs[M <: Messages[M], F <: Messages[F]](i: Input[M, F], errors: ValidationErrors = Seq.empty, requestInfo: RequestInfo = RequestInfoBuilder.local): Future[Map[String, Any]] = {
    i.templateArgs(form(formDef(i)), errors, requestInfo)
  }

  override def widgetFromFixture(fixture: Fixture): Input[EmptyMessages, EmptyMessages] = {
    val widgetName = fixture.option[String]("name").getOrElse("i")
    val args = flattenExtraTemplateRenderArgs(
      fixture.option[JsValue]("label").flatMap(l => (l \ "text").asOpt[String].map(text => SimpleExtraTemplateRenderArgs(s"$widgetName.label.text" -> text))),
      fixture.option[JsValue]("label").flatMap(l => (l \ "html").asOpt[String].map(html => SimpleExtraTemplateRenderArgs(s"$widgetName.label.html" -> html))),
      fixture.option[JsValue]("label").flatMap(l => (l \ "classes").asOpt[String].map(c => Label.classes(c))),
      fixture.option[JsValue]("label").flatMap(l => (l \ "isPageHeading").asOpt[Boolean].map(c => Label.asPageHeading)),
      fixture.option[JsValue]("hint").flatMap(h => (h \ "text").asOpt[String]
        .map(hint => SimpleExtraTemplateRenderArgs(s"$widgetName.hint.text" -> hint))
      ),
      fixture.option[String]("classes").map(c => SimpleExtraTemplateRenderArgs("input.classes" -> c)),
      fixture.option[JsValue]("formGroup").flatMap(fg => (fg \ "classes").asOpt[String].map(c => Input.formGroupClasses(c))),
      fixture.option[String]("autocomplete").map(ac => SimpleExtraTemplateRenderArgs("input.autocomplete" -> ac)),
      fixture.option[String]("pattern").map(p => SimpleExtraTemplateRenderArgs("input.pattern" -> p)),
      fixture.option[String]("type").map(t => SimpleExtraTemplateRenderArgs("input.type" -> t)),
      fixture.option[Boolean]("spellcheck").map(s => Input.spellcheck(s)),
      fixture.option[JsValue]("prefix").flatMap(p => (p \ "text").asOpt[String].map(text => SimpleExtraTemplateRenderArgs("input.prefix.text" -> text))),
      fixture.option[JsValue]("prefix").flatMap(p => (p \ "html").asOpt[String].map(html => SimpleExtraTemplateRenderArgs("input.prefix.html" -> html))),
      fixture.option[JsValue]("prefix").flatMap(p => (p \ "classes").asOpt[String].map(classes => SimpleExtraTemplateRenderArgs("input.prefix.classes" -> classes))),
      fixture.option[JsValue]("prefix").flatMap(p => (p \ "attributes").asOpt[JsValue].map(attrs => SimpleExtraTemplateRenderArgs("input.prefix.attributes" -> convertAttributes(attrs)))),
      fixture.option[JsValue]("suffix").flatMap(s => (s \ "text").asOpt[String].map(text => SimpleExtraTemplateRenderArgs("input.suffix.text" -> text))),
      fixture.option[JsValue]("suffix").flatMap(s => (s \ "html").asOpt[String].map(html => SimpleExtraTemplateRenderArgs("input.suffix.html" -> html))),
      fixture.option[JsValue]("suffix").flatMap(s => (s \ "classes").asOpt[String].map(html => SimpleExtraTemplateRenderArgs("input.suffix.classes" -> html))),
      fixture.option[JsValue]("suffix").flatMap(s => (s \ "attributes").asOpt[JsValue].map(attrs => SimpleExtraTemplateRenderArgs("input.suffix.attributes" -> convertAttributes(attrs)))),
      fixture.option[String]("value").map(v => SimpleExtraTemplateRenderArgs("input.value" -> v)),
      optionAttributes(fixture, Input.attributes),
      fixture.option[String]("inputmode").map(m => SimpleExtraTemplateRenderArgs("input.inputmode" -> m))
    )
    Input.inputText(widgetName, Future.successful(Messages.empty()), noConstraints, args)
  }

  override def validationErrorsFromFixture(fixture: Fixture): ValidationErrors = {
    val widget = widgetFromFixture(fixture)
    fixture.option[JsValue]("errorMessage").fold(Seq.empty[ValidationError])(msg =>
      Seq(WidgetValidationError(widget, widget.name,
        new Constraint {
          override def displayMessage(error: ValidationError): String = (msg \ "text").as[String]
        })
      )
    )
  }
}
