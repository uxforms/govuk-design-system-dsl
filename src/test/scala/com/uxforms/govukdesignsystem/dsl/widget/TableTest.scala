package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain._
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.test.{Fixture, HtmlUtils, TemplateRendererBuilder, WidgetFixtureTest}
import play.api.libs.json._
import utest._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object TableTest extends TestSuite with WidgetFixtureTest[Table[_, _]] {

  private implicit val formLevelMessages: Future[EmptyMessages] = Future.successful(Messages.empty())
  private implicit val renderer: TemplateRenderer = TemplateRendererBuilder.local

  override val componentName: String = "table"
  override val excludedFixtureNames = Set("with falsey items") // Doesn't make sense for us to test type safety of rows as we're already strongly typed

  override def tests: Tests = Tests {
    "table" - {
      "should pass all fixture scenarios" - {
        testFixtures { fixture =>
          runScenario(fixture) { scenario =>
            val result = HtmlUtils.diff(scenario.actual, scenario.expected, "id")
            assert(result)
          }
        }
      }
    }
  }


  private def parseFixtureObject: PartialFunction[(String, JsValue), (String, Any)] = {
    case ("attributes", JsObject(o)) => "attributes" -> o.toMap.map { case (k, JsString(s)) => Map("attribute" -> k, "value" -> s) }.toSeq
    case (k, JsString(s)) => (k -> s)
    case (k, JsNumber(n)) => (k -> n)
    case (k, JsObject(o)) => (k -> o.toMap)
  }

  def widgetFromFixture(fixture: Fixture): Table[EmptyMessages, EmptyMessages] = {
    val rows = fixture.options("rows").as[Seq[Seq[Map[String, JsValue]]]].map(_.map(_.map(parseFixtureObject)))
    val head = fixture.options.get("head").map(_.as[Seq[Map[String, JsValue]]].map(_.map(parseFixtureObject))).getOrElse(Seq.empty)

    val args = flattenExtraTemplateRenderArgs(
      fixture.options.get("caption").map(c => SimpleExtraTemplateRenderArgs("table.caption" -> c.as[String])),
      fixture.options.get("captionClasses").map(c => SimpleExtraTemplateRenderArgs("table.captionClasses" -> c.as[String])),
      fixture.options.get("firstCellIsHeader").map(_ => Table.firstCellIsHeader),
      fixture.options.get("classes").map(c => SimpleExtraTemplateRenderArgs("table.classes" -> c.as[String])),
      optionAttributes(fixture, Table.attributes)
    )
    Table.table("mt", Future.successful(Messages.empty()), head, rows, args)
  }

}
