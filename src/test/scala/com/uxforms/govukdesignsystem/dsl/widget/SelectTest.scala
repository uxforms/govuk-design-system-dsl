package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain._
import com.uxforms.domain.constraint.Required.required
import com.uxforms.domain.constraint.{Constraint, ValidationError, WidgetValidationError}
import com.uxforms.dsl.helpers.MessageHelper.{messages, tupleToMessages}
import com.uxforms.govukdesignsystem.dsl.constraint.ConstraintHelper.noConstraints
import com.uxforms.govukdesignsystem.dsl.template.{Label, TemplateHelper}
import com.uxforms.govukdesignsystem.dsl.test.FormBuilder.form
import com.uxforms.govukdesignsystem.dsl.test.FormDefinitionBuilder.formDef
import com.uxforms.govukdesignsystem.dsl.test.{Fixture, HtmlUtils, RequestInfoBuilder, TemplateRendererBuilder, WidgetFixtureTest}
import com.uxforms.govukdesignsystem.dsl.{AlwaysHidden, TemplateRenderer}
import play.api.libs.json._
import utest._

import java.util.Locale
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future


object SelectTest extends TestSuite with WidgetFixtureTest[Select[_, _, _]] {

  private implicit val renderer: TemplateRenderer = TemplateRendererBuilder.anon
  private implicit val formMessage = Future.successful(Messages.empty())
  private implicit val locale = Locale.UK

  private val choices = ResourceBundleMessages.utf8("radioGroupTestChoices", getClass.getClassLoader)
  private val countryChoices: Messages[ResourceBundleMessages] = ResourceBundleMessages.utf8("countries", getClass.getClassLoader)
  private val requestInfo = RequestInfoBuilder.local

  private def buildSelect(name: String = "mySelect", choices: Messages[ResourceBundleMessages] = countryChoices) = {
    Select.select(name, Future.successful(Messages.empty()), noConstraints, Future.successful(choices))
  }


  override def validationErrorsFromFixture(fixture: Fixture): ValidationErrors = {
    val widget = widgetFromFixture(fixture)
    fixture.option[JsValue]("errorMessage").flatMap(j => (j \ "text").asOpt[String].map(msg => Seq(WidgetValidationError(widget, widget.name, new Constraint {
      override def displayMessage(error: ValidationError): String = msg
    })))).getOrElse(Seq.empty)
  }

  override def formDataFromFixture(fixture: Fixture): FormData = {
    val widget = widgetFromFixture(fixture)
    val data = fixture.option[Seq[JsValue]]("items").flatMap(_
      .find(j => (j \ "selected").asOpt[Boolean].contains(true))
      .map(i => (i \ "value").as[JsValue] match {
        case JsString(s) => Json.obj(widget.name -> s)
        case JsNumber(n) => Json.obj(widget.name -> n.toString)
      })).getOrElse(Json.obj())

    FormData(data)

  }

  override def widgetFromFixture(fixture: Fixture): Select[_, _, _] = {
    implicit val renderer = TemplateRendererBuilder.local
    val widgetName = fixture.option[String]("name").getOrElse("s")

    val choices = new ListMessages(fixture.option[Seq[JsValue]]("items").map(_.flatMap { item => {
      val value = (item \ "value").as[JsValue] match {
        case JsString(s) => s
        case JsNumber(n) => n
        case JsBoolean(b) => b
      }

      Seq(
        (item \ "text").asOpt[String].map(s => ListMessageEntry(s"$value.label.text", s)),
        (item \ "html").asOpt[String].map(s => ListMessageEntry(s"$value.label.html", s)),
        (item \ "disabled").asOpt[Boolean].flatMap(s => if (s) Some(ListMessageEntry(s"$value.disabled", s.toString)) else None)
      ).flatten


    }
    }).getOrElse(Seq.empty), locale)

    val args = flattenExtraTemplateRenderArgs(
      fixture.option[JsValue]("label").flatMap(v => (v \ "text").asOpt[String].map(t => SimpleExtraTemplateRenderArgs("label.text" -> t))),
      fixture.option[JsValue]("label").flatMap(v => (v \ "html").asOpt[String].map(t => SimpleExtraTemplateRenderArgs("label.html" -> t))),
      fixture.option[JsValue]("label").flatMap(v => (v \ "isPageHeading").asOpt[Boolean].map(_ => Label.asPageHeading)),
      fixture.option[JsValue]("hint").flatMap(v => (v \ "text").asOpt[String].map(t => SimpleExtraTemplateRenderArgs("hint.text" -> t))),
      fixture.option[JsValue]("hint").flatMap(v => (v \ "html").asOpt[String].map(t => SimpleExtraTemplateRenderArgs("hint.html" -> t))),
      fixture.option[String]("classes").map(Select.classes),
      fixture.option[JsValue]("formGroup").flatMap(v => (v \ "classes").asOpt[String].map(Select.formGroupClasses)),
      optionAttributes(fixture, Select.attributes)
    )

    Select.select(widgetName, Future.successful(Messages.empty()), noConstraints, Future.successful(choices), args)
  }

  override val componentName: String = "select"


  override val excludedFixtureNames: Set[String] = Set(
    "with selected value", // I think this one's a bug in the scenario. If you can select multiple items then surely the `multiple` attribute should be set. https://github.com/alphagov/govuk-frontend/issues/3317
    "attributes on items", // TODO: Support custom attributes on items
    "with falsey values" // No need to test for unsafe types
  )

  override def tests: Tests = Tests {

    "should pass all fixture scenarios" - {
      testFixtures { fixture =>
        runScenario(fixture) { scenario =>
          val result = HtmlUtils.diff(scenario, HtmlUtils.standardAttributesToIgnore)
          assert(result)
        }
      }
    }


    "buildItems" - {
      val widget = buildSelect("rw")
      val formDefinition = formDef(widget)
      "should set a valid map for the choices" - {
        val items = widget.buildItems(Future.successful(countryChoices), form(formDefinition), Seq.empty, requestInfo)
        items.map { values =>
          val expected = Seq("ENGLAND", "SCOTLAND", "WALES", "NIRELAND")
          val actual = values.map(_("value"))
          assert(actual == expected)
        }
      }

      "should set a item text for the choices" - {
        val items = widget.buildItems(Future.successful(countryChoices), form(formDefinition), Seq.empty, requestInfo)
        items.map { values =>
          val expectedItems = Seq("England", "Scotland", "Wales", "Northern Ireland")
          val actualItems = values.map(_("text"))
          assert(actualItems == expectedItems)
        }
      }

      "should set its value if present in FormData" - {
        val selectedChoice = "WALES"
        val widget = buildSelect()
        widget.buildItems(Future.successful(countryChoices), form(FormData(Json.obj(widget.name -> selectedChoice)), formDef(widget)), Seq.empty, RequestInfoBuilder.local).map { args =>
          val actualChoice: Seq[Boolean] = args.map(_("selected").asInstanceOf[Boolean])
          val expectedChoice = Seq(false, false, true, false)
          assert(actualChoice == expectedChoice)
        }
      }

      "should handle keys with similar prefixes" - {
        import com.uxforms.dsl.helpers.MessageHelper._
        import com.uxforms.govukdesignsystem.dsl.constraint.ConstraintHelper._
        val choices: ListMessages = Seq("FI.label.text" -> "fi", "FIE.label.text" -> "fie", "FIER.label.text" -> "fier")

        val widget = Select.select("s", Future.successful(Messages.empty()), noConstraints, Future(choices))
        val similarItems = widget.buildItems(Future.successful(choices), form(formDefinition), Seq.empty, RequestInfoBuilder.local)

        similarItems.map { values =>
          assert(values.length == 3)
          assert(values.map(_("text")).toSet == Set("fi", "fie", "fier"))
        }
      }

    }

    "templateArgs" - {

      "should set a valid id for the surrounding formGroup" - {
        evalTemplateArgs(buildSelect()).map { args =>
          WidgetScenarios.assertAutoId(args("select.formGroup.attributes"))
        }
      }

      "should set a valid id for the select element itself" - {
        evalTemplateArgs(buildSelect()).map { args =>
          WidgetScenarios.assertElementId(args("select.id").toString)
        }
      }

      "should set the name of the widget" - {
        val widget = buildSelect()
        evalTemplateArgs(widget).map { args =>
          val actual = args("select.name")
          val expected = widget.name
          assert(actual == expected)
        }
      }

      "should allow the name to be overridden" - {
        val inputWithDefaultName = Select.select("originalName", Future.successful(Messages.empty()), noConstraints,
          Future.successful(countryChoices), SimpleExtraTemplateRenderArgs("select.name" -> "overriddenName"))
        evalTemplateArgs(inputWithDefaultName).map { args =>
          val actual = args("select.name")
          assert(actual == "overriddenName")
        }
      }

      "should add the hidden class to other classes passed in" - {
        val hiddenInput = Select.select("hiddenInput", Future.successful(Messages.empty()), noConstraints, Future.successful(choices),
          SimpleExtraTemplateRenderArgs("select.formGroup.classes" -> "custom-class another-custom-class"), AlwaysHidden)

        evalTemplateArgs(hiddenInput).map { args =>
          val actual = WidgetScenarios.parseClassProperty(args("select.formGroup.classes"))
          val expected = Set("custom-class", "another-custom-class", TemplateHelper.hiddenCssClassName)
          assert(actual == expected)
        }
      }

      "should add the hidden class to other classes passed in with a namespace" - {
        val widgetName = "hiddenInput"
        val hiddenInput = Select.select(widgetName, Future.successful(Messages.empty()), noConstraints, Future.successful(choices),
          SimpleExtraTemplateRenderArgs(s"$widgetName.select.formGroup.classes" -> "custom-class another-custom-class"), AlwaysHidden)

        evalTemplateArgs(hiddenInput).map { args =>
          val actual = WidgetScenarios.parseClassProperty(args("select.formGroup.classes"))
          val expected = Set("custom-class", "another-custom-class", TemplateHelper.hiddenCssClassName)
          assert(actual == expected)
        }
      }

      "should add a hidden class if this widget should be hidden" - {
        val hiddenSelect = Select.select("sl", Future.successful(Messages.empty()), noConstraints, Future.successful(choices),
          visibility = AlwaysHidden)
        hiddenSelect.templateArgs(form(formDef(hiddenSelect)), Seq.empty, RequestInfoBuilder.local).map(args => {
          WidgetScenarios.assertHiddenClass(args("select.formGroup.classes"))
        })
      }
    }

    "visibility" - {
      WidgetScenarios.assertVisibility(v => Select.select("n", Future.successful(Messages.empty()), noConstraints, Future.successful(choices), NoExtraRenderArgs, v))
    }

    "describedBy" - {
      val describedByKey = "select.describedBy"
      "should be omitted if there is nothing to associate with" - {
        val inputWithNoMessages = Select.select("mt", Future.successful(Messages.empty()), noConstraints, Future.successful(choices))
        evalTemplateArgs(inputWithNoMessages).map { args =>
          val actual = args.get(describedByKey)
          assert(actual.isEmpty)
        }
      }

      "should include the id of the validation error and hint elements, without any leading or trailing spaces" - {
        val req = required()(Messages.empty())
        val inputWithHint = Select.select("wh", Future.successful(tupleToMessages("wh.hint.text" -> "my hint")), noConstraints, Future.successful(choices))
        evalTemplateArgs(inputWithHint, Seq(WidgetValidationError(inputWithHint, inputWithHint.name, req))).map { args =>
          val actual = args(describedByKey)
          assert(actual == actual.asInstanceOf[String].trim)
          val parsed = WidgetScenarios.parseAriaDescribedbyProperty(actual)
          assert(parsed.size == 2)
        }
      }

      "should append the id of its hint element to any passed-in values" - {
        val inputWithExistingDescribedBy = Select.select("iwe", Future.successful(messages(Seq("iwe.hint.text" -> "hint", describedByKey -> "another_id"))), noConstraints, Future.successful(choices))
        evalTemplateArgs(inputWithExistingDescribedBy).map { args =>
          val parsed = WidgetScenarios.parseAriaDescribedbyProperty(args(describedByKey))
          assert(parsed.size == 2)
          assert(parsed.contains("another_id"))
        }
      }
    }
  }

  def evalTemplateArgs[M <: Messages[M], F <: Messages[F], G <: Messages[G]](i: Select[M, F, G], errors: ValidationErrors = Seq.empty, requestInfo: RequestInfo = RequestInfoBuilder.local): Future[Map[String, Any]] = {
    i.templateArgs(form(formDef(i)), errors, requestInfo)
  }
}
