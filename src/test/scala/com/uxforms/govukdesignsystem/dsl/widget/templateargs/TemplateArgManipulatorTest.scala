package com.uxforms.govukdesignsystem.dsl.widget.templateargs

import com.uxforms.domain.widget.Widget
import com.uxforms.domain.{ListMessages, Messages, NoExtraRenderArgs, SimpleExtraTemplateRenderArgs}
import com.uxforms.dsl.helpers.MessageHelper._
import com.uxforms.govukdesignsystem.dsl.template.TemplateHelper.buildAttribute
import com.uxforms.govukdesignsystem.dsl.test.FormBuilder.form
import com.uxforms.govukdesignsystem.dsl.test.FormDefinitionBuilder.formDef
import com.uxforms.govukdesignsystem.dsl.test.RequestInfoBuilder
import utest._

import java.util.Locale
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future


object TemplateArgManipulatorTest extends TestSuite {

  override def tests: Tests = Tests {

    implicit val locale: Locale = Locale.UK
    val f = form(formDef(Seq.empty[Widget]: _*))

    "collapseProperties" - {
      "prove example on ScalaDoc" - {
        val collapsed = TemplateArgManipulator.collapseProperties("wn", Map(
          "wn.label.text" -> "Specific text",
          "label.text" -> "Default text"
        ))
        assert(collapsed == Map("label.text" -> "Specific text"))
      }
      "should leave root keys alone" - {
        val collapsed = TemplateArgManipulator.collapseProperties("myWidget", Map("first" -> 1, "second" -> "2"))
        assert(collapsed == Map("first" -> 1, "second" -> "2"))
      }

      "should replace widget-prefixed properties" - {
        val collapsed = TemplateArgManipulator.collapseProperties("myWidget", Map("first" -> 1, "myWidget.first" -> "One", "second" -> 2, "other.first" -> "red herring"))
        assert(collapsed == Map("first" -> "One", "second" -> 2, "other.first" -> "red herring"))
      }

      "should append widget-specific classes" - {
        val collapsed = TemplateArgManipulator.collapseProperties("myWidget", Map("label.classes" -> "small hint", "myWidget.label.classes" -> "more-style"))
        assert(collapsed == Map("label.classes" -> "small hint more-style"))
      }

      "should leave classes for different components alone" - {
        val collapsed = TemplateArgManipulator.collapseProperties("myWidget", Map("a.classes" -> "a", "b.classes" -> "b"))
        assert(collapsed == Map("a.classes" -> "a", "b.classes" -> "b"))
      }

      "should merge attributes" - {
        val collapsed = TemplateArgManipulator.collapseProperties("myWidget", Map(
          "label.attributes" -> Seq(Map("attribute" -> "id", "value" -> "1"), Map("attribute" -> "spellcheck", "value" -> false)),
          "myWidget.label.attributes" -> Seq(Map("attribute" -> "spellcheck", "value" -> true))
        ))
        assert(collapsed == Map("label.attributes" -> Seq(Map("attribute" -> "id", "value" -> "1"), Map("attribute" -> "spellcheck", "value" -> true))))
      }

      "should leave attributes for different components alone" - {
        val collapsed = TemplateArgManipulator.collapseProperties("myWidget", Map("a.attributes" -> buildAttribute("id", 4), "b.attributes" -> buildAttribute("id", 5)))
        assert(collapsed == Map("a.attributes" -> buildAttribute("id", 4), "b.attributes" -> buildAttribute("id", 5)))
      }
    }

    "mergeProperties" - {
      "should append properties that end with '.classes'" - {
        val result = TemplateArgManipulator.mergeProperties(Map("a.classes" -> "first"), Map("a.classes" -> "second"))
        assert(result == Map("a.classes" -> "first second"))
      }
      "should append properties that end with '.attributes'" - {
        val attr1 = buildAttribute("data-one", "One")
        val attr2 = buildAttribute("data-two", "Two")
        val result = TemplateArgManipulator.mergeProperties(
          Map("a.attributes" -> Seq(attr1)),
          Map("a.attributes" -> Seq(attr2))
        )
        assert(result == Map("a.attributes" -> Seq(attr1, attr2)))
      }
      "should prefer properties from 'b'" - {
        val result = TemplateArgManipulator.mergeProperties(Map("a" -> "Default"), Map("a" -> "New"))
        assert(result == Map("a" -> "New"))
      }
    }

    "mergeExtraTemplateRenderArgs" - {
      val f = form(formDef(Seq.empty[Widget]: _*))

      "should append properties that end with '.classes'" - {
        val result = TemplateArgManipulator.mergeExtraTemplateRenderArgs(
          SimpleExtraTemplateRenderArgs("a.classes" -> "first"),
          SimpleExtraTemplateRenderArgs("a.classes" -> "second")
        )
        result.extraArgs(f, Seq.empty, RequestInfoBuilder.local).map(args =>
          assert(args == Map("a.classes" -> "first second"))
        )
      }
      "should append properties that end with '.attributes'" - {
        val attr1 = buildAttribute("data-one", "One")
        val attr2 = buildAttribute("data-two", "Two")
        val result = TemplateArgManipulator.mergeExtraTemplateRenderArgs(
          SimpleExtraTemplateRenderArgs("a.attributes" -> Seq(attr1)),
          SimpleExtraTemplateRenderArgs("a.attributes" -> Seq(attr2))
        )
        result.extraArgs(f, Seq.empty, RequestInfoBuilder.local).map { args =>
          assert(args == Map("a.attributes" -> Seq(attr1, attr2)))
        }
      }
      "should prefer properties from 'b'" - {
        val result = TemplateArgManipulator.mergeExtraTemplateRenderArgs(
          SimpleExtraTemplateRenderArgs("a" -> "Default"),
          SimpleExtraTemplateRenderArgs("a" -> "New")
        )
        result.extraArgs(f, Seq.empty, RequestInfoBuilder.local).map { args =>
          assert(args == Map("a" -> "New"))
        }
      }
    }

    "collapseAndMergeAll" - {

      "should merge classes across all three sources" - {
        implicit val formMessages: Future[Messages[ListMessages]] = Future.successful(Seq("a.classes" -> "from-form"))
        val messages: ListMessages = Seq("a.classes" -> "from-msgs")
        val args = SimpleExtraTemplateRenderArgs("a.classes" -> "from-args")
        val result = TemplateArgManipulator.collapseAndMergeAll("widget", Future.successful(messages), args)(f, Seq.empty, RequestInfoBuilder.local)
        result.map(r =>
          assert(r == Map("a.classes" -> "from-form from-msgs from-args"))
        )
      }

      "should collapse properties from all three sources" - {
        implicit val formMessages: Future[Messages[ListMessages]] = Future.successful(Seq("widget.label.text" -> "from-form", "label.text" -> "form-default"))
        val messages: ListMessages = Seq("widget.label.text" -> "from-msgs")
        val args = SimpleExtraTemplateRenderArgs("widget.label.text" -> "from-args")
        val result = TemplateArgManipulator.collapseAndMergeAll("widget", Future.successful(messages), args)(f, Seq.empty, RequestInfoBuilder.local)
        result.map(r =>
          assert(r == Map("label.text" -> "from-args"))
        )
      }
    }

    "mergeAll" - {
      "should prefer properties from ExtraTemplateRenderArgs, Messages and FormMessages in descending order of precedence" - {
        implicit val formMessages: Future[Messages[ListMessages]] = Future.successful(Seq("pageTitle" -> "formLevelTitle"))
        val messages: ListMessages = Seq("pageTitle" -> "messagesLevelTitle")
        val args = SimpleExtraTemplateRenderArgs("pageTitle" -> "argsLevelTitle")
        val messagesAndFormMessages = TemplateArgManipulator.mergeAll(Future.successful(messages), NoExtraRenderArgs)(f, Seq.empty, RequestInfoBuilder.local)
        val messagesAndFormMessagesAndExtraArgs = TemplateArgManipulator.mergeAll(Future.successful(messages), args)(f, Seq.empty, RequestInfoBuilder.local)
        for {
          mf <- messagesAndFormMessages
          mfA <- messagesAndFormMessagesAndExtraArgs
        } yield {
          assert(mf("pageTitle") == "messagesLevelTitle")
          assert(mfA("pageTitle") == "argsLevelTitle")
        }
      }
    }
  }
}
