package com.uxforms.govukdesignsystem.dsl.widget.templateargs

import com.uxforms.domain.SimpleExtraTemplateRenderArgs
import com.uxforms.domain.widget.Widget
import com.uxforms.govukdesignsystem.dsl.test.FormBuilder.form
import com.uxforms.govukdesignsystem.dsl.test.FormDefinitionBuilder.formDef
import com.uxforms.govukdesignsystem.dsl.test.RequestInfoBuilder
import com.uxforms.govukdesignsystem.dsl.widget.templateargs.ExtraTemplateRenderArgsMergeOps.ExtraTemplateRenderArgsOperations
import utest._

import scala.concurrent.ExecutionContext.Implicits.global

object ExtraTemplateRenderArgsMergeOpsTest extends TestSuite {

  override def tests: Tests = Tests {
    "merge" - {
      "RHS should take precedence" - {
        val merged = SimpleExtraTemplateRenderArgs("hint.text" -> "LHS") merge SimpleExtraTemplateRenderArgs("hint.text" -> "RHS")
        merged.extraArgs(form(formDef(Seq.empty[Widget]: _*)), Seq.empty, RequestInfoBuilder.local).map { args =>
          assert(args("hint.text") == "RHS")
        }
      }
    }
  }
}
