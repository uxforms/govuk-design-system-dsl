package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain.{EmptyMessages, Messages, ResourceBundleMessages}
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.test.FormBuilder._
import com.uxforms.govukdesignsystem.dsl.test.FormDefinitionBuilder._
import com.uxforms.govukdesignsystem.dsl.test.{Fixture, HtmlUtils, RequestInfoBuilder, TemplateRendererBuilder, WidgetFixtureTest}
import utest._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object DetailsTest extends TestSuite with WidgetFixtureTest[Content[_, _]] {

  private implicit val formLevelMessages: Future[EmptyMessages] = Future.successful(Messages.empty())
  private implicit val renderer: TemplateRenderer = TemplateRendererBuilder.anon
  private val messages = ResourceBundleMessages.utf8("detailsWidgetMessages", getClass.getClassLoader)

  override def tests: Tests = Tests {

    "details" - {
      val details = Details.details("myDetailsWidget", Future.successful(messages))

      "should set the details widget's summary and details" - {
        details.templateArgs(form(formDef(details)), Seq.empty, RequestInfoBuilder.local).map { actualArgs =>
          assert(actualArgs("details.summaryText") == "Dummy Summary Text")
          assert(actualArgs("details.text") == "Dummy but detailed text.")
        }
      }
    }

    "should pass all fixture scenarios" - {
      testFixtures { fixture =>
        runScenario(fixture) { scenario =>
          val result = HtmlUtils.diff(scenario, HtmlUtils.standardAttributesToIgnore)
          assert(result)
        }
      }
    }
  }

  override val componentName: String = "details"

  override def widgetFromFixture(fixture: Fixture): Content[_, _] = {
    implicit val renderer: TemplateRenderer = TemplateRendererBuilder.local
    val args = flattenExtraTemplateRenderArgs(
      fixture.optionArg[String]("summaryText", "details.summaryText"),
      fixture.optionArg[String]("summaryHtml", "details.summaryHtml"),
      fixture.optionArg[String]("text", "details.text"),
      fixture.optionArg[String]("html", "details.html"),
      fixture.optionArg[Boolean]("open", "details.open"),
      fixture.option[String]("classes").map(Details.classes),
      optionAttributes(fixture, Details.attributes)
    )
    Details.details("d", Future.successful(Messages.empty()), args)
  }
}
