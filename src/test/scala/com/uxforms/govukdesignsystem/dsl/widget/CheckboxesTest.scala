package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain._
import com.uxforms.domain.constraint.{Constraint, ValidationError, WidgetValidationError}
import com.uxforms.domain.widget.Widget
import com.uxforms.dsl.helpers.MessageHelper._
import com.uxforms.dsl.widgets.Content
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.constraint.ConstraintHelper.noConstraints
import com.uxforms.govukdesignsystem.dsl.constraint.FixedChoice
import com.uxforms.govukdesignsystem.dsl.template.TemplateHelper.buildAttribute
import com.uxforms.govukdesignsystem.dsl.test.HtmlUtils.renderAndParseHtml
import com.uxforms.govukdesignsystem.dsl.test.{Fixture, HtmlUtils, TemplateRendererBuilder, WidgetFixtureTest}
import com.uxforms.govukdesignsystem.dsl.widget.HtmlAssertions.{alwaysHidden, assertAttributes, assertVisibilityClass, form}
import play.api.libs.json._
import utest._

import java.util.Locale
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{ExecutionContext, Future}

object CheckboxesTest extends TestSuite with WidgetFixtureTest[MultipleChoice] {

  private implicit val locale: Locale = Locale.UK
  private implicit val formLevelMessages: Future[EmptyMessages] = Future.successful(Messages.empty())
  private implicit val renderer: TemplateRenderer = TemplateRendererBuilder.local

  override val componentName: String = "checkboxes"


  override val excludedFixtureNames: Set[String] = Set(
    "with falsey values", // No need to check for type safety as we're already in a typesafe language
    "items with attributes", // TODO: Add the ability to set attributes in Messages. Maybe as item.attribute.attribute-name=attribute-value ?
    "label with attributes" // TODO: Add the ability to set label attributes in Messages. Maybe as item.label.attributes.attribute-name=attribute-value?
  )

  override def tests: Tests = Tests {

    "checkboxes" - {

      val choices: Messages[ListMessages] = Seq(
        "FIRST.label.text" -> "One",
        "FIRST.hint.text" -> "1",
        "SECOND.label.text" -> "Two",
        "SECOND.hint.text" -> "2"
      )

      "should add a fixedChoice constraint by default" - {
        val checkboxes = Checkboxes.checkboxes("n", Future.successful(Messages.empty), noConstraints, Future.successful(choices))(Future.successful(Messages.empty()), TemplateRendererBuilder.anon, implicitly[ExecutionContext])
        WidgetScenarios.assertHasConstraint[FixedChoice[_]](checkboxes, FormData(Json.obj("n" -> Seq("WHOOPS"))))
      }
    }

    "should set its id attribute" - {

      val w = Checkboxes.checkboxes("cb", Future.successful(Messages.empty()), noConstraints, Future.successful(Messages.empty()))

      renderAndParseHtml(form(w), Seq.empty, w).map { e =>
        val idAttributeValue = e.attr("id")
        // w is actually a [[MultipleChoice]], the checkboxes widget we want the id for is actually its first child.
        val expectedAttribute = HtmlIdGenerator.htmlId(form(w).formDefinition, w.widgets.head)
        assert(idAttributeValue == expectedAttribute)
      }
    }


    "should set the visibility class when hidden" -
      assertVisibilityClass(
        Checkboxes.checkboxes("cb", Future.successful(Messages.empty()), noConstraints, Future.successful(Messages.empty()), Checkboxes.noChoiceWidgets, Checkboxes.formGroupClasses("my-custom-class"), alwaysHidden),
        Set("govuk-form-group", "my-custom-class")
      )

    "should set attributes" - {
      assertAttributes(
        Checkboxes.checkboxes("cb", Future.successful(Messages.empty()), noConstraints, Future.successful(Messages.empty()), Checkboxes.noChoiceWidgets, SimpleExtraTemplateRenderArgs("checkboxes.formGroup.attributes" -> Seq(buildAttribute("data-custom", "true")))),
        Map("data-custom" -> "true")
      )
    }

    "should pass all fixture scenarios" - {
      testFixtures { fixture =>
        runScenario(fixture) { scenario =>
          val result = HtmlUtils.diff(scenario, HtmlUtils.standardAttributesToIgnore)
          assert(result)
        }
      }
    }
  }

  private def widgetNameFromFixture(fixture: Fixture): String =
    fixture.options.get("name").map(_.as[String]).getOrElse("c")

  override def widgetFromFixture(fixture: Fixture): MultipleChoice = {
    val widgetName = widgetNameFromFixture(fixture)
    val choices = new ListMessages(
      fixture.options.get("items").map(_.as[Seq[Map[String, JsValue]]].flatMap { m => {
        m.get("divider") match {
          case Some(d) => Seq(ListMessageEntry("DIVIDER" -> d.as[String]))
          case None => {
            val value = m("value") match {
              case JsString(s) => s
              case JsNumber(n) => n
              case JsBoolean(b) => b
            }
            (m.get("text") match {
              case Some(t) => Seq(ListMessageEntry(s"$value.label.text" -> t.as[String]))
              case None => m.get("html") match {
                case Some(h) => Seq(ListMessageEntry(s"$value.label.html" -> h.as[String]))
                case _ => Seq.empty
              }
            }) ++
              m.get("behaviour").map(b => ListMessageEntry(s"$value.behaviour" -> b.as[String])) ++
              m.get("name").map(n => ListMessageEntry(s"$value.name" -> n.as[String])) ++
              m.get("hint").flatMap(h => (h \ "text").asOpt[String].map(hint => ListMessageEntry(s"$value.hint.text" -> hint))) ++
              m.get("disabled").map(d => ListMessageEntry(s"$value.disabled" -> d.as[Boolean].toString)) ++
              m.get("label").flatMap(l => (l \ "classes").asOpt[String].map(c => ListMessageEntry(s"$value.label.classes" -> c)))
          }
        }
      }
      }).getOrElse(Seq.empty), locale)

    val choiceWidgets = fixture.options.get("items").map {
      _.as[Seq[Map[String, JsValue]]]
        .filter(_.exists { case (s, j) => s == "conditional" && (j \ "html").asOpt[String].nonEmpty })
        .foldLeft(Map.empty[String, Widget])((result, m) => {
          val content = m("conditional").as[Map[String, String]].apply("html")
          // We need to wrap the conditional html in a custom div here as we deliberately don't do that in our own templates.
          // This is because UX Forms needs to control the visibility of elements and their visibility class.
          val checked = fixture.options.get("values").exists(_.asOpt[Seq[String]].exists(_.contains(m("value").as[String]))) ||
            m.get("checked").exists(_.asOpt[Boolean].contains(true))
          val additionalClass = if (!checked) " govuk-checkboxes__conditional--hidden" else ""
          result + (m("value").as[String] -> Content(s"""<div class="govuk-checkboxes__conditional$additionalClass">$content</div>"""))
        })
    }.getOrElse(Map.empty)

    val args = flattenExtraTemplateRenderArgs(
      fixture.options.get("fieldset").flatMap(_.asOpt[JsObject].map { o =>
        flattenExtraTemplateRenderArgs(
          (o \ "legend" \ "text").asOpt[String].map(s => SimpleExtraTemplateRenderArgs(s"$widgetName.checkboxes.legend.text" -> s)),
          (o \ "legend" \ "html").asOpt[String].map(s => SimpleExtraTemplateRenderArgs(s"$widgetName.checkboxes.legend.html" -> s)),
          (o \ "legend" \ "classes").asOpt[String].map(c => Checkboxes.legendClasses(c)),
          (o \ "legend" \ "isPageHeading").asOpt[Boolean].map(h => if (h) Checkboxes.asPageHeading else NoExtraRenderArgs),
          (o \ "classes").asOpt[String].map(c => SimpleExtraTemplateRenderArgs(s"$widgetName.checkboxes.fieldset.classes" -> c)),
          (o \ "attributes").asOpt[JsValue].map(a => SimpleExtraTemplateRenderArgs(s"$widgetName.checkboxes.fieldset.attributes" -> convertAttributes(a)))
        )
      }),
      fixture.options.get("hint").flatMap(h => (h \ "text").asOpt[String].map(hint => SimpleExtraTemplateRenderArgs(s"$widgetName.hint.text" -> hint))),
      fixture.options.get("formGroup").flatMap(fg => (fg \ "classes").asOpt[String].map(c => Checkboxes.formGroupClasses(c))),
      fixture.option[String]("classes").map(c => Checkboxes.classes(c)),
      optionAttributes(fixture, Checkboxes.attributes)
    )
    Checkboxes.checkboxes(widgetName, Future.successful(Messages.empty()), noConstraints, Future.successful(choices), choiceWidgets, args)
  }

  override def formDataFromFixture(fixture: Fixture): FormData = {
    val widgetName = widgetNameFromFixture(fixture)
    val values = fixture.option[Seq[String]]("values")
    val checkedItems = fixture.options.get("items").flatMap(_.asOpt[Seq[Map[String, JsValue]]].map(_.filter {
      _.exists { case (k, v) => k == "checked" && v.asOpt[Boolean].contains(true) }
    }.flatMap(_.get("value").map {
      case JsString(s) => s
      case JsNumber(n) => n.toString
    })))
    val unCheckedItems = fixture.options.get("items").flatMap(_.asOpt[Seq[Map[String, JsValue]]].map(_.filter {
      _.exists { case (k, v) => k == "checked" && v.asOpt[Boolean].contains(false) }
    }.flatMap(_.get("value").map {
      case JsString(s) => s
      case JsNumber(n) => n.toString
    }))).getOrElse(Seq.empty)

    val selectedItems = (values ++ checkedItems).flatten.toSeq.diff(unCheckedItems)

    FormData(Json.obj(widgetName -> selectedItems))
  }

  override def validationErrorsFromFixture(fixture: Fixture): ValidationErrors = {
    val widget = widgetFromFixture(fixture)
    val widgetName = widgetNameFromFixture(fixture)
    fixture.option[JsValue]("errorMessage").fold(Seq.empty[ValidationError])(msg =>
      Seq(WidgetValidationError(widget, widgetName,
        new Constraint {
          override def displayMessage(error: ValidationError): String = (msg \ "text").as[String]
        })
      )
    )

  }
}
