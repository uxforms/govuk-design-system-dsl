package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain.{EmptyMessages, FormData, Messages}
import com.uxforms.dsl.widgets.FileUploadMetadata
import com.uxforms.govukdesignsystem.dsl.constraint.ConstraintHelper.noConstraints
import com.uxforms.govukdesignsystem.dsl.test.FormBuilder.form
import com.uxforms.govukdesignsystem.dsl.test.FormDefinitionBuilder.formDef
import com.uxforms.govukdesignsystem.dsl.test.{RequestInfoBuilder, TemplateRendererBuilder}
import com.uxforms.govukdesignsystem.dsl.{AlwaysHidden, TemplateRenderer}
import play.api.libs.json.Json
import utest._

import java.util.{Locale, UUID}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object FileUploadInlineTest extends TestSuite {

  private implicit val renderer: TemplateRenderer = TemplateRendererBuilder.anon
  private implicit val formMessages: Future[Messages[EmptyMessages]] = Future.successful(Messages.empty())

  private val upload = FileUploadInline.fileUploadInline("fui", Future.successful(Messages.empty()), noConstraints, Set.empty)

  override def tests: Tests = Tests {

    "templateArgs" - {

      implicit val uk: Locale = Locale.UK
      val fd = formDef(upload)
      val f = form(fd)

      "should set a htmlId on the outermost formGroup" - {
        upload.templateArgs(f, Seq.empty, RequestInfoBuilder.local).map {args =>
          WidgetScenarios.assertAutoId(args("fileUpload.formGroup.attributes"))
        }
      }

      "should set an elementId on the file element" - {
        upload.templateArgs(f, Seq.empty, RequestInfoBuilder.local).map { args =>
          WidgetScenarios.assertElementId(args("fileUpload.id").toString)
        }
      }
    }

    "extract" - {
      "should return None when there is no value in FormData" - {
        val result = upload.extract(FormData())
        assert(result.isEmpty)
      }

      "should return None when there is a value but the widget is hidden" - {
        val hiddenWidget = FileUploadInline.fileUploadInline("fui", Future.successful(Messages.empty()), noConstraints, Set.empty, visibility = AlwaysHidden)
        val result = hiddenWidget.extract(FormData(Json.obj(hiddenWidget.name -> Seq(FileUploadMetadata("file.text", 2L, "text", UUID.randomUUID())))))
        assert(result.isEmpty)
      }

      "should return the value when present" - {
        val metadata = Seq(FileUploadMetadata("another.txt", 55L, "text", UUID.randomUUID()))
        val result = upload.extract(FormData(Json.obj(upload.name -> metadata)))
        assert(result.contains(metadata))
      }
    }


  }
}
