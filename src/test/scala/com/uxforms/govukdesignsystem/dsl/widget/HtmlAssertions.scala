package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain.widget.Widget
import com.uxforms.domain.{FormData, HtmlIdGenerator}
import com.uxforms.dsl.Form
import com.uxforms.dsl.widgets.WidgetVisibility
import com.uxforms.govukdesignsystem.dsl.test.HtmlUtils.renderAndParseHtml
import com.uxforms.govukdesignsystem.dsl.test.UTestUtils._
import com.uxforms.govukdesignsystem.dsl.test.{FormBuilder, FormDefinitionBuilder}
import org.jsoup.nodes.Element
import utest.assert

import scala.collection.JavaConverters.{asScalaSetConverter, collectionAsScalaIterableConverter}
import scala.concurrent.{ExecutionContext, Future}

object HtmlAssertions {

  val alwaysHidden: WidgetVisibility = new WidgetVisibility {
    override def shown(data: FormData): Boolean = false
  }

  def form(widgets: Widget*): Form =
    FormBuilder.form(FormDefinitionBuilder.formDef(widgets: _*))

  def assertId(w: Widget, getElementWithId: Element => Element = identity)(implicit ec: ExecutionContext): Future[Unit] = {
    renderAndParseHtml(form(w), Seq.empty, w).map { e =>
      val idAttributeValue = getElementWithId(e).attr("id")
      val expectedAttribute = HtmlIdGenerator.htmlId(form(w).formDefinition, w)
      assert(idAttributeValue == expectedAttribute)
    }
  }

  /**
   * @param existingClasses Any classes set on the element that should not be overwritten by the hidden class.
   */
  def assertVisibilityClass(w: Widget, existingClasses: Set[String] = Set.empty, getElementWithVisibilityClass: Element => Element = identity)(implicit ec: ExecutionContext): Future[Unit] = {
    renderAndParseHtml(form(w), Seq.empty, w).map { e =>
      val classes = getElementWithVisibilityClass(e).classNames().asScala
      val expectedClass = "hidden"
      assert(classes == (existingClasses + expectedClass))
    }
  }

  def assertAttributes(w: Widget, expectedAttributes: Map[String, Any])(implicit ec: ExecutionContext): Future[Unit] = {
    renderAndParseHtml(form(w), Seq.empty, w).map { e =>
      val actualAttributes = e.attributes().asList().asScala
        .flatMap(attribute => Map(attribute.getKey -> attribute.getValue))
        .toMap

      for ((key, value) <- expectedAttributes) {
        withClues("attribute" -> (key, value)) {
          assert(actualAttributes.get(key).contains(value))
        }
      }

    }
  }
}
