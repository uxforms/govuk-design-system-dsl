package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain._
import com.uxforms.domain.constraint.Required.required
import com.uxforms.domain.constraint.{Constraint, WidgetValidationError}
import com.uxforms.domain.widget.Widget
import com.uxforms.dsl.helpers.ConstraintHelper.noConstraints
import com.uxforms.dsl.helpers.MessageHelper.{messages, tupleToMessages}
import com.uxforms.dsl.widgets.{Content, WidgetVisibility, alwaysShown}
import com.uxforms.govukdesignsystem.dsl.test.FormBuilder.form
import com.uxforms.govukdesignsystem.dsl.test.FormDefinitionBuilder.formDef
import com.uxforms.govukdesignsystem.dsl.test.{RequestInfoBuilder, TemplateRendererBuilder}
import com.uxforms.govukdesignsystem.dsl.{AlwaysHidden, TemplateRenderer}
import play.api.libs.json.Json
import utest._

import java.util.Locale
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object CheckboxesWidgetTest extends TestSuite {

  private val choices = ResourceBundleMessages.utf8("radioGroupTestChoices", getClass.getClassLoader)
  private implicit val locale = Locale.UK
  private implicit val renderer: TemplateRenderer = TemplateRendererBuilder.anon

  private def buildCheckboxesWidget[M <: Messages[M]](name: String, msgs: Messages[M], cons: Set[Constraint], widgetChoices: Map[String, Widget], visibility: WidgetVisibility): CheckboxesWidget[M, ResourceBundleMessages, EmptyMessages] = {
    new CheckboxesWidget(name, "checkbox", Future.successful(msgs), cons, Future.successful(choices), None, widgetChoices, "checkboxes/template.mustache", NoExtraRenderArgs, visibility)(Future.successful(Messages.empty()), implicitly[TemplateRenderer])
  }

  private def buildCheckboxesWidget[M <: Messages[M]](name: String, msgs: Messages[M], cons: Set[Constraint]): CheckboxesWidget[M, ResourceBundleMessages, EmptyMessages] =
    buildCheckboxesWidget(name, msgs, cons, Map.empty, alwaysShown)

  private def buildCheckboxesWidget(name: String): CheckboxesWidget[EmptyMessages, ResourceBundleMessages, EmptyMessages] =
    buildCheckboxesWidget(name, Messages.empty(), Set.empty, Map.empty, alwaysShown)

  private def buildCheckboxesWidget(name: String, visibility: WidgetVisibility): CheckboxesWidget[EmptyMessages, ResourceBundleMessages, EmptyMessages] =
    buildCheckboxesWidget(name, Messages.empty(), Set.empty, Map.empty, visibility)

  private def buildCheckboxesWidget[M <: Messages[M]](name: String, msgs: Messages[M]): CheckboxesWidget[M, ResourceBundleMessages, EmptyMessages] =
    buildCheckboxesWidget(name, msgs, Set.empty, Map.empty, alwaysShown)

  override def tests: Tests = Tests {

    "buildItems" - {
      val widget = buildCheckboxesWidget("rw")
      val formDefinition = formDef(widget)

      val items = CheckboxesWidget.buildItems("rw", Future.successful(choices), Checkboxes.noChoiceWidgets)(form(formDefinition), Seq.empty, RequestInfoBuilder.local)

      "should preserve the order of the original choices" - {
        items.map { values =>
          val actual = values.flatMap(v => v.get("value"))
          val expected = Seq("ENGLAND", "SCOTLAND", "WALES")
          assert(actual == expected)
        }
      }

      "should set an item's label" - {
        items.map { values =>
          val actual = values.head("label.text")
          val expected = "England"
          assert(actual == expected)
        }
      }

      "should set an item's hint and flag when present" - {
        items.map { values =>
          val actualHint = values.head("hint.text")
          val expectedHint = "England's hint"
          assert(actualHint == expectedHint)

          val actualFlag = values.head("hasHint")
          val expectedFlag = true
          assert(actualFlag == expectedFlag)
        }
      }

      "should set hasHint to false when the item does not have a hint" - {
        items.map { values =>
          val actual = values(1)("hasHint")
          val expected = false
          assert(actual == expected)
        }
      }

      "should set its id to elementId for the first item, and a random value for all others" - {
        items.map { values =>
          val first = values.head
          WidgetScenarios.assertElementId(first("id"))
          values.tail.filterNot(_.contains("item.divider")).foreach { value => WidgetScenarios.assertRandomIdValue(value("id").toString)}
        }
      }

      "should add an item.divider entry with the text to display if it is a divider" - {
        items.map { values =>
          val actual = values(2)("item.divider")
          val expected = "Or"
          assert(actual == expected)
        }
      }

      "should set the checked value if the item exists in FormData already" - {
        val itemsWithFormData = CheckboxesWidget.buildItems("rw", Future.successful(choices), Checkboxes.noChoiceWidgets)(form(FormData(Json.obj(widget.name -> Seq("SCOTLAND"))), formDefinition), Seq.empty, RequestInfoBuilder.local)
        itemsWithFormData.map { values =>
          val actualEngland = values.head("checked")
          val expectedEngland = false
          assert(actualEngland == expectedEngland)

          val actualScotland = values(1)("checked")
          val expectedScotland = true
          assert(actualScotland == expectedScotland)
        }
      }

      "should render any choice widgets for each value" - {
        val choiceWidgets = Map("SCOTLAND" -> Content("For Scottish eyes only"))
        val widgetWithChoiceWidgets = buildCheckboxesWidget("rw", Messages.empty(), Set.empty, choiceWidgets, alwaysShown)
        val itemsWithChoiceWidgets = CheckboxesWidget.buildItems("rw", Future.successful(choices), choiceWidgets)(form(formDefinition), Seq.empty, RequestInfoBuilder.local)
        itemsWithChoiceWidgets.map { values =>
          assert(!values.head.contains("conditional.html"))
          assert(values.lift(1).flatMap(_.get("conditional.html")).get == "For Scottish eyes only")
        }
      }

      "should handle keys with similar prefixes" - {
        import com.uxforms.dsl.helpers.MessageHelper._
        implicit val formLevelMessages: Future[EmptyMessages] = Future.successful(Messages.empty())
        val choices: ListMessages = Seq("FI.label.text" -> "fi", "FIE.label.text" -> "fie", "FIER.label.text" -> "fier")

        val widget = new CheckboxesWidget("c", "checkboxes", Future.successful(Messages.empty()), noConstraints, Future(choices), None, Checkboxes.noChoiceWidgets, "checkboxes/template.mustache", NoExtraRenderArgs, alwaysShown)
        val similarItems = CheckboxesWidget.buildItems("rw", Future.successful(choices), Checkboxes.noChoiceWidgets)(form(formDefinition), Seq.empty, RequestInfoBuilder.local)

        similarItems.map { values =>
          assert(values.length == 3)
          assert(values.map(_("label.text")).toSet == Set("fi", "fie", "fier"))
        }
      }


    }

    "templateArgs" - {

      "should add a class to hide checkboxes when visibility is false" - {
        val widget = buildCheckboxesWidget("h", AlwaysHidden)

        widget.templateArgs(form(formDef(widget)), Seq.empty, RequestInfoBuilder.local).map { args =>
          WidgetScenarios.assertHiddenClass(args("checkboxes.formGroup.classes"))
        }
      }

      "should set the formGroup's id" - {
        val widget = buildCheckboxesWidget("w")
        widget.templateArgs(form(formDef(widget)), Seq.empty, RequestInfoBuilder.local).map { args =>
          WidgetScenarios.assertAutoId(args("checkboxes.formGroup.attributes"))
        }
      }

      "describedBy" - {
        val describedByKey = "checkboxes.fieldset.describedBy"

        "should be omitted if there is nothing to associate with" - {
          val checkboxWithNoMessages = buildCheckboxesWidget("nm")
          checkboxWithNoMessages.templateArgs(form(formDef(checkboxWithNoMessages)), Seq.empty, RequestInfoBuilder.local).map { args =>
            val actual = args.get(describedByKey)
            assert(actual.isEmpty)
          }
        }

        "should include the id of the validation error and hint elements, without any leading or trailing spaces" - {
          val req = required()(Messages.empty())
          val checkboxWithHint = buildCheckboxesWidget("wh", tupleToMessages("wh.hint.text" -> "my hint"), req)
          checkboxWithHint.templateArgs(form(formDef(checkboxWithHint)), Seq(WidgetValidationError(checkboxWithHint, checkboxWithHint.name, req)), RequestInfoBuilder.local).map { args =>
            val actual = args(describedByKey)
            assert(actual == actual.asInstanceOf[String].trim)
            val parsed = WidgetScenarios.parseAriaDescribedbyProperty(actual)
            assert(parsed.size == 2)
          }
        }

        "should append the id of its hint element to any passed-in values" - {
          val checkboxWithExistingDescribedBy = buildCheckboxesWidget("iwe", messages(Seq("iwe.hint.text" -> "hint", describedByKey -> "another_id")))
          checkboxWithExistingDescribedBy.templateArgs(form(formDef(checkboxWithExistingDescribedBy)), Seq.empty, RequestInfoBuilder.local).map { args =>
            val parsed = WidgetScenarios.parseAriaDescribedbyProperty(args(describedByKey))
            assert(parsed.size == 2)
            assert(parsed.contains("another_id"))
          }
        }
      }
    }

    "visibility" - {
      WidgetScenarios.assertVisibility(v => buildCheckboxesWidget("rw", v))
    }

    "extract" - {
      "should return None when there is no value in FormData" - {
        val result = buildCheckboxesWidget("cw").extract(FormData())
        assert(result.isEmpty)
      }

      "should return None when there is a value but the widget is hidden" - {
        val hiddenWidget = buildCheckboxesWidget("cw", Messages.empty(), noConstraints, Map.empty, AlwaysHidden)
        val result = hiddenWidget.extract(FormData(Json.obj(hiddenWidget.name -> Seq("ONE"))))
        assert(result.isEmpty)
      }

      "should return the value when present" - {
        val widget = buildCheckboxesWidget("cw")
        val result = widget.extract(FormData(Json.obj(widget.name -> Seq("TWO", "THREE"))))
        assert(result.contains(Seq("TWO", "THREE")))
      }
    }

  }

}
