package com.uxforms.govukdesignsystem.dsl.widget

import java.util.UUID

import com.uxforms.domain.FormData
import com.uxforms.domain.constraint.{Constraint, WidgetValidationError}
import com.uxforms.domain.widget.{Hideable, Validated, Widget}
import com.uxforms.dsl.widgets.{WidgetVisibility, alwaysShown}
import com.uxforms.govukdesignsystem.dsl.AlwaysHidden
import com.uxforms.govukdesignsystem.dsl.template.TemplateHelper
import utest.assert

import scala.concurrent.{ExecutionContext, Future}
import scala.reflect.runtime.universe
import scala.reflect.runtime.universe._

object WidgetScenarios {

  def assertAutoId(attributeArgument: Any): Unit = {
    val id = parseAttribute(attributeArgument, "id").get.asInstanceOf[String]
    assertAutoIdFormat(id)
  }

  def assertAutoIdFormat(id: String): Unit = {
    val expectedPrefix = "autoId_"
    assert(id.startsWith(expectedPrefix))

    val idSuffix = id.substring(expectedPrefix.length)
    val parsedId = idSuffix.toInt
    assert(parsedId >= 0)
  }

  def assertRandomIdValue(id: String): Unit = {
    val expectedPrefix = "id_"
    assert(id.startsWith(expectedPrefix))

    val idSuffix = id.substring(expectedPrefix.length)
    UUID.fromString(idSuffix)
    // If fromString didn't throw an exception then we're good
  }

  def assertElementId(idAttributeValue: Any): Unit = {
    val actualId = idAttributeValue.asInstanceOf[String]
    val expectedPrefix = "elementId_"
    assert(actualId.startsWith(expectedPrefix))

    val idSuffix = actualId.substring(expectedPrefix.length)
    val parsedId = idSuffix.toInt
    assert(parsedId >= 0)
  }

  def parseAttributeProperty(argument: Any): Seq[(String, Any)] = {
    val attributes = argument.asInstanceOf[Seq[Map[String, Any]]]
    attributes.map(a => a("attribute").toString -> a("value"))
  }

  def parseAttribute(argument: Any, attributeName: String): Option[Any] = {
    WidgetScenarios.parseAttributeProperty(argument).collectFirst { case (attribute, value) if attribute == attributeName => value }
  }

  def parseClassProperty(argument: Any): Set[String] = {
    argument.asInstanceOf[String].split(" ").toSet
  }

  def parseAriaDescribedbyProperty(argument: Any): Set[String] = {
    argument.asInstanceOf[String].split(" ").toSet
  }

  def assertHiddenClass(classArgument: Any): Unit = {
    val result = parseClassProperty(classArgument).contains(TemplateHelper.hiddenCssClassName)
    assert(result)
  }

  def assertVisibility(w: WidgetVisibility => Widget with Hideable): Unit = {
    val hiddenActual = w(AlwaysHidden).visibility.shown(FormData())
    assert(!hiddenActual)

    val shownActual = w(alwaysShown).visibility.shown(FormData())
    assert(shownActual)
  }

  def assertHasConstraint[C: TypeTag](w: Widget with Validated, dataToTriggerConstraint: FormData)(implicit executionContext: ExecutionContext): Future[Unit] = {

    def instanceToType(a: Any): universe.Type = {
      val mirror = runtimeMirror(a.getClass.getClassLoader)
      val sym = mirror.staticClass(a.getClass.getName)
      sym.selfType
    }

    def isSubType[X <: Constraint](cons: X): Boolean = {
      val tpe = instanceToType(cons)
      val c = typeTag[C].tpe
      tpe <:< c
    }

    w.validate(dataToTriggerConstraint).map { validationResult =>
      val widgetValidationErrors = validationResult.errors.collect {
        case ve@WidgetValidationError(_, _, cons) if isSubType(cons) => ve
      }
      assert(widgetValidationErrors.nonEmpty)
    }
  }

}
