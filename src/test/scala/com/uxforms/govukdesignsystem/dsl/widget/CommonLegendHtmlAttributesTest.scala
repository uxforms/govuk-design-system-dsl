package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain.widget.Widget
import com.uxforms.govukdesignsystem.dsl.styles.Typography.size.l
import com.uxforms.govukdesignsystem.dsl.test.{FormBuilder, FormDefinitionBuilder, RequestInfoBuilder}
import com.uxforms.govukdesignsystem.dsl.widget.templateargs.{CommonHtmlAttributes, CommonLegendHtmlAttributes}
import utest._

import scala.concurrent.ExecutionContext.Implicits.global

object CommonLegendHtmlAttributesTest extends TestSuite {

  object TestWidget extends CommonHtmlAttributes with CommonLegendHtmlAttributes {
    override protected val elementPrefix: String = "testWidget"
  }

  private val form = FormBuilder.form(FormDefinitionBuilder.formDef(Seq.empty[Widget]: _*))

  override def tests: Tests = Tests {
    "asPageHeading" - {
      "should set " - {
        val pageHeadingArgs = TestWidget.asPageHeading
        pageHeadingArgs.extraArgs(form, Seq.empty, RequestInfoBuilder.local).map { args =>
          assert(args("testWidget.legend.isPageHeading") == true)
          assert(args("testWidget.legend.classes") == Fieldset.fieldsetLegendSize(l))
        }
      }
    }
  }
}
