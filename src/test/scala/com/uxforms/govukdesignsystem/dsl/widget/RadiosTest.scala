package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain._
import com.uxforms.domain.constraint.{Constraint, ValidationError, WidgetValidationError}
import com.uxforms.domain.widget.Widget
import com.uxforms.dsl.helpers.MessageHelper._
import com.uxforms.dsl.widgets.Content
import com.uxforms.govukdesignsystem.dsl.constraint.ConstraintHelper.noConstraints
import com.uxforms.govukdesignsystem.dsl.constraint.FixedChoice
import com.uxforms.govukdesignsystem.dsl.test.{Fixture, HtmlUtils, TemplateRendererBuilder, WidgetFixtureTest}
import play.api.libs.json._
import utest._

import java.util.Locale
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{ExecutionContext, Future}

object RadiosTest extends TestSuite with WidgetFixtureTest[MultipleChoice] {

  implicit val locale: Locale = Locale.UK

  override val componentName: String = "radios"

  override val excludedFixtureNames: Set[String] = Set(
    "with falsey items",
    "items with attributes", // TODO: Add the ability to set attributes in Messages. Maybe as item.attribute.attribute-name=attribute-value?
    "label with attributes" // TODO: Add the ability to set label attributes in Messages. Maybe as item.label.attributes.attribute-name=attribute-value?
  )

  override def tests: Tests = Tests {

    "should pass all fixture scenarios" - {
      testFixtures { fixture =>
        runScenario(fixture) { scenario =>
          val result = HtmlUtils.diff(scenario, HtmlUtils.standardAttributesToIgnore)
          assert(result)
        }
      }
    }


    "radios" - {

      val choices: Messages[ListMessages] = Seq(
        "FIRST.label.text" -> "One",
        "FIRST.hint.text" -> "1",
        "SECOND.label.text" -> "Two",
        "SECOND.hint.text" -> "2"
      )

      "should add a fixedChoice constraint by default" - {
        val radios = Radios.radios("n", Future.successful(Messages.empty), noConstraints, Future.successful(choices))(Future.successful(Messages.empty()), null, implicitly[ExecutionContext])
        WidgetScenarios.assertHasConstraint[FixedChoice[_]](radios, FormData(Json.obj("n" -> "WHOOPS")))
      }
    }
  }

  override def widgetFromFixture(fixture: Fixture): MultipleChoice = {

    implicit val renderer = TemplateRendererBuilder.local
    implicit val formLevelMessages = Future.successful(Messages.empty())

    val widgetName = widgetNameFromFixture(fixture)
    val choices = new ListMessages(
      fixture.options.get("items").map(_.as[Seq[Map[String, JsValue]]].flatMap { m => {
        m.get("divider") match {
          case Some(d) => Seq(ListMessageEntry("DIVIDER" -> d.as[String]))
          case None => {
            val value = m("value") match {
              case JsString(s) => s
              case JsNumber(n) => n
              case JsBoolean(b) => b
            }
            (m.get("text") match {
              case Some(t) => Seq(ListMessageEntry(s"$value.label.text" -> t.as[String]))
              case None => m.get("html") match {
                case Some(h) => Seq(ListMessageEntry(s"$value.label.html" -> h.as[String]))
                case _ => Seq.empty
              }
            }) ++
              m.get("behaviour").map(b => ListMessageEntry(s"$value.behaviour" -> b.as[String])) ++
              m.get("name").map(n => ListMessageEntry(s"$value.name" -> n.as[String])) ++
              m.get("hint").flatMap(h => (h \ "text").asOpt[String].map(hint => ListMessageEntry(s"$value.hint.text" -> hint))) ++
              m.get("disabled").map(d => ListMessageEntry(s"$value.disabled" -> d.as[Boolean].toString)) ++
              m.get("label").flatMap(l => (l \ "classes").asOpt[String].map(c => ListMessageEntry(s"$value.label.classes" -> c)))
          }
        }
      }
      }).getOrElse(Seq.empty), locale)

    val choiceWidgets = fixture.options.get("items").map {
      _.as[Seq[Map[String, JsValue]]]
        .filter(_.exists { case (s, j) => s == "conditional" && (j \ "html").asOpt[String].nonEmpty })
        .foldLeft(Map.empty[String, Widget])((result, m) => {
          val content = m("conditional").as[Map[String, String]].apply("html")
          // We need to wrap the conditional html in a custom div here as we deliberately don't do that in our own templates.
          // This is because UX Forms needs to control the visibility of elements and their visibility class.
          val checked = fixture.options.get("value").exists(_.asOpt[String].contains(m("value").as[String])) ||
            m.get("checked").exists(_.asOpt[Boolean].contains(true))
          val additionalClass = if (!checked) " govuk-radios__conditional--hidden" else ""
          result + (m("value").as[String] -> Content(s"""<div class="govuk-radios__conditional$additionalClass">$content</div>"""))
        })
    }.getOrElse(Map.empty)

    val args = flattenExtraTemplateRenderArgs(
      fixture.options.get("fieldset").flatMap(_.asOpt[JsObject].map { o =>
        flattenExtraTemplateRenderArgs(
          (o \ "legend" \ "text").asOpt[String].map(s => SimpleExtraTemplateRenderArgs(s"$widgetName.radios.legend.text" -> s)),
          (o \ "legend" \ "html").asOpt[String].map(s => SimpleExtraTemplateRenderArgs(s"$widgetName.radios.legend.html" -> s)),
          (o \ "legend" \ "classes").asOpt[String].map(c => Radios.legendClasses(c)),
          (o \ "legend" \ "isPageHeading").asOpt[Boolean].map(h => if (h) Radios.asPageHeading else NoExtraRenderArgs),
          (o \ "classes").asOpt[String].map(c => SimpleExtraTemplateRenderArgs(s"$widgetName.radios.fieldset.classes" -> c)),
          (o \ "attributes").asOpt[JsValue].map(a => SimpleExtraTemplateRenderArgs(s"$widgetName.radios.fieldset.attributes" -> convertAttributes(a)))
        )
      }),
      fixture.options.get("hint").flatMap(h => (h \ "text").asOpt[String].map(hint => SimpleExtraTemplateRenderArgs(s"$widgetName.hint.text" -> hint))),
      fixture.options.get("formGroup").flatMap(fg => (fg \ "classes").asOpt[String].map(c => Radios.formGroupClasses(c))),
      fixture.option[String]("classes").map(c => Radios.classes(c)),
      optionAttributes(fixture, Radios.attributes)
    )
    Radios.radios(widgetName, Future.successful(Messages.empty()), noConstraints, Future.successful(choices), choiceWidgets, args)
  }

  def widgetNameFromFixture(fixture: Fixture): String =
    fixture.option[String]("name").getOrElse("r")

  override def formDataFromFixture(fixture: Fixture): FormData = {
    val widgetName = widgetNameFromFixture(fixture)
    val value = fixture.option[String]("value")
    val checkedItem = fixture.options.get("items").flatMap(_.asOpt[Seq[Map[String, JsValue]]].flatMap(_.find {
      _.exists { case (k, v) => k == "checked" && v.asOpt[Boolean].contains(true) }
    }.flatMap(_.get("value").map {
      case JsString(s) => s
      case JsNumber(n) => n.toString
    })))
    val unCheckedItems = fixture.options.get("items").flatMap(_.asOpt[Seq[Map[String, JsValue]]].map(_.filter {
      _.exists { case (k, v) => k == "checked" && v.asOpt[Boolean].contains(false) }
    }.flatMap(_.get("value").map {
      case JsString(s) => s
      case JsNumber(n) => n.toString
    }))).getOrElse(Seq.empty)

    checkedItem.orElse(value.filterNot(unCheckedItems.contains)).fold(FormData())(s => FormData(Json.obj(widgetName -> s)))
  }

  override def validationErrorsFromFixture(fixture: Fixture): ValidationErrors = {
    val widget = widgetFromFixture(fixture)
    val widgetName = widgetNameFromFixture(fixture)
    fixture.option[JsValue]("errorMessage").fold(Seq.empty[ValidationError])(msg =>
      Seq(WidgetValidationError(widget, widgetName,
        new Constraint {
          override def displayMessage(error: ValidationError): String = (msg \ "text").as[String]
        })
      )
    )
  }


}
