package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain.widget.Widget
import com.uxforms.domain._
import com.uxforms.dsl.Form
import com.uxforms.dsl.helpers.ContainerBuilders.PageDetails
import com.uxforms.dsl.helpers.MessageHelper._
import com.uxforms.govukdesignsystem.dsl.Page._
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.test.FormBuilder.form
import com.uxforms.govukdesignsystem.dsl.test.FormDefinitionBuilder.formDef
import com.uxforms.govukdesignsystem.dsl.test.{RequestInfoBuilder, TemplateRendererBuilder}
import com.uxforms.govukdesignsystem.dsl.widget.TaskList._
import utest._

import java.util.Locale
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{ExecutionContext, Future}

object TaskListTest extends TestSuite {

  private implicit val renderer: TemplateRenderer = TemplateRendererBuilder.anon
  private implicit val formLevelMessages: Future[EmptyMessages] = Future.successful(Messages.empty())
  private implicit val uk: Locale = Locale.UK

  private def noOpItemFunction(name: String): (Form, ValidationErrors, RequestInfo) => TaskListItem =
    (_, _, _) => TaskListItem(widget(name), widget("tag"))

  private val noValidationErrors: ValidationErrors = Seq.empty
  private val taskListMessages: Future[ListMessages] = Future.successful(Seq(
    "firstTaskListSection" -> "first heading",
    "secondTaskListSection" -> "second heading",
    "thirdTaskListSection" -> "third heading"
  ))

  override def tests: Tests = Tests {

    "buildFromPage" - {
      "should group Pages by their taskListSectionName, preserving their order as given in the FormDefinition" - {
        val fd = formDef(
          questionPageInTaskList("first", Future.successful(Messages.empty()), PageDetails(), "firstTaskListSection", noOpItemFunction("first"), Seq.empty),
          questionPageInTaskList("second", Future.successful(Messages.empty()), PageDetails(), "secondTaskListSection", noOpItemFunction("second"), Seq.empty),
          questionPageInTaskList("third", Future.successful(Messages.empty()), PageDetails(), "firstTaskListSection", noOpItemFunction("third"), Seq.empty),
          questionPageInTaskList("fourth", Future.successful(Messages.empty()), PageDetails(), "thirdTaskListSection", noOpItemFunction("fourth"), Seq.empty)
        )

        val actual = buildFromPages(taskListMessages)(form(fd), noValidationErrors, RequestInfoBuilder.local)
        assert(actual.length == 3)
        for {
          firstSectionHeading <- actual.head.heading.render(form(fd), noValidationErrors, RequestInfoBuilder.local)
          firstSectionNames <- Future.sequence(actual.head.items.map(_.name.render(form(fd), noValidationErrors, RequestInfoBuilder.local)))
          secondSectionHeading <- actual(1).heading.render(form(fd), noValidationErrors, RequestInfoBuilder.local)
          secondSectionNames <- Future.sequence(actual(1).items.map(_.name.render(form(fd), noValidationErrors, RequestInfoBuilder.local)))
          thirdSectionHeading <- actual(2).heading.render(form(fd), noValidationErrors, RequestInfoBuilder.local)
          thirdSectionNames <- Future.sequence(actual(2).items.map(_.name.render(form(fd), noValidationErrors, RequestInfoBuilder.local)))
        } yield {
          assert(firstSectionHeading == "first heading")
          assert(firstSectionNames == Seq("first", "third"))
          assert(secondSectionHeading == "second heading")
          assert(secondSectionNames == Seq("second"))
          assert(thirdSectionHeading == "third heading")
          assert(thirdSectionNames == Seq("fourth"))
        }
      }

      "should exclude Pages that do not implement IncludeInTaskList" - {
        val fd = formDef(
          questionPage("first", Future.successful(Messages.empty()), PageDetails()),
          questionPageInTaskList("second", Future.successful(Messages.empty()), PageDetails(), "firstTaskListSection", noOpItemFunction("second"), Seq.empty)
        )

        val actual = buildFromPages(taskListMessages)(form(fd), noValidationErrors, RequestInfoBuilder.local)
        assert(actual.length == 1)
        for {
          firstSectionHeading <- actual.head.heading.render(form(fd), noValidationErrors, RequestInfoBuilder.local)
        } yield {
          assert(firstSectionHeading == "first heading")
        }
      }

    }

    "templateArgs" - {

      "taskList.sections" - {

        val sections = Seq(
          TaskListSection(widget("firstSectionHeading"), Seq(
            TaskListItem(widget("firstItemName"), widget("firstItemTag")),
            TaskListItem(widget("secondItemName"), widget("secondItemTag"))
          )),
          TaskListSection(widget("secondSectionHeading"), Seq(
            TaskListItem(widget("thirdItemName"), widget("thirdItemTag"))
          ))
        )
        val tl = TaskList.taskList("tl", Future.successful(Messages.empty()), sections)

        "should automatically number each section, starting from 1" - {
          tl.templateArgs(form(formDef(Seq.empty[Widget]: _*)), noValidationErrors, RequestInfoBuilder.local).map { templateArgs =>
            val sectionEntries = templateArgs("taskList.sections").asInstanceOf[Seq[Map[String, Any]]]
            assert(sectionEntries.length == 2)
            val actual = sectionEntries.map(_ ("number").asInstanceOf[Int])
            assert(actual == Seq(1, 2))
          }
        }

        "should render the name and tag of each item" - {
          tl.templateArgs(form(formDef(Seq.empty[Widget]: _*)), noValidationErrors, RequestInfoBuilder.local).map { templateArgs =>
            val sectionEntries = templateArgs("taskList.sections").asInstanceOf[Seq[Map[String, Any]]]
            val firstSectionItems = sectionEntries.head("items").asInstanceOf[Seq[Map[String, Any]]]
            val secondSectionItems = sectionEntries(1)("items").asInstanceOf[Seq[Map[String, Any]]]

            val expectedFirstSectionItems = Seq(
              Map("name" -> "firstItemName", "tag" -> "firstItemTag"),
              Map("name" -> "secondItemName", "tag" -> "secondItemTag")
            )
            assert(firstSectionItems == expectedFirstSectionItems)

            val expectedSecondSectionItems = Seq(
              Map("name" -> "thirdItemName", "tag" -> "thirdItemTag")
            )
            assert(secondSectionItems == expectedSecondSectionItems)

          }
        }
      }
    }
  }

  private def widget(output: String): Widget = new Widget {
    override def render(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit executionContext: ExecutionContext): Future[String] = {
      Future.successful(output)
    }
  }
}
