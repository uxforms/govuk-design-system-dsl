package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain.Messages
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.test.{Fixture, HtmlUtils, TemplateRendererBuilder, WidgetFixtureTest}
import utest._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object InsetTextTest extends TestSuite with WidgetFixtureTest[Content[_, _]] {

  override def tests: Tests = Tests {

    "should pass all fixture scenarios" - {
      testFixtures { fixture =>
        runScenario(fixture) { scenario =>
          val result = HtmlUtils.diff(scenario.actual, scenario.expected, HtmlUtils.standardAttributesToIgnore: _*)
          assert(result)
        }
      }
    }
  }

  override def widgetFromFixture(fixture: Fixture): Content[_, _] = {
    implicit val templateRenderer: TemplateRenderer = TemplateRendererBuilder.local
    implicit val formLevelMessages = Future.successful(Messages.empty())

    val args = flattenExtraTemplateRenderArgs(
      fixture.optionArg[String]("text", "insetText.text"),
      fixture.optionArg[String]("html", "insetText.html"),
      fixture.option[String]("classes").map(InsetText.classes),
      optionAttributes(fixture, InsetText.attributes)
    )
    InsetText.insetText("name", Future.successful(Messages.empty), args)
  }

  override val componentName: String = "inset-text"
}
