package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.dsl.widgets.Content
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.template.TemplateHelper.buildAttribute
import com.uxforms.govukdesignsystem.dsl.test.HtmlUtils.renderAndParseHtml
import com.uxforms.govukdesignsystem.dsl.test.TemplateRendererBuilder
import com.uxforms.govukdesignsystem.dsl.widget.HtmlAssertions._
import utest._

import scala.concurrent.ExecutionContext.Implicits.global

object DivTest extends TestSuite {

  private implicit val renderer: TemplateRenderer = TemplateRendererBuilder.local

  override def tests: Tests = Tests {

    "div" - {
      "should set its id attribute" - assertId(Div.div(Seq.empty: _*))
      "should set the visibility class when hidden" -
        assertVisibilityClass(Div.div(Div.classes("my-custom-class"), alwaysHidden, Seq.empty: _*), Set("my-custom-class"))

      "should set attributes" - {
        assertAttributes(
          Div.div(Div.attributes(buildAttribute("data-custom", "true")), Seq.empty: _*),
          Map("data-custom" -> "true")
        )
      }

      "should render its widget contents" - {
        val content = """<span>This should go inside the div</span>"""
        val div = Div.div(Content(content))
        val f = form(div)
        renderAndParseHtml(f, Seq.empty, div).map { e =>
          assert(e.html() == content)
          assert(e.selectFirst("span") != null)
        }
      }

    }
  }
}


