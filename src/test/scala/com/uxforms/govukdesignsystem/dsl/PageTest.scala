package com.uxforms.govukdesignsystem.dsl

import com.uxforms.domain.{EmptyMessages, Messages}
import com.uxforms.dsl.containers
import com.uxforms.dsl.helpers.ContainerBuilders.PageDetails
import com.uxforms.govukdesignsystem.dsl.test.FormDefinitionBuilder._
import com.uxforms.govukdesignsystem.dsl.test.TemplateRendererBuilder
import utest._

import java.util.Locale
import scala.concurrent.Future

object PageTest extends TestSuite {

  private implicit val locale: Locale = Locale.UK

  override def tests: Tests = Tests {

    val pageMessages = Future.successful(Messages.empty())
    implicit val formMessages: Future[EmptyMessages] = Future.successful(Messages.empty())
    implicit val templateRenderer: TemplateRenderer = TemplateRendererBuilder.concrete("myTheme")
    val page = Page.questionPage("myPage", pageMessages, PageDetails())

    "pageArgs" - {
      "should set the assertUrl from templateRenderer's staticAssetsThemeUrl" - {
        val args = page.pageArgs(formDef(Seq.empty[containers.Section]: _*), Map.empty)
        val actual = args("assetUrl")
        val expected = templateRenderer.staticAssetsThemeUrl
        assert(actual == expected)
      }
    }
  }
}
