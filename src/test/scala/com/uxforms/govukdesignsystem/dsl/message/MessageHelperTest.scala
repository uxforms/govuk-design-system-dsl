package com.uxforms.govukdesignsystem.dsl.message

import com.uxforms.domain.widget.Widget
import com.uxforms.domain.{RequestInfo, ResourceBundleMessages, ValidationErrors}
import com.uxforms.dsl.Form
import com.uxforms.govukdesignsystem.dsl.message.MessageHelper._
import com.uxforms.govukdesignsystem.dsl.test.{FormBuilder, FormDefinitionBuilder, RequestInfoBuilder}
import utest._

import java.util.Locale
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object MessageHelperTest extends TestSuite {

  override def tests: Tests = Tests {

    val msgs = Future.successful(ResourceBundleMessages.utf8("messageHelperTestMessages", getClass.getClassLoader, Locale.UK))
    val formDef = FormDefinitionBuilder.formDef(Seq.empty[Widget]: _*)
    val form = FormBuilder.form(formDef)

    "applyNamedMessageValuesToMessageEntry" - {
      "should return an empty map if there is no such key in the Messages" - {
        applyNamedMessageValuesToMessageEntry(msgs, "whoops", (_: Form, _: ValidationErrors, _: RequestInfo) => Map.empty[String, Any])
          .extraArgs(form, Seq.empty, RequestInfoBuilder.local)
          .map(result =>
            assert(result == Map.empty)
          )
      }

      "should apply a named parameter value" - {
        applyNamedMessageValuesToMessageEntry(msgs, "named", (_: Form, _: ValidationErrors, _: RequestInfo) => Map("currentDayOfWeek" -> "Tuesday"))
          .extraArgs(form, Seq.empty, RequestInfoBuilder.local)
          .map(result =>
            assert(result == Map("named" -> "Today is Tuesday"))
          )
      }
    }

    "applyNumberedMessageValuesToMessageEntry" - {

      "should return an empty map if there is no such key in the Messages" - {
        applyNumberedMessageValuesToMessageEntry(msgs, "whoops", (_: Form, _: ValidationErrors, _: RequestInfo) => Seq.empty)
          .extraArgs(form, Seq.empty, RequestInfoBuilder.local)
          .map(result =>
            assert(result == Map.empty)
          )
      }

      "should apply a numbered parameter value" - {
        applyNumberedMessageValuesToMessageEntry(msgs, "numeric", (_: Form, _: ValidationErrors, _: RequestInfo) => Seq(2))
          .extraArgs(form, Seq.empty, RequestInfoBuilder.local)
          .map(result =>
            assert(result == Map("numeric" -> "You have 2 days remaining"))
          )
      }
    }
  }
}
