package com.uxforms.govukdesignsystem.dsl

import com.uxforms.domain.widget.{NestedWidgets, Widget}
import com.uxforms.domain.{ExtraTemplateRenderArgs, Messages, RequestInfo, SimpleExtraTemplateRenderArgs}
import com.uxforms.dsl.Form
import com.uxforms.govukdesignsystem.dsl.widget.templateargs.TemplateArgManipulator

import scala.concurrent.{ExecutionContext, Future}


class ConfirmationPage[M <: Messages[M], F <: Messages[F]](layoutTemplateName: String,
                                                           templateName: String,
                                                           messages: Future[Messages[M]],
                                                           extraTemplateRenderArgs: ExtraTemplateRenderArgs,
                                                           override val widgets: Widget*)
                                                          (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer) extends NestedWidgets with Completed {

  def templateArgs(form: Form, info: RequestInfo)(implicit ec: ExecutionContext): Future[Map[String, Any]] = {
    for {
      renderedWidgets <- renderWidgets(form, Seq.empty, info)
      consolidatedMessages <- TemplateArgManipulator.mergeAll(messages, extraTemplateRenderArgs)(form, Seq.empty, info)
    } yield {
      ContainerArgs.containerArgs(form.formDefinition, consolidatedMessages)(info.locale, renderer) ++
        Map("container.widgets" -> renderedWidgets) ++
        consolidatedMessages
    }
  }

  override def render(form: Form, requestInfo: RequestInfo)(implicit ec: ExecutionContext): Future[String] = {
    for {
      args <- templateArgs(form, requestInfo)
      renderedContent = renderer.render(templateName, args)
    } yield {
      renderer.render(layoutTemplateName, Map("content" -> renderedContent) ++ args)
    }
  }
}

object ConfirmationPage {

  def confirmationPage[M <: Messages[M], F <: Messages[F]](messages: Future[Messages[M]],
                                                           extraTemplateRenderArgs: ExtraTemplateRenderArgs,
                                                           widgets: Widget*)
                                                          (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): ConfirmationPage[M, F] = {
    new ConfirmationPage("template.mustache", "uxforms/confirmationPage.mustache", messages, mainWrapperClass ++ extraTemplateRenderArgs, widgets: _*)
  }

  val mainWrapperClass: ExtraTemplateRenderArgs = SimpleExtraTemplateRenderArgs("mainClasses" -> "govuk-main-wrapper--l")
}

