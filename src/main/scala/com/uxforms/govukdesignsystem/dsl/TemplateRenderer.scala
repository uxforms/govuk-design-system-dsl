package com.uxforms.govukdesignsystem.dsl

trait TemplateRenderer {

  def staticAssetsThemeUrl: String

  def render(templateName: String, args: Map[String, Any]): String
}