package com.uxforms.govukdesignsystem.dsl.message

import com.uxforms.domain.{ExtraTemplateRenderArgs, Messages, RequestInfo, ValidationErrors}
import com.uxforms.dsl.Form

import scala.concurrent.{ExecutionContext, Future}

import scala.reflect.runtime.universe._
object MessageHelper {

  /**
   * Overrides an entry in Messages with the same value but with arguments supplied to its MessageFormat.
   *
   * I.e. a neater way of turning
   * {{{key.text=Today is {currentDayOfWeek} }}}
   * into
   * {{{key.text=Today is Tuesday}}}
   *
   * @param msgs
   * @param messageKey The key in the given msgs to be overridden
   * @param buildMessageValues a function to build the ```messageValues``` passed to the ```getString``` function on the message entry.
   */
  def applyNamedMessageValuesToMessageEntry[T: TypeTag, M <: Messages[M]](msgs: Future[Messages[M]], messageKey: String, buildMessageValues: (Form, ValidationErrors, RequestInfo) => Map[String, Any]): ExtraTemplateRenderArgs = new ExtraTemplateRenderArgs {
    override def extraArgs(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit ec: ExecutionContext): Future[Map[String, Any]] = {
      msgs.map(m => m.getString(messageKey, buildMessageValues(form, errors, requestInfo)).map(s => Map(messageKey -> s)).getOrElse(Map.empty))
    }
  }

  /**
   * Overrides an entry in Messages with the same value but with arguments supplied to its MessageFormat.
   *
   * I.e. a neater way of turning
   * {{{key.text=You have {0,number,#} days remaining}}}
   * into
   * {{{key.text=You have 3 days remaining}}}
   *
   * @param msgs
   * @param messageKey The key in the given msgs to be overridden
   * @param buildMessageValues a function to build the ```messageValues``` passed to the ```getString``` function on the message entry.
   */
  def applyNumberedMessageValuesToMessageEntry[M <: Messages[M]](msgs: Future[Messages[M]], messageKey: String, buildMessageValues: (Form, ValidationErrors, RequestInfo) => Seq[Any]): ExtraTemplateRenderArgs = new ExtraTemplateRenderArgs {
    override def extraArgs(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit ec: ExecutionContext): Future[Map[String, Any]] = {
      msgs.map(m => m.getString(messageKey, buildMessageValues(form, errors, requestInfo)).map(s => Map(messageKey -> s)).getOrElse(Map.empty))
    }
  }

}
