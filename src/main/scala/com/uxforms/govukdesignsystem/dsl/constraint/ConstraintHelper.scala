package com.uxforms.govukdesignsystem.dsl.constraint

import com.uxforms.domain.constraint.Constraint

trait ConstraintHelper {

  val noConstraints: Seq[Constraint] = Seq.empty

  implicit def constraintAppender: Constraint => Seq[Constraint] = Seq(_)

}

object ConstraintHelper extends ConstraintHelper
