package com.uxforms.govukdesignsystem.dsl.constraint

import com.uxforms.domain
import com.uxforms.domain.constraint.{MessageConstraint, WidgetValidationError}
import com.uxforms.domain.widget.{Persisted, Widget}
import com.uxforms.domain.{EmptyMessages, FormData, Messages, ValidationResult}

import scala.concurrent.{ExecutionContext, Future}

/**
 * Constrains the selection of a Checkbox to be either the one choice marked as `exclusive`
 * or any of the other permitted choices.
 *
 * Does '''not''' mandate the presence of the field in the data, i.e. it is not also `Required`.
 *
 * @since 1.3.0
 */
case class ExclusiveChoice[C <: Messages[C], E <: Messages[E]](choices: Future[Messages[C]], errorMessages: Messages[E]) extends MessageConstraint[E] {

  private def exclusiveKey[M <: Messages[M]](cs: Messages[M]): Option[String] = {
    val keySuffix = ".behaviour"
    cs.getKeys.find(_.endsWith(keySuffix))
      .filter(key => cs.getUnformattedString(key).contains("exclusive"))
      .map(_.stripSuffix(keySuffix))
  }

  override def validate(widget: Widget with Persisted, fieldName: String, data: FormData, file: Option[domain.SubmittableFile])(implicit ec: ExecutionContext): Future[domain.ValidationResult] = {
    choices.map(msgs =>
      ValidationResult(
        data,

        exclusiveKey(msgs).fold(Seq.empty[WidgetValidationError]) { behaviourKey =>
          (data.textData \ fieldName).asOpt[Seq[String]].fold(Seq.empty[WidgetValidationError]) { values =>
            val uniqueValues = values.toSet

            uniqueValues match {
              case empty if empty.isEmpty => Seq.empty
              case onlyKey if onlyKey.size == 1 && onlyKey.head == behaviourKey => Seq.empty
              case noUnique if !noUnique.contains(behaviourKey) => Seq.empty
              case uniqueAndOthers if uniqueAndOthers.contains(behaviourKey) && uniqueAndOthers.size > 1 => Seq(
                WidgetValidationError(widget, fieldName, this)
              )
            }

          }
        }
      )
    )
  }
}

object ExclusiveChoice {

  def exclusiveChoice[C <: Messages[C], E <: Messages[E]](choices: Future[Messages[C]], errorMessages: Messages[E]): ExclusiveChoice[C, E] =
    ExclusiveChoice(choices, errorMessages)

  def exclusiveChoice[C <: Messages[C], E <: Messages[E]](choices: Future[Messages[C]]): ExclusiveChoice[C, EmptyMessages] =
    ExclusiveChoice(choices, Messages.empty())

}