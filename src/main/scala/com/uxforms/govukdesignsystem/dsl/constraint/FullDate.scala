package com.uxforms.govukdesignsystem.dsl.constraint

import com.uxforms.domain
import com.uxforms.domain.constraint.{Constraint, MessageConstraint, WidgetValidationError}
import com.uxforms.domain.widget.{DateWidget, Persisted, Widget}
import com.uxforms.domain.{FormData, Messages, ValidationResult}

import scala.concurrent.{ExecutionContext, Future}

/**
 * Constrains a widget to have all date fields present.
 *
 */
class FullDate[M <: Messages[M]](override val errorMessages: Messages[M]) extends Constraint with MessageConstraint[M] {

  override def validate(widget: Widget with Persisted, fieldName: String, data: FormData, file: Option[domain.SubmittableFile])(implicit ec: ExecutionContext): Future[ValidationResult] = {

    val day = (data.textData \ fieldName \ "day").asOpt[Short]
    val month = (data.textData \ fieldName \ "month").asOpt[Short]
    val year = (data.textData \ fieldName \ "year").asOpt[Short]

    Future.successful {
      (day, month, year) match {
        case (Some(_), Some(_), Some(_)) => ValidationResult(data, Seq.empty)
        case (None, _, Some(_)) => ValidationResult(data, Seq(WidgetValidationError(widget, DateWidget.dayField(fieldName), this)))
        case (_, None, Some(_)) => ValidationResult(data, Seq(WidgetValidationError(widget, DateWidget.monthField(fieldName), this)))
        case (d, m, None) if d.isDefined || m.isDefined => ValidationResult(data, Seq(WidgetValidationError(widget, s"$fieldName.year", this)))
        case (None, None, None) => ValidationResult(data, Seq.empty)
      }
    }

  }
}

object FullDate {
  def fullDate[M <: Messages[M]]()(implicit errorMessages: Messages[M]): FullDate[M] = new FullDate(errorMessages)
}

