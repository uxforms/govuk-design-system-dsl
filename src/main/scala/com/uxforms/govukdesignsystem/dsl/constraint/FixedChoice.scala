package com.uxforms.govukdesignsystem.dsl.constraint

import com.uxforms.domain._
import com.uxforms.domain.constraint.{MessageConstraint, Required, WidgetValidationError}
import com.uxforms.domain.widget.{Persisted, Widget}
import com.uxforms.govukdesignsystem.dsl.widget.MultipleChoiceItemsHelper.valueFromKey
import play.api.libs.json.{JsArray, JsNull, JsString}

import scala.concurrent.{ExecutionContext, Future}

/**
 * Constrains the value(s) of a widget to be among the given permitted values.
 *
 * Does '''not''' mandate the presence of the field in the data, i.e. it is not also `Required`.
 *
 * @param permittedValues The widget's value(s) must be a subset of these permitted values
 */
case class FixedChoice[M <: Messages[M]](permittedValues: Future[Set[String]], errorMessages: Messages[M]) extends MessageConstraint[M] {

  override def validate(widget: Widget with Persisted, fieldName: String, data: FormData, file: Option[SubmittableFile] = None)(implicit executionContext: ExecutionContext): Future[ValidationResult] =
    permittedValues.map(permitted =>
      if ((data.textData \ fieldName).toOption.forall {
        case JsString(s) => s.trim.isEmpty || permitted.contains(s)
        case JsArray(a) => a.flatMap(_.asOpt[String]).toSet.filter(_.trim.nonEmpty).subsetOf(permitted)
        case JsNull => true
        case _ => false
      }) ValidationResult(data, Seq.empty)
      else ValidationResult(data, Seq(WidgetValidationError(widget, fieldName, this)))
    )

}

object FixedChoice {

  def fixedChoice[C <: Messages[C], M <: Messages[M]](choices: Future[Messages[C]], messages: Messages[M])(implicit executionContext: ExecutionContext): FixedChoice[M] =
    new FixedChoice(choices.map(_.getKeys.map(valueFromKey).toSet), messages)


  def fixedChoice[C <: Messages[C]](choices: Future[Messages[C]])(implicit executionContext: ExecutionContext): FixedChoice[EmptyMessages] =
    fixedChoice(choices, Messages.empty())
}
