package com.uxforms.govukdesignsystem.dsl

import com.uxforms.domain.RequestInfo
import com.uxforms.dsl.Form

import scala.concurrent.{ExecutionContext, Future}

trait Completed {

  def render(form: Form, requestInfo: RequestInfo)(implicit ec: ExecutionContext): Future[String]

}

object Completed {

  /**
   * Can be used when a form has no defined end and so can never reach the completed section.
   */
  def noCompletedSection(): Completed = new Completed {
    override def render(form: Form, requestInfo: RequestInfo)(implicit ec: ExecutionContext): Future[String] = Future.successful("")
  }

}
