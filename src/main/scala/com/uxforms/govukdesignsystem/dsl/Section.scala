package com.uxforms.govukdesignsystem.dsl

import com.uxforms.domain._
import com.uxforms.domain.widget.{FileHandling, ValidatedWidget, Widget}
import com.uxforms.dsl.Form
import com.uxforms.dsl.containers.SubmissionType.{Continue, Previous, SubmissionType, SwitchLocale}
import com.uxforms.dsl.containers.mustache.Section.previousButtonName
import com.uxforms.dsl.containers.{RedirectAfterSubmissions, RedirectBeforeRender, SectionCanContinue, SwitchLocaleButton, Section => DslSection}
import com.uxforms.dsl.helpers.ContainerBuilders.SectionDetails
import com.uxforms.dsl.mustache.MustacheUtils.sectionWidgetVisibilityListenerUrl
import com.uxforms.govukdesignsystem.dsl.template.ErrorSummary
import com.uxforms.govukdesignsystem.dsl.widget.templateargs.TemplateArgManipulator

import java.util.Locale
import scala.concurrent.{ExecutionContext, Future}

class Section[M <: Messages[M], F <: Messages[F]](override val name: String,
                                                  messages: Future[Messages[M]],
                                                  details: SectionDetails,
                                                  layoutTemplateName: String,
                                                  sectionTemplateName: String,
                                                  beforeContent: Seq[Widget],
                                                  sectionWidgets: Widget*)
                                                 (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer) extends DslSection {

  def this(name: String, messages: Future[Messages[M]], details: SectionDetails, beforeContent: Seq[Widget], widgets: Widget*)(implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer) = {
    this(name, messages, details, "template.mustache", "uxforms/section.mustache", beforeContent, widgets: _*)
  }

  override val canContinue: SectionCanContinue = details.canContinue
  override val title: String = name
  override val receivers: Seq[DataTransformer] = details.receivers
  override val redirectBeforeRender: RedirectBeforeRender = details.redirectBeforeRender
  override val submissions: Seq[DataTransformer] = details.submissions
  override val redirectAfterSubmissions: RedirectAfterSubmissions = details.redirectAfterSubmissions
  override val widgets: Seq[Widget] = sectionWidgets ++ details.required.map(n => ValidatedWidget.requiredHiddenField(n)(Messages.empty()))

  override def getFormPostType(postedFormData: Map[String, Seq[String]]): SubmissionType =
    if (postedFormData.contains(previousButtonName)) Previous
    else if (postedFormData.contains(SwitchLocaleButton.buttonName)) SwitchLocale
    else Continue

  def sectionArgs(formDef: FormDefinition, consolidatedMessages: Map[String, Any])(implicit locale: Locale, renderer: TemplateRenderer): Map[String, Any] = {
    ContainerArgs.containerArgs(formDef, consolidatedMessages) ++
      Map(
        "widgetVisibilityListenerUrl" -> sectionWidgetVisibilityListenerUrl(formDef, this),
        // only submit form as form-data when the section contains a file upload widget
        "enctype" -> flattenWidgets.find(_.isInstanceOf[FileHandling]).map(_ => "multipart/form-data").getOrElse("application/x-www-form-urlencoded")
      )
  }

  override def render(form: Form, errors: ValidationErrors, requestInfo: RequestInfo, runtimeWidgets: Widget*)(implicit ec: ExecutionContext): Future[String] = {

    implicit val locale: Locale = requestInfo.locale
    val errorSummary = new ErrorSummary(form.formDefinition, errors)

    for {
      consolidatedArgs <- TemplateArgManipulator.mergeAll(messages, details.extraTemplateArgs)(form, errors, requestInfo)
      formMessages <- formLevelMessages
      renderedWidgets <- renderWidgets(form, errors, requestInfo, runtimeWidgets.toSeq ++ details.required.map(wn => ValidatedWidget.requiredHiddenField(wn)(formMessages)): _*)
      renderedErrorSummary <- errorSummary.render(consolidatedArgs)
      renderedBeforeContent <- Future.sequence(beforeContent.map(_.render(form, errors, requestInfo)))
      progressBar <- details.progressBar.map(_.render(form.formDefinition.sections.indexOf(this), form, errors)).getOrElse(Future.successful(""))
    } yield {

      val sArgs = sectionArgs(form.formDefinition, consolidatedArgs)

      val content = {
        val contentProps = Map(
          "container.widgets" -> renderedWidgets
        ) ++
          renderedErrorSummary.map(s => "container.errorSummary" -> s) ++
          sArgs
        renderer.render(
          sectionTemplateName,
          TemplateArgManipulator.mergeProperties(
            contentProps, consolidatedArgs
          )
        )
      }

      val props = TemplateArgManipulator.mergeProperties(
        Map(
          "beforeContent" -> renderedBeforeContent.mkString(" "),
          "content" -> content,
          "progressBar" -> progressBar
        ) ++ sArgs,
        consolidatedArgs
      )
      renderer.render(layoutTemplateName, props)
    }
  }

}

object Section {

  def section[M <: Messages[M], F <: Messages[F]](name: String,
                                                  messages: Future[Messages[M]],
                                                  details: SectionDetails,
                                                  widgets: Widget*)(implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): Section[M, F] =
    new Section(name, messages, details, Seq.empty, widgets: _*)


  def section[M <: Messages[M], F <: Messages[F]](name: String,
                                                  messages: Future[Messages[M]],
                                                  details: SectionDetails,
                                                  beforeContent: Seq[Widget],
                                                  widgets: Widget*)(implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): Section[M, F] =
    new Section(name, messages, details, beforeContent, widgets: _*)

  /**
   * Question pages pattern.
   *
   * https://design-system.service.gov.uk/patterns/question-pages/
   */
  def questionPage[M <: Messages[M], F <: Messages[F]](name: String,
                                                       messages: Future[Messages[M]],
                                                       details: SectionDetails,
                                                       widgets: Widget*)(implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): Section[M, F] =
    new Section(name, messages, details.copy(extraTemplateArgs = defaultContinueButton ++ details.extraTemplateArgs), "template.mustache", "uxforms/questionPage.mustache", Seq.empty, widgets: _*)


  /**
   * Question pages pattern.
   *
   * https://design-system.service.gov.uk/patterns/question-pages/
   */
  def questionPage[M <: Messages[M], F <: Messages[F]](name: String,
                                                       messages: Future[Messages[M]],
                                                       details: SectionDetails,
                                                       beforeContent: Seq[Widget],
                                                       widgets: Widget*)(implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): Section[M, F] =
    new Section(name, messages, details.copy(extraTemplateArgs = defaultContinueButton ++ details.extraTemplateArgs), "template.mustache", "uxforms/questionPage.mustache", beforeContent, widgets: _*)

  val mainWrapperClass: ExtraTemplateRenderArgs = SimpleExtraTemplateRenderArgs("mainClasses" -> "govuk-main-wrapper--l")
  val defaultContinueButton: ExtraTemplateRenderArgs = SimpleExtraTemplateRenderArgs("main.defaultButton" -> true)
}
