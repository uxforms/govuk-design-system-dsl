package com.uxforms.govukdesignsystem.dsl.spullara

import com.typesafe.config.{Config, ConfigFactory}
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer

import java.io.StringWriter
import scala.util.Try

class SpullaraTemplateRenderer(themeName: String, templatePath: Option[String], config: Config) extends TemplateRenderer {

  def this(themeName: String, templatePath: Option[String]) =
    this(themeName, templatePath, ConfigFactory.load().withFallback(ConfigFactory.load(getClass.getClassLoader)))


  override val staticAssetsThemeUrl: String = {
    config.getString("staticAssetsBaseURL") + "/" + themeName
  }

  protected val serverBaseUrl: String = {
    val baseUrl = config.getString("staticAssetsBaseURL")

    /* Allow templates to be resolved from one location, but the baseUrl for the theme (i.e. given to the browser) from another.
      Only relevant when running forms locally, when the form's docker container needs to resolve internally but the host machine
      needs to resolve to localhost.
     */
    val url = Try(config.getString("staticAssetsBaseServerURL")).getOrElse(baseUrl)
    if (url.endsWith("/")) url else url + "/"
  }

  protected[spullara] lazy val buildPath: String = {
    def stripTrailingSlash(str: String): String = {
      if (str.endsWith("/")) str.substring(0, str.length - 1) else str
    }

    Seq(
      Some(stripTrailingSlash(serverBaseUrl)),
      Some(stripTrailingSlash(themeName)),
      templatePath.map(stripTrailingSlash)
    ).flatten.mkString("", "/", "/")
  }

  override def render(templateName: String, args: Map[String, Any]): String = {
    val template = SCaffeineMustacheFactory.template(TemplateLocation(buildPath, templateName))
    val writer = new StringWriter()
    template.execute(writer, toJava(args))
    writer.toString
  }

  //@formatter:off
  protected[spullara] def toJava(x: Any): Any = {
    import scala.collection.JavaConverters._
    x match {
      case ml: scala.collection.MapLike[_, _, _] =>
        ml.map { case (k, v) => toJava(k) -> toJava(v)}.asJava
      case sl: scala.collection.SetLike[_, _] =>
        sl.map { toJava }.asJava
      case it: Iterable[_] =>
        it.map { toJava }.asJava
      case it: Iterator[_] =>
        toJava(it.toIterable)
      case _ => x
    }
  }
  //@formatter:on
}
