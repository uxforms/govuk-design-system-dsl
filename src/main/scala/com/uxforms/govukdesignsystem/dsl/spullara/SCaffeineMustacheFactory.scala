package com.uxforms.govukdesignsystem.dsl.spullara

import com.github.blemale.scaffeine.{LoadingCache, Scaffeine}
import com.github.mustachejava.{DefaultMustacheFactory, Mustache}
import com.uxforms.domain.env.EnvironmentHelper

import scala.concurrent.duration.DurationInt

object SCaffeineMustacheFactory {

  private def compileTemplate(location: TemplateLocation) = {
    val resolver = new HttpMustacheResolver(location.path)
    val factory = new DefaultMustacheFactory(resolver)
    factory.compile(location.template)
  }

  private val cache: LoadingCache[TemplateLocation, Mustache] = {
    Scaffeine()
      .refreshAfterWrite(2.minutes)
      .build(compileTemplate)
  }

  private val env = new EnvironmentHelper().env

  def template(location: TemplateLocation): Mustache = {
    if (env == "local") compileTemplate(location)
    else cache.get(location)
  }

}

case class TemplateLocation(path: String, template: String)
