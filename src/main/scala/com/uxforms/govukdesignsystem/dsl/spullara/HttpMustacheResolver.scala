package com.uxforms.govukdesignsystem.dsl.spullara

import java.io.{InputStreamReader, Reader}
import java.net.URL

import com.github.mustachejava.{MustacheException, MustacheResolver}
import uk.co.bigbeeconsultants.http.HttpClient
import uk.co.bigbeeconsultants.http.response.{Response, Status}

class HttpMustacheResolver(baseUri: String) extends MustacheResolver {

  private val httpClient = new HttpClient

  override def getReader(resourceName: String): Reader = {
    val response = get(resourceName)
    response.status match {
      case Status.S200_OK => new InputStreamReader(response.body.inputStream)
      case Status.S404_NotFound => throw new MustacheException(s"Template $resourceName not found at $baseUri")
      case sc@_ => throw new MustacheException(s"Unexpected response code ${sc.code} for $resourceName at $baseUri")
    }
  }

  protected[spullara] def head(resourceName: String): Response =
    httpClient.head(new URL(baseUri + resourceName))

  protected[spullara] def get(resourceName: String): Response =
    httpClient.get(new URL(baseUri + resourceName))

  def etag(resourceName: String): Option[String] = {
    val response = head(resourceName)
    response.status match {
      case Status.S200_OK => response.headers.etagHdr.map(_.value)
      case _ => None
    }
  }
}