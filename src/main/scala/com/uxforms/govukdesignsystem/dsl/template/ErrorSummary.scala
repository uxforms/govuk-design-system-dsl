package com.uxforms.govukdesignsystem.dsl.template

import java.util.Locale

import com.uxforms.domain.constraint.{GlobalValidationError, WidgetValidationError}
import com.uxforms.domain.{FormDefinition, HtmlIdGenerator, ValidationErrors}
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.template.TemplateHelper.widgetValidationMessage

import scala.concurrent.{ExecutionContext, Future}

class ErrorSummary(formDef: FormDefinition, errors: ValidationErrors, templateName: String = "error-summary/template.mustache") extends Template {

  override def render(args: Map[String, Any])(implicit renderer: TemplateRenderer, locale: Locale, executionContext: ExecutionContext): Future[Option[String]] = {

    Future.successful {
      if (errors.nonEmpty) {
        Some(renderer.render(templateName, buildArgs(args)))
      } else {
        None
      }
    }
  }

  def buildArgs(args: Map[String, Any])(implicit locale: Locale): Map[String, Any] = {
    val x = errors.map {
      case GlobalValidationError(validationMessage) => Map("item.text" -> validationMessage)
      case wve@WidgetValidationError(widget, fieldName, cons) => Map("item.href" -> s"#${HtmlIdGenerator.elementId(formDef, widget)}", "item.text" -> widgetValidationMessage(args, wve))
    }
    Map("errorSummary.errorList" -> x) ++
      args.get("errorSummary.disableAutoFocus").map(_ => "errorSummary.hasDisableAutoFocus" -> true) ++
      args
  }
}
