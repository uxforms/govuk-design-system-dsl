package com.uxforms.govukdesignsystem.dsl.template

import java.util.{Locale, UUID}

import com.uxforms.domain.FormDefinition
import com.uxforms.domain.constraint.{MessageConstraint, WidgetValidationError}
import com.uxforms.dsl.mustache.MustacheUtils
import com.uxforms.i8n.MessageFormatter

trait TemplateHelper {

  def hiddenCssClassName = "hidden"

  /**
   * De-references widget-specific entries and overrides more general entries.
   * e.g. the value at key `hint.text` will be overridden by the value at `myWidget.hint.text` given
   * the widgetName of `myWidget` and suffix of `hint`.
   */
  def overrideWithWidgetSpecificEntries(widgetName: String, suffix: String, args: Map[String, Any]): Map[String, Any] = {
    args ++ args.collect {
      case (key, value) if key.startsWith(s"$widgetName.$suffix.") => key.substring(widgetName.length + 1, key.length) -> value
    }
  }

  /**
   * Calculates the error message to be rendered for a given WidgetValidationError.
   * Looks for messages in this order of preference:
   *
   * 1. {fieldName}.{errorMessageKey} (e.g. myWidget.constraint.required.errorMessage)
   * 2. {errorMessageKey} (e.g. constraint.required.errorMessage)
   *
   * And if nothing was found then falls back to calling `displayMessage` on the constraint.
   */
  def widgetValidationMessage(args: Map[String, Any], error: WidgetValidationError)(implicit locale: Locale): String = {

    val safeReportingName = error.cons.reportingName.toLowerCase(Locale.UK).replaceAll("\\s", "")
    val specificMessage = error.cons match {
      case mc: MessageConstraint[_] => args.get(s"${error.fieldName}.${mc.errorMessageKey}").orElse(args.get(mc.errorMessageKey)).map(msg => format(msg.toString, Seq(error) ++ mc.extraErrorMessageArgs))
      case _ => args.get(s"${error.fieldName}.$safeReportingName").orElse(args.get(safeReportingName))
    }
    specificMessage.map(_.toString).getOrElse(error.cons.displayMessage(error))
  }

  protected[template] def format(msg: String, msgArgs: Seq[Any])(implicit locale: Locale): String = {
    new MessageFormatter(locale).format(msg, msgArgs.asInstanceOf[Seq[Object]].toArray)
  }

  def visibilityClass(isShown: Boolean): String = {
    if (isShown) "" else hiddenCssClassName
  }

  /**
   * Class parameters are a little different as they're expected to be a space-delimited list of strings.
   *
   * ```Map("myprefix.classes" -> "class1 class2 class3")```
   *
   * So we need to treat them differently in order to be able to add classes to a key
   * that potentially already has some classes set.
   */
  def appendClasses(prefix: String, args: Map[String, Any], additionalClasses: String, suffix: String = "classes"): Map[String, Any] = {
    val key = s"$prefix.$suffix"
    args ++ args.collectFirst {
      case (k, v: String) if k == key => Map(key -> s"$v $additionalClasses")
    }.getOrElse(Map(key -> additionalClasses))
  }

  /**
   * Attribute parameters are a little different as they're expected to be a map:
   *
   * ```Seq(Map("attributes" -> "myAttributeName", "value" -> "myAttributeValue"))```
   *
   * So we need to treat them differently in order to be able to add attributes to a key
   * that potentially already has some attributes set.
   */
  def appendAttributes(prefix: String, args: Map[String, Any], additionalAttributes: Seq[Map[String, Any]], suffix: String = "attributes"): Map[String, Any] = {
    val key = s"$prefix.$suffix"

    args ++ args.collectFirst {
      case (k, v: Seq[_]) if k == key => Map(k -> (v ++ additionalAttributes))
    }.getOrElse(Map(key -> additionalAttributes))
  }

  def buildAttribute(attributeName: String, attributeValue: Any): Map[String, Any] =
    Map("attribute" -> attributeName, "value" -> attributeValue)


  /**
   * Assumes the page's urlPath is a simple string. Does not work for regex-based page paths.
   */
  def pagePath(fd: FormDefinition, urlPath: String) =
    s"${MustacheUtils.pageRootUrl(fd)}/$urlPath"

  def filePath(fd: FormDefinition, uuid: UUID): String =
    s"${MustacheUtils.formPath(fd)}/preview/${uuid.toString}"



}

object TemplateHelper extends TemplateHelper
