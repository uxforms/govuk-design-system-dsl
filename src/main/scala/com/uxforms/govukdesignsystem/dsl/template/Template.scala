package com.uxforms.govukdesignsystem.dsl.template

import java.util.Locale

import com.uxforms.govukdesignsystem.dsl.TemplateRenderer

import scala.concurrent.{ExecutionContext, Future}

trait Id {
  def id: String
}

trait Template {

  def render(args: Map[String, Any])(implicit renderer: TemplateRenderer, locale: Locale, executionContext: ExecutionContext): Future[Option[String]]
}
