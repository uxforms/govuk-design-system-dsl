package com.uxforms.govukdesignsystem.dsl.template

object i18n {

  def govukPluralisedAttributes(translationKey: String, pluralForms: Seq[(String, String)]): Seq[String] = {
    pluralForms.map(pf => s"""data-i18n.$translationKey.${pf._1}="${pf._2}"""")
  }

}
