package com.uxforms.govukdesignsystem.dsl.template

import java.util.Locale

import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.template.TemplateHelper.overrideWithWidgetSpecificEntries

import scala.concurrent.{ExecutionContext, Future}

class Hint(widgetId: String, widgetName: String, templateName: String = "hint/template.mustache", suffix: String = "hint") extends Template with Id {

  override def render(args: Map[String, Any])(implicit renderer: TemplateRenderer, locale: Locale, executionContext: ExecutionContext): Future[Option[String]] = {
    val builtArgs = buildArgs(args)
    Future.successful(
      if (shouldRenderHint(builtArgs)) {
        Some(renderer.render(templateName, builtArgs))
      } else None
    )
  }

  def shouldRenderHint(args: Map[String, Any]): Boolean = {
    args.contains("hint.text") || args.contains("hint.html")
  }

  override val id = s"$widgetId-$suffix"

  def buildArgs(args: Map[String, Any]): Map[String, Any] = {
    Map("hint.id" -> id) ++
      args ++
      overrideWithWidgetSpecificEntries(widgetName, suffix, args)
  }


}
