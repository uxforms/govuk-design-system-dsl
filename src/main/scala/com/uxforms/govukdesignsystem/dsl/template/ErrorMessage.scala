package com.uxforms.govukdesignsystem.dsl.template

import java.util.Locale

import com.uxforms.domain.constraint.WidgetValidationError
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.template.TemplateHelper.{overrideWithWidgetSpecificEntries, widgetValidationMessage}

import scala.concurrent.{ExecutionContext, Future}

class ErrorMessage(widgetId: String, widgetName: String, errors: Seq[WidgetValidationError], templateName: String = "error-message/template.mustache") extends Template with Id {

  protected val suffix = "errorMessage"

  override val id: String = s"$widgetId-error"

  override def render(args: Map[String, Any])(implicit renderer: TemplateRenderer, locale: Locale, executionContext: ExecutionContext): Future[Option[String]] = {

    Future.successful(
      if (errors.nonEmpty) {
        Some(renderer.render(templateName, buildArgs(args)))
      } else {
        None
      }
    )
  }

  protected[template] def buildArgs(args: Map[String, Any])(implicit locale: Locale): Map[String, Any] = {
    val mergedArgs = args ++ overrideWithWidgetSpecificEntries(widgetName, suffix, args)
    Map("errorMessage.id" -> id,
      "errorMessage.html" -> errors.map(e => widgetValidationMessage(args, e)).mkString("<br />"),
      "errorMessage.visuallyHiddenText" -> mergedArgs.getOrElse("errorMessage.visuallyHiddenText", "Error")
    ) ++
      mergedArgs
  }


}
