package com.uxforms.govukdesignsystem.dsl.template

import com.uxforms.domain.{ExtraTemplateRenderArgs, SimpleExtraTemplateRenderArgs}
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.styles.Typography
import com.uxforms.govukdesignsystem.dsl.styles.Typography.size.l
import com.uxforms.govukdesignsystem.dsl.template.TemplateHelper.overrideWithWidgetSpecificEntries
import com.uxforms.govukdesignsystem.dsl.widget.templateargs.CommonHtmlAttributes

import java.util.Locale
import scala.concurrent.{ExecutionContext, Future}

class Label(widgetId: String, widgetName: String, templateName: String = "label/template.mustache") extends Template {

  private val suffix = "label"

  override def render(args: Map[String, Any])(implicit renderer: TemplateRenderer, locale: Locale, executionContext: ExecutionContext): Future[Option[String]] =
    Future.successful {
      val enhancedArgs = buildArgs(args)
      if (shouldRenderLabel(enhancedArgs)) {
        Some(renderer.render(templateName, enhancedArgs))
      } else None
    }

  private def shouldRenderLabel(args: Map[String, Any]): Boolean =
    args.contains("label.html") || args.contains("label.text")

  def buildArgs(args: Map[String, Any]): Map[String, Any] =
    Map("label.for" -> widgetId) ++
      args ++
      overrideWithWidgetSpecificEntries(widgetName, suffix, args)
}

object Label extends CommonHtmlAttributes {

  override protected val elementPrefix: String = "label"

  val asPageHeading: ExtraTemplateRenderArgs =
    SimpleExtraTemplateRenderArgs(s"$elementPrefix.isPageHeading" -> true, s"$elementPrefix.classes" -> labelSize(l))

  import Typography.size._

  /**
   * Generates a css class name for the size of a label.
   * Typically passed as an argument to [[Label.classes]].
   */
  def labelSize(size: size): String = s"govuk-label--$size"

}