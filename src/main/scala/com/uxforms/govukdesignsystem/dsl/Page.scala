package com.uxforms.govukdesignsystem.dsl

import com.uxforms.domain._
import com.uxforms.domain.widget.{FileHandling, Widget}
import com.uxforms.dsl.Form
import com.uxforms.dsl.containers.{RedirectAfterSubmissions, RedirectBeforeRender, Page => DslPage}
import com.uxforms.dsl.helpers.ContainerBuilders.PageDetails
import com.uxforms.dsl.mustache.MustacheUtils.pageWidgetVisibilityListenerUrl
import com.uxforms.govukdesignsystem.dsl.template.ErrorSummary
import com.uxforms.govukdesignsystem.dsl.widget.templateargs.TemplateArgManipulator
import com.uxforms.govukdesignsystem.dsl.widget.{IncludeInTaskList, TaskListItem}

import java.util.Locale
import scala.concurrent.{ExecutionContext, Future}

class Page[M <: Messages[M], F <: Messages[F]](override val urlPath: String,
                                               messages: Future[Messages[M]],
                                               details: PageDetails,
                                               layoutTemplateName: String,
                                               pageTemplateName: String,
                                               beforeContent: Seq[Widget],
                                               pageWidgets: Widget*)
                                              (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer)
  extends DslPage {

  override val title: String = urlPath
  override val receivers: Seq[DataTransformer] = details.receivers
  override val redirectBeforeRender: RedirectBeforeRender = details.redirectBeforeRender
  override val submissions: Seq[DataTransformer] = details.submissions
  override val redirectAfterSubmissions: RedirectAfterSubmissions = details.redirectAfterSubmissions
  override val widgets: Seq[Widget] = pageWidgets

  def pageArgs(formDef: FormDefinition, consolidatedArgs: Map[String, Any])(implicit locale: Locale, renderer: TemplateRenderer): Map[String, Any] = {
    ContainerArgs.containerArgs(formDef, consolidatedArgs) ++
      Map(
        "widgetVisibilityListenerUrl" -> pageWidgetVisibilityListenerUrl(formDef, this),
        // only submit form as form-data when the section contains a file upload widget
        "enctype" -> flattenWidgets.find(_.isInstanceOf[FileHandling]).map(_ => "multipart/form-data").getOrElse("application/x-www-form-urlencoded")
      )
  }

  override def render(form: Form, errors: ValidationErrors, requestInfo: RequestInfo, runtimeWidgets: Widget*)(implicit ec: ExecutionContext): Future[String] = {

    implicit val locale: Locale = requestInfo.locale
    val errorSummary = new ErrorSummary(form.formDefinition, errors)

    for {
      consolidatedArgs <- TemplateArgManipulator.mergeAll(messages, details.extraTemplateArgs)(form, errors, requestInfo)
      renderedWidgets <- renderWidgets(form, errors, requestInfo, runtimeWidgets.toSeq: _*)
      renderedErrorSummary <- errorSummary.render(consolidatedArgs)
      renderedBeforeContent <- Future.sequence(beforeContent.map(_.render(form, errors, requestInfo)))
    } yield {

      val pArgs = pageArgs(form.formDefinition, consolidatedArgs)

      val content = {
        val contentProps = Map(
          "container.widgets" -> renderedWidgets
        ) ++
          renderedErrorSummary.map(s => "container.errorSummary" -> s) ++
          pArgs
        renderer.render(
          pageTemplateName,
          TemplateArgManipulator.mergeProperties(
            contentProps, consolidatedArgs
          )
        )
      }

      val props = TemplateArgManipulator.mergeProperties(
        Map(
          "beforeContent" -> renderedBeforeContent.mkString(" "),
          "content" -> content
        ) ++ pArgs,
        consolidatedArgs
      )

      renderer.render(layoutTemplateName, props)
    }
  }

}

object Page {

  def page[M <: Messages[M], F <: Messages[F]](urlPath: String,
                                               messages: Future[Messages[M]],
                                               details: PageDetails,
                                               layoutTemplateName: String,
                                               pageTemplateName: String,
                                               beforeContent: Seq[Widget],
                                               pageWidgets: Widget*)
                                              (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): Page[M, F] = {
    new Page(urlPath, messages, details, layoutTemplateName, pageTemplateName, beforeContent, pageWidgets: _*)
  }

  def page[M <: Messages[M], F <: Messages[F]](urlPath: String,
                                               messages: Future[Messages[M]],
                                               details: PageDetails,
                                               layoutTemplateName: String,
                                               pageTemplateName: String,
                                               pageWidgets: Widget*)
                                              (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): Page[M, F] = {
    new Page(urlPath, messages, details, layoutTemplateName, pageTemplateName, Seq.empty, pageWidgets: _*)
  }

  /**
   * Question pages pattern.
   *
   * https://design-system.service.gov.uk/patterns/question-pages/
   */
  def questionPage[M <: Messages[M], F <: Messages[F]](urlPath: String,
                                                       messages: Future[Messages[M]],
                                                       details: PageDetails,
                                                       beforeContent: Seq[Widget],
                                                       pageWidgets: Widget*)
                                                      (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): Page[M, F] = {
    new Page(urlPath, messages, details.copy(extraTemplateArgs = defaultContinueButton ++ details.extraTemplateArgs), "template.mustache", "uxforms/questionPage.mustache", beforeContent, pageWidgets: _*)
  }

  /**
   * Question pages pattern.
   *
   * https://design-system.service.gov.uk/patterns/question-pages/
   */
  def questionPage[M <: Messages[M], F <: Messages[F]](urlPath: String,
                                                       messages: Future[Messages[M]],
                                                       details: PageDetails,
                                                       pageWidgets: Widget*)
                                                      (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): Page[M, F] = {
    new Page(urlPath, messages, details.copy(extraTemplateArgs = defaultContinueButton ++ details.extraTemplateArgs), "template.mustache", "uxforms/questionPage.mustache", Seq.empty, pageWidgets: _*)
  }

  /**
   * Question pages pattern to be included within a Task List pattern.
   *
   * Note: This does not define a page that ''displays'' a task list, but one that should
   * be shown as a step ''in'' a task list.
   *
   * [[https://design-system.service.gov.uk/patterns/question-pages/]]
   * [[https://design-system.service.gov.uk/patterns/task-list-pages/]]
   *
   * @since 1.2.0
   */
  def questionPageInTaskList[M <: Messages[M], F <: Messages[F]](urlPath: String,
                                                                 messages: Future[Messages[M]],
                                                                 details: PageDetails,
                                                                 taskListSectionName: String,
                                                                 taskListItemFunction: (Form, ValidationErrors, RequestInfo) => TaskListItem,
                                                                 beforeContent: Seq[Widget],
                                                                 pageWidgets: Widget*)
                                                                (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): Page[M, F] = {
    val renamedTaskListSectionNameToAvoidShadowing = taskListSectionName
    new Page(urlPath, messages, details, "template.mustache", "uxforms/questionPage.mustache", beforeContent, pageWidgets: _*) with IncludeInTaskList {
      override val taskListSectionName: String = renamedTaskListSectionNameToAvoidShadowing

      override def taskListItem(form: Form, errors: ValidationErrors, requestInfo: RequestInfo): TaskListItem = taskListItemFunction(form, errors, requestInfo)
    }
  }

  /**
   * Error pages pattern.
   *
   * https://design-system.service.gov.uk/patterns/page-not-found-pages/
   * https://design-system.service.gov.uk/patterns/problem-with-the-service-pages/
   * https://design-system.service.gov.uk/patterns/service-unavailable-pages/
   */
  def errorPage[M <: Messages[M], F <: Messages[F]](urlPath: String,
                                                    messages: Future[Messages[M]],
                                                    details: PageDetails,
                                                    pageWidgets: Widget*)
                                                   (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): Page[M, F] = {
    new Page(urlPath, messages, details.copy(extraTemplateArgs = mainWrapperClass ++ details.extraTemplateArgs),
      "template.mustache", "uxforms/errorPage.mustache", Seq.empty, pageWidgets: _*)
  }

  val mainWrapperClass: ExtraTemplateRenderArgs = SimpleExtraTemplateRenderArgs("mainClasses" -> "govuk-main-wrapper--l")
  val defaultContinueButton: ExtraTemplateRenderArgs = SimpleExtraTemplateRenderArgs("main.defaultButton" -> true)
}