package com.uxforms.govukdesignsystem.dsl

import java.util.Locale

import com.uxforms.domain.FormDefinitionTypes.{AccessValidationFunction, HttpStatusCode}
import com.uxforms.domain.{DataTransformer, FormDefinition, FormDefinitionName, Messages, RequestInfo}
import com.uxforms.dsl.{Form, containers}

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration.Duration

trait FormDefinitionBuilder {

  def formDefinition[F <: Messages[F]](formDefinitionName: FormDefinitionName,
                                       formLevelMessages: Messages[F],
                                       resolvedLocale: Locale,
                                       formDefinitionPages: Seq[containers.Page],
                                       formDefinitionResumePages: Seq[containers.Page],
                                       formDefinitionSubmissions: Seq[DataTransformer],
                                       dataRetention: Duration,
                                       completed: Completed,
                                       formDefinitionSections: containers.Section*
                                      ): FormDefinition =
    new FormDefinition {
      override val name: FormDefinitionName = formDefinitionName
      override val messages: Messages[_] = formLevelMessages
      override val locale: Locale = resolvedLocale
      override val pages: Seq[containers.Page] = formDefinitionPages

      override val resumePages: Seq[containers.Page] = formDefinitionResumePages
      override val submissions: Seq[DataTransformer] = formDefinitionSubmissions
      override val inflightDataRetention: Duration = dataRetention
      override val sections: Seq[containers.Section] = formDefinitionSections

      override def renderComplete(form: Form, requestInfo: RequestInfo)(implicit ec: ExecutionContext): Future[String] =
        completed.render(form, requestInfo)
    }

  def formDefinition[F <: Messages[F]](formDefinitionName: FormDefinitionName,
                                       formLevelMessages: Messages[F],
                                       resolvedLocale: Locale,
                                       formDefinitionPages: Seq[containers.Page],
                                       formDefinitionResumePages: Seq[containers.Page],
                                       formDefinitionSubmissions: Seq[DataTransformer],
                                       dataRetention: Duration,
                                       completed: Completed,
                                       formDefinitionAccess: AccessValidationFunction,
                                       formDefinitionErrorPages: PartialFunction[Either[HttpStatusCode, Throwable], containers.Page],
                                       formDefinitionSections: containers.Section*
                                      ): FormDefinition =
    new FormDefinition {
      override val name: FormDefinitionName = formDefinitionName
      override val messages: Messages[_] = formLevelMessages
      override val locale: Locale = resolvedLocale
      override val pages: Seq[containers.Page] = formDefinitionPages

      override val resumePages: Seq[containers.Page] = formDefinitionResumePages
      override val submissions: Seq[DataTransformer] = formDefinitionSubmissions
      override val inflightDataRetention: Duration = dataRetention
      override val sections: Seq[containers.Section] = formDefinitionSections

      override val validateAccess: AccessValidationFunction = formDefinitionAccess
      override val errorPages: PartialFunction[Either[HttpStatusCode, Throwable], containers.Page] = formDefinitionErrorPages

      override def renderComplete(form: Form, requestInfo: RequestInfo)(implicit ec: ExecutionContext): Future[String] =
        completed.render(form, requestInfo)
    }
}

object FormDefinitionBuilder extends FormDefinitionBuilder