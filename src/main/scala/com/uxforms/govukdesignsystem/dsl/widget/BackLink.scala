package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain._
import com.uxforms.dsl.Form
import com.uxforms.dsl.widgets.{WidgetVisibility, alwaysShown}
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.widget.templateargs.CommonHtmlAttributes

import scala.concurrent.{ExecutionContext, Future}

/**
 * Implementation of GOV.UK Design System's [[https://design-system.service.gov.uk/components/back-link/ Back link component]]
 *
 * Use the button variant to save the user's answers before going back, without triggering validation errors.
 *
 * {{{
 *   BackLink.buttonToPreviousSection("back", messages)
 * }}}
 *
 * When on a Page, use the linkToLatestSection variant to go back to the latest section in the journey.
 *
 * {{{
 *   BackLink.linkToLatestSection("back", messages)
 * }}}
 *
 * It's most likely you'll want a back link on most views within the form, so it may make most sense to put the label
 * for this in formMessages:
 *
 * {{{
 *   backLink.text=Back
 * }}}
 */
object BackLink extends CommonHtmlAttributes {

  override protected val elementPrefix: String = "backLink"

  def linkToEarlierSection[M <: Messages[M], F <: Messages[F]](name: String, messages: Future[Messages[M]], sectionName: String, extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs, visibility: WidgetVisibility = alwaysShown)(implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer) =
    new Content(name, "backLink", messages, "back-link/template.mustache", sectionHref(sectionName) ++ extraTemplateRenderArgs, visibility)

  def buttonToPreviousSection[M <: Messages[M], F <: Messages[F]](name: String, messages: Future[Messages[M]], extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs, visibility: WidgetVisibility = alwaysShown)(implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer) =
    new Content(name, "backLink", messages, "back-link/template.mustache", asButton ++ extraTemplateRenderArgs, visibility)

  def linkToLatestSection[M <: Messages[M], F <: Messages[F]](name: String, messages: Future[Messages[M]], extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs, visibility: WidgetVisibility = alwaysShown)(implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer) =
    new Content(name, "backLink", messages, "back-link/template.mustache", formHref ++ extraTemplateRenderArgs, visibility)

  def sectionHref(sectionName: String): ExtraTemplateRenderArgs = new ExtraTemplateRenderArgs {
    override def extraArgs(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit ec: ExecutionContext): Future[Map[String, Any]] = {
      Future.successful {
        val index = form.formDefinition.sections.indexWhere(_.name == sectionName)
        href(s"/${form.formDefinition.name}/section/$index/reopen")
      }
    }
  }

  val formHref: ExtraTemplateRenderArgs = new ExtraTemplateRenderArgs {
    override def extraArgs(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit ec: ExecutionContext): Future[Map[String, Any]] = {
      Future.successful(href(s"/${form.formDefinition.name}"))
    }
  }

  val asButton: ExtraTemplateRenderArgs =
    SimpleExtraTemplateRenderArgs(s"$elementPrefix.isButton" -> true)

  def href(target: String): Map[String, Any] =
    Map(s"$elementPrefix.href" -> target)

}
