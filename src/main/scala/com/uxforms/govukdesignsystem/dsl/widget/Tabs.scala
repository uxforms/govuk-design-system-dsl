package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain.MessagesOps.MessagesOperations
import com.uxforms.domain._
import com.uxforms.domain.widget._
import com.uxforms.dsl.Form
import com.uxforms.dsl.widgets.{WidgetVisibility, alwaysShown}
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.template.TemplateHelper._
import com.uxforms.govukdesignsystem.dsl.widget.templateargs.{CommonHtmlAttributes, TemplateArgManipulator}
import play.api.libs.json.{JsObject, Json}

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try

class Tabs[M <: Messages[M], F <: Messages[F]](override val name: String,
                                               messages: Future[Messages[M]],
                                               templateName: String,
                                               tabContents: Seq[Widget],
                                               persistState: Boolean,
                                               extraTemplateRenderArgs: ExtraTemplateRenderArgs,
                                               override val visibility: WidgetVisibility
                                              )(formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer)
  extends Widget with Named with Hideable with Persisted with Jsonable {

  override def merge(formPostData: Map[String, Seq[String]], existingFormData: FormData): FormData =
    existingFormData ++ FormData(toJson(formPostData))

  override def toJson(formPostData: Map[String, Seq[String]]): JsObject = {
    if (persistState) {
      formPostData.get(name).flatMap(_.headOption.flatMap(value => Try(value.toInt).toOption.map(intValue => Json.obj(name -> intValue)))).getOrElse(Json.obj())
    } else Json.obj()
  }

  protected[widget] def templateArgs(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)
                                    (implicit executionContext: ExecutionContext): Future[Map[String, Any]] = {
    val lastSelectedIndex = if (!persistState) 0 else (form.data.textData \ name).asOpt[Int].getOrElse(0)
    for {
      msgs <- messages
      consolidatedArgs <- TemplateArgManipulator.collapseAndMergeAll(name, messages, extraTemplateRenderArgs)(form, errors, requestInfo)(formLevelMessages, executionContext)
      tabItems <- tabContentToTemplateArguments(msgs, tabContents, form, errors, requestInfo, lastSelectedIndex)
    } yield {

      val localProps = Map(
        "tabs.id" -> HtmlIdGenerator.htmlId(form.formDefinition, this),
        "tabs.name" -> name,
        "tabs.classes" -> visibilityClass(visibility.shown(form.data)),
        tabItems,
        "tabs.items.nonEmpty" -> tabItems._2.nonEmpty,
        "tabs.value" -> lastSelectedIndex,
        "tabs.persistState" -> persistState
      )

      TemplateArgManipulator.mergeProperties(
        localProps,
        consolidatedArgs
      )
    }
  }


  protected[widget] def tabContentToTemplateArguments(messages: Messages[M], widgets: Seq[Widget], form: Form,
                                                      errors: ValidationErrors, requestInfo: RequestInfo, lastSelectedIndex: Int = 0)
                                                     (implicit executionContext: ExecutionContext): Future[(String, Seq[Map[String, Any]])] = {

    val ops = new MessagesOperations(messages)
    val tabTitles = ops.getUnformattedEntries.filter(_.key.startsWith(s"$name.label")).map(entries => entries.value).toList
    val rowsWithTitles = widgets.zip(tabTitles)

    val eventualMaps: Seq[Future[Map[String, Any]]] = rowsWithTitles.zipWithIndex.map {
      case ((row, title), index) => {

        val renderedAction = row.render(form, errors, requestInfo)
        val id = HtmlIdGenerator.randomId()

        renderedAction.map(action => {
          Map(
            "label" -> title,
            "id" -> id,
            "html" -> action,
            "selected" -> (index == lastSelectedIndex),
            "link" -> id)
        })
      }
    }

    Future.sequence(eventualMaps).map(value => "tabs.items" -> value)
  }

  override def render(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit executionContext: ExecutionContext): Future[String] = {
    templateArgs(form, errors, requestInfo)
      .map(args => renderer.render(templateName, args))
  }

  override def validate(data: FormData)(implicit ec: ExecutionContext): Future[ValidationResult] = {
    Future.successful(ValidationResult(data, Seq.empty))
  }
}

/**
 * Implementation of GOV.UK Design System's [[https://design-system.service.gov.uk/components/tabs/ Tabs component]]
 *
 * @param messages     A [[com.uxforms.domain.Messages]] that contains the label for each tab. E.g.
 * {{{
 *                       name.label=First tab label
 *                       name.label=Second tab label
 * }}}
 * @param tabContents  A widget to render the body of each tab. Must contain as many widgets as there are tabs.
 * @param persistState If true, will save the currently selected tab into FormData so it is "remembered" for next
 *                     time the widget is shown. Otherwise it will always default back to showing the first tab.
 */
object Tabs extends CommonHtmlAttributes {

  override protected val elementPrefix: String = "tabs"

  def tabs[M <: Messages[M], F <: Messages[F]](name: String,
                                               messages: Future[Messages[M]],
                                               tabContents: Seq[Widget],
                                               extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs,
                                               visibility: WidgetVisibility = alwaysShown)
                                              (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): Tabs[M, F] = {
    new Tabs(name, messages, "tabs/template.mustache", tabContents, false, extraTemplateRenderArgs, visibility)(formLevelMessages, renderer)
  }

  def persistedTabs[M <: Messages[M], F <: Messages[F]](name: String,
                                                        messages: Future[Messages[M]],
                                                        tabContents: Seq[Widget],
                                                        extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs,
                                                        visibility: WidgetVisibility = alwaysShown)
                                                       (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): Tabs[M, F] =
    new Tabs(name, messages, "tabs/template.mustache", tabContents, true, extraTemplateRenderArgs, visibility)(formLevelMessages, renderer)
}
