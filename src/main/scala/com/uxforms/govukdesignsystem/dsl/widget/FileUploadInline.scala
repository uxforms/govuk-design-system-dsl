package com.uxforms.govukdesignsystem.dsl.widget

import java.util.UUID
import com.uxforms.domain._
import com.uxforms.domain.constraint.{Constraint, FileUploadWithAllFormDataConstraint, FirstOnly}
import com.uxforms.domain.widget.{Extractable, Extractor}
import com.uxforms.dsl.Form
import com.uxforms.dsl.helpers.MessageHelper
import com.uxforms.dsl.widgets.file.{DataSizeFormatter, FileUploadActionNames}
import com.uxforms.dsl.widgets.{FileUploadMetadata, WidgetVisibility, alwaysShown}
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.widget.FileUpload.extractUploadedFiles
import com.uxforms.govukdesignsystem.dsl.widget.templateargs.{CommonFormGroupHtmlAttributes, CommonHtmlAttributes}
import play.api.libs.json.{JsObject, Json, Reads}

import scala.concurrent.{ExecutionContext, Future}

class FileUploadInline[M <: Messages[M], F <: Messages[F]](override val name: String,
                                                           override val reportingName: String,
                                                           override val messages: Future[Messages[M]],
                                                           override val constraints: Set[Constraint],
                                                           fileConstraints: Set[FileUploadWithAllFormDataConstraint],
                                                           override val templateName: String,
                                                           override val extraTemplateRenderArgs: ExtraTemplateRenderArgs,
                                                           override val visibility: WidgetVisibility)
                                                          (implicit override val formLevelMessages: Future[Messages[F]], override val renderer: TemplateRenderer)
  extends FileUploadBase[M, F] with Extractable[Seq[FileUploadMetadata]] {

  override def filesToDelete(postedData: Map[String, Seq[String]]): Seq[String] = Seq.empty

  override def extract(data: FormData)(implicit fjs: Reads[Seq[FileUploadMetadata]]): Option[Seq[FileUploadMetadata]] = {
    if (visibility.shown(data)) {
      super.extract(data)
    } else None
  }

  override def toJson(formPostData: Map[String, Seq[String]]): JsObject = {
    val values = formPostData.getOrElse(name, Seq.empty).toList
    if (values.length >= 4)
      Json.obj(name -> Json.toJson(Seq(FileUploadMetadata(values.head, values(1).toLong, values(2), UUID.fromString(values(3))))))
    else
      Json.obj()
  }


  override def validate(mergedFormData: FormData)(implicit ec: ExecutionContext): Future[ValidationResult] = {
    val submittableFile = for {
      meta <- FileUpload.extractUploadedFiles(name, mergedFormData).headOption
      file <- mergedFormData.files.get(meta.uuid.toString)
    } yield {
      SubmittableFile(meta.uuid.toString, file)
    }

    fileConstraints.foldLeft(super.validate(mergedFormData)) {
      case (result, constraint) => for {
        ValidationResult(_, resultErrors) <- result
        ValidationResult(_, constraintErrors) <- constraint.validate(this, name, mergedFormData, submittableFile)
        totalErrors = resultErrors ++ constraintErrors
        resultFormData = if (totalErrors.isEmpty) mergedFormData else mergedFormData - name
      } yield ValidationResult(resultFormData, totalErrors)
    }
  }

}

/**
 * Implementation of GOV.UK Design System's [[https://design-system.service.gov.uk/components/file-upload/ File upload component]]
 *
 * Instead of requiring the user to explicitly use the 'upload' button to attach the file as per [[FileUpload]], we can declare a FileUploadInline
 * widget that uploads the file when continuing from the current Page or Section.
 *
 * It requires more plumbing than a regular widget. E.g.
 *
 * On the Section or Page containing the widget:
 * {{{
 *   withReceivers(Seq(FileUploadInline.clearUploadedInlineFile("myInlineFileWidget")))
 * }}}
 *
 * Then the following widget declarations:
 * {{{
 *   FileUploadInline.fileUploadInline(
 *    "myInlineFileWidget",
 *    pageMessages,
 *    noConstraints,
 *    noFileUploadWithAllFormDataConstraints,
 *    visibility = FileUploadInline.showWhenNoFile("myInlineFileWidget")
 *   ),
 *   FileUploadInline.fileUploadInlineSummaryList(
 *    "inlineSummary",
 *    "myInlineFileWidget",
 *    pageMessages,
 *    visibility = FileUploadInline.showWhenHasFile("myInlineFileWidget")
 *  )
 * }}}
 *
 * The summary list is here to present the details of the file to the user should they come back to this
 * Page / Section after having previously added a file.
 *
 */
object FileUploadInline extends Extractor[Seq[FileUploadMetadata]] with CommonHtmlAttributes with CommonFormGroupHtmlAttributes {

  override protected val elementPrefix: String = "fileUpload"

  def fileUploadInline[M <: Messages[M], F <: Messages[F]](name: String,
                                                           messages: Future[Messages[M]],
                                                           constraints: Seq[Constraint],
                                                           fileConstraints: Set[FileUploadWithAllFormDataConstraint],
                                                           extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs,
                                                           visibility: WidgetVisibility = alwaysShown)
                                                          (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): FileUploadInline[M, F] = {
    new FileUploadInline(name, "file upload", messages, new FirstOnly(constraints), fileConstraints, "file-upload/template.mustache", extraTemplateRenderArgs, visibility)
  }

  /**
   * Clears an uploaded file's metadata from FormData.
   *
   * Typically used as a ```Receiver``` on the
   * [[FileUploadInline]]'s `com.uxforms.dsl.containers.Container`.
   */
  def clearUploadedInlineFile(fileUploadInlineName: String): DataTransformer = new DataTransformer {

    import play.api.libs.json._

    override def transform(form: Form, requestInfo: RequestInfo)(implicit ec: ExecutionContext): Future[DataTransformationResult] = {
      val originalTextData = form.data.textData
      val newTextData = if (requestInfo.queryData.contains(FileUploadActionNames.deleteAction(fileUploadInlineName)))
        originalTextData.transform((__ \ fileUploadInlineName).json.update(__.read[JsArray].map(_ => JsArray()))).fold(error => originalTextData, identity)
      else originalTextData
      Future.successful(DataTransformationResult(form.data.copy(textData = newTextData), None))
    }
  }

  /**
   * Builds a [[SummaryList]] to show the file uploaded by the ```fileUploadWidgetName``` widget.
   * **Note:** Assumes the `com.uxforms.dsl.containers.Container` on which this widget is rendered has a ```Receiver```
   * configured by [[clearUploadedInlineFile]]
   *
   */
  def fileUploadInlineSummaryList[M <: Messages[M], F <: Messages[F]](name: String,
                                                          fileUploadWidgetName: String,
                                                          messages: Future[Messages[M]],
                                                          extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs,
                                                          visibility: WidgetVisibility = alwaysShown
                                                         )(implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer, executionContext: ExecutionContext): SummaryList[M, F] = {

    val rows: (Form, ValidationErrors, RequestInfo) => Future[Seq[SummaryListRow]] = (form, errors, requestInfo) => {
      Future.sequence {
        extractUploadedFiles(fileUploadWidgetName, form.data).map(metadata => {
          Link.toFile(metadata.uuid.toString, Future.successful(MessageHelper.tupleToMessages("link.html" -> metadata.filename)(requestInfo.locale)), metadata.uuid).render(form, errors, requestInfo).map(renderedFilenameLink =>
            SummaryListRow(
              Map(
                "row.key.html" -> renderedFilenameLink,
                "row.value.text" -> DataSizeFormatter.humanReadableByteCountSI(metadata.sizeBytes)
              ),
              Seq(Button.link(FileUploadActionNames.deleteAction(fileUploadWidgetName), messages, Button.secondary ++ SimpleExtraTemplateRenderArgs(Button.href(s"?${FileUploadActionNames.deleteAction(fileUploadWidgetName)}")))))
          )
        })
      }
    }
    SummaryList.dynamicSummaryList(name, messages, rows, extraTemplateRenderArgs, visibility)
  }

  def showWhenHasFile(widgetName: String): WidgetVisibility = new WidgetVisibility {
    override def shown(data: FormData): Boolean = FileUpload.extractUploadedFiles(widgetName, data).nonEmpty
  }

  def showWhenNoFile(widgetName: String): WidgetVisibility = new WidgetVisibility {
    override def shown(data: FormData): Boolean = FileUpload.extractUploadedFiles(widgetName, data).isEmpty
  }

}

