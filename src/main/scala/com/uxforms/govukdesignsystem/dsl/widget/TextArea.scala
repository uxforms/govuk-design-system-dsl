package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain._
import com.uxforms.domain.constraint.{Constraint, FirstOnly}
import com.uxforms.domain.widget.{Extractable, Extractor, ValidatedWidget, ValidationFailureKPILabel}
import com.uxforms.dsl.Form
import com.uxforms.dsl.widgets.{WidgetVisibility, alwaysShown}
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.template.TemplateHelper._
import com.uxforms.govukdesignsystem.dsl.template.{ErrorMessage, Hint, Label}
import com.uxforms.govukdesignsystem.dsl.widget.templateargs.{CommonFormGroupHtmlAttributes, CommonHtmlAttributes, Spellcheck, TemplateArgManipulator}
import play.api.libs.json.Reads

import java.util.Locale
import scala.concurrent.{ExecutionContext, Future}

class TextArea[M <: Messages[M], F <: Messages[F]](override val name: String,
                                                   override val reportingName: String,
                                                   messages: Future[Messages[M]],
                                                   override val constraints: Set[Constraint],
                                                   templateName: String,
                                                   extraTemplateRenderArgs: ExtraTemplateRenderArgs,
                                                   override val visibility: WidgetVisibility)
                                                  (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer)
  extends ValidatedWidget with ValidationFailureKPILabel with Extractable[String] {

  def this(name: String, messages: Future[Messages[M]], constraints: Set[Constraint],
           extraTemplateRenderArgs: ExtraTemplateRenderArgs, visibility: WidgetVisibility)
          (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer) = {
    this(name, "text area", messages, constraints, "textarea/template.mustache", extraTemplateRenderArgs, visibility)
  }


  override def extract(data: FormData)(implicit fjs: Reads[String]): Option[String] = {
    if (visibility.shown(data)) {
      super.extract(data)
    } else None
  }

  override def validationFailureLabel(implicit ec: ExecutionContext): Future[String] = {
    messages.map(m => m.getString(s"$name.label.text").orElse(m.getString("label.text")).getOrElse("Unknown"))
  }

  def id(formDef: FormDefinition): String = HtmlIdGenerator.elementId(formDef, this)

  protected def visibilityClass(data: FormData): String = {
    if (visibility.shown(data)) "" else hiddenCssClassName
  }

  def templateArgs(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit ec: ExecutionContext): Future[Map[String, Any]] = {
    val taId = id(form.formDefinition)
    val hint = new Hint(taId, name)
    val errorMessage = new ErrorMessage(taId, name, myErrors(errors))
    val label = new Label(taId, name)
    implicit val locale: Locale = requestInfo.locale

    for {
      consolidatedProps <- TemplateArgManipulator.collapseAndMergeAll(name, messages, extraTemplateRenderArgs)(form, errors, requestInfo)
      renderedHint <- hint.render(consolidatedProps)
      renderedError <- errorMessage.render(consolidatedProps)
      renderedLabel <- label.render(consolidatedProps)
    } yield {

      val describedBy = {
        val key = "textArea.describedBy"
        val additionalValues = additionalDescribedBy(form, consolidatedProps) + Seq(renderedHint.map(_ => hint.id), renderedError.map(_ => errorMessage.id)).flatten.mkString(" ", " ", "")
        consolidatedProps.get(key).map(_ + additionalValues).getOrElse(additionalValues).trim match {
          case "" => Map.empty
          case s => Map(key -> s)
        }
      }

      val localProps = Map(
        "textArea.formGroup.attributes" -> Seq(buildAttribute("id", HtmlIdGenerator.htmlId(form.formDefinition, this))),
        "textArea.formGroup.classes" -> visibilityClass(form.data),
        "textArea.id" -> id(form.formDefinition),
        "textArea.name" -> name
      ) ++
        (form.data.textData \ name).asOpt[String].map(v => "textArea.value" -> v) ++
        optionToMap("textArea.govukLabel", renderedLabel) ++
        optionToMap("textArea.hint", renderedHint) ++
        optionToMap("textArea.errorMessage", renderedError)

      TemplateArgManipulator.mergeProperties(
        localProps,
        consolidatedProps
      ) ++ describedBy
    }
  }

  def additionalDescribedBy(form: Form, consolidatedArgs: Map[String, Any]): String = ""

  override def render(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit executionContext: ExecutionContext): Future[String] = {
    templateArgs(form, errors, requestInfo).map(args =>
      renderer.render(templateName, args)
    )
  }

  private def optionToMap(key: String, value: Option[Any]): Map[String, Any] =
    value.fold(Map.empty[String, Any])(v => Map(key -> v))

}

/**
 * Implementation of GOV.UK Design System's [[https://design-system.service.gov.uk/components/date-input/ Date input component]]
 *
 * Some of the most useful available Message properties:
 *
 * {{{
 *   widgetName.label.text=Question text
 *   widgetName.hint.text=Question hint
 *
 *   # When a required() constraint is added to the widget
 *   widgetName.constraint.required.errorMessage=Answer the question
 * }}}
 *
 * If this is the only question on the page then you may want to make the label a page heading.
 *
 * {{{
 *  TextArea.textArea("widgetName", messages, noConstraints, Label.asPageHeading)
 * }}}
 *
 * If you want to make the text area longer then you can override the number of rows.
 *
 * {{{
 *   TextArea.textArea("widgetName", messages, noConstraints, TextArea.rows(20))
 * }}}
 */
object TextArea extends Extractor[String] with CommonHtmlAttributes with CommonFormGroupHtmlAttributes with Spellcheck {

  override protected val elementPrefix: String = "textArea"

  def textArea[M <: Messages[M], F <: Messages[F]](name: String,
                                                   reportingName: String,
                                                   messages: Future[Messages[M]],
                                                   constraints: Seq[Constraint],
                                                   templateName: String,
                                                   extraTemplateRenderArgs: ExtraTemplateRenderArgs,
                                                   visibility: WidgetVisibility)
                                                  (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): TextArea[M, F] =
    new TextArea(name, reportingName, messages, new FirstOnly(constraints), templateName, extraTemplateRenderArgs, visibility)

  def textArea[M <: Messages[M], F <: Messages[F]](name: String,
                                                   messages: Future[Messages[M]],
                                                   constraints: Seq[Constraint],
                                                   extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs,
                                                   visibility: WidgetVisibility = alwaysShown)
                                                  (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): TextArea[M, F] =
    new TextArea(name, messages, new FirstOnly(constraints), extraTemplateRenderArgs, visibility)

  def rows(numberOfRows: Int): ExtraTemplateRenderArgs = SimpleExtraTemplateRenderArgs(s"$elementPrefix.rows" -> numberOfRows)

}
