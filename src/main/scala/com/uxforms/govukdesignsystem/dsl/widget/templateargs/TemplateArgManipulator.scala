package com.uxforms.govukdesignsystem.dsl.widget.templateargs

import com.uxforms.domain.{ExtraTemplateRenderArgs, Messages, RequestInfo, ValidationErrors}
import com.uxforms.dsl.Form
import com.uxforms.dsl.mustache.MustacheUtils.convertMessages

import scala.concurrent.{ExecutionContext, Future}

object TemplateArgManipulator {

  /**
   * Collapse property keys down to their lowest levels, overwriting general key values with those from mose specific keys.
   *
   * E.g. given the following props:
   *
   * {{{
   * Map(
   *  "wn.label.text" -> "Specific text",
   *  "label.text" -> "Default text"
   * )
   * }}}
   *
   * Calling collapseProperties with a widgetName of `wn` would return:
   *
   * {{{
   *  Map(
   *    "label.text" -> "Specific text"
   *  )
   * }}}
   *
   */
  def collapseProperties(widgetName: String, props: Map[String, Any]): Map[String, Any] = {
    val widgetKeyPrefix = s"$widgetName."
    val (widgetSpecificEntries, rootEntries) = props.partition { case (key, _) => key.startsWith(widgetKeyPrefix) }

    rootEntries ++ widgetSpecificEntries.map { case (key, widgetSpecificValue) =>
      val newKey = key.substring(widgetKeyPrefix.length)
      val rootValue = rootEntries.get(newKey)

      (rootValue, widgetSpecificValue) match {
        case (None, wv: Any) => (newKey -> wv)
        case (Some(s: String), wv: String) if newKey.endsWith(".classes") => {
          newKey -> (s.split(" ") ++ wv.split(" ")).toSet.mkString(" ")
        }
        case (Some(rootAttributes: Seq[Map[String, Any]]), widgetSpecificAttributes: Seq[Map[String, Any]]) if newKey.endsWith(".attributes") => {
          val attributeNameInBothMaps: Map[String, Any] => Boolean = _.get("attribute").exists(a => widgetSpecificAttributes.exists(_.get("attribute").contains(a)))
          val filtered = rootAttributes.filterNot(attributeNameInBothMaps)
          val mergedAttributes = filtered ++ widgetSpecificAttributes
          newKey -> mergedAttributes
        }
        case (Some(_), _) => newKey -> widgetSpecificValue
      }
    }

  }

  /**
   * Applies special merge rules to merge these two maps of properties together.
   * Where both maps contain the same key, then the value from 'b' will be preferred.
   *
   * '''Except''' where:
   *
   *  - The key ends with `.classes`, in which case the *values* of the two key values will be appended together
   *  - The key ends with `.attributes` in which case the *values* of the two key values will be appended together
   */
  def mergeProperties(a: Map[String, Any], b: Map[String, Any]): Map[String, Any] = {
    a ++ b.map { case (bKey, bValue) =>
      val aValue = a.get(bKey)

      (aValue, bValue) match {
        case (None, bv@_) => bKey -> bv
        case (Some(as: String), bs: String) if bKey.endsWith("classes") => bKey -> (as.split(" ") ++ bs.split(" ")).toSet.mkString(" ")
        case (Some(aAttributes: Seq[Map[String, Any]]), bAttributes: Seq[Map[String, Any]]) if bKey.endsWith(".attributes") => {
          val attributeNameInbAttributes: Map[String, Any] => Boolean = _.get("attribute").exists(a => bAttributes.exists(_.get("attribute").contains(a)))
          val filtered = aAttributes.filterNot(attributeNameInbAttributes)
          val mergedAttributes = filtered ++ bAttributes
          bKey -> mergedAttributes
        }
        case (_, b) => bKey -> b
      }
    }
  }

  /**
   * Applies special merge rules to merge these two ExtraTemplateRenderArgs together.
   * Uses the same rules as [[mergeProperties]].
   *
   */
  def mergeExtraTemplateRenderArgs(a: ExtraTemplateRenderArgs, b: ExtraTemplateRenderArgs): ExtraTemplateRenderArgs = {
    new ExtraTemplateRenderArgs {
      override def extraArgs(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit ec: ExecutionContext): Future[Map[String, Any]] = {
        for {
          aArg <- a.extraArgs(form, errors, requestInfo)
          bArg <- b.extraArgs(form, errors, requestInfo)
        } yield {
          mergeProperties(aArg, bArg)
        }
      }
    }
  }

  /**
   *
   * Collapse and merge `formLevelMessages`, `messages` and `extraTemplateRenderArgs` in increasing order of precedence.
   *
   * First collapses each of the properties to be merged, then performs the merge.
   */
  def collapseAndMergeAll[M <: Messages[M], F <: Messages[F]](widgetName: String, messages: Future[Messages[M]], args: ExtraTemplateRenderArgs)
                                                             (form: Form, errors: ValidationErrors, requestInfo: RequestInfo)
                                                             (implicit formLevelMessages: Future[Messages[F]], ec: ExecutionContext): Future[Map[String, Any]] = {
    for {
      eArgs <- args.extraArgs(form, errors, requestInfo)
      fM <- formLevelMessages
      m <- messages
    } yield {
      val collapsedArgs = collapseProperties(widgetName, eArgs)
      val collapsedFormMessageProperties = collapseProperties(widgetName, convertMessages(fM))
      val collapsedMessageProperties = collapseProperties(widgetName, convertMessages(m))

      mergeProperties(
        mergeProperties(collapsedFormMessageProperties, collapsedMessageProperties),
        collapsedArgs
      )
    }
  }

  /**
   * Merge formLevelMessages, messages and extraTemplateRenderArgs in increasing order of precedence.
   * Does not collapse any properties, so this is not expected to be used for Widgets.
   */
  def mergeAll[M <: Messages[M], F <: Messages[F]](messages: Future[Messages[M]], args: ExtraTemplateRenderArgs)
                                                  (form: Form, errors: ValidationErrors, requestInfo: RequestInfo)
                                                  (implicit formLevelMessages: Future[Messages[F]], ec: ExecutionContext): Future[Map[String, Any]] = {
    for {
      eArgs <- args.extraArgs(form, errors, requestInfo)
      fM <- formLevelMessages
      m <-messages
    } yield {
      mergeProperties(
        mergeProperties(convertMessages(fM), convertMessages(m)),
        eArgs
      )
    }
  }

}
