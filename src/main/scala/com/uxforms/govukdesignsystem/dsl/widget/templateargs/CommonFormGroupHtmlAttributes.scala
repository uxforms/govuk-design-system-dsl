package com.uxforms.govukdesignsystem.dsl.widget.templateargs

import com.uxforms.domain.{ExtraTemplateRenderArgs, SimpleExtraTemplateRenderArgs}

trait CommonFormGroupHtmlAttributes {

  this: CommonHtmlAttributes =>

  def formGroupClasses(classes: String): ExtraTemplateRenderArgs =
    SimpleExtraTemplateRenderArgs(s"$elementPrefix.formGroup.classes" -> classes)

}
