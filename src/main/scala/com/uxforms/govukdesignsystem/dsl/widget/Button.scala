package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain._
import com.uxforms.dsl.Form
import com.uxforms.dsl.widgets.{WidgetVisibility, alwaysShown}
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.template.TemplateHelper
import com.uxforms.govukdesignsystem.dsl.widget.templateargs.CommonHtmlAttributes

import java.util.UUID
import scala.concurrent.{ExecutionContext, Future}

/**
 * Implementation of GOV.UK Design System's [[https://design-system.service.gov.uk/components/button/ Button component]]
 *
 * Reads properties from `messages`, e.g.
 *
 * {{{
 *   widgetName.button.text=Save and continue
 * }}}
 *
 * To group buttons use an enclosing div.
 *
 * {{{
 *  Div.div(
 *    Div.classes(Button.buttonGroupClass),
 *    Button.button("saveState", pageMessages),
 *    Link.toPage("resetState", pageMessages, ResetStatePage.urlPath)
 *  ),
 * }}}
 */
object Button extends CommonHtmlAttributes {

  override protected val elementPrefix: String = "button"

  def link[M <: Messages[M], F <: Messages[F]](name: String, messages: Future[Messages[M]], extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs, visibility: WidgetVisibility = alwaysShown)(implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer) =
    new Content(name, "button", messages, "button/template.mustache", asLink ++ withName(name) ++ extraTemplateRenderArgs, visibility)

  def button[M <: Messages[M], F <: Messages[F]](name: String, messages: Future[Messages[M]], extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs, visibility: WidgetVisibility = alwaysShown)(implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer) =
    new Content(name, "button", messages, "button/template.mustache", asButton ++ withName(name) ++ extraTemplateRenderArgs, visibility)

  def input[M <: Messages[M], F <: Messages[F]](name: String, messages: Future[Messages[M]], extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs, visibility: WidgetVisibility = alwaysShown)(implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer) =
    new Content(name, "button", messages, "button/template.mustache", asInput ++ withName(name) ++ extraTemplateRenderArgs, visibility)


  def withName(name: String): ExtraTemplateRenderArgs = SimpleExtraTemplateRenderArgs(s"$elementPrefix.name" -> name)

  def withValue(value: String): ExtraTemplateRenderArgs = SimpleExtraTemplateRenderArgs(s"$elementPrefix.value" -> value)

  val asLink: ExtraTemplateRenderArgs = SimpleExtraTemplateRenderArgs(s"$elementPrefix.isLink" -> true)
  val asButton: ExtraTemplateRenderArgs = SimpleExtraTemplateRenderArgs(s"$elementPrefix.isButton" -> true)
  val asInput: ExtraTemplateRenderArgs = SimpleExtraTemplateRenderArgs(s"$elementPrefix.isInput" -> true)

  val secondary: ExtraTemplateRenderArgs = classes("govuk-button--secondary")
  val warning: ExtraTemplateRenderArgs = classes("govuk-button--warning")

  val disabled: ExtraTemplateRenderArgs = SimpleExtraTemplateRenderArgs(s"$elementPrefix.disabled" -> true)
  val asStart: ExtraTemplateRenderArgs = SimpleExtraTemplateRenderArgs(s"$elementPrefix.isStartButton" -> true)

  def preventDoubleClick(prevent: Boolean = true): ExtraTemplateRenderArgs =
    SimpleExtraTemplateRenderArgs(
      s"$elementPrefix.preventDoubleClickIsDefined" -> true,
      s"$elementPrefix.preventDoubleClick" -> prevent
    )

  def href(target: String): Map[String, Any] =
    Map("button.href" -> target)

  val buttonGroupClass = "govuk-button-group"

  /**
   * Assumes the page's urlPath is a simple string. Does not work for regex-based page paths.
   */
  def pageUrlArg(pageUrlPath: String): ExtraTemplateRenderArgs = new ExtraTemplateRenderArgs {
    override def extraArgs(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit ec: ExecutionContext): Future[Map[String, Any]] =
      Future.successful(href(TemplateHelper.pagePath(form.formDefinition, pageUrlPath)))
  }

  def fileUrlArg(uuid: UUID): ExtraTemplateRenderArgs = new ExtraTemplateRenderArgs {
    override def extraArgs(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit ec: ExecutionContext): Future[Map[String, Any]] = {
      Future.successful(href(TemplateHelper.filePath(form.formDefinition, uuid)))
    }
  }

}
