package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain.{ExtraTemplateRenderArgs, Messages, NoExtraRenderArgs, SimpleExtraTemplateRenderArgs}
import com.uxforms.dsl.widgets.{WidgetVisibility, alwaysShown}
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.widget.templateargs.CommonHtmlAttributes

import scala.concurrent.Future

/**
 * Implementation of GOV.UK Design System's [[https://design-system.service.gov.uk/components/tag/ Tag component]]
 *
 * {{{
 *   Tag.tag("tagInactive", messages, Tag.grey)
 * }}}
 *
 * {{{
 *   tagInactive.tag.text=Inactive
 * }}}
 *
 * @since 1.2.0
 */
object Tag extends CommonHtmlAttributes {

  override protected val elementPrefix: String = "tag"

  def tag[M <: Messages[M], F <: Messages[F]](name: String,
                                              messages: Future[Messages[M]],
                                              extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs,
                                              visibility: WidgetVisibility = alwaysShown)
                                             (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): Content[M, F] = {
    new Content(name, "tag", messages, "tag/template.mustache", extraTemplateRenderArgs, visibility)
  }


  val grey: ExtraTemplateRenderArgs = classes("govuk-tag--grey") 
  val green: ExtraTemplateRenderArgs = classes("govuk-tag--green") 
  val turquoise: ExtraTemplateRenderArgs = classes("govuk-tag--turquoise")
  val blue: ExtraTemplateRenderArgs = classes("govuk-tag--blue") 
  val purple: ExtraTemplateRenderArgs = classes("govuk-tag--purple") 
  val pink: ExtraTemplateRenderArgs = classes("govuk-tag--pink") 
  val red: ExtraTemplateRenderArgs = classes("govuk-tag--red")
  val orange: ExtraTemplateRenderArgs = classes("govuk-tag--orange")
  val yellow: ExtraTemplateRenderArgs = classes("govuk-tag--yellow")
  
}
