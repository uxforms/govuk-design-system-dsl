package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain.{ExtraTemplateRenderArgs, Messages, NoExtraRenderArgs}
import com.uxforms.dsl.widgets.{WidgetVisibility, alwaysShown}
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.widget.templateargs.ExtraTemplateRenderArgsMergeOps.ExtraTemplateRenderArgsOperations
import com.uxforms.govukdesignsystem.dsl.widget.templateargs.{CommonHtmlAttributes, TemplateArgManipulator}

import scala.concurrent.Future

/**
 * Implementation of GOV.UK Design System's [[https://design-system.service.gov.uk/styles/typography/#paragraphs Paragraph typography]]
 *
 * {{{
 *   Paragraph.body("myPara", messages)
 * }}}
 *
 * {{{
 *   myPara.paragraph.text=Some text here
 *   # Or, if you need to include html markup
 *   myPara.paragraph.html=Some <strong>text href</strong>
 * }}}
 */
object Paragraph extends CommonHtmlAttributes {

  override protected val elementPrefix: String = "paragraph"

  def paragraph[M <: Messages[M], F <: Messages[F]](name: String, messages: Future[Messages[M]], extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs, visibility: WidgetVisibility = alwaysShown)(implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer) =
    new Content(name, "paragraph", messages, "paragraph/template.mustache", extraTemplateRenderArgs, visibility)

  def body[M <: Messages[M], F <: Messages[F]](name: String, messages: Future[Messages[M]], extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs, visibility: WidgetVisibility = alwaysShown)(implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer) =
    new Content(name, "paragraph", messages, "paragraph/template.mustache", asBody merge extraTemplateRenderArgs, visibility)

  def lead[M <: Messages[M], F <: Messages[F]](name: String, messages: Future[Messages[M]], extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs, visibility: WidgetVisibility = alwaysShown)(implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer) =
    new Content(name, "paragraph", messages, "paragraph/template.mustache", asLead merge extraTemplateRenderArgs, visibility)

  def small[M <: Messages[M], F <: Messages[F]](name: String, messages: Future[Messages[M]], extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs, visibility: WidgetVisibility = alwaysShown)(implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer) =
    new Content(name, "paragraph", messages, "paragraph/template.mustache", asSmall merge extraTemplateRenderArgs, visibility)

  val asBody: ExtraTemplateRenderArgs = classes("govuk-body")
  val asLead: ExtraTemplateRenderArgs = classes("govuk-body-l")
  val asSmall: ExtraTemplateRenderArgs = classes("govuk-body-s")

}
