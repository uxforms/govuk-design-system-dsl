package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain._
import com.uxforms.domain.widget.{Widget, WidgetGroup}
import com.uxforms.dsl.Form
import play.api.libs.json.{JsNull, Json}

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Random

class MultipleChoice(primaryWidget: Widget with ChoiceWidgets) extends WidgetGroup {

  override val widgets: Seq[Widget] = primaryWidget +: primaryWidget.choiceWidgets.values.toSeq

  override def render(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit executionContext: ExecutionContext): Future[String] =
    primaryWidget.render(form, errors, requestInfo)
}

/**
 * @since 1.6.0
 */
class RandomisedMultipleChoice(primaryWidget: Widget with ChoiceWidgets,
                               val randomisedItemOrderKey: String,
                               buildItems: (Form, ValidationErrors, RequestInfo) => Future[Seq[Map[String, Any]]]
                              ) extends MultipleChoice(primaryWidget) {

  /**
   * Set this as a receiver on the Container that declares this widget to ensure the item order is randomised once per form instance.
   */
  val randomiseAndPersistItemOrder: DataTransformer = new DataTransformer {
    override def transform(form: Form, requestInfo: RequestInfo)(implicit ec: ExecutionContext): Future[DataTransformationResult] = {
      RandomisedMultipleChoice.extractRandomisedItemValueOrder(randomisedItemOrderKey, form.data) match {
        case None => buildItems(form, Seq.empty, requestInfo).map { items =>
          val randomised = RandomisedMultipleChoice.randomiseItems(items)
          DataTransformationResult(form.data + Json.obj(randomisedItemOrderKey -> randomised.map(_("value").toString)), None)
        }
        case _ => Future.successful(DataTransformationResult(form.data, None))
      }
    }
  }

  /**
   * Can be used to clear a previously persisted randomised item order.
   */
  val clearPersistedItemOrder: DataTransformer = new DataTransformer {
    override def transform(form: Form, requestInfo: RequestInfo)(implicit ec: ExecutionContext): Future[DataTransformationResult] =
      Future.successful(DataTransformationResult(form.data + Json.obj(randomisedItemOrderKey -> JsNull), None))
  }


}

object RandomisedMultipleChoice {

  def extractRandomisedItemValueOrder(key: String, data: FormData): Option[Seq[String]] = {
    (data.textData \ key).asOpt[Seq[String]]
  }

  /**
   * Because we're randomising the order of the already-built items, we have to go back
   * and move the 'elementId' html id up to the first item in the list. This is so
   * validation errors will link to the first item in the list.
   */
  private def ensureFirstItemHasHtmlId(items: Seq[Map[String, Any]]): Seq[Map[String, Any]] = {
    val nonDividerItems = items.filterNot(_.contains("item.divider"))
    val indexOfElementId = nonDividerItems.indexWhere(_.get("id").exists {
      case id:String => id.startsWith("elementId")
      case _ => false
    })

    if (indexOfElementId <= 0) {
      items
    } else {
      val indexOfFirstNonDivider = items.indexWhere(m =>
        m.get("id").exists {
          case id: String => !id.startsWith("elementId")
          case _ => false
        } && !m.contains("item.divider")
      )

      val withElementId = items.patch(indexOfFirstNonDivider, Seq(items(indexOfFirstNonDivider) ++ Map("id" -> items(indexOfElementId)("id"))), 1)
      if (indexOfFirstNonDivider >= 0) {
        withElementId.patch(indexOfElementId, Seq(items(indexOfElementId) ++ Map("id" -> items(indexOfFirstNonDivider)("id"))), 1)
      } else {
        items
      }
    }
  }

  def randomiseItems(items: Seq[Map[String, Any]]): Seq[Map[String, Any]] = {
    val preserveIndexValues = items.zipWithIndex.collect { case (item, i) if item.get("preserveOrder").contains(true) || item.get("preserveOrder").contains("true") => (item -> i) }
    val toBeShuffled = items.filterNot(m => preserveIndexValues.exists { case (k, _) => m("value") == k("value") })
    val shuffled = Random.shuffle(toBeShuffled)
    preserveIndexValues.foldLeft(shuffled)((result, m) => result.patch(m._2, Seq(m._1), 0))
  }

  def buildItemsWithPersistedOrder(key: String, data: FormData, buildItems: => Future[Seq[Map[String, Any]]])(implicit ec: ExecutionContext): Future[Seq[Map[String, Any]]] = {
    buildItems.map { items =>
      extractRandomisedItemValueOrder(key, data) match {
        case None => items
        case Some(order) => {
          val reOrdered = order.flatMap(o => items.find(i => i("value") == o))
          ensureFirstItemHasHtmlId(reOrdered)
        }
      }
    }
  }

  def randomiseAndPersistItemValueOrder(key: String, buildItems: (Form, RequestInfo) => Future[Seq[Map[String, Any]]]): DataTransformer = new DataTransformer {
    override def transform(form: Form, requestInfo: RequestInfo)(implicit ec: ExecutionContext): Future[DataTransformationResult] = {
      extractRandomisedItemValueOrder(key, form.data) match {
        case None => buildItems(form, requestInfo).map { items =>
          val randomised = randomiseItems(items)
          DataTransformationResult(form.data + Json.obj(key -> randomised.map(_("value").toString)), None)
        }
        case _ => Future.successful(DataTransformationResult(form.data, None))
      }
    }
  }

}


trait ChoiceWidgets {
  def choiceWidgets: Map[String, Widget]
}

