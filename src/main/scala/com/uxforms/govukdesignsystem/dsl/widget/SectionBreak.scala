package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain.{ExtraTemplateRenderArgs, Messages, NoExtraRenderArgs, SimpleExtraTemplateRenderArgs}
import com.uxforms.dsl.widgets.{WidgetVisibility, alwaysShown}
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.widget.templateargs.CommonHtmlAttributes

import scala.concurrent.Future

object SectionBreak extends CommonHtmlAttributes {

  override protected val elementPrefix: String = "sectionbreak"

  def xl[F <: Messages[F]](name: String, extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs, visibility: WidgetVisibility = alwaysShown)(implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer) =
    new Content(name, "sectionbreak", Future.successful(Messages.empty()), "section-break/template.mustache", SimpleExtraTemplateRenderArgs("sectionbreak.xl" -> true) ++ extraTemplateRenderArgs, visibility)

  def l[M <: Messages[M], F <: Messages[F]](name: String, extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs, visibility: WidgetVisibility = alwaysShown)(implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer) =
    new Content(name, "sectionbreak", Future.successful(Messages.empty()), "section-break/template.mustache", SimpleExtraTemplateRenderArgs("sectionbreak.l" -> true) ++ extraTemplateRenderArgs, visibility)

  def m[M <: Messages[M], F <: Messages[F]](name: String, extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs, visibility: WidgetVisibility = alwaysShown)(implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer) =
    new Content(name, "sectionbreak", Future.successful(Messages.empty()), "section-break/template.mustache", SimpleExtraTemplateRenderArgs("sectionbreak.m" -> true) ++ extraTemplateRenderArgs, visibility)

  def s[M <: Messages[M], F <: Messages[F]](name: String, extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs, visibility: WidgetVisibility = alwaysShown)(implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer) =
    new Content(name, "sectionbreak", Future.successful(Messages.empty()), "section-break/template.mustache", SimpleExtraTemplateRenderArgs("sectionbreak.s" -> true) ++ extraTemplateRenderArgs, visibility)


  val visible: ExtraTemplateRenderArgs = classes("govuk-section-break--visible")
}
