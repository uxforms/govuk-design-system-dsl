package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain.{ExtraTemplateRenderArgs, Messages, NoExtraRenderArgs}
import com.uxforms.dsl.widgets.{WidgetVisibility, alwaysShown}
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.widget.templateargs.CommonHtmlAttributes

import scala.concurrent.Future

/**
 * Implementation of GOV.UK Design System's [[https://design-system.service.gov.uk/components/warning-text/ Warning text component]]
 *
 * {{{
 *   WarningText.warningText("widgetName", messages)
 * }}}
 *
 * Some of the most useful available Message properties:
 *
 * {{{
 *   widgetName.warningText.text=This is the warning
 *   # Or, if you want your text to include html
 *   widgetName.warningText.html=<i>This</i> is the warning
 * }}}
 */
object WarningText extends CommonHtmlAttributes {

  override protected val elementPrefix: String = "warningText"

  def warningText[M <: Messages[M], F <: Messages[F]](name: String, messages: Future[Messages[M]], extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs, visibility: WidgetVisibility = alwaysShown)
                                               (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer) =
    new Content(name, "warningText", messages, "warning-text/template.mustache", extraTemplateRenderArgs, visibility)

}
