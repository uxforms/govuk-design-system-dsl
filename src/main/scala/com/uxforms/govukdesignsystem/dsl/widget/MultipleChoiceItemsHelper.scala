package com.uxforms.govukdesignsystem.dsl.widget

import scala.collection.mutable

trait MultipleChoiceItemsHelper {

  def valueFromKey(key: String): String = {
    key.split("\\.").headOption.getOrElse(key)
  }
}

object MultipleChoiceItemsHelper extends MultipleChoiceItemsHelper {

  // Thanks to https://stackoverflow.com/questions/9594431/scala-groupby-preserving-insertion-order
  implicit class GroupByOrderedImplicit[A](val t: Iterable[A]) extends AnyVal {
    def groupByOrdered[B](f: A => B): collection.Map[B, Iterable[A]] = {
      val m = t.zipWithIndex groupBy (x => f(x._1))
      val lhm = mutable.LinkedHashMap(m.toSeq sortBy (_._2.head._2): _*)
      lhm mapValues (_ map (_._1))
    }
  }

}
