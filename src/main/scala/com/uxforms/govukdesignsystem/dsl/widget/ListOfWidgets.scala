package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain.widget.{Widget, WidgetGroup => DslWidgetGroup}
import com.uxforms.domain.{ExtraTemplateRenderArgs, Messages, RequestInfo, ValidationErrors}
import com.uxforms.dsl.Form
import com.uxforms.dsl.widgets.WidgetVisibility
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer

import scala.concurrent.{ExecutionContext, Future}

class ListOfWidgets[M <: Messages[M], F <: Messages[F]](override val name: String,
                                                        widgetType: String,
                                                        messages: Future[Messages[M]],
                                                        templateName: String,
                                                        override val widgets: Seq[Widget],
                                                        renderedWidgetsKey: String,
                                                        extraTemplateRenderArgs: ExtraTemplateRenderArgs,
                                                        override val visibility: WidgetVisibility)
                                                       (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer) extends
  Content(name, widgetType, messages, templateName, extraTemplateRenderArgs, visibility) with DslWidgetGroup {


  def renderEachWidget(form: Form, validationErrors: ValidationErrors, requestInfo: RequestInfo)(implicit executionContext: ExecutionContext): Future[Seq[String]] = {
    Future.sequence(
      widgets.map(_.render(form, validationErrors, requestInfo))
    )
  }

  override def templateArgs(form: Form, validationErrors: ValidationErrors, requestInfo: RequestInfo)(implicit executionContext: ExecutionContext): Future[Map[String, Any]] = {
    for {
      renderedWidgets <- renderEachWidget(form, validationErrors, requestInfo)
      superArgs <- super.templateArgs(form, validationErrors, requestInfo)
    } yield superArgs ++ Map(renderedWidgetsKey -> renderedWidgets)
  }
}
