package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain.{ExtraTemplateRenderArgs, Messages, NoExtraRenderArgs, SimpleExtraTemplateRenderArgs}
import com.uxforms.dsl.widgets.{WidgetVisibility, alwaysShown}
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.widget.templateargs.CommonHtmlAttributes

import scala.concurrent.Future

/**
 * A Pre element.
 *
 * Not part of GOV.UK's Design System.
 *
 * {{{
 *   Pre.pre("myWidget", messages)
 * }}}
 *
 * {{{
 *   myWidget.pre.text=Preformatted text in here
 * }}}
 *
 * @since 1.2.0
 */
object Pre extends CommonHtmlAttributes {


  override protected val elementPrefix: String = "pre"

  def pre[M <: Messages[M], F <: Messages[F]](name: String,
                                              messages: Future[Messages[M]],
                                              extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs,
                                              visibility: WidgetVisibility = alwaysShown)
                                             (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer) =
    new Content(name, "pre", messages, "pre/template.mustache", extraTemplateRenderArgs, visibility)

  /**
   * Surrounds the content of the `<pre>` element with a `<code>` element.
   * <br /><br />
   * Primarily to allow code snippets to be supplied as plain text without having to explicitly wrap them in your
   * own `<code>` element.
   */
  val isCode: ExtraTemplateRenderArgs = SimpleExtraTemplateRenderArgs("pre.isCode" -> true)

}
