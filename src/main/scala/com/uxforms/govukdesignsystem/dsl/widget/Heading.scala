package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain.{ExtraTemplateRenderArgs, Messages, NoExtraRenderArgs, SimpleExtraTemplateRenderArgs}
import com.uxforms.dsl.widgets.{WidgetVisibility, alwaysShown}
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.widget.templateargs.CommonHtmlAttributes

import scala.concurrent.Future

/**
 *  Implementation of GOV.UK Design System's [[https://design-system.service.gov.uk/components/heading/ Heading component]]
 *
 *  {{{
 *    myHeading.heading.heading=Heading text
 *    myHeading.heading.caption=Caption text
 *  }}}
 *
 *  To apply a margin above the heading, e.g. when placed between other widgets in a Container:
 *
 *  {{{
 *    Heading.m("myHeading", messages, Heading.classes(Spacing.responsiveMargin(9, direction.top)))
 *  }}}
 *
 *  '''Note:''' `Heading.s` does not support a caption.
 */
object Heading extends CommonHtmlAttributes {

  override protected val elementPrefix: String = "heading"

  def xl[M <: Messages[M], F <: Messages[F]](name: String, messages: Future[Messages[M]], extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs, visibility: WidgetVisibility = alwaysShown)(implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer) =
    new Content(name, "heading", messages, "heading/template.mustache", SimpleExtraTemplateRenderArgs("heading.xl" -> true) ++ extraTemplateRenderArgs, visibility)

  def l[M <: Messages[M], F <: Messages[F]](name: String, messages: Future[Messages[M]], extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs, visibility: WidgetVisibility = alwaysShown)(implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer) =
    new Content(name, "heading", messages, "heading/template.mustache", SimpleExtraTemplateRenderArgs("heading.l" -> true) ++ extraTemplateRenderArgs, visibility)

  def m[M <: Messages[M], F <: Messages[F]](name: String, messages: Future[Messages[M]], extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs, visibility: WidgetVisibility = alwaysShown)(implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer) =
    new Content(name, "heading", messages, "heading/template.mustache", SimpleExtraTemplateRenderArgs("heading.m" -> true) ++ extraTemplateRenderArgs, visibility)

  def s[M <: Messages[M], F <: Messages[F]](name: String, messages: Future[Messages[M]], extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs, visibility: WidgetVisibility = alwaysShown)(implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer) =
    new Content(name, "heading", messages, "heading/template.mustache", SimpleExtraTemplateRenderArgs("heading.s" -> true) ++ extraTemplateRenderArgs, visibility)

}
