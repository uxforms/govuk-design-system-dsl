package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain._
import com.uxforms.domain.constraint.{Constraint, FirstOnly}
import com.uxforms.domain.widget._
import com.uxforms.dsl.Form
import com.uxforms.dsl.mustache.MustacheUtils.convertMessages
import com.uxforms.dsl.widgets.{WidgetVisibility, alwaysShown}
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.constraint.FixedChoice.fixedChoice
import com.uxforms.govukdesignsystem.dsl.styles.Typography.size._
import com.uxforms.govukdesignsystem.dsl.template.TemplateHelper._
import com.uxforms.govukdesignsystem.dsl.template.{ErrorMessage, Hint}
import com.uxforms.govukdesignsystem.dsl.widget.MultipleChoiceItemsHelper._
import com.uxforms.govukdesignsystem.dsl.widget.templateargs.{CommonFormGroupHtmlAttributes, CommonHtmlAttributes, CommonLegendHtmlAttributes}
import play.api.libs.json.{JsDefined, JsString, Reads}

import java.util.Locale
import scala.concurrent.{ExecutionContext, Future}

/**
 * Implementation of GOV.UK Design System's [[https://design-system.service.gov.uk/components/radios/ Radios Component]]
 *
 * === Question text ===
 *
 * Reads its label and hint from `messages`, e.g.
 *
 * {{{
 *   widgetName.radios.legend.text=This is the label
 *   widgetName.hint.text=This is the hint
 * }}}
 *
 * === Choices ===
 *
 * `choices` are expected to be in the format below, and support multiple settings per item:
 *
 * {{{
 *   FIRST.label.text=First radio label
 *   AFTER_FIRST.divider=This divider text is shown between radio items
 *   SECOND.label.text=Second radio label
 *   SECOND.hint.text=Second radio hint text
 *   THIRD.divider=This divider text is also shown between radio items
 * }}}
 *
 *  - The order of keys in Messages is preserved when rendering. This even allows different locales to have differently ordered items.
 *  - Keys with a `.divider` suffix are treated as text ''between'' radio items. Multiple `.divider` entries are possible, but they do need unique prefixes.
 *  - The value saved back to FormData is the prefix of the key. E.g. if the first item above is chosen, then `"FIRST"` is saved to FormData.
 *
 * === Conditionally revealing widgets ===
 *
 * Sometimes you want another widget to appear within the radio group when selecting one of the radio's options. E.g. Here's what  `choiceWidgets` could look like for a text box to explain choosing 'other'.
 * {{{
 *   Map("OTHER" -> Input.inputText("otherDescription",
 *                    messages,
 *                    required(),
 *                    Input.formGroupClasses(Radios.conditionalWidgetClass),
 *                    showWhenWidget("radioWidgetName") hasValue "OTHER"))
 * }}}
 *
 * Note the additional formGroup class on the input to give it a grey left margin to visually associate it with the radio item above. Also the
 * visibility function to ensure the input is only shown when the radio item with value `OTHER` is selected.
 */
object Radios extends Extractor[String] with CommonHtmlAttributes with CommonLegendHtmlAttributes with CommonFormGroupHtmlAttributes {

  override protected val elementPrefix: String = "radios"

  def radios[M <: Messages[M], C <: Messages[C], F <: Messages[F]](name: String,
                                                                   messages: Future[Messages[M]],
                                                                   constraints: Seq[Constraint],
                                                                   choices: Future[Messages[C]],
                                                                   choiceWidgets: Map[String, Widget] = Radios.noChoiceWidgets,
                                                                   extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs,
                                                                   visibility: WidgetVisibility = alwaysShown)
                                                                  (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer, executionContext: ExecutionContext) =
    new MultipleChoice(new RadiosWidget(name, "radio group", messages, new FirstOnly(fixedChoice(choices) +: constraints), choices, None, choiceWidgets, "radios/template.mustache", extraTemplateRenderArgs, visibility))

  /**
   * Use this method if you want to construct Radios whose item order is randomised.
   *
   * === Fix individual item order ===
   * If you want a specific item to remain in its place in the ordering while the rest of the items are shuffled
   * then you can add a `VALUE.preserveOrder=true` entry with the prefix of the value you want to remain in its current place.
   * E.g.
   * {{{
   *   GB.label.text=United Kingdom
   *   IE.label.text=Ireland
   *   GI.label.text=Gibraltar
   *   GI.preserveOrder=true
   * }}}
   * Will randomise the order of United Kingdom and Ireland but always present Gibraltar last.
   *
   * === Randomise item order and persist the order to FormData ===
   * If you want the items to be randomised only once per form instance, you'll need to store the randomised order
   * in FormData. To do so, add the returned [[RandomisedMultipleChoice]]'s [[RandomisedMultipleChoice.randomiseAndPersistItemOrder]]
   * DataTransformer to the widget's enclosing Container's Receivers.
   *
   * Tip: You'll probably want to instantiate your Radios widget before the instance is passed to the Container,
   * which will allow you to call this member on the widget for the Container's Receivers.
   * {{{myWidget.randomiseAndPersistItemOrder}}}
   */
  def radiosWithRandomisedItemOrder[M <: Messages[M], C <: Messages[C], F <: Messages[F]](name: String,
                                                                                          messages: Future[Messages[M]],
                                                                                          constraints: Seq[Constraint],
                                                                                          choices: Future[Messages[C]],
                                                                                          randomisedItemOrderKey: String,
                                                                                          choiceWidgets: Map[String, Widget] = Radios.noChoiceWidgets,
                                                                                          extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs,
                                                                                          visibility: WidgetVisibility = alwaysShown)
                                                                                         (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer, executionContext: ExecutionContext) =
    new RandomisedMultipleChoice(new RadiosWidget(
      name,
      "radio group",
      messages,
      new FirstOnly(fixedChoice(choices) +: constraints),
      choices,
      Some(randomisedItemOrderKey),
      choiceWidgets,
      "radios/template.mustache",
      extraTemplateRenderArgs,
      visibility), randomisedItemOrderKey, RadiosWidget.buildItems(name, choices, choiceWidgets))

  val inline: ExtraTemplateRenderArgs = formGroupClasses("govuk-radios--inline")

  val small: ExtraTemplateRenderArgs = formGroupClasses("govuk-radios--small")

  val noChoiceWidgets: Map[String, Widget] = Map.empty

  val conditionalWidgetClass = "govuk-radios__conditional"

}

/**
 * Not expected to be used as a widget in its own right as it relies on being included within a [[MultipleChoice]]
 * WidgetGroup to make its embedded widgets behave correctly.
 */
class RadiosWidget[M <: Messages[M], C <: Messages[C], F <: Messages[F]](override val name: String,
                                                                         override val reportingName: String,
                                                                         messages: Future[Messages[M]],
                                                                         override val constraints: Set[Constraint],
                                                                         choices: Future[Messages[C]],
                                                                         randomisedItemOrderKey: Option[String],
                                                                         override val choiceWidgets: Map[String, Widget],
                                                                         templateName: String,
                                                                         extraTemplateRenderArgs: ExtraTemplateRenderArgs,
                                                                         override val visibility: WidgetVisibility
                                                                       )(implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer)
  extends ValidatedWidget with ChoiceWidgets with ValidationFailureKPILabel with Extractable[String] {


  override def extract(data: FormData)(implicit fjs: Reads[String]): Option[String] = {
    if (visibility.shown(data)) {
      super.extract(data)
    } else None
  }

  override def validationFailureLabel(implicit ec: ExecutionContext): Future[String] = {
    messages.map(m => m.getString(s"$name.radios.legend.text").orElse(m.getString("radios.legend.text")).getOrElse("Unknown"))
  }

  def buildItems(choices: Future[Messages[C]], form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit ec: ExecutionContext): Future[Seq[Map[String, Any]]] = {
    randomisedItemOrderKey  match {
      case None => RadiosWidget.buildItems(name, choices, choiceWidgets)(form, errors, requestInfo)
      case Some(key) => RandomisedMultipleChoice.buildItemsWithPersistedOrder(key, form.data, RadiosWidget.buildItems(name, choices, choiceWidgets)(form, errors, requestInfo))
    }
  }

  def templateArgs(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit executionContext: ExecutionContext): Future[Map[String, Any]] = {

    implicit val locale: Locale = requestInfo.locale

    val id = HtmlIdGenerator.elementId(form.formDefinition, this)
    val hint = new Hint(id, name)
    val errorMessage = new ErrorMessage(id, name, myErrors(errors))

    for {
      extraArgs <- extraTemplateRenderArgs.extraArgs(form, errors, requestInfo)
      formMessages <- formLevelMessages
      widgetMessages <- messages
      consolidatedArgs = overrideWithWidgetSpecificEntries(name, reportingName, convertMessages(formMessages) ++ convertMessages(widgetMessages) ++ extraArgs) ++
        overrideWithWidgetSpecificEntries(name, "radios", convertMessages(formMessages) ++ convertMessages(widgetMessages) ++ extraArgs)
      cs <- choices
      items <- buildItems(choices, form, errors, requestInfo)
      renderedHint <- hint.render(consolidatedArgs)
      renderedError <- errorMessage.render(consolidatedArgs)
    } yield {
      val attributes = appendAttributes("radios.formGroup", consolidatedArgs, Seq(buildAttribute("id", HtmlIdGenerator.htmlId(form.formDefinition, this))))
      val classes = appendClasses("radios.formGroup", attributes, visibilityClass(visibility.shown(form.data)))

      val describedBy = {
        val key = "radios.fieldset.describedBy"
        val additionalValues = Seq(renderedHint.map(_ => hint.id), renderedError.map(_ => errorMessage.id)).flatten.mkString(" ")
        classes.get(key).map(_ + " " + additionalValues).getOrElse(additionalValues).trim match {
          case "" => Map.empty
          case s => Map(key -> s)
        }
      }

      val hasLegend = classes.exists(kv => kv._1.endsWith("radios.legend.text") || kv._1.endsWith("radios.legend.html"))

      classes ++
        Map(
          "radios.fieldset.hasLegend" -> hasLegend,
          "radios.fieldset" -> hasLegend,
          "radios.items" -> items
        ) ++
        renderedHint.map(r => "radios.hint" -> r) ++
        renderedError.map(r => "radios.errorMessage" -> r) ++
        describedBy
    }
  }


  override def render(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit executionContext: ExecutionContext): Future[String] = {
    templateArgs(form, errors, requestInfo).map(args =>
      renderer.render(templateName, args)
    )
  }
}

object RadiosWidget {

  def buildItems[C <: Messages[C]](widgetName: String, choices: Future[Messages[C]], choiceWidgets: Map[String, Widget])(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit ec: ExecutionContext): Future[Seq[Map[String, Any]]] = {

    def isSelected(key: String, data: FormData): Boolean = {
      (data.textData \ widgetName) match {
        case JsDefined(JsString(s)) => s == key
        case _ => false
      }
    }

    def build(notYetProcessedKeys: Seq[String], result: Future[Seq[Map[String, Any]]] = Future.successful(Seq.empty)): Future[Seq[Map[String, Any]]] = {

      notYetProcessedKeys.toList match {
        case Nil => result
        case key :: tail =>
          val keyRoot = valueFromKey(key)
          for {
            res <- result
            cs <- choices
            renderedChoiceWidgets <- choiceWidgets.get(keyRoot).map(cw => cw.render(form, errors, requestInfo).map(rw => Map("conditional.html" -> rw))).getOrElse(Future.successful(Map.empty[String, String]))
            m <- {
              // keyRoot of `DIVIDER` is deprecated. Use `ANYROOT.divider=text` instead.
              if (keyRoot == "DIVIDER") build(tail, Future.successful(res :+ cs.getString(key).map(s => Map("item.divider" -> s)).getOrElse(Map.empty)))
              else {
                val (keysWithSameRoot, allOtherKeys) = notYetProcessedKeys.partition(_.startsWith(keyRoot + "."))
                val m = Map(
                  "id" -> (if (res.forall(_.contains("item.divider"))) HtmlIdGenerator.elementId(form.formDefinition, widgetName) else HtmlIdGenerator.randomId()),
                  "name" -> widgetName,
                  "value" -> keyRoot,
                  "hasHint" -> keysWithSameRoot.exists(k => k.endsWith("hint.text") || k.endsWith("hint.html")),
                  "checked" -> isSelected(keyRoot, form.data)
                ) ++
                  keysWithSameRoot.flatMap(k => cs.getString(k).map(v => (k.substring(keyRoot.length + 1) -> v))).toMap ++
                  keysWithSameRoot.find(_ == s"$keyRoot.divider").flatMap(divKey => cs.getString(divKey).map(s => "item.divider" -> s)) ++
                  renderedChoiceWidgets
                build(allOtherKeys, Future.successful(res :+ m))
              }
            }
          } yield {
            m
          }
      }
    }

    choices.flatMap(cs => build(cs.getKeys))
  }

}