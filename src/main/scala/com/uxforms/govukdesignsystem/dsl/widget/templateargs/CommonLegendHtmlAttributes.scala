package com.uxforms.govukdesignsystem.dsl.widget.templateargs

import com.uxforms.domain.{ExtraTemplateRenderArgs, SimpleExtraTemplateRenderArgs}
import com.uxforms.govukdesignsystem.dsl.styles.Typography.size.l
import com.uxforms.govukdesignsystem.dsl.widget.Fieldset

trait CommonLegendHtmlAttributes {

  this: CommonHtmlAttributes =>

  /**
   * css classes to be added to the Widget's Legend.
   *
   * E.g. {{{
   *   MyWidget.legendClasses(Fieldset.fieldsetLegendSize(l))
   * }}}
   */
  def legendClasses(cssClasses: String): ExtraTemplateRenderArgs =
    SimpleExtraTemplateRenderArgs(s"$elementPrefix.legend.classes" -> cssClasses)

  /**
   * Add parameters to the Legend to tell it to render as a page heading
   */
  lazy val asPageHeading: ExtraTemplateRenderArgs =
    SimpleExtraTemplateRenderArgs(
      s"$elementPrefix.legend.isPageHeading" -> true,
      s"$elementPrefix.legend.classes" -> Fieldset.fieldsetLegendSize(l)
    )


}
