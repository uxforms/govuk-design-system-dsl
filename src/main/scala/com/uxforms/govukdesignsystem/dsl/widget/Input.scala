package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain._
import com.uxforms.domain.constraint.{Constraint, FirstOnly}
import com.uxforms.domain.widget.{Extractable, Extractor, ValidatedWidget, ValidationFailureKPILabel}
import com.uxforms.dsl.Form
import com.uxforms.dsl.widgets.{WidgetVisibility, alwaysShown}
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.template.TemplateHelper._
import com.uxforms.govukdesignsystem.dsl.template.{ErrorMessage, Hint, Label}
import com.uxforms.govukdesignsystem.dsl.widget.templateargs.{CommonFormGroupHtmlAttributes, CommonHtmlAttributes, Spellcheck, TemplateArgManipulator}
import play.api.libs.json.Reads

import java.util.Locale
import scala.concurrent.{ExecutionContext, Future}

class Input[M <: Messages[M], F <: Messages[F]](override val name: String,
                                                override val reportingName: String,
                                                messages: Future[Messages[M]],
                                                override val constraints: Set[Constraint],
                                                templateName: String,
                                                extraTemplateRenderArgs: ExtraTemplateRenderArgs,
                                                override val visibility: WidgetVisibility)
                                               (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer)
  extends ValidatedWidget with ValidationFailureKPILabel with Extractable[String] {


  override def extract(data: FormData)(implicit fjs: Reads[String]): Option[String] = {
    if (visibility.shown(data)) {
      super.extract(data)
    } else None
  }

  override def validationFailureLabel(implicit ec: ExecutionContext): Future[String] = {
    messages.map(m => m.getString(s"$name.label.text").orElse(m.getString("label.text")).getOrElse("Unknown"))
  }

  override def render(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit executionContext: ExecutionContext): Future[String] = {
    templateArgs(form, errors, requestInfo).map(args =>
      renderer.render(templateName, args)
    )
  }

  protected[widget] def elementId(formDefinition: FormDefinition): String = {
    HtmlIdGenerator.elementId(formDefinition, this)
  }

  protected[widget] def templateArgs(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit ec: ExecutionContext): Future[Map[String, Any]] = {
    val inputId = elementId(form.formDefinition)
    val hint = new Hint(inputId, name)
    val errorMessage = new ErrorMessage(inputId, name, myErrors(errors))
    val label = new Label(inputId, name)
    implicit val locale: Locale = requestInfo.locale

    for {
      overriddenArgs <- TemplateArgManipulator.collapseAndMergeAll(name, messages, extraTemplateRenderArgs)(form, errors, requestInfo)
      renderedHint <- hint.render(overriddenArgs)
      renderedError <- errorMessage.render(overriddenArgs)
      renderedLabel <- label.render(overriddenArgs)
    } yield {

      val describedBy = {
        val key = "input.describedBy"
        val additionalValues = Seq(renderedHint.map(_ => hint.id), renderedError.map(_ => errorMessage.id)).flatten.mkString(" ")
        overriddenArgs.get(key).map(_ + " " + additionalValues).getOrElse(additionalValues).trim match {
          case "" => Map.empty
          case s => Map(key -> s)
        }
      }

      val hasPrefix = overriddenArgs.contains("input.prefix.text") || overriddenArgs.contains("input.prefix.html")
      val hasSuffix = overriddenArgs.contains("input.suffix.text") || overriddenArgs.contains("input.suffix.html")

      val localProps = Map(
        "input.formGroup.classes" -> visibilityClass(visibility.shown(form.data)),
        "input.formGroup.attributes" -> Seq(buildAttribute("id", HtmlIdGenerator.htmlId(form.formDefinition, this))),
        "input.id" -> inputId,
        "input.name" -> name,
        "input.hasPrefixOrSuffix" -> (hasPrefix || hasSuffix),
        "input.hasPrefix" -> hasPrefix,
        "input.hasSuffix" -> hasSuffix
      ) ++
        (form.data.textData \ name).asOpt[String].map("input.value" -> _) ++
        renderedLabel.map("input.govukLabel" -> _) ++
        renderedHint.map("input.hint" -> _) ++
        renderedError.map("input.errorMessage" -> _)

      TemplateArgManipulator.mergeProperties(
        localProps,
        overriddenArgs
      ) ++ describedBy
    }
  }

}

/**
 * Implementation of GOV.UK Design System's [[https://design-system.service.gov.uk/components/text-input/ Text input component]]
 *
 * {{{
 *   Input.inputText("myInput", messages, required())
 * }}}
 *
 * {{{
 *   myInput.label.text=Question text
 *   myInput.hint.text=Hint text
 *   myInput.constraint.required.errorMessage=Enter some text
 * }}}
 *
 * Flags can be passed to alter its presentation. E.g.
 *
 * {{{
 *   Input.inputText("anotherInput", messages, noConstraints, Input.width10)
 * }}}
 */
object Input extends Extractor[String] with CommonHtmlAttributes with CommonFormGroupHtmlAttributes with Spellcheck {

  override protected val elementPrefix: String = "input"

  def inputText[M <: Messages[M], F <: Messages[F]](name: String,
                                                    messages: Future[Messages[M]],
                                                    constraints: Seq[Constraint],
                                                    extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs,
                                                    visibility: WidgetVisibility = alwaysShown)
                                                   (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): Input[M, F] =
    new Input(name, "text input", messages, new FirstOnly(constraints), "input/template.mustache", extraTemplateRenderArgs, visibility)

  def inputWholeNumber[M <: Messages[M], F <: Messages[F]](name: String,
                                                           messages: Future[Messages[M]],
                                                           constraints: Seq[Constraint],
                                                           extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs,
                                                           visibility: WidgetVisibility = alwaysShown)
                                                          (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): Input[M, F] =
    new Input(name, "number input", messages, new FirstOnly(constraints), "input/template.mustache", wholeNumber ++ extraTemplateRenderArgs, visibility)

  def inputDecimalNumber[M <: Messages[M], F <: Messages[F]](name: String,
                                                             messages: Future[Messages[M]],
                                                             constraints: Seq[Constraint],
                                                             extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs,
                                                             visibility: WidgetVisibility = alwaysShown)
                                                            (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): Input[M, F] =
    new Input(name, "decimal input", messages, new FirstOnly(constraints), "input/template.mustache", decimalNumber ++ extraTemplateRenderArgs, visibility)


  val wholeNumber: ExtraTemplateRenderArgs =
    SimpleExtraTemplateRenderArgs(
      s"$elementPrefix.inputmode" -> "numeric",
      s"$elementPrefix.pattern" -> "[0-9]*",
      s"$elementPrefix.attributes" -> Seq(buildAttribute("spellcheck", false))
    )

  val decimalNumber: ExtraTemplateRenderArgs = spellcheck(false)

  val width20: ExtraTemplateRenderArgs = classes("govuk-input--width-20")

  val width10: ExtraTemplateRenderArgs = classes("govuk-input--width-10")

  val width5: ExtraTemplateRenderArgs = classes("govuk-input--width-5")

  val width4: ExtraTemplateRenderArgs = classes("govuk-input--width-4")

  val width3: ExtraTemplateRenderArgs = classes("govuk-input--width-3")

  val width2: ExtraTemplateRenderArgs = classes("govuk-input--width-2")

  val widthFull: ExtraTemplateRenderArgs = classes("govuk-!-width-full")

  val widthThreeQuarters: ExtraTemplateRenderArgs = classes("govuk-!-width-three-quarters")

  val widthTwoThirds: ExtraTemplateRenderArgs = classes("govuk-!-width-two-thirds")

  val widthOneHalf: ExtraTemplateRenderArgs = classes("govuk-!-width-one-half")

  val widthOneThird: ExtraTemplateRenderArgs = classes("govuk-!-width-one-third")

  val widthOneQuarter: ExtraTemplateRenderArgs = classes("govuk-!-width-one-quarter")

}