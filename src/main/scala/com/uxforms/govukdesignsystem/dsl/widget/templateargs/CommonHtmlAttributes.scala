package com.uxforms.govukdesignsystem.dsl.widget.templateargs

import com.uxforms.domain.{ExtraTemplateRenderArgs, SimpleExtraTemplateRenderArgs}

trait CommonHtmlAttributes {

  protected val elementPrefix: String

  /**
   * css classes to be added to the element.
   *
   * E.g. {{{
   *   MyWidget.classes(Spacing.staticMargin(0, Spacing.direction.bottom))
   * }}}
   */
  def classes(cssClasses: String): ExtraTemplateRenderArgs = SimpleExtraTemplateRenderArgs(s"$elementPrefix.classes" -> cssClasses)

  /**
   * html attributes to be added to the element.
   *
   * @param htmlAttributes Each attribute is represented as a Map with two members, one for the attribute name and the
   *                       other for the attribute value. E.g. {{{
   *                          Map("attribute" -> "role", "value" -> "navigation")
   *                       }}}
   *
   *                       Can also be constructed using [[com.uxforms.govukdesignsystem.dsl.template.TemplateHelper.buildAttribute]]
   */
  def attributes(htmlAttributes: Map[String, Any]*): ExtraTemplateRenderArgs = SimpleExtraTemplateRenderArgs(s"$elementPrefix.attributes" -> htmlAttributes)

}
