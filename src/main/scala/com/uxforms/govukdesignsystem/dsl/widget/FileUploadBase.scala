package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain._
import com.uxforms.domain.widget.{FileHandling, ValidatedWidget}
import com.uxforms.dsl.Form
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.template.TemplateHelper._
import com.uxforms.govukdesignsystem.dsl.template.{ErrorMessage, Hint, Label}
import com.uxforms.govukdesignsystem.dsl.widget.templateargs.TemplateArgManipulator

import java.util.Locale
import scala.concurrent.{ExecutionContext, Future}

trait FileUploadBase[M <: Messages[M], F <: Messages[F]] extends ValidatedWidget with FileHandling {

  def extraTemplateRenderArgs: ExtraTemplateRenderArgs

  def formLevelMessages: Future[Messages[F]]

  def messages: Future[Messages[M]]

  def templateName: String

  def renderer: TemplateRenderer

  def templateArgs(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit locale: Locale, renderer: TemplateRenderer, executionContext: ExecutionContext): Future[Map[String, Any]] = {
    val fileUploadId = HtmlIdGenerator.elementId(form.formDefinition, this)
    val hint = new Hint(fileUploadId, name)
    val errorMessage = new ErrorMessage(fileUploadId, name, myErrors(errors))
    val label = new Label(fileUploadId, name)
    implicit val locale: Locale = requestInfo.locale

    for {
      consolidatedProps <- TemplateArgManipulator.collapseAndMergeAll(name, messages, extraTemplateRenderArgs)(form, errors, requestInfo)(formLevelMessages, executionContext)
      renderedHint <- hint.render(consolidatedProps)
      renderedError <- errorMessage.render(consolidatedProps)
      renderedLabel <- label.render(consolidatedProps)
    } yield {

      val localProps = Map(
        "fileUpload.name" -> name,
        "fileUpload.id" -> fileUploadId,
        "fileUpload.formGroup.classes" -> visibilityClass(visibility.shown(form.data)),
        "fileUpload.formGroup.attributes" -> Seq(buildAttribute("id", HtmlIdGenerator.htmlId(form.formDefinition, this)))
      ) ++
        renderedLabel.map("fileUpload.govukLabel" -> _) ++
        renderedError.map("fileUpload.errorMessage" -> _) ++
        renderedHint.map("fileUpload.hint" -> _)

      TemplateArgManipulator.mergeProperties(
        localProps,
        consolidatedProps
      )
    }
  }

  override def render(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit executionContext: ExecutionContext): Future[String] = {
    templateArgs(form, errors, requestInfo)(requestInfo.locale, renderer, executionContext).map(args =>
      renderer.render(templateName, args))
  }

}
