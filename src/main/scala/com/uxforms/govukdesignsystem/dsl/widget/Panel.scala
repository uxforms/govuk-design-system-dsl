package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain._
import com.uxforms.dsl.Form
import com.uxforms.dsl.widgets.{WidgetVisibility, alwaysShown}
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.widget.templateargs.CommonHtmlAttributes

import scala.concurrent.{ExecutionContext, Future}

/**
 * Implementation of GOV.UK Design System's [[https://design-system.service.gov.uk/components/panel/ Panel component]]
 *
 * {{{
 *   Panel.panel("myWidget", messages)
 * }}}
 *
 * {{{
 *   myWidget.panel.titleText=Application complete
 *   myWidget.panel.html=Your reference number <br><strong>HDJ2123F</strong>
 * }}}
 *
 * === With dynamic data read from the form ===
 *
 * {{{
 *   Panel.panel("dynPan", pageMessages,
 *     applyNamedMessageValuesToMessageEntry(pageMessages, "dynPan.panel.html", (form, _, _) => {
 *        val reference = (form.data.textData \ "reference").asOpt[String].getOrElse("n/a")
 *        Map("reference" -> reference)
 *     })
 *   )
 * }}}
 *
 * {{{
 *   dynPan.panel.titleText=Application complete
 *   dynPan.panel.html=Your reference number <br><strong>{reference}</strong>
 * }}}
 */
object Panel extends CommonHtmlAttributes {

  override protected val elementPrefix: String = "panel"

  def panel[M <: Messages[M], F <: Messages[F]](name: String, messages: Future[Messages[M]], extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs, visibility: WidgetVisibility = alwaysShown)
                                               (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): Content[M, F] =

    new Content(name, "panel", messages, "panel/template.mustache", extraTemplateRenderArgs, visibility) {
      override def templateArgs(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit executionContext: ExecutionContext): Future[Map[String, Any]] = {
        super.templateArgs(form, errors, requestInfo).map(args =>
          Map("panel.hasBody" -> (args.contains("panel.html") || args.contains("panel.text"))) ++ args
        )
      }
    }

}
