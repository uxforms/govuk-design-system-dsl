package com.uxforms.govukdesignsystem.dsl.widget.templateargs

import com.uxforms.domain.ExtraTemplateRenderArgs
import com.uxforms.govukdesignsystem.dsl.template.TemplateHelper.buildAttribute

/**
 * Helper function set set whether a widget has spellcheck enabled.
 *
 * @since 1.5.0
 */
trait Spellcheck {

  this: CommonHtmlAttributes =>

  /**
   * Can be used to mitigate Chrome and MS Edge's [[https://www.bleepingcomputer.com/news/security/google-microsoft-can-get-your-passwords-via-web-browsers-spellcheck/ spell-jacking vulnerability ]]
   */
  def spellcheck(check: Boolean = true): ExtraTemplateRenderArgs = {
    attributes(buildAttribute("spellcheck", check))
  }

}
