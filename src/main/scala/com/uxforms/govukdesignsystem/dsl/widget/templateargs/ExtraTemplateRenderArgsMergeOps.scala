package com.uxforms.govukdesignsystem.dsl.widget.templateargs

import com.uxforms.domain.ExtraTemplateRenderArgs

/**
 * Import this for an implicit member on [[ExtraTemplateRenderArgs]].
 *
 * A much less verbose way of calling [[TemplateArgManipulator.mergeExtraTemplateRenderArgs(target, operand)]]
 *
 * @since 1.6.0
 */
object ExtraTemplateRenderArgsMergeOps {

  implicit class ExtraTemplateRenderArgsOperations(target: ExtraTemplateRenderArgs) {
    def merge(operand: ExtraTemplateRenderArgs): ExtraTemplateRenderArgs = {
      TemplateArgManipulator.mergeExtraTemplateRenderArgs(target, operand)
    }
  }

}
