package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain._
import com.uxforms.domain.widget.{Hideable, Widget}
import com.uxforms.dsl.Form
import com.uxforms.dsl.widgets.{WidgetVisibility, alwaysShown}
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.styles.Typography.size.size
import com.uxforms.govukdesignsystem.dsl.template.TemplateHelper._
import com.uxforms.govukdesignsystem.dsl.widget.Table.TableCell
import com.uxforms.govukdesignsystem.dsl.widget.templateargs.{CommonHtmlAttributes, TemplateArgManipulator}

import scala.concurrent.{ExecutionContext, Future}

class Table[M <: Messages[M], F <: Messages[F]](name: String,
                                                reportingName: String,
                                                messages: Future[Messages[M]],
                                                templateName: String,
                                                head: Seq[TableCell],
                                                rows: Seq[Seq[TableCell]],
                                                extraTemplateRenderArgs: ExtraTemplateRenderArgs,
                                                override val visibility: WidgetVisibility
                                               )(formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer)
  extends Widget with Hideable {

  def templateArgs(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit executionContext: ExecutionContext): Future[Map[String, Any]] = {
    for {
      consolidatedProps <- TemplateArgManipulator.collapseAndMergeAll(name, messages, extraTemplateRenderArgs)(form, errors, requestInfo)(formLevelMessages, executionContext)
      headArgs <- Table.headToTemplateArguments(head, form, errors, requestInfo)
      rowArgs <- Table.rowsToTemplateArguments(rows, consolidatedProps.get("table.firstCellIsHeader").contains(true), form, errors, requestInfo)
    } yield {

      val localProps = Map(
        "table.classes" -> visibilityClass(visibility.shown(form.data)),
        "table.attributes" -> Seq(buildAttribute("id", HtmlIdGenerator.htmlId(form.formDefinition, this)))
      ) ++ headArgs ++ rowArgs

      TemplateArgManipulator.mergeProperties(
        localProps,
        consolidatedProps
      )
    }
  }

  override def render(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit executionContext: ExecutionContext): Future[String] = {
    templateArgs(form, errors, requestInfo).map(args => renderer.render(templateName, args))
  }
}

/**
 * Implementation of GOV.UK Design System's [[https://design-system.service.gov.uk/components/table/ Table component]]
 *
 * === Caption text ===
 *
 * Reads its caption from `messages`,  e.g.
 *
 * {{{
 *   widgetName.table.caption=My caption
 * }}}
 *
 * === Table data ===
 *
 * Like the original nunjucks template, the optional header row is a Seq of Map. E.g.
 *
 * {{{
 *   Seq(
 *      Map("text" -> "Column heading 1"),
 *      Map("text" -> "Column heading 2")
 *   )
 * }}}
 *
 * And rows are a Seq of Seq of Map. E.g.
 *
 * {{{
 *   Seq(
 *      Seq(
 *        Map("text" -> "Row 1 column 1 value"),
 *        Map("text" -> "Row 1 column 2 value")
 *      ),
 *      Seq(
 *        Map("text" -> "Row 2 column 1 value"),
 *        Map("text" -> "Row 2 column 2 value")
 *      )
 *   )
 * }}}
 *
 * Both header and rows support additional properties in the map, e.g.
 *
 * {{{
 *   Map("text" -> 34, "format" -> "numeric")
 * }}}
 *
 * But it's more often that table data will need to be read from FormData, which is possible with the [[#dynamicTable]] function
 * which makes FormData available to dynamically construct both the header and rows. E.g.
 *
 * {{{
 *  def convertFormDataToHeader: (Form, ValidationErrors, RequestInfo) => Future[Seq[TableCell]] = (_,_,_) =>
 *    Future.successful(Seq.empty)
 *
 *  def convertFormDataToRows: (Form, ValidationErrors, RequestInfo) => Future[(Boolean, Seq[Seq[TableCell]])] = (form, _,_ ) => {
 *    Future.successful(true -> (form.data.textData \ "keyName").as[Seq[Map[String, String]]].map { record =>
 *      Seq(Map("text" -> record("displayValue")))
 *    })
 *  }
 *
 *  Table.dynamicTable("widgetName", messages, convertFormDataToHeader, convertFormDataToRows)
 * }}}
 *
 * @since 1.4.0
 *
 */
object Table extends CommonHtmlAttributes {

  override protected val elementPrefix: String = "table"

  type TableCell = Map[String, Any]

  def table[M <: Messages[M], F <: Messages[F]](name: String,
                                                messages: Future[Messages[M]],
                                                head: Seq[TableCell],
                                                rows: Seq[Seq[TableCell]],
                                                extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs,
                                                visibility: WidgetVisibility = alwaysShown)
                                               (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): Table[M, F] = {
    new Table(name, "table", messages, "table/template.mustache", head, rows, extraTemplateRenderArgs, visibility)(formLevelMessages, renderer)
  }

  /**
   * Dynamically build table headings and content from FormData.
   *
   * Note: Don't use {{{Table.firstCellIsHeader}}} as this is read from the Boolean returned by the rows argument instead.
   *
   * @param rows A boolean flag for firstCellIsHeader, and the rows of columns to be rendered by the table.
   *
   */
  def dynamicTable[M <: Messages[M], F <: Messages[F]](name: String,
                                                       messages: Future[Messages[M]],
                                                       head: (Form, ValidationErrors, RequestInfo) => Future[Seq[TableCell]],
                                                       rows: (Form, ValidationErrors, RequestInfo) => Future[(Boolean, Seq[Seq[TableCell]])],
                                                       extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs,
                                                       visibility: WidgetVisibility = alwaysShown)
                                                      (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): Table[M, F] = {
    new Table(name, "table", messages, "table/template.mustache", Seq.empty, Seq.empty, withDynamicHead(head) ++ withDynamicRows(rows) ++ extraTemplateRenderArgs, visibility)(formLevelMessages, renderer)
  }

  /**
   * Generates a css class name for the size of a Table's Caption.
   * Typically passed as an argument to [[Table.captionClasses]].
   */
  def captionSize(size: size): String = s"govuk-table__caption--$size"

  /**
   * css classes to be applied to the Table's Caption.
   * E.g. {{{
   *   Table.captionClasses(Table.captionSize(Typography.size.m))
   * }}}
   */
  def captionClasses(cssClasses: String): ExtraTemplateRenderArgs = SimpleExtraTemplateRenderArgs(s"$elementPrefix.captionClasses" -> cssClasses)

  def withDynamicHead(head: (Form, ValidationErrors, RequestInfo) => Future[Seq[TableCell]]): ExtraTemplateRenderArgs = new ExtraTemplateRenderArgs {
    override def extraArgs(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit ec: ExecutionContext): Future[Map[String, Any]] = {
      head(form, errors, requestInfo).flatMap(h => headToTemplateArguments(h, form, errors, requestInfo))
    }
  }

  def withDynamicRows(rows: (Form, ValidationErrors, RequestInfo) => Future[(Boolean, Seq[Seq[TableCell]])]): ExtraTemplateRenderArgs = new ExtraTemplateRenderArgs {
    override def extraArgs(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit ec: ExecutionContext): Future[Map[String, Any]] = {
      rows(form, errors, requestInfo).flatMap { case (firstRowIsHeader, r) =>
        rowsToTemplateArguments(r, firstRowIsHeader, form, errors, requestInfo)
      }
    }
  }

  val firstCellIsHeader: ExtraTemplateRenderArgs = SimpleExtraTemplateRenderArgs("table.firstCellIsHeader" -> true)

  def headToTemplateArguments(head: Seq[TableCell], form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit ec: ExecutionContext): Future[Map[String, Any]] = {
    Future.successful(Map(
      s"$elementPrefix.hasHead" -> head.nonEmpty,
      s"$elementPrefix.head" -> head.map(_.map { case (key, value) => (s"item.$key" -> value) })
    ))
  }

  def rowsToTemplateArguments(rows: Seq[Seq[TableCell]], firstCellIsHeader: Boolean, form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit ec: ExecutionContext): Future[Map[String, Any]] = {
    Future.successful(Map("table.rows" -> rows.map { row =>
      Map(
        "row.cells" -> row.zipWithIndex.map { case (rowCells, index) =>
          rowCells.map { case (key, value) => (s"cell.$key" -> value) } +
            ("cell.isHeader" -> (index == 0 && firstCellIsHeader))
        }
      )
    }))
  }

}