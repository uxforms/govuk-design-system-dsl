package com.uxforms.govukdesignsystem.dsl.widget

import java.util.UUID
import com.uxforms.domain._
import com.uxforms.dsl.Form
import com.uxforms.dsl.mustache.MustacheUtils
import com.uxforms.dsl.widgets.{WidgetVisibility, alwaysShown}
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.template.TemplateHelper
import com.uxforms.govukdesignsystem.dsl.template.TemplateHelper.buildAttribute
import com.uxforms.govukdesignsystem.dsl.widget.templateargs.CommonHtmlAttributes

import scala.concurrent.{ExecutionContext, Future}

/**
 * Implementation of GOV.UK Design System's [[https://design-system.service.gov.uk/styles/typography/#links Link typography]]
 *
 * {{{
 *   Link.toExternalUrl("myLink", messages, "https://example.com")
 * }}}
 *
 * {{{
 *   myLink.link.text=Link text
 * }}}
 */
object Link extends CommonHtmlAttributes {

  override protected val elementPrefix: String = "link"

  def toPage[M <: Messages[M], F <: Messages[F]](name: String, messages: Future[Messages[M]], pageUrlPath: String, extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs, visibility: WidgetVisibility = alwaysShown)
                                                (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): Content[M, F] =
    new Content(name, "link", messages, "link/template.mustache", pageUrlArg(pageUrlPath) ++ extraTemplateRenderArgs, visibility)

  def toExternalUrl[M <: Messages[M], F <: Messages[F]](name: String, messages: Future[Messages[M]], externalUrl: String, extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs, visibility: WidgetVisibility = alwaysShown)
                                                       (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): Content[M, F] =
    new Content(name, "link", messages, "link/template.mustache", SimpleExtraTemplateRenderArgs(href(externalUrl)) ++ extraTemplateRenderArgs, visibility)

  def toStartForm[M <: Messages[M], F <: Messages[F]](name: String, messages: Future[Messages[M]], extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs, visibility: WidgetVisibility = alwaysShown)
                                                     (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): Content[M, F] =
    new Content(name, "link", messages, "link/template.mustache", startFormArg() ++ extraTemplateRenderArgs, visibility)

  def toFile[M <: Messages[M], F <: Messages[F]](name: String, messages: Future[Messages[M]], uuid: UUID, extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs, visibility: WidgetVisibility = alwaysShown)
                                                (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): Content[M, F] = {
    new Content(name, "link", messages, "link/template.mustache", fileUrlArg(uuid) ++ inNewTab ++ extraTemplateRenderArgs, visibility)
  }

  val inNewTab: ExtraTemplateRenderArgs = SimpleExtraTemplateRenderArgs(
    Map("link.attributes" -> (Seq(buildAttribute("rel", "noreferrer noopener")) :+ buildAttribute("target", "_blank")))
  )

  /**
   * Assumes the page's urlPath is a simple string. Does not work for regex-based page paths.
   */
  def pageUrlArg(pageUrlPath: String): ExtraTemplateRenderArgs = new ExtraTemplateRenderArgs {
    override def extraArgs(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit ec: ExecutionContext): Future[Map[String, Any]] = {
      Future.successful(href(TemplateHelper.pagePath(form.formDefinition, pageUrlPath)))
    }
  }

  def fileUrlArg(uuid: UUID): ExtraTemplateRenderArgs = new ExtraTemplateRenderArgs {
    override def extraArgs(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit ec: ExecutionContext): Future[Map[String, Any]] = {
      Future.successful(href(TemplateHelper.filePath(form.formDefinition, uuid)))
    }
  }

  def startFormArg(): ExtraTemplateRenderArgs = new ExtraTemplateRenderArgs {
    override def extraArgs(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit ec: ExecutionContext): Future[Map[String, Any]] = {
      Future.successful(href(MustacheUtils.formPath(form.formDefinition) + "/start"))
    }
  }

  def href(target: String): Map[String, Any] =
    Map("link.href" -> target)

  val noVisited: ExtraTemplateRenderArgs = classes("govuk-link--no-visited-state")
}
