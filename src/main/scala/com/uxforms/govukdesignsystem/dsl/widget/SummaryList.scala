package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain._
import com.uxforms.domain.widget.{Hideable, Widget}
import com.uxforms.dsl.Form
import com.uxforms.dsl.widgets.{WidgetVisibility, alwaysShown}
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.template.TemplateHelper._
import com.uxforms.govukdesignsystem.dsl.widget.templateargs.{CommonHtmlAttributes, TemplateArgManipulator}

import scala.concurrent.{ExecutionContext, Future}

class SummaryList[M <: Messages[M], F <: Messages[F]](name: String,
                                                      reportingName: String,
                                                      messages: Future[Messages[M]],
                                                      templateName: String,
                                                      rows: Seq[SummaryListRow],
                                                      cardActions: Seq[Widget],
                                                      extraTemplateRenderArgs: ExtraTemplateRenderArgs,
                                                      override val visibility: WidgetVisibility
                                                     )(formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer)
  extends Widget with Hideable {

  def templateArgs(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit executionContext: ExecutionContext): Future[Map[String, Any]] = {

    for {
      consolidatedProps <- TemplateArgManipulator.collapseAndMergeAll(name, messages, extraTemplateRenderArgs)(form, errors, requestInfo)(formLevelMessages, executionContext)
      rowMap <- SummaryList.rowsToTemplateArguments(rows, form, errors, requestInfo)
      cardActionMap <- SummaryList.cardActionWidgetsToTemplateArguments(cardActions, form, errors, requestInfo)
    } yield {

      val localProps = Map(
        "summaryList.classes" -> visibilityClass(visibility.shown(form.data)),
        "summaryList.attributes" -> Seq(buildAttribute("id", HtmlIdGenerator.htmlId(form.formDefinition, this))),
        "summaryList.card.title" -> (consolidatedProps.contains("summaryList.card.title.html") || consolidatedProps.contains("summaryList.card.title.text"))
      ) ++ rowMap ++ cardActionMap

      TemplateArgManipulator.mergeProperties(
        localProps,
        consolidatedProps
      )
    }
  }

  override def render(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit executionContext: ExecutionContext): Future[String] = {
    templateArgs(form, errors, requestInfo).map(args =>
      renderer.render(templateName, args)
    )
  }
}

/**
 * Implementation of GOV.UK Design System's [[https://design-system.service.gov.uk/components/summary-list/ Summary list component]]
 *
 * === Rows ===
 *
 * A [[SummaryListRow]] takes two arguments.
 *
 * `properties`: A Map of key and value to be displayed in the row. E.g.
 * {{{
 *  Map("row.key.text" -> "Name", "row.value.text" -> "Jane Doe")
 * }}}
 *
 * `actions`: An optional Map of actions for the row. Typically actions are used to edit the info in that row so you probably
 * want to use a Link. E.g.
 * {{{
 *   Seq(Link.toPage("edit", messages, BackLink.sectionHref("earlierSectionName")))
 * }}}
 *
 * Rows can be provided as a static Seq of `SummaryListRow` but it's much more likely to want to populate them dynamically
 * from `FormData`. E.g.
 *
 * {{{
 *   def buildRows(form: Form, validationErrors: ValidationErrors, requestInfo: RequestInfo): Future[Seq[SummaryListRow]] = {
 *     msgs.map { m =>
 *        Seq(
 *          SummaryListRow(Map("row.key.text" -> m.getString("firstKey").get, "row.value.text" -> (form.data.textData \ "firstValue").as[String])),
 *          SummaryListRow(Map("row.key.text" -> m.getString("secondKey").get, "row.value.text" -> (form.data.textData \ "secondValue").as[String]))
 *        )
 *     }
 *   }
 *   SummaryList.dynamicSummaryList("", msgs, buildRows)
 * }}}
 *
 * === Summary Card ===
 *
 * In addition to rows, as per a standard Summary List, a Summary Card can also have actions on its header.
 * These actions are given as a `Seq[Widget]` and, as for per-row actions, it's most likely you'll want them to be `Link`s.
 *
 * Summary Card also can take content which, by default, is read from its messages.
 *
 * {{{
 *   summaryList.card.title.html or summaryList.card.title.text to supply the title of the summary card.
 *   summaryList.card.title.headingLevel a number to set the heading level, e.g. h1, h2 or h3 etc. Defaults to 2.
 * }}}
 *
 * And the widget declaration can look something like this (for a dynamic summary card whose rows and actions are
 * populated dynamically based on FormData):
 *
 * {{{
 *
 *   def buildRows(form: Form, validationErrors: ValidationErrors, requestInfo: RequestInfo): Future[Seq[SummaryListRow]] = ???
 *   def buildCardActions(form: Form, validationErrors: ValidationErrors, requestInfo: RequestInfo): Future[Seq[Widget]] = ???
 *
 *   SummaryList.dynamicSummaryCard("myCard", msgs, buildRows, buildCardActions)
 * }}}
 *
 */
object SummaryList extends CommonHtmlAttributes {

  override protected val elementPrefix: String = "summaryList"

  def summaryList[M <: Messages[M], F <: Messages[F]](name: String,
                                                      messages: Future[Messages[M]],
                                                      rows: Seq[SummaryListRow],
                                                      extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs,
                                                      visibility: WidgetVisibility = alwaysShown)
                                                     (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): SummaryList[M, F] = {
    new SummaryList(name, "summaryList", messages, "summary-list/template.mustache", rows, Seq.empty, extraTemplateRenderArgs, visibility)(formLevelMessages, renderer)
  }

  def dynamicSummaryList[M <: Messages[M], F <: Messages[F]](name: String,
                                                             messages: Future[Messages[M]],
                                                             rows: (Form, ValidationErrors, RequestInfo) => Future[Seq[SummaryListRow]],
                                                             extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs,
                                                             visibility: WidgetVisibility = alwaysShown)
                                                            (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): SummaryList[M, F] = {
    summaryList(name, messages, Seq.empty, withDynamicRows(rows) ++ extraTemplateRenderArgs, visibility)
  }

  def summaryCard[M <: Messages[M], F <: Messages[F]](name: String,
                                                      messages: Future[Messages[M]],
                                                      rows: Seq[SummaryListRow],
                                                      cardActions: Seq[Widget],
                                                      extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs,
                                                      visibility: WidgetVisibility = alwaysShown)
                                                     (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): SummaryList[M, F] = {
    new SummaryList(name, "summaryList", messages, "summary-list/template.mustache", rows, cardActions, summaryCardFlag ++ extraTemplateRenderArgs, visibility)(formLevelMessages, renderer)
  }

  def dynamicSummaryCard[M <: Messages[M], F <: Messages[F]](name: String,
                                                             messages: Future[Messages[M]],
                                                             rows: (Form, ValidationErrors, RequestInfo) => Future[Seq[SummaryListRow]],
                                                             cardActions: (Form, ValidationErrors, RequestInfo) => Future[Seq[Widget]],
                                                             extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs,
                                                             visibility: WidgetVisibility = alwaysShown)
                                                            (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): SummaryList[M, F] = {
    summaryList(name, messages, Seq.empty, summaryCardFlag ++ withDynamicRows(rows) ++ withDynamicCardActions(cardActions) ++ extraTemplateRenderArgs, visibility)
  }

  val noBorder: ExtraTemplateRenderArgs = classes("govuk-summary-list--no-border")

  val summaryCardFlag: ExtraTemplateRenderArgs = SimpleExtraTemplateRenderArgs("summaryList.card" -> true)

  def withDynamicRows(rows: (Form, ValidationErrors, RequestInfo) => Future[Seq[SummaryListRow]]): ExtraTemplateRenderArgs = new ExtraTemplateRenderArgs {
    override def extraArgs(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit ec: ExecutionContext): Future[Map[String, Any]] = {
      rows(form, errors, requestInfo).flatMap(rs =>
        rowsToTemplateArguments(rs, form, errors, requestInfo)
      )
    }
  }

  def withDynamicCardActions(cardActions: (Form, ValidationErrors, RequestInfo) => Future[Seq[Widget]]): ExtraTemplateRenderArgs = new ExtraTemplateRenderArgs {
    override def extraArgs(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit ec: ExecutionContext): Future[Map[String, Any]] = {
      cardActions(form, errors, requestInfo).flatMap(widgets =>
        cardActionWidgetsToTemplateArguments(widgets, form, errors, requestInfo)
      )
    }
  }

  def cardActionWidgetsToTemplateArguments(widgets: Seq[Widget], form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit executionContext: ExecutionContext): Future[Map[String, Any]] = {
    Future.sequence(widgets.map(_.render(form, errors, requestInfo))).map(renderedWidgets =>
      Map(
        "summaryList.card.actions.items" -> renderedWidgets,
        "summaryList.card.actions.items.nonEmpty" -> renderedWidgets.nonEmpty,
        "summaryList.card.actions.items.isSingle" -> (renderedWidgets.length == 1)
      )
    )
  }

  def rowsToTemplateArguments(rows: Seq[SummaryListRow], form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit executionContext: ExecutionContext): Future[Map[String, Any]] = {
    Future.sequence(rows.map(r => rowToTemplateArguments(r, form, errors, requestInfo))).map(rs =>
      Map(
        s"$elementPrefix.rows" -> rs,
        s"$elementPrefix.anyRowHasActions" -> rows.exists(_.actions.nonEmpty)
      )
    )
  }

  def rowToTemplateArguments(row: SummaryListRow, form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit executionContext: ExecutionContext): Future[Map[String, Any]] = {
    Future.sequence(row.actions.map(_.render(form, errors, requestInfo))).map(renderedActions =>
      Map(
        "row.actions.items" -> renderedActions,
        "row.actions.items.isSingle" -> (renderedActions.length == 1),
        "row.hasActions" -> renderedActions.nonEmpty
      ) ++
        row.properties
    )
  }
}

case class SummaryListRow(properties: Map[String, Any], actions: Seq[Widget] = Seq.empty)
