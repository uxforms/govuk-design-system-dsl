package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain._
import com.uxforms.domain.widget.{Widget, WidgetGroup => DslWidgetGroup}
import com.uxforms.dsl.Form
import com.uxforms.dsl.widgets.{WidgetVisibility, alwaysShown}
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.styles.Typography.size.size
import com.uxforms.govukdesignsystem.dsl.widget.templateargs.{CommonHtmlAttributes, CommonLegendHtmlAttributes}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{ExecutionContext, Future}

/**
 * A Fieldset component.
 *
 * https://design-system.service.gov.uk/components/fieldset/
 */
class Fieldset[M <: Messages[M], F <: Messages[F]](override val name: String,
                                                   widgetType: String,
                                                   messages: Future[Messages[M]],
                                                   templateName: String,
                                                   override val widgets: Seq[Widget],
                                                   extraTemplateRenderArgs: ExtraTemplateRenderArgs,
                                                   override val visibility: WidgetVisibility)
                                                  (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer) extends
  Content(name, widgetType, messages, templateName, extraTemplateRenderArgs, visibility) with DslWidgetGroup {

  override def templateArgs(form: Form, validationErrors: ValidationErrors, requestInfo: RequestInfo)(implicit executionContext: ExecutionContext): Future[Map[String, Any]] = {
    for {
      renderedWidgets <- renderWidgets(form, validationErrors, requestInfo)
      superArgs <- super.templateArgs(form, validationErrors, requestInfo)
    }
    yield Map("fieldset.widgets" -> renderedWidgets) ++ superArgs
  }
}

object Fieldset extends CommonHtmlAttributes with CommonLegendHtmlAttributes {

  override protected val elementPrefix: String = "fieldset"

  def fieldset[M <: Messages[M], F <: Messages[F]](name: String, messages: Future[Messages[M]], extraTemplateRenderArgs: ExtraTemplateRenderArgs, visibility: WidgetVisibility, widgets: Widget*)(implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): Fieldset[M, F] = {
    new Fieldset(name, "fieldset", messages, "fieldset/template.mustache", widgets, extraTemplateRenderArgs, visibility)
  }

  def fieldset[M <: Messages[M], F <: Messages[F]](name: String, messages: Future[Messages[M]], visibility: WidgetVisibility, widgets: Widget*)(implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): Fieldset[M, F] = {
    new Fieldset(name, "fieldset", messages, "fieldset/template.mustache", widgets, NoExtraRenderArgs, visibility)
  }

  def fieldset[M <: Messages[M], F <: Messages[F]](name: String, messages: Future[Messages[M]], extraTemplateRenderArgs: ExtraTemplateRenderArgs, widgets: Widget*)(implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): Fieldset[M, F] = {
    new Fieldset(name, "fieldset", messages, "fieldset/template.mustache", widgets, extraTemplateRenderArgs, alwaysShown)
  }

  /**
   * Generates a css class name for the size of a fieldset's legend.
   */
  def fieldsetLegendSize(size: size): String = s"govuk-fieldset__legend--$size"
}
