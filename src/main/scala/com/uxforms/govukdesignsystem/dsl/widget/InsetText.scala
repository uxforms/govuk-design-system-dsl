package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain.{ExtraTemplateRenderArgs, Messages, NoExtraRenderArgs}
import com.uxforms.dsl.widgets.{WidgetVisibility, alwaysShown}
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.widget.templateargs.CommonHtmlAttributes

import scala.concurrent.Future

/**
 *  Implementation of GOV.UK Design System's [[https://design-system.service.gov.uk/components/inset-text/ Inset text component]]
 *
 *  {{{
 *    Inset.insetText("myWidget", messages)
 *  }}}
 *
 *  {{{
 *    myWidget.insetText.text=Here's the text to inset
 *  }}}
 */
object InsetText extends CommonHtmlAttributes {

  override protected val elementPrefix: String = "insetText"

  def insetText[M <: Messages[M], F <: Messages[F]](name: String, messages: Future[Messages[M]], extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs, visibility: WidgetVisibility = alwaysShown)(implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer) =
    new Content(name, "insetText", messages, "inset-text/template.mustache", extraTemplateRenderArgs, visibility)

}
