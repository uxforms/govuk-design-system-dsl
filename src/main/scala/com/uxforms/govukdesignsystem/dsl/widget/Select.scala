package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain._
import com.uxforms.domain.constraint.{Constraint, FirstOnly}
import com.uxforms.domain.widget._
import com.uxforms.dsl.Form
import com.uxforms.dsl.widgets.{WidgetVisibility, alwaysShown}
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.constraint.FixedChoice.fixedChoice
import com.uxforms.govukdesignsystem.dsl.template.TemplateHelper._
import com.uxforms.govukdesignsystem.dsl.template.{ErrorMessage, Hint, Label}
import com.uxforms.govukdesignsystem.dsl.widget.MultipleChoiceItemsHelper.valueFromKey
import com.uxforms.govukdesignsystem.dsl.widget.templateargs.{CommonFormGroupHtmlAttributes, CommonHtmlAttributes, TemplateArgManipulator}
import play.api.libs.json.{JsDefined, JsString, Reads}

import java.util.Locale
import scala.concurrent.{ExecutionContext, Future}

/**
 * Implementation of GOV.UK Design System's [[https://design-system.service.gov.uk/components/select/ Select component]]
 *
 * {{{
 *   val choices = Future.successful(ResourceBundleMessages.utf8("myChoices", classLoader, locale))
 *   Select.select("myWidget", messages, required(), choices)
 * }}}
 *
 * {{{
 *   myWidget.label.text=Label text
 *   myWidget.hint.text=Hint text
 *   myWidget.constraint.required.errorMessage=Select an answer
 * }}}
 *
 * Where choices are also read from Messages. In this case, `myChoices.properties`.
 *
 * The prefix is the value stored in FormData, and the label.text is what's presented to the user.
 * In this case, possible stored values would be FIRST, NEXT and ANOTHER.
 *
 * {{{
 *   FIRST.label.text=First in list
 *   NEXT.label.text=Second in list
 *   ANOTHER.label.text=Third in list
 * }}}
 */
object Select extends Extractor[String] with CommonHtmlAttributes with CommonFormGroupHtmlAttributes {

  override protected val elementPrefix: String = "select"

  def select[M <: Messages[M], C <: Messages[C], F <: Messages[F]](name: String,
                                                                   messages: Future[Messages[M]],
                                                                   constraints: Seq[Constraint],
                                                                   choices: Future[Messages[C]],
                                                                   extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs,
                                                                   visibility: WidgetVisibility = alwaysShown)
                                                                  (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer, executionContext: ExecutionContext): Select[M, C, F]
  = new Select(name, "select", messages, new FirstOnly(fixedChoice(choices) +: constraints), choices, None, "select/template.mustache", extraTemplateRenderArgs, visibility)


  /**
   * Use this method if you want to construct a Select whose item order is randomised.
   *
   * === Fix individual item order ===
   * If you want a specific item to remain in its place in the ordering while the rest of the items are shuffled
   * then you can add a `VALUE.preserveOrder=true` entry with the prefix of the value you want to remain in its current place.
   * E.g.
   * {{{
   *   GB.label.text=United Kingdom
   *   IE.label.text=Ireland
   *   GI.label.text=Gibraltar
   *   GI.preserveOrder=true
   * }}}
   * Will randomise the order of United Kingdom and Ireland but always present Gibraltar last.
   *
   * === Randomise item order and persist the order to FormData ===
   * If you want the items to be randomised only once per form instance, you'll need to store the randomised order
   * in FormData. To do so, add the returned [[RandomisedMultipleChoice]]'s [[RandomisedMultipleChoice.randomiseAndPersistItemOrder]]
   * DataTransformer to the widget's enclosing Container's Receivers.
   *
   * Tip: You'll probably want to instantiate your Select widget before the instance is passed to the Container,
   * which will allow you to call this member on the widget for the Container's Receivers.
   * {{{myWidget.randomiseAndPersistItemOrder}}}
   */
  def selectWithRandomisedItemOrder[M <: Messages[M], C <: Messages[C], F <: Messages[F]](name: String,
                                                                                          messages: Future[Messages[M]],
                                                                                          constraints: Seq[Constraint],
                                                                                          choices: Future[Messages[C]],
                                                                                          randomisedItemOrderKey: String,
                                                                                          extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs,
                                                                                          visibility: WidgetVisibility = alwaysShown)
                                                                                         (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer, executionContext: ExecutionContext): RandomisedMultipleChoice = {
    new RandomisedMultipleChoice(new Select(
      name,
      "select",
      messages,
      new FirstOnly(fixedChoice(choices) +: constraints),
      choices,
      Some(randomisedItemOrderKey),
      "select/template.mustache",
      extraTemplateRenderArgs,
      visibility
    ), randomisedItemOrderKey, SelectWidget.buildItems(name, choices))
  }


}

class Select[M <: Messages[M], C <: Messages[C], F <: Messages[F]](override val name: String,
                                                                   override val reportingName: String,
                                                                   messages: Future[Messages[M]],
                                                                   override val constraints: Set[Constraint],
                                                                   choices: Future[Messages[C]],
                                                                   randomisedItemOrderKey: Option[String],
                                                                   templateName: String,
                                                                   extraTemplateRenderArgs: ExtraTemplateRenderArgs,
                                                                   override val visibility: WidgetVisibility
                                                                  )(implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer)
  extends ValidatedWidget with ChoiceWidgets with Extractable[String] with ValidationFailureKPILabel {


  override def choiceWidgets: Map[String, Widget] = Map.empty

  override def extract(data: FormData)(implicit fjs: Reads[String]): Option[String] = {
    if (visibility.shown(data)) {
      super.extract(data)
    } else None
  }

  override def validationFailureLabel(implicit ec: ExecutionContext): Future[String] = {
    messages.map(m => m.getString(s"$name.label.text").orElse(m.getString("label.text")).getOrElse("Unknown"))
  }

  def buildItems(choices: Future[Messages[C]], form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit ec: ExecutionContext): Future[Seq[Map[String, Any]]] = {
    randomisedItemOrderKey match {
      case None => SelectWidget.buildItems(name, choices)(form, errors, requestInfo)
      case Some(key) => RandomisedMultipleChoice.buildItemsWithPersistedOrder(key, form.data, SelectWidget.buildItems(name, choices)(form, errors, requestInfo))
    }
  }


  def templateArgs(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit executionContext: ExecutionContext): Future[Map[String, Any]] = {

    implicit val locale: Locale = requestInfo.locale

    val id = HtmlIdGenerator.elementId(form.formDefinition, this)
    val hint = new Hint(id, name)
    val label = new Label(id, name)
    val errorMessage = new ErrorMessage(id, name, myErrors(errors))

    for {
      consolidatedProps <- TemplateArgManipulator.collapseAndMergeAll(name, messages, extraTemplateRenderArgs)(form, errors, requestInfo)
      cs <- choices
      items <- buildItems(choices, form, errors, requestInfo)
      renderedHint <- hint.render(consolidatedProps)
      renderedError <- errorMessage.render(consolidatedProps)
      renderedLabel <- label.render(consolidatedProps)
    } yield {

      val localProps = Map(
        "select.formGroup.attributes" -> Seq(buildAttribute("id", HtmlIdGenerator.htmlId(form.formDefinition, this))),
        "select.formGroup.classes" -> visibilityClass(visibility.shown(form.data)),
        "select.items" -> items,
        "select.id" -> id,
        "select.name" -> name
      ) ++
        renderedLabel.map(r => "select.govukLabel" -> r) ++
        renderedHint.map(r => "select.hint" -> r) ++
        renderedError.map(r => "select.errorMessage" -> r)

      val describedBy = {
        val key = "select.describedBy"
        val additionalValues = Seq(renderedHint.map(_ => hint.id), renderedError.map(_ => errorMessage.id)).flatten.mkString(" ")
        consolidatedProps.get(key).map(_ + " " + additionalValues).getOrElse(additionalValues).trim match {
          case "" => Map.empty
          case s => Map(key -> s)
        }
      }

      TemplateArgManipulator.mergeProperties(
        localProps,
        consolidatedProps
      ) ++ describedBy

    }
  }


  override def render(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit executionContext: ExecutionContext): Future[String] = {
    templateArgs(form, errors, requestInfo).map(args => renderer.render(templateName, args))
  }
}

object SelectWidget {
  def buildItems[C <: Messages[C]](widgetName: String, choices: Future[Messages[C]])(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit ec: ExecutionContext): Future[Seq[Map[String, Any]]] = {

    def isSelected(key: String, data: FormData): Boolean = {
      (data.textData \ widgetName) match {
        case JsDefined(JsString(s)) => s == key
        case _ => false
      }
    }

    def build(notYetProcessedKeys: Seq[String], result: Future[Seq[Map[String, Any]]] = Future.successful(Seq.empty)): Future[Seq[Map[String, Any]]] = {
      choices.flatMap(cs =>
        notYetProcessedKeys.toList match {
          case Nil => result
          case key :: tail =>
            val keyRoot = valueFromKey(key)
            result.flatMap { res =>
              val (keysWithSameRoot, allOtherKeys) = notYetProcessedKeys.partition(_.startsWith(keyRoot + "."))
              val m = Map(
                "value" -> keyRoot,
                "text" -> cs.getString(key).getOrElse(""),
                "selected" -> isSelected(keyRoot, form.data)
              ) ++ keysWithSameRoot.flatMap(k => cs.getString(k).map(v => (k.substring(keyRoot.length + 1) -> v))).toMap
              build(allOtherKeys, Future.successful(res :+ m))
            }
        }
      )
    }

    choices.flatMap(cs => build(cs.getKeys))
  }

}


