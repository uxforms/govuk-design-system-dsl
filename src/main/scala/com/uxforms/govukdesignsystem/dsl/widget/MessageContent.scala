package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain.widget.Widget
import com.uxforms.domain._
import com.uxforms.dsl.Form

import scala.concurrent.{ExecutionContext, Future}

/**
 * Renders a single value from a Messages instance.
 *
 * Note: This widget does not implement [[com.uxforms.domain.widget.Hideable]] as that requires a surrounding html element
 * to attach a hidden style to. Out of the abundance of caution, this widget does not write out any html - it doesn't
 * even have a template in the form's theme.
 *
 * @param messageKey              The key from within the Messages to write
 * @param messageArgs             Any formatting arguments that should be applied to the message entry before it is rendered
 * @param extraTemplateRenderArgs Any formatting arguments that should be applied to the message entry before it is
 *                                rendered. These take precedence over entries with the same key in `messageArgs`.
 * @since 1.2.0
 */
class MessageContent[M <: Messages[M]](messages: Future[Messages[M]],
                                       messageKey: String,
                                       messageArgs: Map[String, Any],
                                       extraTemplateRenderArgs: ExtraTemplateRenderArgs) extends Widget {

  protected[widget] def templateRenderArgs(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit executionContext: ExecutionContext): Future[Map[String, Any]] = {
    extraTemplateRenderArgs.extraArgs(form, errors, requestInfo).map(args => messageArgs ++ args)
  }

  override def render(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit executionContext: ExecutionContext): Future[String] = {
    for {
      args <- templateRenderArgs(form, errors, requestInfo)
      msgs <- messages
    } yield {
      msgs.getString(messageKey, args).getOrElse("")
    }
  }
}

object MessageContent {

  def messageContent[M <: Messages[M]](messages: Future[Messages[M]],
                                       messageKey: String,
                                       messagesArgs: Map[String, Any] = Map.empty,
                                       extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs): MessageContent[M] = {
    new MessageContent[M](messages, messageKey, messagesArgs, extraTemplateRenderArgs)
  }
}
