package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain._
import com.uxforms.domain.widget.{Hideable, Widget}
import com.uxforms.dsl.Form
import com.uxforms.dsl.widgets.WidgetVisibility
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.template.TemplateHelper._
import com.uxforms.govukdesignsystem.dsl.widget.templateargs.TemplateArgManipulator._

import scala.concurrent.{ExecutionContext, Future}

class Content[M <: Messages[M], F <: Messages[F]](val name: String,
                                                  widgetType: String,
                                                  messages: Future[Messages[M]],
                                                  templateName: String,
                                                  extraTemplateRenderArgs: ExtraTemplateRenderArgs,
                                                  override val visibility: WidgetVisibility)
                                                 (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer) extends Widget with Hideable {

  def templateArgs(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit executionContext: ExecutionContext): Future[Map[String, Any]] = {

    collapseAndMergeAll(name, messages, extraTemplateRenderArgs)(form, errors, requestInfo)
      .map(consolidatedArgs => {
        val internalProps = Map(
          s"$widgetType.classes" -> visibilityClass(visibility.shown(form.data)),
          s"$widgetType.attributes" -> Seq(buildAttribute("id", HtmlIdGenerator.htmlId(form.formDefinition, this)))
        )
        mergeProperties(internalProps, consolidatedArgs)
      })
  }

  override def render(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit executionContext: ExecutionContext): Future[String] = {
    templateArgs(form, errors, requestInfo).map(args => renderer.render(templateName, args))
  }
}
