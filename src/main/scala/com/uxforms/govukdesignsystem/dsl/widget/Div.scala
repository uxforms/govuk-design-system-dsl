package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain._
import com.uxforms.domain.widget.{Hideable, Widget, WidgetGroup}
import com.uxforms.dsl.Form
import com.uxforms.dsl.widgets.{WidgetVisibility, alwaysShown}
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.template.TemplateHelper.{buildAttribute, visibilityClass}
import com.uxforms.govukdesignsystem.dsl.widget.templateargs.CommonHtmlAttributes
import com.uxforms.govukdesignsystem.dsl.widget.templateargs.TemplateArgManipulator.mergeProperties

import scala.concurrent.{ExecutionContext, Future}

class Div(override val widgets: Seq[Widget], templateName: String, extraTemplateRenderArgs: ExtraTemplateRenderArgs, override val visibility: WidgetVisibility)
         (implicit renderer: TemplateRenderer)
  extends WidgetGroup with Hideable {

  def templateArgs(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit executionContext: ExecutionContext): Future[Map[String, Any]] = {

    for {
      extraArgs <- extraTemplateRenderArgs.extraArgs(form, errors, requestInfo)
      renderedWidgets <- Future.sequence(widgets.map(_.render(form, errors, requestInfo))).map(_.mkString(" "))
    } yield {
      val extraProperties = Map(
        "div.html" -> renderedWidgets,
        "div.classes" -> visibilityClass(visibility.shown(form.data)),
        "div.attributes" -> Seq(buildAttribute("id", HtmlIdGenerator.htmlId(form.formDefinition, this)))
      )
      mergeProperties(extraProperties, extraArgs)
    }
  }

  override def render(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit executionContext: ExecutionContext): Future[String] = {
    templateArgs(form, errors, requestInfo).map(args =>
      renderer.render(templateName, args)
    )
  }
}

/**
 * Create a `<div>` wrapper around one or more [[Widget]]s.
 *
 * Can pass additional classes to the div via its ExtraTemplateRenderArgs. E.g.
 *
 * {{{
 *   Div.div(
 *    Div.classes("my-custom-class"),
 *    ...
 *   )
 * }}}
 *
 * @since 1.5.0
 */
object Div extends CommonHtmlAttributes {

  override protected val elementPrefix: String = "div"

  def div(widgets: Widget*)(implicit renderer: TemplateRenderer): Div =
    div(NoExtraRenderArgs, alwaysShown, widgets: _*)

  def div(visibility: WidgetVisibility, widgets: Widget*)(implicit renderer: TemplateRenderer): Div =
    div(NoExtraRenderArgs, visibility, widgets: _*)

  def div(extraTemplateRenderArgs: ExtraTemplateRenderArgs, widgets: Widget*)(implicit renderer: TemplateRenderer): Div =
    div(extraTemplateRenderArgs, alwaysShown, widgets: _*)

  def div(extraTemplateRenderArgs: ExtraTemplateRenderArgs, visibility: WidgetVisibility, widgets: Widget*)(implicit renderer: TemplateRenderer): Div =
    new Div(widgets, "div/template.mustache", extraTemplateRenderArgs, visibility)

}

