package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain.{ExtraTemplateRenderArgs, Messages, NoExtraRenderArgs, SimpleExtraTemplateRenderArgs}
import com.uxforms.dsl.widgets.{WidgetVisibility, alwaysShown}
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.widget.templateargs.CommonHtmlAttributes

import scala.concurrent.Future

/**
 * Implementation of GOV.UK Design System's [[https://design-system.service.gov.uk/components/details/ Details component]]
 *
 * Some of the most useful available Message properties:
 *
 * {{{
 *   widgetName.details.summaryText=The text to show in the short link (i.e. visible when collapsed)
 *   widgetName.details.text=The text to show when expanded
 *   # Or, if you want to include markup in the body
 *   widgetName.details.html=The text to show when expanded, which can include html markup
 * }}}
 *
 */
object Details extends CommonHtmlAttributes {

  override protected val elementPrefix: String = "details"

  def details[M <: Messages[M], F <: Messages[F]](name: String, messages: Future[Messages[M]], extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs, visibility: WidgetVisibility = alwaysShown)(implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer) =
    new Content(name, "details", messages, "details/template.mustache", extraTemplateRenderArgs, visibility)
}
