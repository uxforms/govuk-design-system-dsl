package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain._
import com.uxforms.domain.constraint.{Constraint, FirstOnly, MaxLength, MaxWords}
import com.uxforms.domain.widget.Extractor
import com.uxforms.dsl.Form
import com.uxforms.dsl.widgets.{WidgetVisibility, alwaysShown}
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.template.TemplateHelper.overrideWithWidgetSpecificEntries
import com.uxforms.govukdesignsystem.dsl.template.{Hint, i18n}
import com.uxforms.govukdesignsystem.dsl.widget.templateargs.ExtraTemplateRenderArgsMergeOps.ExtraTemplateRenderArgsOperations
import com.uxforms.govukdesignsystem.dsl.widget.templateargs.{CommonFormGroupHtmlAttributes, CommonHtmlAttributes, Spellcheck, TemplateArgManipulator}

import scala.concurrent.{ExecutionContext, Future}

class CharacterCount[M <: Messages[M], F <: Messages[F]](override val name: String,
                                                         override val reportingName: String,
                                                         messages: Future[Messages[M]],
                                                         override val constraints: Set[Constraint],
                                                         characterCountTemplateName: String,
                                                         textAreaTemplateName: String,
                                                         extraTemplateRenderArgs: ExtraTemplateRenderArgs,
                                                         override val visibility: WidgetVisibility)
                                                        (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer)
  extends TextArea(name, reportingName, messages, constraints, textAreaTemplateName, extraTemplateRenderArgs, visibility) {

  override def additionalDescribedBy(form: Form, consolidatedArgs: Map[String, Any]): String = {
    val countHint = new Hint(super.id(form.formDefinition), name, suffix = "info")
    super.additionalDescribedBy(form, consolidatedArgs) + (if (countHint.shouldRenderHint(countHintDeferencedArgs(consolidatedArgs))) countHint.id else "")
  }

  private def countHintDeferencedArgs(consolidatedArgs: Map[String, Any]): Map[String, Any] = {
    val merged = {
      val mp = TemplateArgManipulator.mergeProperties(
        Map(s"hint.classes" -> "govuk-character-count__message"),
        overrideWithWidgetSpecificEntries("characterCount.info", "hint",
          overrideWithWidgetSpecificEntries(s"$name.info", "hint",
            overrideWithWidgetSpecificEntries("info", "hint", consolidatedArgs - "hint.text" - "hint.html")
          )
        )
      )
      if (!mp.contains("hint.text") && !mp.contains("hint.html")) {
        mp ++ mp.get("hint.default").map(m => "hint.text" -> m)
      } else {
        mp
      }
    }

    if (hasNoLimit(merged)) {
      merged - "hint.text" - "hint.html"
    } else {
      val maxWords = merged.get("characterCount.maxwords")
      val maxLength = merged.get("characterCount.maxlength")
      val length = maxWords.orElse(maxLength).get
      merged ++ {
        merged.get("hint.text").map {
          case v: String => Map("hint.text" -> v.replace("%{count}", length.toString))
          case v => Map("hint.text" -> v)
        }.orElse(merged.get("hint.html").map {
          case v: String => Map("hint.html" -> v.replace("%{count}", length.toString))
          case v => Map("hint.html" -> v)
        }).getOrElse(Map.empty)
      }
    }
  }

  private def i18nArgs(mapKey: String, translationKey: String, args: Map[String, Any]): (String, Any) = {
    val pluralForms = args.filterKeys(_.startsWith(s"$mapKey.")).foldLeft(Seq.empty[(String, String)]) { case (result, (key, value)) =>
      result :+ (key.substring(mapKey.length + 1) -> value.toString)
    }
    mapKey -> i18n.govukPluralisedAttributes(translationKey, pluralForms)
  }

  def hasNoLimit(args: Map[String, Any]): Boolean = {
    !(args.contains("characterCount.maxlength") || args.contains("characterCount.maxwords"))
  }


  def characterCountArgs(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit executionContext: ExecutionContext): Future[Map[String, Any]] = {
    val countHint = new Hint(super.id(form.formDefinition), name, suffix = "info") {
      override def shouldRenderHint(args: Map[String, Any]): Boolean = true
    }
    for {
      consolidatedArgs <- TemplateArgManipulator.collapseAndMergeAll(name, messages, extraTemplateRenderArgs)(form, errors, requestInfo).map { m =>
        m + ("hasNoLimit" -> hasNoLimit(m))
      }
      renderedTextArea <- super.render(form, errors, requestInfo)
      renderedCountHint <- countHint.render(countHintDeferencedArgs(consolidatedArgs))(implicitly[TemplateRenderer], requestInfo.locale, implicitly[ExecutionContext]).map(_.getOrElse(""))
    } yield {
      TemplateArgManipulator.mergeProperties(
        Map(
          i18nArgs("characterCount.charactersUnderLimitText", "characters-under-limit", consolidatedArgs),
          i18nArgs("characterCount.charactersOverLimitText", "characters-over-limit", consolidatedArgs),
          i18nArgs("characterCount.wordsUnderLimitText", "words-under-limit", consolidatedArgs),
          i18nArgs("characterCount.wordsOverLimitText", "words-over-limit", consolidatedArgs),
          "characterCount.textArea" -> renderedTextArea,
          "characterCount.hint" -> renderedCountHint
        ),
        consolidatedArgs
      )
    }
  }

  override def render(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit executionContext: ExecutionContext): Future[String] = {
    characterCountArgs(form, errors, requestInfo).map(args =>
      renderer.render(characterCountTemplateName, args)
    )
  }
}

/**
 * Implementation of GOV.UK Design System's [[https://design-system.service.gov.uk/components/character-count/ Character count component]]
 *
 * Implemented as a wrapper around [[TextArea]].
 *
 * Some of the most useful available Message properties:
 *
 * {{{
 *   widgetName.label.text=Enter a job description
 *   widgetName.hint.text=A brief summary of its main responsibilities
 *   widgetName.constraint.maxlength.errorMessage=Job description must be {1, number, #} characters or less
 * }}}
 *
 * Now the template supports internationalisation, the dynamic javascript text can also be overridden with Messages.
 * Default values for these properties are already set in javascript so they can be safely omitted
 * if you're happy with the defaults provided by the design system.
 *
 * {{{
 *   widgetName.characterCount.charactersUnderLimitText=...
 *   widgetName.characterCount.charactersAtLimitText=...
 *   widgetName.characterCount.charactersOverLimitText=...
 *   widgetName.characterCount.wordsUnderLimitText=...
 *   widgetName.characterCount.wordsAtLimitText=...
 *   widgetName.characterCount.wordsOverLimitText=...
 * }}}
 *
 * Our CharacterCount widget and template handles GOV.UK's template's `textareaDescriptionText` rather differently.
 * Instead it assumes a default entry in its messages:
 *
 * {{{
 *  characterCount.info.hint.default=You can enter up to {count,number,#} {maxProperty, select, maxwords {words} other {characters}}
 * }}}
 *
 * And our widget takes care of populating the `count` and `maxProperty` parameters. This avoids hard-coded English text in
 * our templates and also avoids the need for the template to overwrite the `%{count}` placeholder.
 *
 */
object CharacterCount extends Extractor[String] with CommonHtmlAttributes with CommonFormGroupHtmlAttributes with Spellcheck {

  override protected val elementPrefix: String = "characterCount"

  def wordCount[M <: Messages[M], F <: Messages[F]](name: String,
                                                    messages: Future[Messages[M]],
                                                    constraints: Seq[Constraint],
                                                    maxNumberOfWords: Int,
                                                    extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs,
                                                    visibility: WidgetVisibility = alwaysShown)
                                                   (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): TextArea[M, F] =
    new CharacterCount(name, "character count", messages, new FirstOnly(MaxWords.maxWords(maxNumberOfWords)(Messages.empty()) +: constraints), "character-count/template.mustache", "textarea/template.mustache",
      defaultTextAreaClasses merge maxWords(maxNumberOfWords, name, messages, formLevelMessages) merge extraTemplateRenderArgs,
      visibility
    )

  def characterCount[M <: Messages[M], F <: Messages[F]](name: String,
                                                         messages: Future[Messages[M]],
                                                         constraints: Seq[Constraint],
                                                         maxNumberOfCharacters: Int,
                                                         extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs,
                                                         visibility: WidgetVisibility = alwaysShown)
                                                        (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): TextArea[M, F] =
    new CharacterCount(name, "character count", messages, new FirstOnly(MaxLength.maxLength(maxNumberOfCharacters)(Messages.empty()) +: constraints), "character-count/template.mustache", "textarea/template.mustache",
      defaultTextAreaClasses merge maxCharacters(maxNumberOfCharacters, name, messages, formLevelMessages) merge extraTemplateRenderArgs,
      visibility
    )

  def characterCountWithoutLimit[M <: Messages[M], F <: Messages[F]](name: String,
                                                                     messages: Future[Messages[M]],
                                                                     constraints: Seq[Constraint],
                                                                     extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs,
                                                                     visibility: WidgetVisibility = alwaysShown)
                                                                    (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): TextArea[M, F] =
    new CharacterCount(name, "character count", messages, new FirstOnly(constraints), "character-count/template.mustache", "textarea/template.mustache",
      defaultTextAreaClasses merge extraTemplateRenderArgs,
      visibility
    )


  def defaultTextAreaClasses: ExtraTemplateRenderArgs = TextArea.classes("govuk-js-character-count")

  def maxWords[M <: Messages[M], F <: Messages[F]](max: Int, widgetName: String, msgs: Future[Messages[M]], fMsgs: Future[Messages[F]]): ExtraTemplateRenderArgs =
    maxArg(max, "maxwords", widgetName, msgs, fMsgs)

  def maxCharacters[M <: Messages[M], F <: Messages[F]](max: Int, widgetName: String, msgs: Future[Messages[M]], fMsgs: Future[Messages[F]]): ExtraTemplateRenderArgs =
    maxArg(max, "maxlength", widgetName, msgs, fMsgs)

  private def maxArg[M <: Messages[M], F <: Messages[F]](max: Int, maxProperty: String, widgetName: String, msgs: Future[Messages[M]], fMsgs: Future[Messages[F]]): ExtraTemplateRenderArgs =
    new ExtraTemplateRenderArgs {
      override def extraArgs(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit ec: ExecutionContext): Future[Map[String, Any]] = {
        for {
          m <- msgs
          f <- fMsgs
        } yield {
          val args = Map("count" -> max, "maxProperty" -> maxProperty)
          Map(
            s"$elementPrefix.$maxProperty" -> max
          ) ++
            m.getString(s"$widgetName.info.hint.text", args)
              .orElse(m.getString(s"$elementPrefix.info.hint.text", args))
              .orElse(m.getString("info.hint.text", args))
              .orElse(f.getString(s"$widgetName.info.hint.text", args))
              .orElse(f.getString(s"$elementPrefix.info.hint.text", args))
              .orElse(f.getString("info.hint.text", args))
              .map(m => s"$elementPrefix.info.hint.text" -> m) ++
            m.getString(s"$widgetName.info.hint.html", args)
              .orElse(m.getString(s"$elementPrefix.info.hint.html", args))
              .orElse(m.getString("info.hint.html", args))
              .orElse(f.getString(s"$widgetName.info.hint.html", args))
              .orElse(f.getString(s"$elementPrefix.info.hint.html", args))
              .orElse(f.getString("info.hint.html", args))
              .map(m => s"$elementPrefix.info.hint.html" -> m) ++
            m.getString(s"$elementPrefix.info.hint.default", args)
              .orElse(f.getString(s"$elementPrefix.info.hint.default", args))
              .map(m => s"$elementPrefix.info.hint.default" -> m)
        }
      }
    }

  def threshold(threshold: Int): ExtraTemplateRenderArgs =
    SimpleExtraTemplateRenderArgs(s"$elementPrefix.threshold" -> threshold)

  /**
   * Will append given classes on to those set by [[defaultTextAreaClasses]].
   *
   * If you want to set classes on the wrapped [[TextArea]] then use [[TextArea.classes]] instead.
   */
  override def classes(cssClasses: String): ExtraTemplateRenderArgs = {
    defaultTextAreaClasses merge super.classes(cssClasses)
  }

  /**
   * Sets attributes on CharacterCount's top-level div.
   * If you want to set attributes on the wrapped [[TextArea]] then use [[TextArea.attributes]] instead.
   */
  override def attributes(htmlAttributes: Map[String, Any]*): ExtraTemplateRenderArgs = {
    super.attributes(htmlAttributes: _*)
  }

  /**
   * Delegates to [[TextArea.formGroupClasses]]
   */
  override def formGroupClasses(classes: String): ExtraTemplateRenderArgs = {
    TextArea.formGroupClasses(classes)
  }

  /**
   * Delegates to [[TextArea.spellcheck]]
   */
  override def spellcheck(check: Boolean): ExtraTemplateRenderArgs =
    TextArea.spellcheck(check)
}