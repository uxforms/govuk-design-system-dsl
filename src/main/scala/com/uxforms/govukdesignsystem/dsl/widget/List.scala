package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain.widget.Widget
import com.uxforms.domain.{ExtraTemplateRenderArgs, Messages, NoExtraRenderArgs, SimpleExtraTemplateRenderArgs}
import com.uxforms.dsl.widgets.{WidgetVisibility, alwaysShown}
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.widget.templateargs.CommonHtmlAttributes

import scala.concurrent.Future

object List extends CommonHtmlAttributes {

  override protected val elementPrefix: String = "list"

  def list[M <: Messages[M], F <: Messages[F]](name: String, messages: Future[Messages[M]], widgets: Seq[Widget], extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs, visibility: WidgetVisibility = alwaysShown)(implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer) =
    new ListOfWidgets(name, "list", messages, "list/template.mustache", widgets, "list.items", unordered ++ extraTemplateRenderArgs, visibility)


  val numbered: ExtraTemplateRenderArgs = classes("govuk-list--number")

  val bulleted: ExtraTemplateRenderArgs = classes("govuk-list--bullet")

  val unordered: ExtraTemplateRenderArgs =
    SimpleExtraTemplateRenderArgs(s"$elementPrefix.isOrdered" -> false)

  val ordered: ExtraTemplateRenderArgs =
    SimpleExtraTemplateRenderArgs(s"$elementPrefix.isOrdered" -> true)

}
