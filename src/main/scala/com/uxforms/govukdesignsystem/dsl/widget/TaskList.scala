package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain._
import com.uxforms.domain.widget.Widget
import com.uxforms.dsl.Form
import com.uxforms.dsl.containers.Page
import com.uxforms.dsl.widgets.{WidgetVisibility, alwaysShown}
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.widget.templateargs.CommonHtmlAttributes

import scala.concurrent.{ExecutionContext, Future}

/**
 * Implementation of GOV.UK Design System's [[https://design-system.service.gov.uk/patterns/task-list-pages/ Task list pattern]]
 *
 * For the most simple task list, we can build the entries statically in our Container's declaration:
 *
 * {{{
 *  TaskList.taskList("tl", messages, Seq(
 *    TaskListSection(messageContent(messages, "tl.firstSection.heading"), Seq(
 *      TaskListItem(Link.toExternalUrl("tl.firstItem", messages, "#"), tag("completeTag", messages, TaskList.rightAlignTag)),
 *      TaskListItem(Link.toExternalUrl("tl.secondItem", messages, "#"), tag("inProgressTag", messages, mergeExtraTemplateRenderArgs(TaskList.rightAlignTag, Tag.blue)))
 *    )),
 *    TaskListSection(messageContent(messages, "tl.secondSection.heading"), Seq(
 *      TaskListItem(Link.toExternalUrl("tl.thirdItem", messages, "#"), tag("notStartedTag", messages, mergeExtraTemplateRenderArgs(TaskList.rightAlignTag, Tag.grey))),
 *      TaskListItem(messageContent(messages, "tl.fourthItem"), tag("cannotStartTag", messages, mergeExtraTemplateRenderArgs(TaskList.rightAlignTag, Tag.grey)))
 *    ))
 *  ))
 * }}}
 *
 * with Messages
 *
 * {{{
 *   tl.firstSection.heading=Check before you start
 *   tl.firstItem.link.text=Check eligibility
 *   tl.secondItem.link.text=Read declaration
 *
 *   tl.secondSection.heading=Prepare application
 *   tl.thirdItem.link.text=Company information
 *   tl.fourthItem=Your contact details
 *
 *   completeTag.tag.text=Completed
 *   inProgressTag.tag.text=In progress
 *   notStartedTag.tag.text=Not started
 *   cannotStartTag.tag.text=Cannot start yet
 * }}}
 *
 * === Dynamically building Task Lists from FormData ===
 *
 * But typically we're going to want to build task lists dynamically based upon the end-user's progress through their
 * user journey. So instead we can build up both TaskListSections and TaskListItems from a combination of the form
 * definition itself and the contents of FormData.
 *
 * Using a task list assumes you're going to want to allow end users to jump around between different stages within
 * the form. We can support this by putting our questions within Pages instead of Sections. We can then get even more
 * clever by adding the IncludeInTaskList trait to every Page which should be included in the Task List. This allows
 * each individual Page to tell us its status on the task list, as well as be in control of whether it should have a
 * link to navigate to it or not (steps within the task list that are not yet permitted to be visited should be
 * displayed as plain text instead of a link). Once this is done, then we can build up our TaskList like so:
 *
 * {{{
 *  TaskList.taskList("dli",
 *    pageMessages,
 *    TaskList.buildFromPages(pageMessages) _
 *  )
 * }}}
 *
 * The order of sections within the task list is taken from the order in which the Pages are declared in your FormDefinition.
 *
 * And your definition of a Page could look something like this:
 *
 * {{{
 *  questionPageInTaskList(urlPath, pageMessages,
 *    PageDetails(),
 *    "taskListSectionKey",
 *    taskListItemFunction(pageMessages),
 *    ... widgets to ask for the answers for this entry in the task list here ...
 *  )
 *
 *  ...
 *
 *  def taskListItemFunction[M <: Messages[M], F <: Messages[F]]
 *    (messages: Future[Messages[M]])
 *    (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer):
 *    (Form, ValidationErrors, RequestInfo) => TaskListItem = (form, _, _) => {
 *
 *    val linkToPage = Link.toPage("linkToPage", messages, urlPath)
 *    Input.extract("in", data) match {
 *      case None => TaskListItem(linkToPage, Tag.tag("notStartedTag", messages, TaskList.rightAlignTag.merge(Tag.grey)))
 *      case Some(_) => TaskListItem(linkToPage, Tag.tag("completeTag", messages, TaskList.rightAlignTag))
 *    }
 *  }
 * }}}
 *
 * @since 1.2.0
 */
object TaskList extends CommonHtmlAttributes {

  override protected val elementPrefix: String = "taskList"

  def taskList[M <: Messages[M], F <: Messages[F]](name: String,
                                                   messages: Future[Messages[M]],
                                                   sections: (Form, ValidationErrors, RequestInfo) => Seq[TaskListSection],
                                                   extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs,
                                                   visibility: WidgetVisibility = alwaysShown)
                                                  (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): Content[M, F] = {

    def buildSectionArgs(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit executionContext: ExecutionContext): Future[Seq[Map[String, Any]]] = {

      def buildItem(item: TaskListItem, form: Form, errors: ValidationErrors, requestInfo: RequestInfo): Future[Map[String, String]] = {
        for {
          renderedName <- item.name.render(form, errors, requestInfo)
          renderedTag <- item.tag.render(form, errors, requestInfo)
        } yield {
          Map("name" -> renderedName, "tag" -> renderedTag)
        }
      }

      sections(form, errors, requestInfo).zipWithIndex.foldLeft(Future.successful(Seq.empty[Map[String, Any]])) { case (result, (section, index)) =>
        for {
          r <- result
          itemEntries <- Future.sequence(section.items.map(item => buildItem(item, form, errors, requestInfo)))
          heading <- section.heading.render(form, errors, requestInfo)
        } yield {
          r :+ Map(
            "number" -> (index + 1),
            "heading" -> heading,
            "items" -> itemEntries
          )
        }
      }
    }

    new Content(name, "taskList", messages, "task-list/template.mustache", extraTemplateRenderArgs, visibility) {
      override def templateArgs(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit executionContext: ExecutionContext): Future[Map[String, Any]] = {
        for {
          parent <- super.templateArgs(form, errors, requestInfo)
          sectionEntries <- buildSectionArgs(form, errors, requestInfo)
        } yield {
          Map("taskList.sections" -> sectionEntries) ++ parent
        }
      }
    }
  }


  def taskList[M <: Messages[M], F <: Messages[F]](name: String,
                                                   messages: Future[Messages[M]],
                                                   sections: Seq[TaskListSection],
                                                   extraTemplateRenderArgs: ExtraTemplateRenderArgs,
                                                   visibility: WidgetVisibility)
                                                  (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): Content[M, F] = {
    taskList(name, messages, (_, _, _) => sections, extraTemplateRenderArgs, visibility)
  }

  def taskList[M <: Messages[M], F <: Messages[F]](name: String,
                                                   messages: Future[Messages[M]],
                                                   sections: Seq[TaskListSection],
                                                   visibility: WidgetVisibility)
                                                  (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): Content[M, F] = {
    taskList(name, messages, (_, _, _) => sections, NoExtraRenderArgs, visibility)
  }

  def taskList[M <: Messages[M], F <: Messages[F]](name: String,
                                                   messages: Future[Messages[M]],
                                                   sections: Seq[TaskListSection],
                                                   extraTemplateRenderArgs: ExtraTemplateRenderArgs)
                                                  (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): Content[M, F] = {
    taskList(name, messages, (_, _, _) => sections, extraTemplateRenderArgs, alwaysShown)
  }

  def taskList[M <: Messages[M], F <: Messages[F]](name: String,
                                                   messages: Future[Messages[M]],
                                                   sections: Seq[TaskListSection])
                                                  (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): Content[M, F] = {
    taskList(name, messages, (_, _, _) => sections, NoExtraRenderArgs, alwaysShown)
  }


  def buildFromPages[M <: Messages[M]](messages: Future[Messages[M]])(form: Form, errors: ValidationErrors, requestInfo: RequestInfo): Seq[TaskListSection] = {
    val taskListItemsBySectionName = form.formDefinition.pages.collect { case p: Page with IncludeInTaskList => p }.foldLeft(Seq.empty[(String, Seq[TaskListItem])]) { (result, page) =>
      result.find(_._1 == page.taskListSectionName).fold(result :+ (page.taskListSectionName, Seq(page.taskListItem(form, errors, requestInfo)))) { case (key, items) =>
        result.updated(result.indexWhere(_._1 == key), (key, items :+ page.taskListItem(form, errors, requestInfo)))
      }
    }

    taskListItemsBySectionName.map { case (taskListSectionName, taskListItems) =>
      TaskListSection(MessageContent.messageContent(messages, taskListSectionName), taskListItems)
    }

  }

  /**
   * Assuming that in the majority of cases we're going to want to show tags on our task list, this custom class
   * will tell a tag component to align on the right of the summary list, instead of directly after the item's name.
   *
   * Note the non-standard class name; Presumably this will change once the Task List is promoted from its current
   * 'Experimental' status and given a concrete implementation in GOV.UK's Design System.
   */
  val rightAlignTag: ExtraTemplateRenderArgs = Tag.classes("app-task-list__tag")
}

case class TaskListSection(heading: Widget, items: Seq[TaskListItem])

case class TaskListItem(name: Widget, tag: Widget)

/**
 * Mix this trait into your `Page`s that should be included in your dynamic [[TaskList]].
 *
 * A page is mapped to a [[TaskListItem]], and the [[IncludeInTaskList#taskListSectionName]] is used to group these items together into
 * [[TaskListSection]]s.
 */
trait IncludeInTaskList {

  /**
   * Used to group multiple [[IncludeInTaskList]]s together to be in the same [[TaskListSection]].
   */
  val taskListSectionName: String

  def taskListItem(form: Form, errors: ValidationErrors, requestInfo: RequestInfo): TaskListItem
}