package com.uxforms.govukdesignsystem.dsl.widget

import com.uxforms.domain.ExtraTemplateRenderArgsOps._
import com.uxforms.domain._
import com.uxforms.domain.constraint.{Constraint, WidgetValidationError}
import com.uxforms.domain.widget.{DateWidget, Extractable, Extractor, ValidationFailureKPILabel}
import com.uxforms.dsl.Form
import com.uxforms.dsl.widgets.{WidgetVisibility, alwaysShown}
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.template.TemplateHelper._
import com.uxforms.govukdesignsystem.dsl.template.{ErrorMessage, Hint, Label}
import com.uxforms.govukdesignsystem.dsl.widget.templateargs.{CommonFormGroupHtmlAttributes, CommonHtmlAttributes, CommonLegendHtmlAttributes, TemplateArgManipulator}
import play.api.libs.json._

import java.time.LocalDate
import java.util.Locale
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try

class DateInput[M <: Messages[M], F <: Messages[F]](override val name: String,
                                                    override val reportingName: String,
                                                    templateName: String,
                                                    messages: Future[Messages[M]],
                                                    override val constraints: Set[Constraint],
                                                    extraTemplateRenderArgs: ExtraTemplateRenderArgs,
                                                    override val visibility: WidgetVisibility
                                                   )(implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer)
  extends DateWidget with ValidationFailureKPILabel with Extractable[LocalDate] {


  override def extract(data: FormData)(implicit fjs: Reads[LocalDate]): Option[LocalDate] = {
    if (visibility.shown(data)) {
      DateInput.extract(name, data)
    } else None
  }

  override val yearField: String = s"$name.year"

  override def myErrors(errors: ValidationErrors): Seq[WidgetValidationError] =
    errors.collect { case wve: WidgetValidationError if wve.fieldName == name || wve.fieldName == dayField || wve.fieldName == monthField || wve.fieldName == yearField => wve }

  override def validationFailureLabel(implicit ec: ExecutionContext): Future[String] = {
    messages.map(m => m.getString(s"$name.dateInput.legend.text").orElse(m.getString("dateInput.legend.text")).getOrElse("Unknown"))
  }

  private val dateInputId = HtmlIdGenerator.randomId()

  protected[widget] def buildDateFields(data: FormData, consolidatedMessages: Map[String, Any], errors: ValidationErrors): Seq[Input[M, F]] = {
    val dayValue = (data.textData \ name \ "day").asOpt[Short].map(_.toString).getOrElse("")
    val monthValue = (data.textData \ name \ "month").asOpt[Short].map(_.toString).getOrElse("")
    val yearValue = (data.textData \ name \ "year").asOpt[Short].map(_.toString).getOrElse("")


    def fieldClasses(inputFieldName: String) = {

      SimpleExtraTemplateRenderArgs(Map(
        "label.classes" -> "govuk-date-input__label",
        "input.classes" -> "govuk-date-input__input"
      ) ++ Try(inputFieldName.substring(name.length + 1)).toOption.flatMap(prefix => consolidatedMessages.get(s"$prefix.label.text").map(s => ("label.text" -> s)))
      )
    }

    def errorClasses(inputFieldName: String) = {
      errors.collectFirst { case WidgetValidationError(_, fieldName, _) if fieldName == inputFieldName => " govuk-input--error" } match {
        case None => NoExtraRenderArgs
        case Some(s) => SimpleExtraTemplateRenderArgs("input.classes" -> s)
      }

    }

    def buildInput(name: String, eArgs: ExtraTemplateRenderArgs, id: FormDefinition => String) = {
      new Input(name, "number input", messages, Set.empty, "input/template.mustache", extraTemplateRenderArgs ++ Input.attributes(buildAttribute("inputmode", "numeric")) ++ eArgs, alwaysShown) {
        override protected[widget] def elementId(formDefinition: FormDefinition): String = {
          id(formDefinition)
        }
      }
    }

    val dayInput = buildInput(dayField, fieldClasses(dayField) merge Input.width2 merge errorClasses(dayField) ++ SimpleExtraTemplateRenderArgs("input.value" -> dayValue),
      HtmlIdGenerator.elementId(_, this))
    val monthInput = buildInput(monthField, fieldClasses(monthField) merge Input.width2 merge errorClasses(monthField) ++ SimpleExtraTemplateRenderArgs("input.value" -> monthValue),
      _ => HtmlIdGenerator.randomId())
    val yearInput = buildInput(yearField, fieldClasses(yearField) merge Input.width4 merge errorClasses(yearField) ++ SimpleExtraTemplateRenderArgs("input.value" -> yearValue),
      _ => HtmlIdGenerator.randomId())

    Seq(dayInput, monthInput, yearInput)
  }


  protected[widget] def templateArgs(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit ec: ExecutionContext): Future[Map[String, Any]] = {
    val hint = new Hint(dateInputId, name)
    val errorMessage = new ErrorMessage(dateInputId, name, myErrors(errors))
    val label = new Label(dateInputId, name)
    implicit val locale: Locale = requestInfo.locale


    for {
      consolidatedProps <- TemplateArgManipulator.collapseAndMergeAll(name, messages, extraTemplateRenderArgs)(form, errors, requestInfo)
      renderedHint <- hint.render(consolidatedProps)
      renderedError <- errorMessage.render(consolidatedProps)
      renderedLabel <- label.render(consolidatedProps)

      dateFields <- Future.sequence(
        buildDateFields(form.data, consolidatedProps, errors).map(
          _.render(form, Seq.empty, requestInfo) // don't pass validation errors through to date field rendering - error messages need to be associated with this date widget, not each individual field
        )
      )
    } yield {

      val describedBy = {
        val key = "dateInput.fieldset.describedBy"
        val additionalValues = Seq(renderedHint.map(_ => hint.id), renderedError.map(_ => errorMessage.id)).flatten.mkString(" ")
        consolidatedProps.get(key).map(_ + " " + additionalValues).getOrElse(additionalValues).trim match {
          case "" => Map.empty
          case s => Map(key -> s)
        }
      }

      val hasLegend = consolidatedProps.exists(kv => kv._1.endsWith("dateInput.legend.text") || kv._1.endsWith("dateInput.legend.html"))

      val localProps = Map(
        "dateInput.formGroup.classes" -> visibilityClass(visibility.shown(form.data)),
        "dateInput.formGroup.attributes" -> Seq(buildAttribute("id", HtmlIdGenerator.htmlId(form.formDefinition, this))),
        "dateInput.items" -> dateFields,
        "dateInput.fieldset" -> hasLegend,
        "dateInput.fieldset.hasLegend" -> hasLegend
      ) ++
        renderedLabel.map("dateInput.govukLabel" -> _) ++
        renderedHint.map("dateInput.hint" -> _) ++
        renderedError.map("dateInput.errorMessage" -> _) ++
        describedBy

      TemplateArgManipulator.mergeProperties(
        localProps,
        consolidatedProps
      )
    }
  }


  override def toJson(formPostData: Map[String, Seq[String]]): JsObject = {
    def toShortVal(oss: Option[Seq[String]]): Option[Short] = oss.flatMap(_.headOption.flatMap(s => Try(s.toShort).toOption))

    def fieldHasValue(fieldName: String) = formPostData.get(fieldName).exists(_.exists(_.trim.nonEmpty))

    if (fieldHasValue(dayField) || fieldHasValue(monthField) || fieldHasValue(yearField)) {
      Json.obj(
        name -> Json.obj(
          "day" -> toShortVal(formPostData.get(dayField)),
          "month" -> toShortVal(formPostData.get(monthField)),
          "year" -> toShortVal(formPostData.get(yearField))
        )
      )
    } else {
      Json.obj()
    }
  }

  override def render(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit executionContext: ExecutionContext): Future[String] = {
    templateArgs(form, errors, requestInfo).map(args =>
      renderer.render(templateName, args)
    )
  }

}

/**
 * Implementation of GOV.UK Design System's [[https://design-system.service.gov.uk/components/date-input/ Date input component]]
 *
 * Some of the most useful available Message properties:
 *
 * {{{
 *   widgetName.dateInput.legend.text=Question text
 *   widgetName.hint.text=Question hint
 *
 *   # When a required() constraint is added to the widget
 *   widgetName.constraint.required.errorMessage=Enter the date
 *
 *   # Per-field labels. Usually put these in formMessages so they're applied automatically to every wigdet instance.
 *   day.label.text=Day
 *   month.label.text=Month
 *   year.label.text=Year
 * }}}
 *
 * By default does not include any constraints on valid date ranges.
 */
object DateInput extends Extractor[LocalDate] with CommonHtmlAttributes with CommonLegendHtmlAttributes with CommonFormGroupHtmlAttributes {

  override protected val elementPrefix: String = "dateInput"

  import com.uxforms.domain.constraint.FirstOnly

  def date[M <: Messages[M], F <: Messages[F]](name: String, messages: Future[Messages[M]], constraints: Seq[Constraint], args: ExtraTemplateRenderArgs = NoExtraRenderArgs, visibility: WidgetVisibility = alwaysShown)(implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): DateInput[M, F] = {
    new DateInput(name, "date", "date-input/template.mustache", messages, new FirstOnly(constraints), args, visibility)
  }

  /**
   * Note: Does not set min or max constraints as the design system's guidance suggests missing or incomplete information (e.g. a Required constraint)
   * should take precedence over an incorrect value.
   */
  def dateOfBirth[M <: Messages[M], F <: Messages[F]](name: String, messages: Future[Messages[M]], constraints: Seq[Constraint], args: ExtraTemplateRenderArgs = NoExtraRenderArgs, visibility: WidgetVisibility = alwaysShown)(implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): DateInput[M, F] = {
    val auto = autoComplete(name, "bday-day", "bday-month", "bday-year")
    new DateInput(name, "date", "date-input/template.mustache", messages, new FirstOnly(constraints), auto ++ args, visibility)
  }

  def autoComplete(widgetName: String, day: String, month: String, year: String): ExtraTemplateRenderArgs =
    SimpleExtraTemplateRenderArgs(s"$widgetName.day.input.autocomplete" -> day, s"$widgetName.month.input.autocomplete" -> month, s"$widgetName.year.input.autocomplete" -> year)

  override def extract(widgetName: String, data: FormData)(implicit fjs: Reads[LocalDate]): Option[LocalDate] = {
    for {
      day <- (data.textData \ widgetName \ "day").asOpt[Short]
      month <- (data.textData \ widgetName \ "month").asOpt[Short]
      year <- (data.textData \ widgetName \ "year").asOpt[Short]
    } yield {
      LocalDate.of(year, month, day)
    }
  }
}