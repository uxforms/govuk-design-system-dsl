package com.uxforms.govukdesignsystem.dsl.widget

import java.io.File
import com.uxforms.domain._
import com.uxforms.domain.constraint.{Constraint, FileUploadWithAllFormDataConstraint, FirstOnly, Required, WidgetValidationError}
import com.uxforms.domain.widget.{Extractable, Extractor, ValidationFailureKPILabel}
import com.uxforms.dsl.Form
import com.uxforms.dsl.helpers.MessageHelper
import com.uxforms.dsl.widgets.file.{DataSizeFormatter, FileUploadActionNames, SubmittableFileUploadSupport}
import com.uxforms.dsl.widgets.{FileUploadMetadata, ValidatingFileTransform, ValidatingFileTransformResult, WidgetVisibility, alwaysShown}
import com.uxforms.govukdesignsystem.dsl.TemplateRenderer
import com.uxforms.govukdesignsystem.dsl.template.TemplateHelper._
import com.uxforms.govukdesignsystem.dsl.widget.templateargs.{CommonFormGroupHtmlAttributes, CommonHtmlAttributes}
import play.api.libs.json.{Json, Reads}

import scala.concurrent.{ExecutionContext, Future}

class FileUpload[M <: Messages[M], F <: Messages[F]](override val name: String,
                                                     override val reportingName: String,
                                                     override val messages: Future[Messages[M]],
                                                     override val constraints: Set[Constraint],
                                                     fileConstraints: Set[FileUploadWithAllFormDataConstraint],
                                                     fileTransform: Option[ValidatingFileTransform],
                                                     override val templateName: String,
                                                     override val extraTemplateRenderArgs: ExtraTemplateRenderArgs,
                                                     override val visibility: WidgetVisibility)
                                                    (implicit override val formLevelMessages: Future[Messages[F]], override val renderer: TemplateRenderer)
  extends FileUploadBase[M, F] with SubmittableFileUploadSupport with ValidationFailureKPILabel with Extractable[Seq[FileUploadMetadata]] {


  override def extract(data: FormData)(implicit fjs: Reads[Seq[FileUploadMetadata]]): Option[Seq[FileUploadMetadata]] = {
    if (visibility.shown(data)) {
      super.extract(data)
    } else None
  }

  override def validationFailureLabel(implicit ec: ExecutionContext): Future[String] = {
    messages.map(m => m.getString(s"$name.label.text").orElse(m.getString("label.text")).getOrElse("Unknown"))
  }

  private val requiredFileConstraint = new Required(Messages.empty())

  override def validateSubmission(postedFormData: Map[String, Seq[String]], existingFormData: FormData, file: Option[File])(implicit ec: ExecutionContext): Future[ValidationResult] = {

    def removeFile(uuid: String): FormData = {
      val metadataWithoutThisFile = (existingFormData.textData \ name).asOpt[Seq[FileUploadMetadata]].map(_.filterNot(_.uuid.toString == uuid)).getOrElse(Seq.empty)
      FormData(Json.obj(name -> metadataWithoutThisFile), existingFormData.files - uuid)
    }

    def addFile(): Option[Future[ValidationResult]] = {

      def appendFileToFormData(formData: FormData, metadata: FileUploadMetadata, file: File) = FormData(
        formData.textData ++ Json.obj(name -> (FileUpload.extractUploadedFiles(name, formData) :+ metadata)),
        formData.files + (metadata.uuid.toString -> file)
      )

      for {
        pf <- toJson(postedFormData).asOpt[FileUploadMetadata]
        f <- file
      } yield {
        validateFileConstraints(this, fileConstraints, SubmittableFile(pf.uuid.toString, f), appendFileToFormData(existingFormData, pf, f)).flatMap {
          case ValidationResult(_, errors) if errors.nonEmpty => Future.successful(ValidationResult(existingFormData, errors))
          case vr@ValidationResult(validatedFormData, _) => {
            fileTransform match {
              case Some(ft) => ft.transform(validatedFormData, pf.filename, pf.contentType, f).map {
                case ValidatingFileTransformResult(_, Some(error)) => ValidationResult(validatedFormData, Seq(error))
                case ValidatingFileTransformResult(transformedFileData, None) => {
                  val transformedFile = overwriteFileContents(f, transformedFileData.data)
                  val formDataWithTransformedFile = appendFileToFormData(validatedFormData, FileUploadMetadata(transformedFileData.filename, transformedFile.length(), transformedFileData.contentType, pf.uuid), transformedFile)
                  ValidationResult(formDataWithTransformedFile, Seq.empty)
                }
              }
              case None => Future.successful(vr)
            }
          }
        }
      }
    }

    if (isFormPostToAddFile(postedFormData)) {
      addFile()
        .getOrElse(Future.successful(ValidationResult(existingFormData, Seq(WidgetValidationError(this, name, requiredFileConstraint)))))
    } else if (isFormPostToDeleteFile(postedFormData)) {
      Future.successful {
        postedFormData.get(deleteAction).flatMap(_.headOption).map(uuidToDelete =>
          ValidationResult(removeFile(uuidToDelete), Seq.empty)
        ).getOrElse(ValidationResult(existingFormData, Seq(WidgetValidationError(this, name, requiredFileConstraint))))
      }
    } else {
      Future.successful {
        ValidationResult(existingFormData, Seq.empty)
      }
    }
  }

}

/**
 *  Implementation of GOV.UK Design System's [[https://design-system.service.gov.uk/components/file-upload/ File upload component]]
 *
 *  The default implementation presents a file upload component as well as a dedicated button to attach the file to the form.
 *
 *  {{{
 *    FileUpload.fileUpload("myFileUpload", messages, required()),
 *    Button.input(FileUploadActionNames.uploadAction("myFileUpload"), messages)
 *  }}}
 *
 *  With messages
 *
 *  {{{
 *    myFileUpload.label.text=Upload a file
 *    myFileUpload-submit.button.text=Upload file
 *    myFileUpload-delete.button.text=Remove file
 *    myFileUpload.constraint.required.errorMessage=Select a file
 *  }}}
 *
 *  === Showing uploaded files ===
 *
 *  To show a summary of files uploaded by the above widget:
 *
 *  {{{
 *    FileUpload.fileSummaryList("fileSummary", "myFileUpload", messages)
 *  }}}
 *
 * === Uploading a file when continuing from the Page / Section ===
 *
 * See [[FileUploadInline]]
 *
 */
object FileUpload extends Extractor[Seq[FileUploadMetadata]] with CommonHtmlAttributes with CommonFormGroupHtmlAttributes {

  override protected val elementPrefix: String = "fileUpload"

  def fileUpload[M <: Messages[M], F <: Messages[F]](name: String,
                                                     messages: Future[Messages[M]],
                                                     constraints: Seq[Constraint],
                                                     fileConstraints: Set[FileUploadWithAllFormDataConstraint],
                                                     fileTransform: Option[ValidatingFileTransform] = None,
                                                     extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs,
                                                     visibility: WidgetVisibility = alwaysShown)
                                                    (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer): FileUpload[M, F] = {
    new FileUpload(name, "file upload", messages, new FirstOnly(constraints), fileConstraints, fileTransform, "file-upload/template.mustache", extraTemplateRenderArgs, visibility)
  }

  /**
   * Add an ```accept``` attribute to the file upload html element.
   *
   * @param fileTypes As per https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/file#Unique_file_type_specifiers
   *                  where each fileType will be separated by a comma.
   */
  def accept(fileTypes: String*): ExtraTemplateRenderArgs =
    attributes(buildAttribute("accept", fileTypes.mkString(",")))

  def fileSummaryList[M <: Messages[M], F <: Messages[F]](name: String,
                                                          fileUploadWidgetName: String,
                                                          messages: Future[Messages[M]],
                                                          extraTemplateRenderArgs: ExtraTemplateRenderArgs = NoExtraRenderArgs,
                                                          visibility: WidgetVisibility = alwaysShown
                                                         )(implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer, executionContext: ExecutionContext): SummaryList[M, F] = {

    val rows: (Form, ValidationErrors, RequestInfo) => Future[Seq[SummaryListRow]] = (form, errors, requestInfo) => {
      Future.sequence {
        extractUploadedFiles(fileUploadWidgetName, form.data).map(metadata => {
          Link.toFile(metadata.uuid.toString, Future.successful(MessageHelper.tupleToMessages("link.html" -> metadata.filename)(requestInfo.locale)), metadata.uuid).render(form, errors, requestInfo).map(renderedFilenameLink =>
            SummaryListRow(
              Map(
                "row.key.html" -> renderedFilenameLink,
                "row.value.text" -> DataSizeFormatter.humanReadableByteCountSI(metadata.sizeBytes)
              ),
              Seq(Button.button(FileUploadActionNames.deleteAction(fileUploadWidgetName), messages, Button.withValue(metadata.uuid.toString) ++ Button.secondary)))
          )
        })
      }
    }
    SummaryList.dynamicSummaryList(name, messages, rows, extraTemplateRenderArgs, visibility)
  }


  def extractUploadedFiles(widgetName: String, data: FormData): Seq[FileUploadMetadata] = {
    (data.textData \ widgetName).asOpt[Seq[FileUploadMetadata]].getOrElse(Seq.empty)
  }
}