package com.uxforms.govukdesignsystem.dsl.styles

object Typography {

  object size extends Enumeration {
    type size = Value
    val s, m, l, xl = Value
  }
}
