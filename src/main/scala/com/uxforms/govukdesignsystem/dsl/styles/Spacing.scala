package com.uxforms.govukdesignsystem.dsl.styles

/**
 * Helper functions to generate classes described in [[https://design-system.service.gov.uk/styles/spacing/]]
 */
object Spacing {

  object direction extends Enumeration {
    type direction = Value
    val right, left, top, bottom = Value
  }

  import direction._

  def responsiveMargin(units: Short) = s"govuk-!-margin-$units"
  def responsiveMargin(units: Short, direction: direction) = s"govuk-!-margin-$direction-$units"

  def responsivePadding(units: Short) = s"govuk-!-padding-$units"
  def responsivePadding(units: Short, direction: direction) = s"govuk-!-padding-$direction-$units"

  def staticMargin(units: Short) = s"govuk-!-static-margin-$units"
  def staticMargin(units: Short, direction: direction) = s"govuk-!-static-margin-$direction-$units"
  def staticPadding(units: Short) = s"govuk-!-static-padding-$units"
  def staticPadding(units: Short, direction: direction) = s"govuk-!-static-padding-$direction-$units"

}
