package com.uxforms.govukdesignsystem.dsl

import com.uxforms.domain.FormDefinition
import com.uxforms.dsl.containers.SwitchLocaleButton
import com.uxforms.dsl.mustache.MustacheUtils

import java.util.Locale

object ContainerArgs {

  def containerArgs(formDef: FormDefinition, consolidatedMessages: Map[String, Any])(implicit locale: Locale, renderer: TemplateRenderer): Map[String, Any] = Map(
    "assetUrl" -> renderer.staticAssetsThemeUrl,
    "header.serviceNameOrNavigation" -> (consolidatedMessages.contains("header.serviceName") || consolidatedMessages.contains("header.navigation")),
    "header.serviceUrl" -> MustacheUtils.formPath(formDef),
    "switchLocaleButtonName" -> SwitchLocaleButton.buttonName,
    "htmlLang" -> locale.getLanguage,
    "pageTitleLang" -> locale.getLanguage
  )


}
